<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('#', function () {
     $exitCode = Artisan::call('cache:clear');
     return 'abc';
})->name('#');
/*route nav*/

Route::get('/', 'PageController@index')->name('index');
Route::get('/checkdb', 'PageController@connectionPdo');
Route::get('/show/{id?}', 'PageController@show')->name('index.show');

// newsletter
Route::post('/newsletter/store', 'NewsletterController@store')->name('newsletter.store');
/*shop*/

Route::get('shop/{id?}', 'ShopController@index')->name('shop');
Route::get('shop/show/{id?}', 'ShopController@show')->name('shop.show');
Route::post('shop/groupselected/{id?}', 'ShopController@groupSelected')->name('shop.groupselected');

/*blogs*/

Route::get('blogs', 'BlogWebController@index')->name('blogs');
Route::get('blogs/show/{id?}', 'BlogWebController@show')->name('blogs.show');

/*newlook*/
Route::get('newlook', 'NewlookController@index')->name('newlook');

/*promotions*/
Route::get('promotions', 'PromotionsController@index')->name('promotions');

/*route admin*/
Route::get('login/admin', function () {
    return view('auth.admin.login');
})->name('login.admin');
Route::get('test', function () {
    return view('auth.password.reset');
})->name('test');
Route::get('formelement', function () {
    return view('admin.pages.formelement');
})->name('formelement');
Route::get('multiselect', function () {
    return view('admin.pages.multiselect');
})->name('multiselect');

Route::get('formvalidation', function () {
    return view('admin.pages.formvalidation');
})->name('formvalidation');
Route::get('datepicker', function () {
    return view('admin.pages.datepicker');
})->name('datepicker');
Route::get('datatable', function () {
    return view('admin.pages.datatable');
})->name('datatable');
Route::get('generaltable', function () {
    return view('admin.pages.generaltable');
})->name('generaltable');

Route::get('product/product-detail', 'PageController@productDetail')->name('product-detail');
Route::get('blog/blog-detail', 'PageController@blogDetail')->name('blog-detail');
Route::post('contact/send', 'PageController@sendCoba')->name('sendEmail');
Route::get('wishlist', function () {
    return view('pages.wishlist');
})->name('wishlist');
Route::get('faqs', 'PageController@faqs')->name('faqs');
Route::get('about', 'PageController@about')->name('about');
Route::get('contact', function () {
    return view("pages.contact");
})->name('contact');


// filter
Route::get('/filter/{kode}/{sub}', 'FilterController@filterPrice')->name('filter.price');
Route::get('/filter/search', 'FilterController@filterSearch')->name('filter.search');
Route::get('/filter/category/', 'FilterController@filterCategory');
Route::post('/filter/product', 'FilterController@filterProduct')->name('filter.product');

//Route::get('/category', 'CategoryController@index')->name('category');

// cart
Route::get('/cart', 'CartController@index')->name('cart');
Route::post('/cart/store', 'CartController@store')->name('cart.store');
Route::get('/cart/update', 'CartController@update')->name('cart.update');
Route::get('/cart/destroy/{id}', 'CartController@destroy')->name('cart.destroy');
Route::get('/cart/clear', 'CartController@clear')->name('cart.clear');

// raja ongkir
Route::get('/getSub/', 'ProductController@subCat');
Route::get('/ongkir/province', 'OngkirController@getProvince')->name('ongkir.province');
// Route::get('/ongkir/city', 'OngkirController@getCity')->name('ongkir.city');
Route::post('/ongkir/cost', 'OngkirController@getCost')->name('ongkir.cost');
Route::get('/ongkir/city/', 'OngkirController@getCity');
Route::get('/ongkir/calculate/', 'CheckoutController@calculateOngkir');
Route::get('/ongkir/calculateDeliv/', 'CheckoutController@calculateOngkirDeliv');
Route::get('/set/session/dropship/', 'CheckoutController@setSession');

// coupon
Route::get('/coupon/use', 'CouponController@useCoupon')->name('coupon.use');
Route::get('/coupon/cancel', 'CouponController@cancelUse')->name('coupon.cancel');

View::composer(['*'], function ($view) {
    $datas = Cart::getContent();
    $view->with('datas', $datas);
});

Auth::routes();

Route::group(['prefix' => 'admin', 'middleware' => ['auth' => 'admin']], function () {

    // admin
    Route::get('/', 'DashboardController@index')->name('admin');
    Route::get('/print-Dashboard', 'DashboardController@print')->name('dashboard.print');

    // slider
    Route::get('/slider', 'SliderController@index')->name('admin.slider');
    Route::get('/slider/create', 'SliderController@create')->name('slider.create');
    Route::post('/slider/store', 'SliderController@store')->name('slider.store');
    Route::get('/slider/edit/{id?}', 'SliderController@edit')->name('slider.edit');
    Route::post('/slider/update/{id?}', 'SliderController@update')->name('slider.update');
    Route::get('/slider/status/{id?}', 'SliderController@statusActive')->name('slider.status');
    Route::get('/slider/delete/{id?}', 'SliderController@statusDelete')->name('slider.delete');

    // caraousel
    Route::get('/carousel', 'CarouselController@index')->name('admin.carousel');
    Route::get('/carousel/create', 'CarouselController@create')->name('carousel.create');
    Route::post('/carousel/store', 'CarouselController@store')->name('carousel.store');
    Route::get('/carousel/edit/{id?}', 'CarouselController@edit')->name('carousel.edit');
    Route::post('/carousel/update/{id?}', 'CarouselController@update')->name('carousel.update');
    Route::get('/carousel/status/{id?}', 'CarouselController@statusActive')->name('carousel.status');

    // carousel categories
    Route::get('/slider-categories', 'SliderCategoryController@index')->name('admin.sliderCat');
    Route::get('/slider-categories/create', 'SliderCategoryController@create')->name('sliderCat.create');
    Route::post('/slider-categories/store', 'SliderCategoryController@store')->name('sliderCat.store');
    Route::get('/slider-categories/edit/{id?}', 'SliderCategoryController@edit')->name('sliderCat.edit');
    Route::post('/slider-categories/update/{id?}', 'SliderCategoryController@update')->name('sliderCat.update');
    Route::get('/slider-categories/status/{id?}', 'SliderCategoryController@statusActive')->name('sliderCat.status');
    Route::get('/slider-categories/delete/{id?}', 'SliderCategoryController@statusDelete')->name('sliderCat.delete');

    // testimonial
    Route::get('/testimonial', 'TestimonialController@index')->name('admin.testimonial');
    Route::get('/testimonial/show/{id}', 'TestimonialController@show')->name('testimonial.show');
    Route::get('/testimonial/status/{id?}', 'TestimonialController@statusActive')->name('testimonial.status');

    // promotion
    Route::get('/promotion', 'PromotionController@index')->name('admin.promotion');
    Route::get('/promotion/create', 'PromotionController@create')->name('promotion.create');
    Route::post('/promotion/store', 'PromotionController@store')->name('promotion.store');
    Route::get('/promotion/edit/{id?}', 'PromotionController@edit')->name('promotion.edit');
    Route::post('/promotion/update/{id?}', 'PromotionController@update')->name('promotion.update');
    Route::get('/promotion/status/{id?}', 'PromotionController@statusActive')->name('promotion.status');
    Route::get('/promotion/delete/{id?}', 'PromotionController@statusDelete')->name('promotion.delete');

    // product
    Route::get('/getSub/', 'ProductController@subCat');
    Route::get('/getSubCategories', 'ProductController@getSubCategories');
    Route::get('/product', 'ProductController@index')->name('admin.product');
    Route::get('/product/create', 'ProductController@create')->name('product.create');
    Route::post('/product/store', 'ProductController@store')->name('product.store');
    Route::get('/product/show/{id?}', 'ProductController@show')->name('product.show');
    Route::get('/product/showVariasi/{id?}', 'ProductController@showVariasi')->name('product.showVariasi');
    Route::get('/product/edit/{id?}', 'ProductController@edit')->name('product.edit');
    Route::post('/product/update/{id?}', 'ProductController@update')->name('product.update');
    Route::get('/product/status/{id?}', 'ProductController@statusActive')->name('product.status');
    Route::get('/product/delete/{id?}', 'ProductController@statusdelete')->name('product.delete');
    Route::get('/product/quickUpdate/{id?}', 'ProductController@quickUpdate')->name('product.quickUpdate');
    Route::get('/product/destroy/{id}', 'ProductController@destroy')->name('product.destroy');
    Route::get('/product/import', 'ProductController@import')->name('product.import');
    Route::post('/product/import/excel', 'ProductController@importExcel')->name('product.import.excel');

    // about
    Route::get('/about', 'AboutController@index')->name('admin.about');
    Route::get('/about/create', 'AboutController@create')->name('about.create');
    Route::post('/about/store', 'AboutController@store')->name('about.store');
    Route::get('/about/show/{id?}', 'AboutController@show')->name('about.show');
    Route::get('/about/edit/{id?}', 'AboutController@edit')->name('about.edit');
    Route::post('/about/update/{id?}', 'AboutController@update')->name('about.update');
    Route::get('/about/status/{id?}', 'AboutController@statusActive')->name('about.status');
    Route::get('/about/delete/{id?}', 'AboutController@statusDelete')->name('about.delete');
    Route::get('/about/destroy/{id}', 'AboutController@destroy')->name('about.destroy');

    // faq
    Route::get('/faq', 'FaqController@index')->name('admin.faq');
    Route::get('/faq/create', 'FaqController@create')->name('faq.create');
    Route::post('/faq/store', 'FaqController@store')->name('faq.store');
    Route::get('/faq/show/{id?}', 'FaqController@show')->name('faq.show');
    Route::get('/faq/edit/{id?}', 'FaqController@edit')->name('faq.edit');
    Route::post('/faq/update/{id?}', 'FaqController@update')->name('faq.update');
    Route::get('/faq/status/{id?}', 'FaqController@statusActive')->name('faq.status');
    Route::get('/faq/delete/{id?}', 'FaqController@statusDelete')->name('faq.delete');
    Route::get('/faq/destroy/{id}', 'FaqController@destroy')->name('faq.destroy');

    // blog
    Route::get('/blog', 'BlogController@index')->name('admin.blog');
    Route::get('/blog/create', 'BlogController@create')->name('blog.create');
    Route::post('/blog/store', 'BlogController@store')->name('blog.store');
    Route::get('/blog/show/{id?}', 'BlogController@show')->name('blog.show');
    Route::get('/blog/edit/{id?}', 'BlogController@edit')->name('blog.edit');
    Route::post('/blog/update/{id?}', 'BlogController@update')->name('blog.update');
    Route::get('/blog/status/{id?}', 'BlogController@statusActive')->name('blog.status');
    Route::get('/blog/delete/{id?}', 'BlogController@statusDelete')->name('blog.delete');
    Route::get('/blog/destroy/{id}', 'BlogController@destroy')->name('blog.destroy');

    // user
    Route::get('/user', 'UserController@index')->name('admin.user');
    Route::get('/user/create', 'UserController@create')->name('user.create');
    Route::post('/user/store', 'UserController@store')->name('user.store');
    Route::get('/user/show/{id?}', 'UserController@show')->name('user.show');
    Route::get('/user/edit/{id?}', 'UserController@edit')->name('user.edit');
    Route::post('/user/update/{id?}', 'UserController@update')->name('user.update');
    Route::get('/user/status/{id?}', 'UserController@statusActive')->name('user.status');
    Route::get('/user/delete/{id?}', 'UserController@statusDelete')->name('user.delete');
    Route::get('/user/destroy/{id}', 'UserController@destroy')->name('user.destroy');

    // reseller
    Route::get('/reseller', 'ResellerController@index')->name('admin.reseller');
    Route::get('/reseller/create', 'ResellerController@create')->name('reseller.create');
    Route::post('/reseller/store', 'ResellerController@store')->name('reseller.store');
    Route::get('/reseller/show/{id?}', 'ResellerController@show')->name('reseller.show');
    Route::get('/reseller/edit/{id?}', 'ResellerController@edit')->name('reseller.edit');
    Route::post('/reseller/update/{id?}', 'ResellerController@update')->name('reseller.update');
    Route::get('/reseller/status/{id?}', 'ResellerController@statusActive')->name('reseller.status');
    Route::get('/reseller/delete/{id?}', 'ResellerController@statusDelete')->name('reseller.delete');
    Route::get('/reseller/destroy/{id}', 'ResellerController@destroy')->name('reseller.destroy');

    // order
    Route::get('/order', 'OrderController@index')->name('admin.order');
    Route::get('/order/show/{id?}', 'OrderController@show')->name('order.show');
    Route::get('/order/show/print/{id?}', 'OrderController@showprint')->name('order.print');
    Route::get('/order/edit/{id?}', 'OrderController@edit')->name('order.edit');
    Route::post('/order/update/{id?}', 'OrderController@update')->name('order.update');
    Route::get('/order/status/{id?}', 'OrderController@statusChange')->name('order.status');
    Route::get('/order/status/cancel/{id?}', 'OrderController@statusCancel')->name('order.cancel');
    Route::post('/order/status/confirm', 'ConfirmationController@changeConfirm')->name('order.changeConfirm');
    Route::post('/order/bookingcode', 'OrderController@bookingCode')->name('order.bookingcode');
    Route::get('/order/printMultiple', 'OrderController@printMultirple')->name('order.printMultirple');

    // pre order
    Route::get('/pre-order', 'PreOrderController@index')->name('admin.preorder');
    Route::get('/pre-order/add/{id}', 'PreOrderController@add')->name('preorder.add');
    Route::get('/pre-order/show/{id?}', 'PreOrderController@show')->name('preorder.show');
    Route::post('/pre-order/store', 'PreOrderController@store')->name('preorder.store');
    Route::get('/pre-order/edit/{id}', 'PreOrderController@edit')->name('preorder.edit');
    Route::post('/pre-order/update/{id}', 'PreOrderController@update')->name('preorder.update');
    Route::get('/pre-order/destroy/{id}', 'PreOrderController@destroy')->name('preorder.destroy');
    Route::get('/pre-order/groupJson/{id}', 'PreOrderController@groupJson')->name('preorder.groupJson');

    // coupons
    Route::get('/coupon', 'CouponController@index')->name('admin.coupon');
    Route::get('/coupon/create', 'CouponController@create')->name('coupon.create');
    Route::post('/coupon/store', 'CouponController@store')->name('coupon.store');
    Route::get('/coupon/show/{id?}', 'CouponController@show')->name('coupon.show');
    Route::get('/coupon/edit/{id?}', 'CouponController@edit')->name('coupon.edit');
    Route::post('/coupon/update/{id?}', 'CouponController@update')->name('coupon.update');
    Route::get('/coupon/status/{id?}', 'CouponController@statusActive')->name('coupon.status');
    Route::get('/coupon/delete/{id?}', 'CouponController@statusDelete')->name('coupon.delete');
    Route::get('/coupon/quickUpdate/{id?}', 'CouponController@quickUpdate')->name('coupon.quickUpdate');
    Route::get('/coupon/destroy/{id}', 'CouponController@destroy')->name('coupon.destroy');

    Route::get('/log-activity', 'LogController@index')->name('admin.log');
    //Route::post('/hak-akses/update/{id}', 'AksesController@update')->name('hakakses.update');

    Route::get('/hak-akses', 'AksesController@index')->name('admin.hakakses');
    Route::post('/hak-akses/update/{id}', 'AksesController@update')->name('hakakses.update');

    Route::get('/web', 'AksesController@webindex')->name('admin.web');
    Route::get('/web_up', 'AksesController@webUp')->name('web.up');
    Route::get('/web_down', 'AksesController@webDown')->name('web.down');

    Route::get('group/store', 'GroupController@store')->name('group.store');
    Route::post('group/update/{id?}', 'GroupController@update')->name('group.update');
    Route::get('group/delete/{id?}', 'GroupController@destroy')->name('group.destroy');
    Route::get('/job_request/index', 'JobRequestController@index')->name('jobrequest.index');
    Route::post('/job_request/store', 'JobRequestController@store')->name('jobrequest.store');
});

Route::group(['middleware' => 'auth'], function () {

    Route::get('/home', 'HomeController@index')->name('home');

    // test
    Route::get('getAddress', 'Api\ProfileController@getAddress');

    // account
    Route::get('/account', 'AccountController@index')->name('account');
    Route::get('/change-password/{id}', 'AccountController@update')->name('account.changepswd');

    // checkout
    Route::get('cart/checkout', 'CheckoutController@index')->name('checkout');

    // dropship
    Route::post('/dropship/store', 'DropshipController@store')->name('dropship.store');
    Route::get('/dropship/update', 'DropshipController@update')->name('dropship.update');

    // testimonial
    Route::post('/testimonial/store', 'TestimonialController@store')->name('testimonial.store');

    // address
    Route::post('/address/store', 'AddressController@store')->name('address.store');
    Route::get('/address/create/{type?}', 'AddressController@create')->name('address.create');
    Route::get('/address/show/{id?}', 'AddressController@show')->name('address.show');
    Route::get('/address/edit/{type?}/{id?}', 'AddressController@edit')->name('address.edit');
    Route::post('/address/update/{id}', 'AddressController@update')->name('address.update');

    // newsletter
    Route::post('/newsletter/update', 'NewsletterController@update')->name('newsletter.update');

    // order
    Route::post('/order/store', 'OrderController@store')->name('order.store');
    Route::get('/order/detail/{id?}', 'OrderController@detail')->name('order.detail');
    Route::get('/order/check/{id?}', 'OrderController@checkResi')->name('order.checkResi');

    //confirmation
    Route::get('/order/confirmation/{id?}', 'ConfirmationController@create')->name('confirmation.create');
    Route::post('/order/ratconfirmationing/store', 'ConfirmationController@store')->name('confirmation.store');

    // rating
    Route::get('/order/rating/{id?}/{order?}', 'RatingController@create')->name('rating.create');
    Route::post('/order/rating/store', 'RatingController@store')->name('rating.store');

    Route::post('/account/store', 'ResellerController@storeUser')->name('reseller.store.user');

    // request
    // Route::get('/job_request/index', 'JobRequestController@index')->name('jobrequest.index');
    // Route::post('/job_request/store', 'JobRequestController@store')->name('jobrequest.store');

});
