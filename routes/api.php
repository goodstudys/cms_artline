<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');
Route::get('formDelivery', 'Api\OrderController@formDelivery');
Route::post('register', 'Api\RegisterController@register');

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('getUser', 'Api\AuthController@getUser');

    Route::get('getSlider', 'Api\SliderController@getSlider');
    Route::get('home', 'Api\SliderController@getHome');

    Route::get('getPromotion', 'Api\PromotionController@getPromotion');

    Route::get('getCategories', 'Api\CategoriesController@getCategories');
    Route::get('getDetailCategories/{id?}', 'Api\CategoriesController@getDetailCategories');
    Route::get('allProductsCategory/{id?}', 'Api\CategoriesController@getAllProductsCategory');
    Route::get('getProductsCategories/{id?}', 'Api\CategoriesController@getProductsCategories');
    Route::get('getProductsSubCategories/{id?}', 'Api\CategoriesController@getProductsSubCategories');
    Route::get('slider/category/{id?}', 'Api\CategoriesController@getSlider');

    Route::get('getRecom', 'Api\ProductController@getRecom');
    Route::post('search', 'Api\ProductController@search');
    Route::get('/getDetailProduct/{id?}', 'Api\ProductController@getDetailProduct');
    Route::post('groupSelected/{id?}', 'Api\ProductController@groupSelected');
    
    Route::post('getCost', 'Api\OrderController@getCost');
    Route::post('getService', 'Api\OrderController@getService');
    Route::post('createOrder', 'Api\OrderController@store');
    Route::get('getLastOrder', 'Api\OrderController@getLastOrder');

    Route::get('getAddress', 'Api\ProfileController@getAddress');
    Route::get('getAddressDetail/{id}', 'Api\ProfileController@getAddressDetail');
    Route::post('createAddress', 'Api\ProfileController@createAddress');
    Route::post('updateAddress/{id}', 'Api\ProfileController@updateAddress');
    Route::post('requestReseller', 'Api\ProfileController@pengajuanReseller');

    Route::post('updatePhoto/{id}', 'Api\ProfileController@updatePhoto');

    Route::get('getOrder', 'Api\ProfileController@getOrder');
    Route::get('getSalesOrder', 'Api\ProfileController@getSalesOrder');
    Route::get('getPreOrder', 'Api\ProfileController@getPreOrder');
    Route::get('getOrderDetail/{id}', 'Api\ProfileController@getOrderDetail');
    Route::get('trackingOrder/{id}', 'Api\ProfileController@trackingOrder');

    Route::post('changePassword/{id}', 'Api\ProfileController@changePassword');
    Route::get('editProfile/{id}', 'Api\ProfileController@editProfile');
    Route::post('updateProfile/{id}', 'Api\ProfileController@trackingOrder');

    Route::post('confirmation', 'Api\OrderController@confirmation');
    Route::get('cancelOrder/{id?}', 'Api\OrderController@statusCancel');

    // khusus admin
    Route::post('changeConfirm', 'Api\OrderController@changeConfirm');

    


});
