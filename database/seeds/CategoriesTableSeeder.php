<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'id' => 1,
            'name' => 'Markers',
            'code' => 'MRK',
        ]);
        DB::table('categories')->insert(
            [
                'id' => 2,
                'name' => 'Highlighters',
                'code' => 'HLT',]

        );
        DB::table('categories')->insert(
            [
                'id' => 3,
                'name' => 'Pens',
                'code' => 'PEN',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 4,
                'name' => 'Erasers',
                'code' => 'ERA',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 5,
                'name' => 'Stamp Pad',
                'code' => 'PAD',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 6,
                'name' => 'Artline Stix',
                'code' => 'STX',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 7,
                'name' => 'Artline Supreme',
                'code' => 'SUP',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 8,
                'name' => 'Water Colours',
                'code' => 'WAT',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 9,
                'name' => 'Pencils',
                'code' => 'PEL',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 10,
                'name' => 'Cap Sharpener',
                'code' => 'CAP',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 11,
                'name' => 'Glue Sticks',
                'code' => 'GLU',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 12,
                'name' => 'Correction Pens',
                'code' => 'COR',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 13,
                'name' => 'Fingerprint Pad',
                'code' => 'FIG',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 14,
                'name' => 'Oil Pastels',
                'code' => 'OIL',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 15,
                'name' => 'Wax Crayons',
                'code' => 'CRA',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 16,
                'name' => 'XTENSIONS',
                'code' => 'XTE',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 17,
                'name' => 'TAT',
                'code' => 'TAT',
            ]);
        DB::table('categories')->insert(
            [
                'id' => 18,
                'name' => 'Xstamper',
                'code' => 'XTA',
            ]);
    }
}
