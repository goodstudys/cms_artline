<?php

use Illuminate\Database\Seeder;

class SubCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sub_categories')->insert([
            'id' => 1,
            'categories_id' => 1,
            'name' => 'Permanent Markers',
            'code' => 'PRM',
        ]

        );
        DB::table('sub_categories')->insert(
            [
                'id' => 2,
                'categories_id' => 1,
                'name' => 'Whiteboard Markers',
                'code' => 'WBM',
            ]

        );
        DB::table('sub_categories')->insert(
            [
                'id' => 3,
                'categories_id' => 1,
                'name' => 'Whiteboard Marker Caddy',
                'code' => 'WBC',
            ]

        );
        DB::table('sub_categories')->insert(
            [
                'id' => 4,
                'categories_id' => 1,
                'name' => 'Whiteboard Erasers',
                'code' => 'WBE',
            ]

        );
        DB::table('sub_categories')->insert(
            [
                'id' => 5,
                'categories_id' => 1,
                'name' => 'Paint Markers',
                'code' => 'PAM',
            ]

        );
        DB::table('sub_categories')->insert(
            [
                'id' => 6,
                'categories_id' => 1,
                'name' => 'Metallic Ink Markers',
                'code' => 'MET',
            ]

        );
        DB::table('sub_categories')->insert(
            [
                'id' => 7,
                'categories_id' => 1,
                'name' => 'Special Purpose Markers',
                'code' => 'SPC',
            ]

        );
        DB::table('sub_categories')->insert(
            [
                'id' => 8,
                'categories_id' => 1,
                'name' => 'Industrial Markers',
                'code' => 'IND',
            ]

        );
        DB::table('sub_categories')->insert(
            [
                'id' => 9,
                'categories_id' => 1,
                'name' => 'Industrial Markers',
                'code' => 'IND',
            ]

        );
        DB::table('sub_categories')->insert(
            [
                'id' => 10,
                'categories_id' => 1,
                'name' => 'Poster Markers',
                'code' => 'POS',
            ]

        );
        DB::table('sub_categories')->insert(
            [
                'id' => 11,
                'categories_id' => 1,
                'name' => 'Board Markers',
                'code' => 'BOR',
            ]

        );
        DB::table('sub_categories')->insert(
            [
                'id' => 12,
                'categories_id' => 1,
                'name' => 'Overhead Projection Markers',
                'code' => 'OHP',
            ]

        );
        DB::table('sub_categories')->insert(
            [
                'id' => 13,
                'categories_id' => 3,
                'name' => 'Drawing System Pens',
                'code' => 'DRA',
            ]

        );
        DB::table('sub_categories')->insert(
            [
                'id' => 14,
                'categories_id' => 3,
                'name' => 'Calligraphy Pens',
                'code' => 'CAL',
            ]
        );
        DB::table('sub_categories')->insert(
            [
                'id' => 15,
                'categories_id' => 3,
                'name' => 'Multi Pens',
                'code' => 'MUL',
            ]
        );
        DB::table('sub_categories')->insert(
            [
                'id' => 16,
                'categories_id' => 3,
                'name' => 'Writing Pens',
                'code' => 'WRI',
            ]
        );
    }
}
