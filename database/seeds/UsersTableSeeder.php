<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'firstName' => 'Admin',
            'lastName' => 'Artline',
            'displayName' => 'Admin Artline',
            'company' => 'goodnews',
            'phone' => '123123123',
            'email' => 'admin@goodnews.id',
            'image' => '',
            'level' => 1,
            'dateCreated' => Carbon::now(),
            'rowPointer' => Uuid::uuid4()->getHex(),
            'password' => bcrypt('admin'),
        ]);
    }
}
