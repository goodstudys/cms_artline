<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resellers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('shopfor');
            $table->string('ktp');
            $table->string('ktp_image');
            $table->string('npwp')->nullable();
            $table->string('npwp_image')->nullable();
            $table->string('work_address');
            $table->string('work_city');
            $table->string('work_phone');
            $table->string('position');
            $table->dateTime('dateCreated');
            $table->string('rowPointer');
            $table->tinyInteger('isActive')->nullable();
            $table->tinyInteger('isObsolete')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resellers');
    }
}
