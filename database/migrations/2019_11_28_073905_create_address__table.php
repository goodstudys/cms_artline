<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('userId');
            $table->string('label');
            $table->string('type');
            $table->string('name');
            $table->text('address');
            $table->string('province');
            $table->string('city');
            $table->string('kecamatan');
            $table->string('kelurahan');
            $table->string('phone');
            $table->string('kodePos');
            $table->dateTime('dateCreated');
            $table->string('rowPointer');
            $table->tinyInteger('isActive')->nullable();
            $table->tinyInteger('isObsolete')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address');
    }
}
