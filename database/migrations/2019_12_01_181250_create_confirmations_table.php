<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfirmationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('confirmations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('orderId');
            $table->string('tujuan');
            $table->string('bank');
            $table->string('akun');
            $table->string('nama');
            $table->string('jumlah');
            $table->dateTime('dateCreated');
            $table->string('rowPointer');
            $table->tinyInteger('isActive')->nullable();
            $table->tinyInteger('isObsolete')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('confirmations');
    }
}
