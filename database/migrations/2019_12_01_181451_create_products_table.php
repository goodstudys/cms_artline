<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');;
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->char('colorHex')->nullable();
            $table->string('colorName')->nullable();
            $table->string('sku')->nullable();
            $table->string('category')->nullable();
            $table->string('subcategory')->nullable();
            $table->string('tags')->nullable();
            $table->string('brand')->nullable();
            $table->string('weight')->nullable();
            $table->string('dimensions')->nullable();
            $table->string('price')->nullable();
            $table->string('wholesale_price')->nullable();
            $table->string('discAmount')->nullable();
            $table->string('discPercent')->nullable();
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->string('image4')->nullable();
            $table->string('wholesale_type');
            $table->string('flag');
            $table->dateTime('dateCreated');
            $table->string('rowPointer');
            $table->tinyInteger('isActive')->nullable();
            $table->tinyInteger('isFeatured')->nullable();
            $table->tinyInteger('isObsolete')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
