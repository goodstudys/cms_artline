<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstName');
            $table->string('lastName');
            $table->string('displayName');
            $table->string('company');
            $table->string('level')->nullable();
            $table->string('phone');
            $table->string('image')->nullable();
            $table->string('dropImage')->nullable();
            $table->dateTime('dateCreated');
            $table->string('rowPointer');
            $table->tinyInteger('isDropship')->nullable();
            $table->string('dropName')->nullable();
            $table->tinyInteger('isActive')->nullable();
            $table->tinyInteger('isObsolete')->nullable();
            $table->string('email')->unique();
            $table->string('newsletter')->nullable();
            $table->string('password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
