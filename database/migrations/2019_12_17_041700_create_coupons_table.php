<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode');
            $table->string('category_id');
            $table->string('discAmount');
            $table->string('discPercent');
            $table->dateTime('date_start');
            $table->dateTime('date_end');
            $table->string('minOrder');
            $table->dateTime('dateCreated');
            $table->string('rowPointer');
            $table->tinyInteger('isActive')->nullable();
            $table->tinyInteger('isObsolete')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
