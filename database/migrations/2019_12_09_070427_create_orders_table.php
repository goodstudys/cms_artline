<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('userId')->nullable();
            $table->string('orderCode')->nullable();
            $table->string('billName');
            $table->string('billCompany')->nullable();
            $table->string('billProvince');
            $table->string('billCity');
            $table->string('billAddress');
            $table->string('billKecamatan');
            $table->string('billKelurahan');
            $table->string('billPhone');
            $table->string('billKodePos');
            $table->string('billEmail')->nullable();
            $table->string('billNotes')->nullable();
            $table->string('deliveryName')->nullable();
            $table->string('deliveryCompany')->nullable();
            $table->string('deliveryProvince')->nullable();
            $table->string('deliveryCity')->nullable();
            $table->string('deliveryAddress')->nullable();
            $table->string('deliveryKecamatan')->nullable();
            $table->string('deliveryKelurahan')->nullable();
            $table->string('deliveryPhone')->nullable();
            $table->string('deliveryKodePos')->nullable();
            $table->string('kurirName');
            $table->integer('kurirRate')->nullable();
            $table->integer('payment')->nullable();
            $table->integer('confirmationId')->nullable();
            $table->string('bookingCode')->nullable();
            $table->string('paymentNode')->nullable();
            $table->string('promocode')->nullable();
            $table->integer('quantity');
            $table->integer('subtotal');
            $table->integer('discount')->nullable();
            $table->integer('total');
            $table->string('status')->nullable();
            $table->string('noResi')->nullable();
            $table->string('type');
            $table->dateTime('dateCreated');
            $table->string('rowPointer');
            $table->tinyInteger('isObsolete')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
