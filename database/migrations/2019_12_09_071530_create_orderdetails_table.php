<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderdetails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('orderId');
            $table->string('name');
            $table->char('colorHex');
            $table->string('colorName');
            $table->string('sku');
            $table->string('category');
            $table->string('subcategory')->nullable();
            $table->string('tags');
            $table->string('brand');
            $table->string('weight');
            $table->string('dimensions');
            $table->string('price');
            $table->string('discAmount');
            $table->string('discPercent');
            $table->integer('quantity');
            $table->string('image')->nullable();
            $table->dateTime('dateCreated');
            $table->string('rowPointer');
            $table->tinyInteger('isObsolete')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderdetails');
    }
}
