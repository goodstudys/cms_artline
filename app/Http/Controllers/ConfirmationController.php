<?php

namespace App\Http\Controllers;

use App\Models\Confirmation;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use App\Helpers\CharozaLog;

class ConfirmationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('components.account.order.confirmation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tujuan' => ['required', 'string', 'max:255'],
            'bank' => ['required', 'string', 'max:255'],
            'akun' => ['required', 'string', 'max:255'],
            'nama' => ['required', 'string', 'max:255'],
            'jumlah' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();

        try {

            $id = Confirmation::max('id') + 1;

            Confirmation::insert([
                'id' => $id,
                'orderId' => $request->order,
                'tujuan' => $request->tujuan,
                'bank' => $request->bank,
                'akun' => $request->akun,
                'nama' => $request->nama,
                'jumlah' => $request->jumlah,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 1,
                'isObsolete' => 0,
            ]);

            DB::commit();

            return redirect()->route('confirmation.create')->with(['status' => 'success', 'message' => 'Confirmation saved']);

        }catch (Exception $e) {

            DB::rollback();

            return redirect()->route('confirmation.create')->with(['status' => 'danger', 'message' => 'Confirmation failed to save'.$e]);
        }
    }


    public function changeConfirm(Request $request)
    {
        $request->validate([
            'tujuan' => ['required', 'string', 'max:255'],
            'bank' => ['required', 'string', 'max:255'],
            'akun' => ['required', 'string', 'max:255'],
            'nama' => ['required', 'string', 'max:255'],
            'jumlah' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();

        try {

            $id = Confirmation::max('id') + 1;

            Confirmation::insert([
                'id' => $id,
                'orderId' => $request->order,
                'tujuan' => $request->tujuan,
                'bank' => $request->bank,
                'akun' => $request->akun,
                'nama' => $request->nama,
                'jumlah' => $request->jumlah,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 1,
                'isObsolete' => 0,
            ]);
            CharozaLog::add('confirmation', $request->nama, 'create', 'Input Confirmation');
            DB::commit();

            return redirect()->route('admin.order')->with(['status' => 'success', 'message' => 'Confirmation saved']);

        }catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.order')->with(['status' => 'danger', 'message' => 'Confirmation failed to save'.$e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tujuan' => ['required', 'string', 'max:255'],
            'bank' => ['required', 'string', 'max:255'],
            'akun' => ['required', 'string', 'max:255'],
            'nama' => ['required', 'string', 'max:255'],
            'jumlah' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();

        try {

            $data = Confirmation::where('rowPointer',$id);

            $data::update([
                'tujuan' => $request->tujuan,
                'bank' => $request->bank,
                'akun' => $request->akun,
                'nama' => $request->nama,
                'jumlah' => $request->jumlah,
                'dateCreated' => Carbon::now(),
            ]);

            DB::commit();

            return redirect()->route('confirmation.edit')->with(['status' => 'success', 'message' => 'Confirmation saved']);

        }catch (Exception $e) {

            DB::rollback();

            return redirect()->route('confirmation.edit')->with(['status' => 'danger', 'message' => 'Confirmation failed to save'.$e]);
        }
    }

    public function statusObsolete($id) {
        DB::beginTransaction();

        try {


            $data = User::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isObsolete == 1) {
                $data->update([
                    'isObsolete' => 0
                ]);
            } else {
                $data->update([
                    'isObsolete' => 1
                ]);
            }

            DB::commit();

            return back()->with(['status' => 'success', 'message' => 'Success update confirmation']);

        } catch (Exception $e) {

            DB::rollback();

            return back()->with(['status' => 'danger', 'message' => 'Failed update confirmation'.$e ]);

        }
    }

    public function statusActive($id) {
        DB::beginTransaction();

        try {


            $data = User::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0
                ]);
            } else {
                $data->update([
                    'isActive' => 1
                ]);
            }

            DB::commit();

            return back()->with(['status' => 'success', 'message' => 'Success update confirmation']);

        } catch (Exception $e) {

            DB::rollback();

            return back()->with(['status' => 'danger', 'message' => 'Failed update confirmation'.$e ]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
