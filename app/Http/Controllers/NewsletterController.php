<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Newsletter;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'newsletter' => ['required', 'string', 'email', 'max:255'],
        ]);

        DB::beginTransaction();

        try {

            $id = Newsletter::max('id') + 1;

            Newsletter::insert([
                'id' => $id,
                'email' => $request->newsletter,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
            ]);

            DB::commit();

            return back()->with(['status' => 'success', 'message' => "Thanks for joining the newsletter! You're awesome."]);

        } catch (Exception $e) {

            DB::rollback();

            return back()->with(['message' => 'Newsletter failed to save'.$e ]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'newsletter' => ['required', 'string', 'email', 'max:255'],
        ]);

        DB::beginTransaction();

        try {


            $data = User::where('id', Auth::user()->id);

            $data->update([
                'newsletter' => $request->newsletter,
            ]);

            DB::commit();

            return back()->with(['status' => 'success', 'message' => 'Newsletter saved']);

        } catch (Exception $e) {

            DB::rollback();

            return back()->with(['status' => 'danger', 'message' => 'Newsletter failed to save'.$e ]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
