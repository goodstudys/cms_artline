<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use DB;
use App\Models\Group;
use App\Helpers\CharozaLog;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        DB::beginTransaction();

        try {

            $id = Group::max('id') + 1;

            Group::insert([
                'id' => $id,
                'productId' => $request->default,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 1,
                'isObsolete' => 0,
            ]);

            $feeder = [];
            $ids = DB::table('groupsdetails')->max('id') + 1;
            $for = $request->get('name');
//            $hex = $request->get('hex');
//            $colorname = $request->get('color');
            $price = $request->get('price');
            $discamount = $request->get('discAmount');
            $discpercent = $request->get('discPercent');
            if ($for == null) {
                return back()->with(['status' => 'danger', 'message' => 'Gagal Menambahkan Variasi / Data Kosong']);
            }
            for($i=0;$i<count($for);$i++){
                array_push($feeder, [
                    'id' => $ids++,
                    'groupId' => $id,
                    'name' => $for[$i],
//                    'colorHex' => $hex[$i],
//                    'colorName' => $colorname[$i],
                    'price' => $price[$i],
                    'discAmount' => $discamount[$i],
                    'discPercent' => $discpercent[$i],
                    'dateCreated' => Carbon::now(),
                    'rowPointer' => Uuid::uuid4()->getHex(),
                    'isActive' => 1,
                    'isObsolete' => 0
                ]);
            }


            DB::table('groupsdetails')->insert($feeder);
            CharozaLog::add('groupdetails', $id, 'create', 'Create Group details');
            DB::commit();

            return redirect()->route('admin.product')->with(['status' => 'success', 'message' => 'Save']);

        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.product')->with(['status' => 'danger', 'message' => 'Order failed' . $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
//            'colorhex' => ['required', 'string', 'max:255'],
//            'colorname' => ['required', 'string', 'max:255'],
            'price' => ['required', 'string', 'max:255'],
            'discAmount' => ['required', 'string', 'max:255'],
            'discPercent' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();

        try {

            $data = DB::table('groupsdetails')->where('rowPointer', $id);
            $datas = DB::table('groupsdetails')->where('rowPointer', $id)->first();


            $data->update([
                'name' => $request->name,
//                'colorHex' => $request->colorhex,
//                'colorName' => $request->colorname,
                'price' => $request->price,
                'discAmount' => $request->discAmount,
                'discPercent' => $request->discPercent,
            ]);
            CharozaLog::add('groupsdetails', $datas->name, 'Edit', 'Edit Group details');
            DB::commit();

            return redirect()->route('product.showVariasi', $request->pointer)->with(['status' => 'success', 'message' => 'Variasi saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('product.showVariasi', $request->pointer)->with(['status' => 'danger', 'message' => 'Variasi failed to save' . $e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

            $data = DB::table('groupsdetails')->where('rowPointer', $id)->first();
            $delete = DB::table('groupsdetails')->where('rowPointer', $id)->delete();


//            $model =  DB::table('groupsdetails')->find($id);
//            $model->delete();
            CharozaLog::add('groupsdetails', $data->name, 'delete', 'Delete Variasi');
            return back()->with(['status' => 'success', 'message' => 'Success delete variasi']);

        }catch (Exception $e){
            DB::rollback();

            return back()->with(['status' => 'danger', 'message' => 'Success delete variasi' . $e]);
        }
    }
}
