<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Order;
use App\Models\Testimonial;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id', Auth::user()->id)->first();

        $billings = Address::where('userId', Auth::user()->id)
            ->where('type', 'BILLING')
            ->get();

        $deliverys = Address::where('userId', Auth::user()->id)
            ->where('type', 'DELIVERY')
            ->get();

        $orders = Order::where('userId', Auth::user()->id)->where('type', 'DEFAULT')
            ->get();

        $preorders = Order::where('userId', Auth::user()->id)->where('type', 'RESELLER')
            ->get();
        $testimonial = Testimonial::where('userId', Auth::user()->id)->first();
        $address = Address::where('userId', Auth::user()->id)->first();
        $reseller = DB::table('resellers')->where('user_id', $user->id)->first();
        return view('pages.account', [
            'user' => $user,
            'address' => $address,
            'billings' => $billings,
            'deliverys' => $deliverys,
            'testimonial' => $testimonial,
            'orders' => $orders,
            'preorders' => $preorders,
            'reseller' => $reseller,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'cur_pass' => ['required', 'string', 'max:255'],
            'password_confirmation' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        DB::beginTransaction();

        try {

            $data = User::where('id', $id);

            $check = User::where('id', $id)->first();

            if (!Hash::check($request->cur_pass, $check->password)) {
                return back()->with(['status' => 'danger', 'message' => 'Password lama anda salah']);
            } else {
                $data->update([
                    'password' => Hash::make($request->password),
                ]);

                DB::commit();

                return back()->with(['status' => 'success', 'message' => 'Password berhasil dirubah']);
            }

        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('blog.create')->with(['status' => 'danger', 'message' => 'Password failed to save' . $e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function name()
    {
        $words = explode(" ", Auth::user()->displayName);
        $acronym = "";

        foreach ($words as $w) {
            $acronym .= $w[0];
        }

        dd($acronym);
    }
}
