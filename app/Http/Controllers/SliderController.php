<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Slider;
use DB;
use Ramsey\Uuid\Uuid;
use App\Helpers\CharozaLog;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $sliders = Slider::where('isObsolete',0)->get();

        return view('admin.pages.slider.index')->with('sliders', $sliders);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'subtitle' => ['required', 'string', 'max:255'],
            'button' => ['required', 'string', 'max:255'],
            'link' => ['required', 'string', 'max:255'],
            'image' => ['required', 'image', 'max:2048'],
        ]);

        DB::beginTransaction();

        try {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();

            request()->image->move(public_path() . '/upload/users/sliders', $imageName);

            $id = Slider::max('id') + 1;

            Slider::insert([
                'id' => $id,
                'title' => $request->title,
                'subtitle' => $request->subtitle,
                'button' => $request->button,
                'link' => $request->link,
                'image' => $imageName,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 1,
                'isObsolete' => 0,
            ]);
            CharozaLog::add('slider', $request->title, 'created', 'Create Slider');
            DB::commit();

            return redirect()->route('slider.create')->with(['status' => 'success', 'message' => 'Slider saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('slider.create')->with(['status' => 'danger', 'message' => 'Slider failed to save' . $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Slider::where('rowPointer', $id)->first();

        return view('admin.pages.slider.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'subtitle' => ['required', 'string', 'max:255'],
            'button' => ['required', 'string', 'max:255'],
            'link' => ['required', 'string', 'max:255'],

        ]);

        DB::beginTransaction();

        try {

            $data = Slider::where('rowPointer', $id);

            $imageDb = Slider::where('rowPointer', $id)->first();

            $images1 = $request->image;

            if ($images1 != null) {

                $imageName1 = time() . '.' . request()->image->getClientOriginalExtension();

                request()->image->move(public_path() . '/upload/users/sliders', $imageName1);
            } else {
                $imageName1 = $imageDb->image;
            }

            $data->update([
                'title' => $request->title,
                'subtitle' => $request->subtitle,
                'button' => $request->button,
                'link' => $request->link,
                'image' => $imageName1,
                'dateCreated' => Carbon::now(),
                'isActive' =>$request->isActive,
                // 'isObsolete' => 0,
            ]);
            CharozaLog::add('slider', $request->title, 'edit', 'Edit Slider');
            DB::commit();

            return redirect()->route('slider.edit', $id)->with(['status' => 'success', 'message' => 'Slider saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('slider.edit'. $id)->with(['status' => 'danger', 'message' => 'Slider failed to save' . $e]);
        }
    }

    public function statusActive($id)
    {
        DB::beginTransaction();

        try {


            $data = Slider::where('rowPointer', $id);

            $datas = $data->first();


            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0
                ]);
            } else {
                $data->update([
                    'isActive' => 1
                ]);
            }
            CharozaLog::add('slider', $datas->title, 'update', 'Change Status Active Slider');
            DB::commit();

            return redirect()->route('admin.slider')->with(['status' => 'success', 'message' => 'Success update slider']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.slider')->with(['status' => 'danger', 'message' => 'Failed update slider' . $e]);
        }
    }

    public function statusDelete($id)
    {
        DB::beginTransaction();

        try {


            $data = Slider::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isObsolete == 0) {
                $data->update([
                    'isObsolete' => 1
                ]);
            } else {
                $data->update([
                    'isObsolete' => 1
                ]);
            }
            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0
                ]);
            } else {
                $data->update([
                    'isActive' => 0
                ]);
            }
            CharozaLog::add('slider', $datas->title, 'update', 'Change Status Obsulete Slider');
            DB::commit();

            return redirect()->route('admin.slider')->with(['status' => 'success', 'message' => 'Success delete slider']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.slider')->with(['status' => 'danger', 'message' => 'Failed delete slider' . $e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

            $data = Slider::where('rowPointer', $id);

            $datas = $data->first();

            // if ($datas->isActive == 1) {
            //     $data->update([
            //         'isActive' => 0
            //     ]);
            // } else {
            //     $data->update([
            //         'isActive' => 1
            //     ]);
            // }
            $model = Slider::find($id);
            $model->delete();
            CharozaLog::add('slider', $datas->title, 'delete', 'Delete Slider');
            return redirect()->route('admin.slider')->with(['status' => 'success', 'message' => 'Success delete Slider']);

        }catch (Exception $e){
            DB::rollback();

            return redirect()->route('admin.slider', $id)->with(['status' => 'danger', 'message' => 'Slider failed to delete' . $e]);
        }
    }
}
