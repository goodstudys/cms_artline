<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Models\JobRequest;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use App\Helpers\CharozaLog;
use Illuminate\Support\Facades\File;

class ResellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $users = User::where('level', '!=', 1)->where('level', '!=', 2)->get();
        $data = DB::table('resellers')->get();
        return view('admin.pages.reseller.index')->with('data', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = User::select('id', 'firstName')->get();
        return view('admin.pages.reseller.create')->with(['data' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {

            $ktpimageName = time() . '.' . request()->ktp_image->getClientOriginalExtension();

            request()->ktp_image->move(base_path() . '/public/upload/reseller/', $ktpimageName);
            if ($request->npwp_image != null) {
                $npwpimageName = time() . '.' . request()->npwp_image->getClientOriginalExtension();

                request()->npwp_image->move(base_path() . '/public/upload/reseller/', $npwpimageName);
            } else {
                $npwpimageName = null;
            }

            $id = DB::table('resellers')->max('id') + 1;

            DB::table('resellers')->insert([
                'id' => $id,
                'user_id' => $request->user_id,
                'shopfor' => $request->shopfor,
                'ktp' => $request->ktp,
                'ktp_image' => $ktpimageName,
                'npwp' => $request->npwp,
                'npwp_image' => $npwpimageName,
                'work_address' => $request->work_address,
                'work_city' => $request->work_city,
                'work_phone' => $request->work_phone,
                'position' => $request->position,
                'isActive' => '1',
                'isObsolete' => '0',
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
            ]);

            DB::commit();

            return redirect()->route('reseller.create')->with(['status' => 'success', 'message' => 'resellers saved']);

        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('reseller.create')->with(['status' => 'danger', 'message' => 'resellers failed to save' . $e]);
        }
    }
    public function storeUser(Request $request)
    {
        DB::beginTransaction();

        try {

            $ktpimageName = time() . '.' . request()->ktp_image->getClientOriginalExtension();

            request()->ktp_image->move(base_path() . '/public/upload/reseller/', $ktpimageName);
            if ($request->npwp_image != null) {
                $npwpimageName = time() . '.' . request()->npwp_image->getClientOriginalExtension();

                request()->npwp_image->move(base_path() . '/public/upload/reseller/', $npwpimageName);
            } else {
                $npwpimageName = null;
            }

            $id = DB::table('resellers')->max('id') + 1;

            DB::table('resellers')->insert([
                'id' => $id,
                'user_id' => $request->user_id,
                'shopfor' => $request->shopfor,
                'ktp' => $request->ktp,
                'ktp_image' => $ktpimageName,
                'npwp' => $request->npwp,
                'npwp_image' => $npwpimageName,
                'work_address' => $request->work_address,
                'work_city' => $request->work_city,
                'work_phone' => $request->work_phone,
                'position' => $request->position,
                'isActive' => '0',
                'isObsolete' => '0',
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
            ]);
            $idX = JobRequest::max('id') + 1;
            JobRequest::insert([
                'id' => $idX,
                'user_id' => Auth::user()->id,
                'job' => 'RESELLER',
                'created_at' => Carbon::now()
            ]);
            DB::commit();

            return redirect()->route('account')->with(['status' => 'success', 'message' => 'resellers saved']);  

        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('account')->with(['status' => 'danger', 'message' => 'resellers failed to save' . $e]);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $user = User::get();
        $data = DB::table('resellers')->where('rowPointer', $id)->first();

        return view('admin.pages.reseller.edit', ['user' => $user, 'data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        try {

            $reseller = DB::table('resellers')->where('rowPointer', $id);
            $data = $reseller->first();

            if ($request->ktp_image != null) {
                $file_path1 = app_path("/upload/reseller/" . $request->ktp_image);
                if (File::exists($file_path1)) {
                    File::delete($file_path1);
                }
                $ktpimageName = time() . '.' . request()->ktp_image->getClientOriginalExtension();

                request()->ktp_image->move(public_path() . '/upload/reseller/', $ktpimageName);
            } else {
                $ktpimageName = $data->ktp_image;
            }
            if ($request->npwp_image != null) {
                $file_path1 = app_path("/upload/reseller/" . $request->npwp_image);
                if (File::exists($file_path1)) {
                    File::delete($file_path1);
                }
                $npwpimageName = time() . '.' . request()->npwp_image->getClientOriginalExtension();

                request()->npwp_image->move(public_path() . '/upload/reseller/', $npwpimageName);
            } else {
                $npwpimageName = $data->npwp_image;
            }

            $reseller->update([
                'user_id' => $request->user_id,
                'shopfor' => $request->shopfor,
                'ktp' => $request->ktp,
                'ktp_image' => $ktpimageName,
                'npwp' => $request->npwp,
                'npwp_image' => $npwpimageName,
                'work_address' => $request->work_address,
                'work_city' => $request->work_city,
                'work_phone' => $request->work_phone,
                'position' => $request->position,
            ]);
            CharozaLog::add('reseller', $request->user_id, 'edit', 'Edit Reseller');
            DB::commit();

            return redirect()->route('reseller.edit', $id)->with(['status' => 'success', 'message' => 'Reseller saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('reseller.edit', $id)->with(['status' => 'danger', 'message' => 'Reseller failed to save' . $e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
