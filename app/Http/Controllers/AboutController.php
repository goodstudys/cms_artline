<?php

namespace App\Http\Controllers;

use App\About;
use App\Helpers\CharozaLog;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = About::get();

        return view('admin.pages.about.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = About::where('id', $id)->first();

        return view('admin.pages.about.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'description' => ['required', 'string'],
        ]);

        DB::beginTransaction();

        try {

            $data = About::where('id', $id)->first();
            if ($request->header_image != null) {

                $imageName1 = time() . '.' . request()->header_image->getClientOriginalExtension();

                request()->header_image->move(public_path() . '/upload/users/abouts/', $imageName1);
            } else {
                $imageName1 = $data->header_image;
            }
            if ($request->banner_image != null) {

                $imageName2 = time() . '.' . request()->banner_image->getClientOriginalExtension();

                request()->banner_image->move(public_path() . '/upload/users/abouts/', $imageName2);
            } else {
                $imageName2 = $data->banner_image;
            }


            $data = About::where('id', $id);

            $data->update([
                'description' => $request->description,
                'header_image' => $imageName1,
                'banner_image' => $imageName2,
            ]);
            CharozaLog::add('About', $id, 'edit', 'Edit About');
            DB::commit();

            return redirect()->route('admin.about')->with(['status' => 'success', 'message' => 'About saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('about.edit', $id)->with(['status' => 'danger', 'message' => 'About failed to save' . $e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
