<?php

namespace App\Http\Controllers;

use App\Faq;
use App\Helpers\CharozaLog;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Faq::get();

        return view('admin.pages.faq.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.faq.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();

        try {

            $id = Faq::max('id') + 1;

            Faq::insert([
                'id' => $id,
                'type' => $request->type,
                'title' => $request->title,
                'description' => $request->description,

            ]);
            CharozaLog::add('Faq', $request->title, 'created', 'Create Faq');
            DB::commit();

            return redirect()->route('admin.faq')->with(['status' => 'success', 'message' => 'Faq saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('faq.create')->with(['status' => 'danger', 'message' => 'Faq failed to save' . $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Faq::where('id', $id)->first();

        return view('admin.pages.faq.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();

        try {

            $data = Faq::where('id', $id);

            $data->update([
                'type' => $request->type,
                'title' => $request->title,
                'description' => $request->description,
            ]);
            CharozaLog::add('faq', $request->title, 'edit', 'Edit Faq');
            DB::commit();

            return redirect()->route('admin.faq', $id)->with(['status' => 'success', 'message' => 'Faq saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('faq.edit', $id)->with(['status' => 'danger', 'message' => 'Faq failed to save' . $e]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
