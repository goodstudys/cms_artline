<?php

namespace App\Http\Controllers;

use App\Models\Carousel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use DB;
use App\Helpers\CharozaLog;

class CarouselController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $carousels = Carousel::where('isObsolete',0)->get();

        return view('admin.pages.carousel.index', ['carousels' => $carousels]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.carousel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'subtitle' => ['required', 'string', 'max:255'],
            'button' => ['required', 'string', 'max:255'],
            'link' => ['required', 'string', 'max:255'],
            'image' => ['required', 'image', 'max:2048'],
        ]);

        DB::beginTransaction();

        try {

            $imageName = time().'.'.request()->image->getClientOriginalExtension();

            request()->image->move(public_path() . '/upload/users/carousels', $imageName);

            $id = Carousel::max('id') + 1;

            Carousel::insert([
                'id' => $id,
                'title' => $request->title,
                'subtitle' => $request->subtitle,
                'button' => $request->button,
                'link' => $request->link,
                'image' => $imageName,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 1,
                'isObsolete' => 0,
            ]);
            CharozaLog::add('carousel', $request->title, 'created', 'Create Carousel');
            DB::commit();

            return redirect()->route('carousel.create')->with(['status' => 'success', 'message' => 'Carousel saved']);

        }catch (Exception $e) {

            DB::rollback();

            return redirect()->route('carousel.create')->with(['status' => 'danger', 'message' => 'Carousel failed to save'.$e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Carousel::where('rowPointer', $id)->first();

        return view('admin.pages.carousel.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'subtitle' => ['required', 'string', 'max:255'],
            'button' => ['required', 'string', 'max:255'],
            'link' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();

        try {



            $data = Carousel::where('rowPointer', $id);
            $imageDb = Carousel::where('rowPointer', $id)->first();

            $images1 = $request->image;

            if ($images1 != null) {

                $imageName1 = time() . '.' . request()->image->getClientOriginalExtension();

                request()->image->move(public_path() . '/upload/users/carousels', $imageName1);
            } else {
                $imageName1 = $imageDb->image;
            }


            $data->update([
                'title' => $request->title,
                'subtitle' => $request->subtitle,
                'button' => $request->button,
                'link' => $request->link,
                'image' => $imageName1,
                'dateCreated' => Carbon::now(),
                'isActive' => $request->isActive,

            ]);
            CharozaLog::add('carousel', $request->title, 'edit', 'Edit Carousel');
            DB::commit();

            return redirect()->route('carousel.edit', $id)->with(['status' => 'success', 'message' => 'Carousel saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('carousel.edit'. $id)->with(['status' => 'danger', 'message' => 'Carousel failed to save' . $e]);
        }
    }

    public function statusActive($id)
    {
        DB::beginTransaction();

        try {


            $data = Carousel::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0
                ]);
            } else {
                $data->update([
                    'isActive' => 1
                ]);
            }
            CharozaLog::add('carousel', $datas->title, 'update', 'Change Status Active Carousel');
            DB::commit();

            return redirect()->route('admin.carousel')->with(['status' => 'success', 'message' => 'Success update carousel']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.carousel')->with(['status' => 'danger', 'message' => 'Failed update carousel' . $e]);
        }
    }
    public function statusDelete($id)
    {
        DB::beginTransaction();

        try {


            $data = Carousel::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isObsolete == 0) {
                $data->update([
                    'isObsolete' => 1
                ]);
            } else {
                $data->update([
                    'isObsolete' => 1
                ]);
            }
            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0
                ]);
            } else {
                $data->update([
                    'isActive' => 0
                ]);
            }
            CharozaLog::add('carousel', $datas->title, 'update', 'Change Status Obsulete Carousel');
            DB::commit();

            return redirect()->route('admin.carousel')->with(['status' => 'success', 'message' => 'Success delete carousel']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.carousel')->with(['status' => 'danger', 'message' => 'Failed delete carousel' . $e]);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

            $data = Carousel::where('rowPointer', $id);

            $datas = $data->first();

            // if ($datas->isActive == 1) {
            //     $data->update([
            //         'isActive' => 0
            //     ]);
            // } else {
            //     $data->update([
            //         'isActive' => 1
            //     ]);
            // }
            $model = Carousel::find($id);
            $model->delete();
            CharozaLog::add('carousel', $datas->title, 'delete', 'Delete carousel');
            return redirect()->route('admin.carousel')->with(['status' => 'success', 'message' => 'Success delete carousel']);

        }catch (Exception $e){
            DB::rollback();

            return redirect()->route('admin.carousel', $id)->with(['status' => 'danger', 'message' => 'carousel failed to delete' . $e]);
        }

    }
}
