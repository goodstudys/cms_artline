<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DropshipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'dropship_name' => ['required', 'string', 'max:255'],
            'dropship_image' => ['required', 'image', 'max:2048'],
        ]);

        DB::beginTransaction();

        try {


            $data = User::where('id', Auth::user()->id);

            $imageName = time().'.'.request()->dropship_image->getClientOriginalExtension();

            request()->dropship_image->move(public_path() . '/upload/users/dropships', $imageName);

            $data->update([
                'dropName' => $request->dropship_name,
                'dropImage' => $imageName,
                'isDropship' => 1
            ]);

            DB::commit();

            return back()->with(['status' => 'success', 'message' => 'Dropship saved']);

        } catch (Exception $e) {

            DB::rollback();

            return back()->with(['status' => 'danger', 'message' => 'Dropship failed to save'.$e ]);

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        DB::beginTransaction();

        try {


            $data = User::where('id', Auth::user()->id);

            $datas = $data->first();

            if ($datas->isDropship == 1) {
                $data->update([
                    'isDropship' => 0
                ]);
            } else {
                $data->update([
                    'isDropship' => 1
                ]);
            }

            DB::commit();

            return back()->with(['status' => 'success', 'message' => 'Success update dropship']);

        } catch (Exception $e) {

            DB::rollback();

            return back()->with(['status' => 'danger', 'message' => 'Failed update dropship'.$e ]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
