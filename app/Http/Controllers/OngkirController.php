<?php

namespace App\Http\Controllers;

use App\Helpers\RajaOngkir;
use Illuminate\Http\Request;
use Session;

class OngkirController extends Controller
{
    public function getProvince()
    {
        $data = RajaOngkir::getProvince();

        return $data;
    }

    public function getCity(Request $request)
    {
        $id_province = $request->province;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.rajaongkir.com/starter/city?province=$id_province",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Key: c4f9fd3ae116d5f8a950007e4d8171d2"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

//            echo "cURL Error #:" . $err;
            $result = "Connection problem";

        } else {

            $result = json_decode($response, true);
        }

        return $result;
    }

    public function getCost(Request $request)
    {
        $destionation = $request->cityRow;
        $weight = $request->weightRow;

        $data = RajaOngkir::getCost(444, $destionation, $weight, "jne");

        if ($data['rajaongkir']['status']['code'] == 200) {

            $result = $data['rajaongkir']['results'];

            $value = $result['0']['costs']['0']['cost']['0']['value'];
            $estimasi = $result['0']['costs']['0']['cost']['0']['etd'];

            Session::put('ongkir', $value);
            Session::put('ongkir-etd', $estimasi);

            return back();
        } else {
            Session::put('ongkir', "Cannot calculate shipping, please check your input");
            Session::put('ongkir-etd', "Cannot calculate shipping, please check your input");

            return back();
        }
    }
}
