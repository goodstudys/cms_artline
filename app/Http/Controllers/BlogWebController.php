<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use DB;
class BlogWebController extends Controller
{
    public function index()
    {
        $blogs = Blog::where('blogs.isActive',1)
        ->join('users','users.id','userId')
        ->select('blogs.id as blogId',
         'users.id as usersId','userId',
         'blogs.image as blogImage',
         'blogs.rowPointer as blogrowPointer',
         'blogs.category as blogCategory',
         'blogs.dateCreated as blogDate',
         'users.image as userImage',
         'blogs.title as blogTitle',
         'users.displayName as userDisplayname')->paginate(9);
        return view('pages.blog.index')->with('blogs', $blogs);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::where('rowPointer', $id)->first();


        $blogs = Blog::where('blogs.isActive',1)
        ->join('users','users.id','userId')
        ->select('blogs.id as blogId',
         'users.id as usersId','userId',
         'blogs.image as blogImage',
         'blogs.rowPointer as blogrowPointer',
         'blogs.category as blogCategory',
         'blogs.dateCreated as blogDate',
         'users.image as userImage',
         'blogs.title as blogTitle',
         'users.displayName as userDisplayname')->get();
       

        $blognext = Blog::where('rowPointer', $id)->max('id') + 1;
        $nextdata = Blog::where('id', $blognext)->first();
        $nextusers = $nextdata !=null ?             $nextusers = DB::table('blogs')
        ->join('users', function ($join) use ($nextdata) {
            $join->on('blogs.userId', '=', 'users.id')
                 ->where('blogs.id', '=', $nextdata->id);
        })
        ->first():0;

        $blogprev = Blog::where('rowPointer', $id)->max('id') - 1;
        $prevdata = Blog::where('id', $blogprev)->first();
        $prevusers =$prevdata != null ?            $prevusers = DB::table('blogs')
        ->join('users', function ($join) use ($prevdata) {
            $join->on('blogs.userId', '=', 'users.id')
                 ->where('blogs.id', '=', $prevdata->id);
        })
        ->first():0;


        
        
        $users = DB::table('blogs')
        ->join('users', function ($join) use ($blog) {
            $join->on('blogs.userId', '=', 'users.id')
                 ->where('blogs.id', '=', $blog->id);
        })
        ->first();


        $next = Blog::where('id', $blog->id +1)->pluck('rowPointer')->first();

        $previous = Blog::where('id', $blog->id -1)->pluck('rowPointer')->first();

        return view('pages.blog.show', [
            'blogs' => $blogs,
            'blog' => $blog,
            'next' => $next,
            'nextdata' => $nextdata,
            'nextusers' => $nextusers,
            'prevdata' => $prevdata,
            'prevusers' => $prevusers,
            'users' => $users,
            'previous' => $previous
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
