<?php

namespace App\Http\Controllers;

use App\Helpers\RajaOngkir;
use Illuminate\Http\Request;
use Cart;
use App\Models\Product;
use Session;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = Cart::getContent();

        $provinces = RajaOngkir::getProvince();

        $city = RajaOngkir::getCity();

        return view('pages.cart', [
            'datas' => $datas,
            'provinces' => $provinces,
            'citys' => $city
        ]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Product::where('rowPointer', $request->default)->first();

        $wholesale_price = (float) $request->wholesale_price;

        if ($request->row == null) {
            Cart::add([
                'id' => $request->default,
                'name' => $data->name,
                'quantity' => $request->qty,
                'price' => $request->prices,
                'attributes' => array(
                    'image' => $data->image1,
                    'wholesale_price' => $wholesale_price,
                    'color' => $request->colorss,
                    'colorname' => $request->colorname,
                    'weight' => $data->weight,
                    'category' => $request->category,
                    'default' => $request->default
                )
            ]);
        } else {
            Cart::add([
                'id' => $request->row,
                'name' => $data->name,
                'quantity' => $request->qty,
                'price' => $request->prices,
                'attributes' => array(
                    'image' => $data->image1,
                    'wholesale_price' => $wholesale_price,
                    'color' => $request->colorss,
                    'colorname' => $request->colorname,
                    'weight' => $data->weight,
                    'category' => $request->category,
                    'default' => $request->default
                )
            ]);
        }

        Session::forget('prodPrice');
        Session::forget('proddiscAmount');
        Session::forget('proddiscPercent');
        Session::forget('prodHex');
        Session::forget('prodName');
        Session::forget('prodPointer');

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request)
    {
        $qty = $request->newQty;
        $row = $request->rowId;
        $type = $request->type;

        $qtys = $qty - $qty + 1;
        $qtyss = $qty - $qty - 1;

        if ($type == 'inc') {
            Cart::update($row, array(
                'quantity' => $qtys, // so if the current product has a quantity of 4, it will subtract 1 and will result to 3
            ));
        } elseif ($type == 'dec')
            Cart::update($row, array(
                'quantity' => $qtyss, // so if the current product has a quantity of 4, it will subtract 1 and will result to 3
            ));


        echo "success";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
        Session::forget('kode');
        Session::forget('category');
        Session::forget('discAmount');
        Session::forget('discPercent');
        Session::forget('getTotal');

        return back();
    }

    public function clear()
    {
        Cart::clear();
        Session::forget('kode');
        Session::forget('category');
        Session::forget('discAmount');
        Session::forget('discPercent');
        Session::forget('getTotal');

        return back();
    }
}
