<?php

namespace App\Http\Controllers;

use App\Helpers\RajaOngkir;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Models\Group;
use Ramsey\Uuid\Uuid;

class PreOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('orders')
            ->where('type', 'RESELLER')
            ->leftJoin('confirmations', 'orders.id', '=', 'confirmations.orderId')
            ->select('orders.id',
                'orders.userId', 'confirmations.id as confirmationsid',
                'orders.billName', 'confirmations.orderId as confirmationsorderId', 'confirmations.tujuan as confirmationstujuan',
                'orders.billCompany', 'confirmations.bank as confirmationsbank',
                'orders.billProvince', 'confirmations.akun as confirmationsakun',
                'orders.billCity', 'confirmations.nama as confirmationsnama',
                'orders.billAddress', 'confirmations.jumlah as confirmationsjumlah',
                'orders.billKecamatan', 'confirmations.dateCreated as confirmationsdateCreated',
                'orders.billKelurahan', 'confirmations.rowPointer as confirmationsrowPointer',
                'orders.billPhone', 'confirmations.isActive as confirmationsisActive',
                'orders.billKodePos',
                'orders.orderCode',
                'orders.billEmail',
                'orders.kurirName',
                'orders.quantity',
                'orders.subtotal',
                'orders.total',
                'orders.status',
                'orders.dateCreated',
                'orders.rowPointer', 'orders.bookingCode')
            ->get();

        return view('admin.pages.preorder.index')->with('orders', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = Product::where('rowPointer', $request->prodPointer)->first();

        $order = Order::where('rowPointer', $request->orderPointer)->first();

        $id =  DB::table('orderdetails')->max('id') + 1;

        $data = DB::table('orderdetails')->where('orderId', $order->id)->where('sku', $product->sku)->count();

        $data2 = DB::table('orderdetails')->where('orderId', $order->id)->where('sku', $product->sku)->first();

        if ($request->variasi == '0') {
            if ($data == 1) {
                $update = DB::table('orderdetails')->where('orderId', $order->id)->where('sku', $product->sku)->where('colorName', $product->colorName);
                $updatedata = DB::table('orderdetails')->where('orderId', $order->id)->where('sku', $product->sku)->where('colorName', $product->colorName)->first();

                $qty = $updatedata->quantity + $request->qty;

                $update->update([
                   'quantity' => $qty
                ]);
            } else {

                DB::table('orderdetails')->insert([
                    'id' => $id,
                    'orderId' => $order->id,
                    'name' => $product->name,
                    'colorHex' => $product->colorHex,
                    'colorName' => $product->colorName,
                    'sku' => $product->sku,
                    'category' => $product->category,
                    'subcategory' => $product->subcategory,
                    'tags' => $product->tags,
                    'brand' => $product->brand,
                    'weight' => $product->weight,
                    'dimensions' => $product->dimensions,
                    'price' => $product->wholesale_price,
                    'discAmount' => $product->discAmount,
                    'discPercent' => $product->discPercent,
                    'quantity' => $request->qty,
                    'image' => $product->image1,
                    'dateCreated' => Carbon::now(),
                    'rowPointer' => Uuid::uuid4()->getHex(),
                    'isObsolete' => 0,
                ]);
            }
        } else {

            $groupdetails = DB::table('groupsdetails')->where('rowPointer', $request->variasi)->first();

            $data3 = DB::table('orderdetails')->where('orderId', $order->id)->where('sku', $product->sku.'-'.$groupdetails->name)->where('colorName', $groupdetails->name)->count();

            if ($data == 1) {
                if ($data2->colorName == $groupdetails->name) {
                    $update = DB::table('orderdetails')->where('orderId', $order->id)->where('sku', $product->sku.'-'.$groupdetails->name)->where('colorName', $groupdetails->name);
                    $updatedata = DB::table('orderdetails')->where('orderId', $order->id)->where('sku', $product->sku.'-'.$groupdetails->name)->where('colorName', $groupdetails->name)->first();

                    $qty = $updatedata->quantity + $request->qty;

                    $update->update([
                        'quantity' => $qty
                    ]);
                } elseif ($data3 == 1) {
                    $update = DB::table('orderdetails')->where('orderId', $order->id)->where('sku', $product->sku.'-'.$groupdetails->name)->where('colorName', $groupdetails->name);
                    $updatedata = DB::table('orderdetails')->where('orderId', $order->id)->where('sku', $product->sku.'-'.$groupdetails->name)->where('colorName', $groupdetails->name)->first();

                    $qty = $updatedata->quantity + $request->qty;

                    $update->update([
                        'quantity' => $qty
                    ]);
                }else {
                    DB::table('orderdetails')->insert([
                        'id' => $id,
                        'orderId' => $order->id,
                        'name' => $product->name,
                        'colorHex' => $product->colorHex,
                        'colorName' => $groupdetails->name,
                        'sku' => $product->sku.'-'.$groupdetails->name,
                        'category' => $product->category,
                        'subcategory' => $product->subcategory,
                        'tags' => $product->tags,
                        'brand' => $product->brand,
                        'weight' => $product->weight,
                        'dimensions' => $product->dimensions,
                        'price' => $product->wholesale_price,
                        'discAmount' => $product->discAmount,
                        'discPercent' => $product->discPercent,
                        'quantity' => $request->qty,
                        'image' => $product->image1,
                        'dateCreated' => Carbon::now(),
                        'rowPointer' => Uuid::uuid4()->getHex(),
                        'isObsolete' => 0,
                    ]);
                }
            } else {

                DB::table('orderdetails')->insert([
                    'id' => $id,
                    'orderId' => $order->id,
                    'name' => $product->name,
                    'colorHex' => $product->colorHex,
                    'colorName' => $groupdetails->name,
                    'sku' => $product->sku.'-'.$groupdetails->name,
                    'category' => $product->category,
                    'subcategory' => $product->subcategory,
                    'tags' => $product->tags,
                    'brand' => $product->brand,
                    'weight' => $product->weight,
                    'dimensions' => $product->dimensions,
                    'price' => $product->wholesale_price,
                    'discAmount' => $product->discAmount,
                    'discPercent' => $product->discPercent,
                    'quantity' => $request->qty,
                    'image' => $product->image1,
                    'dateCreated' => Carbon::now(),
                    'rowPointer' => Uuid::uuid4()->getHex(),
                    'isObsolete' => 0,
                ]);
            }

        }

        return back()->with(['status' => 'success', 'message' => 'Success add product']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provinces = RajaOngkir::getProvince();

        $city = RajaOngkir::getCity();

        $detail = DB::table('orders')
            ->join('orderdetails', 'orderdetails.orderId', 'orders.id')
            ->where('orders.rowPointer', $id)->get();
        $confirm = DB::table('orders')
            ->join('confirmations', 'confirmations.orderId', 'orders.id')
            ->where('orders.rowPointer', $id)
            ->select('tujuan', 'bank', 'akun', 'nama', 'jumlah')
            ->get();
        $order = Order::where('rowPointer', $id)->first();
        return view('admin.pages.preorder.show')->with([
            'order' => $order,
            'detail' => $detail,
            'confirm' => $confirm,
            'provinces' => $provinces,
            'citys' => $city,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Order::where('rowPointer', $id)->first();

        $order = DB::table('orderdetails')->where('orderId', $data->id)->get();

        return view('admin.pages.preorder.edit', [
            'data' => $data,
            'order' => $order
        ]);
    }

    public function add()
    {
        $products = Product::where('isObsolete', 0)->leftjoin('categories','category','categories.id')->select('products.*','categories.name as categoryName')->get();
        return view('admin.pages.preorder.create')->with('products', $products);
    }

    public function groupJson($id)
    {
        $data = Group::where('productId', $id)->with('detail')->first();

        return response()->json([
            'data' => $data
        ]);
    }
    
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = DB::table('orderdetails')->where('id', $id);

        $datas = DB::table('orderdetails')->where('id', $id)->first();



        $data->update([
            'quantity' => $request->quantity
        ]);

        $order = Order::where('rowPointer', $request->orderPointer);

//        $order->update([
//            'total' => $request->allTotals
//        ]);

        return redirect()->route('preorder.edit', $request->orderPointer)->with(['status' => 'success', 'message' => 'Product order successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('orderdetails')->where('id', $id)->delete();

        return back()->with(['status' => 'success', 'message' => 'Product order successfully deleted']);
    }
}
