<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/admin';

    protected function authenticated(Request $request, $user)
    {
      if ($user->level == 3 || $user->level == 4) {
            $words = explode(" ", Auth::user()->displayName);
            $acronym = "";

            foreach ($words as $w) {
                $acronym .= $w[0];
            }

            Session::put('inisial', $acronym);

            return redirect('/account');
        } else {
            $words = explode(" ", Auth::user()->displayName);
            $acronym = "";

            foreach ($words as $w) {
                $acronym .= $w[0];
            }

            Session::put('inisial', $acronym);

            return redirect('/admin');
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function credentials(Request $request)
    {
        if(is_numeric($request->get('email'))){
            return ['phone'=>$request->get('email'),'password'=>$request->get('password')];
        }
        return $request->only($this->username(), 'password');
    }
}
