<?php

namespace App\Http\Controllers;

use App\Helpers\RajaOngkir;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Address;
use Ramsey\Uuid\Uuid;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provinces = RajaOngkir::getProvince();

        $city = RajaOngkir::getCity();

        return view('components.account.address.create', [
            'provinces' => $provinces,
            'citys' => $city
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'label' => ['required', 'string', 'max:100'],
            'type' => ['required', 'string', 'max:20'],
            'address' => ['required', 'string', 'max:255'],
            'province' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'kecamatan' => ['required', 'string', 'max:255'],
            'kelurahan' => ['required', 'string', 'max:255'],
            'kodepos' => ['required', 'string', 'max:10'],
        ]);

        DB::beginTransaction();

        try {

            $id = Address::max('id') + 1;

            if ($request->type == 'billing') {

                Address::insert([
                    'id' => $id,
                    'userId' => Auth::user()->id,
                    'label' => $request->label,
                    'type' => 'BILLING',
                    'name' => $request->name,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'province' => $request->province,
                    'city' => $request->city,
                    'kecamatan' => $request->kecamatan,
                    'kelurahan' => $request->kelurahan,
                    'kodepos' => $request->kodepos,
                    'dateCreated' => Carbon::now(),
                    'rowPointer' => Uuid::uuid4()->getHex(),
                    'isActive' => 1,
                    'isObsolete' => 0,
                ]);

                DB::commit();

                return redirect('address/create/'.$request->type)->with(['status' => 'success', 'message' => 'Address saved']);

            } elseif ($request->type == 'shopping') {

                Address::insert([
                    'id' => $id,
                    'userId' => Auth::user()->id,
                    'label' => $request->label,
                    'type' => 'DELIVERY',
                    'name' => $request->name,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'province' => $request->province,
                    'city' => $request->city,
                    'kecamatan' => $request->kecamatan,
                    'kelurahan' => $request->kelurahan,
                    'kodepos' => $request->kodepos,
                    'dateCreated' => Carbon::now(),
                    'rowPointer' => Uuid::uuid4()->getHex(),
                    'isActive' => 1,
                    'isObsolete' => 0,
                ]);

                DB::commit();

                return redirect('address/create/'.$request->type)->with(['status' => 'success', 'message' => 'Address saved']);

            } else {

                return back()->with(['status' => 'danger', 'message' => 'Ups, Something went wrong!']);

            }


        }catch (Exception $e) {

            DB::rollback();

            return redirect()->route('address.create')->with(['status' => 'danger', 'message' => 'Address failed to save'.$e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Address::where('rowPointer',$id)->first();

        $provinces = RajaOngkir::getProvince();

        $city = RajaOngkir::getCity();

        return view('components.account.address.show', [
            'address' => $data,
            'provinces' => $provinces,
            'citys' => $city
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($type, $id)
    {
        $data = Address::where('rowPointer',$id)->first();

        $provinces = RajaOngkir::getProvince();

        $city = RajaOngkir::getCity();

        return view('components.account.address.edit', [
            'address' => $data,
            'provinces' => $provinces,
            'citys' => $city
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'label' => ['required', 'string', 'max:100'],
            'type' => ['required', 'string', 'max:20'],
            'address' => ['required', 'string', 'max:255'],
            'province' => ['required', 'string', 'max:255'],
            'city' => ['required', 'string', 'max:255'],
            'kecamatan' => ['required', 'string', 'max:255'],
            'kelurahan' => ['required', 'string', 'max:255'],
            'kodepos' => ['required', 'string', 'max:10'],
        ]);

        DB::beginTransaction();

        try {

            $data = Address::where('rowPointer', $id);

            $data->update([
                'label' => $request->label,
                'address' => $request->address,
                'province' => $request->province,
                'city' => $request->city,
                'kecamatan' => $request->kecamatan,
                'kelurahan' => $request->kelurahan,
                'kodepos' => $request->kodepos,
                'dateCreated' => Carbon::now(),
            ]);

            DB::commit();

            return redirect('address/edit/'.$request->type. '/' .$id)->with(['status' => 'success', 'message' => 'Address saved']);


        }catch (Exception $e) {

            DB::rollback();

            return redirect()->route('address/edit/'.$request->type. '/' .$id)->with(['status' => 'danger', 'message' => 'Address failed to save'.$e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
