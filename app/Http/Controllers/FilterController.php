<?php

namespace App\Http\Controllers;

// use App\Helpers\CharozaApi;
use App\Models\Product;
use DB;
use Illuminate\Http\Request;

class FilterController extends Controller
{
    // public function filterPrice(Request $request)
    // {
    //     $categorys = DB::table('categories')->get();
    //     $subcategories = DB::table('sub_categories')->get();

    //     if ($request->minamount == null && $request->maxamount == null && $request->kode == null && $request->sub == null) {
    //         $data = Product::where('isActive', 1);
    //        // dd('anjay1');
    //     } elseif ($request->minamount != null && $request->kode != null && $request->sub == null) {
    //         $min = (int) $request->minamount;
    //         $max = (int) $request->maxamount;
    //         $data = Product::where('isActive', 1)->where('category', $request->kode)
    //             ->where('price', '>=', $min)
    //             ->where('price', '<=', $max);
    //     } elseif ($request->maxamount != null && $request->kode == null && $request->sub != null) {
    //        // dd('anjay3');
    //         $min = (int) $request->minamount;
    //         $max = (int) $request->maxamount;
    //         $data = Product::where('isActive', 1)->where('subcategory', $request->sub)
    //             ->where('price', '>=', $min)
    //             ->where('price', '<=', $max);
    //     } elseif ($request->maxamount != null && $request->kode != null && $request->sub != null) {
    //        // dd('anjay4');
    //         $min = (int) $request->minamount;
    //         $max = (int) $request->maxamount;
    //         $data = Product::where('isActive', 1)->where('subcategory', $request->sub)->orWhere('category', $request->kode)
    //             ->where('price', '>=', $min)
    //             ->where('price', '<=', $max);
    //     } elseif ($request->kode != null && $request->sub == null && $request->minamount == null && $request->maxamount == null) {
    //        // dd('anjay5');
    //         $data = Product::where('isActive', 1)->where('category', $request->kode);
    //     } elseif ($request->kode == null && $request->sub != null && $request->minamount == null && $request->maxamount == null) {
    //        // dd('anjay6');
    //         $data = Product::where('isActive', 1)->where('subcategory', $request->sub);
    //     } elseif ($request->kode != null && $request->sub != null && $request->minamount == null && $request->maxamount == null) {
    //        // dd('anjay7');
    //         $data = Product::where('isActive', 1)->where('category', $request->kode)->orWhere('subcategory', $request->sub);
    //     } else {
    //        // dd('anjay8');
    //         $min = (int) $request->minamount;
    //         $max = (int) $request->maxamount;
    //         $data = Product::where('isActive', 1)
    //             ->where('price', '>=', $min)
    //             ->where('price', '<=', $max)
    //         ;
    //     }

    //     // dd($data);
    //     $result = $data->paginate(20);
    //     return view('pages.shop.index', [
    //         'products' => $result,
    //         'categories' => $categorys,
    //         'subcategories' => $subcategories,
    //     ]);
    // }

    public function filterPrice(Request $request, $kode, $sub)
    {
        $categorys = DB::table('categories')->get();
        $subcategories = DB::table('sub_categories')->get();

        if ($request->minamount == null && $request->maxamount == null && $kode == 'kosong' && $sub == 'kosong') {
            $data = Product::where('isActive', 1);
            // isi min max default //
            $min = 0;
            $max = 1000000;
            // isi min max default //
            // dd('anjay1');
        } elseif ($request->minamount != null && $kode != 'kosong' && $sub == 'kosong') {
            // dd('anjay2');
            $min = (int) $request->minamount;
            $max = (int) $request->maxamount;
            $data = Product::where('isActive', 1)->whereBetween('price', [$min, $max])->where('category', $kode)
            // ->where('price', '>=', $min)
            // ->where('price', '<=', $max)
            ;
        } elseif ($request->maxamount != null && $kode == 'kosong' && $sub != 'kosong') {
            //dd('anjay3');
            $min = (int) $request->minamount;
            $max = (int) $request->maxamount;
            $data = Product::where('isActive', 1)->whereBetween('price', [$min, $max])->where('subcategory', $sub);
            // ->where('price', '>=', $min)
            // ->where('price', '<=', $max);
        } elseif ($request->maxamount != null && $kode != 'kosong' && $sub != 'kosong') {
            // dd('anjay4');
            $min = (int) $request->minamount;
            $max = (int) $request->maxamount;
            $data = Product::where('isActive', 1)
                ->where('subcategory', $sub)->Where('category', $kode)
            //->whereBetween('price', $range)
                ->where('price', '>=', $min)
                ->where('price', '<=', $max)
            ;

            //dd('min = ' . $min . ' max = ' . $max);
        } elseif ($kode != 'kosong' && $sub == 'kosong' && $request->minamount == null && $request->maxamount == null) {
            //dd('anjay5');
            $data = Product::where('isActive', 1)->where('category', $kode);
            // isi min max default //
            $min = 0;
            $max = 1000000;
            // isi min max default //
        } elseif ($kode == 'kosong' && $sub != 'kosong' && $request->minamount == null && $request->maxamount == null) {
            //dd('anjay6');
            $data = Product::where('isActive', 1)->where('subcategory', $sub);
            // isi min max default //
            $min = 0;
            $max = 1000000;
            // isi min max default //
        } elseif ($kode != 'kosong' && $sub != 'kosong' && $request->minamount == null && $request->maxamount == null) {
            // dd('anjay7');
            $data = Product::where('isActive', 1)->where('category', $kode)->orWhere('subcategory', $sub);
            // isi min max default //
            $min = 0;
            $max = 1000000;
            // isi min max default //
        } else {
            // dd('anjay8');
            $min = (int) $request->minamount;
            $max = (int) $request->maxamount;
            $data = Product::where('isActive', 1)->whereBetween('price', [$min, $max])
            // ->where('price', '>=', $min)
            // ->where('price', '<=', $max)
            ;
        }

        // dd($data);

        $result = $data->paginate(20);
        return view('pages.shop.index', [
            'kategori' => $kode,
            'subkategori' => $sub,
            'min' => $min,
            'max' => $max,
            'products' => $result,
            'categories' => $categorys,
            'subcategories' => $subcategories,
        ]);
    }

    public function filterSearch(Request $request)
    {

        $categorys = DB::table('categories')->get();
        $subcategories = DB::table('sub_categories')->get();

        $cari = $request->q;
        $data = Product::where('name', 'like', '%' . $cari . '%')
            ->paginate(20);

        return view('pages.shop.index', [
            'products' => $data,
            'categories' => $categorys,
            'subcategories' => $subcategories,
        ]);
    }

    public function filterCategory($id)
    {
        $categorys = DB::table('categories')->get();
        $subcategories = DB::table('sub_categories')->get();

        $data = Product::where('category', $id)
            ->paginate(20);

        return view('pages.shop.index', [
            'products' => $data,
            'categories' => $categorys,
            'subcategories' => $subcategories,
        ]);
    }
    public function filterProduct(Request $request)
    {
        $categorys = DB::table('categories')->get();
        $subcategories = DB::table('sub_categories')->get();
        $data = json_decode($request->data);
        return view('pages.shop.index', [
            'products' => $data,
            'categories' => $categorys,
            'subcategories' => $subcategories,
        ]);
    }
}
