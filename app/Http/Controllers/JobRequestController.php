<?php

namespace App\Http\Controllers;

use App\Helpers\CharozaLog;
use App\Models\JobRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;

class JobRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = JobRequest::join('users','users.id','user_id')
            ->get();

        return view('admin.pages.request.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {

            $id = JobRequest::max('id') + 1;

            $validate = JobRequest::where('user_id',Auth::user()->id)->count();
            if ($validate == 0) {
                JobRequest::insert([
                    'id' => $id,
                    'user_id' => Auth::user()->id,
                    'job' => $request->job,
                    'created_at' => Carbon::now()
                ]);
            } else {
                return back()->with(['status' => 'danger', 'message' => 'Anda sudah melakukan pengajuan, harap menuggu konfirmasi']);
            }

            DB::commit();

            return back()->with(['status' => 'success', 'message' => 'Pengajuan reseller berhasil, menunggu konfirmasi']);
        } catch (Exception $e) {

            DB::rollback();

            return back()->with(['status' => 'danger', 'message' => 'Request failed to save' . $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JobRequest  $jobRequest
     * @return \Illuminate\Http\Response
     */
    public function show(JobRequest $jobRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobRequest  $jobRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(JobRequest $jobRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobRequest  $jobRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobRequest $jobRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobRequest  $jobRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobRequest $jobRequest)
    {
        //
    }
}
