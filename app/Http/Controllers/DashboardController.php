<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Helpers\RajaOngkir;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::get();
        $news = Order::where('status','NEW')->get();
        $cancels = Order::where('status','CANCEL')->get();
        $sends = Order::where('status','PACKING')->get();
        
        //dd($sends);

        return view('admin.pages.dashboard', [
            'news' => $news,
            'cancels' => $cancels,
            'sends' => $sends
        ]);
    }
    public function print()
    {
        $provinces = RajaOngkir::getProvince();
        $city = RajaOngkir::getCity();
        $orders = Order::get();
        $news = Order::where('status','NEW')->get();
        $cancels = Order::where('status','CANCEL')->get();
        $sends = Order::where('status','PACKING')->get();

        return view('admin.pages.printDashboard', [
            'news' => $news,
            'cancels' => $cancels,
            'provinces' => $provinces,
            'sends' => $sends,
            'citys' => $city
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
