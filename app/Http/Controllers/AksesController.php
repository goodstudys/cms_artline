<?php

namespace App\Http\Controllers;

use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class AksesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::where('level','!=', 3)->where('level','!=',4)->get();

        return view('admin.pages.hakakses.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function webindex()
    {
        $data = DB::table('setting')->first();
        return view('admin.pages.websetting.index')->with(['data' => $data]);
    }
    public function webDown()
    {
        Artisan::call('down');
        DB::table('setting')->where('active',0)->orWhere('active',1)->update(['active'=>0]);
        return back();
    }
    public function webUp()
    {
        Artisan::call('up');
        DB::table('setting')->where('active',0)->orWhere('active',1)->update(['active'=>1]);
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        DB::beginTransaction();

        try {

            $data = User::where('rowPointer', $id);

            $data->update([
                'level' => $request->level,
            ]);

            DB::commit();

            return redirect()->route('admin.hakakses')->with(['status' => 'success', 'message' => 'User level successfuly change']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.hakakses')->with(['status' => 'success', 'message' => 'Failed']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
