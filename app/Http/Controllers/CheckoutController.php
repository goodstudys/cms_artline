<?php

namespace App\Http\Controllers;

use App\Helpers\RajaOngkir;
use App\Models\Address;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $datas = Cart::getContent();
        if (count($datas) != 0) {
            $totalberat = 0;
            $totalss = 0;
            $zero = 0;
            foreach ($datas as $value) {

                $perweight = $value->attributes->weight * $value->quantity;
                $allweight = $totalberat += $perweight;
                $prodprice = $value->price * $value->quantity;
                $gettotal = $zero += $prodprice;
            }
            Session::put('getTotal',  $gettotal);
            Session::put('weightRow', $allweight);
            Session::put('qtys', $totalss += $value->quantity);
        }

        $provinces = RajaOngkir::getProvince();

        $city = RajaOngkir::getCity();

        $address = Address::where('userId', Auth::user()->id)->get();

        if ($address->count() != 0) {
            if ($request->label == null) {
                $requestAddr = Address::where('userId', Auth::user()->id)->first();

                $ongkir = RajaOngkir::getCost(444, $requestAddr->city, Session::get('weightRow'), "jne");

                $option = '2';

                if ($ongkir['rajaongkir']['status']['code'] == 200) {

                    $result = $ongkir['rajaongkir']['results'];

                    $value = $result['0']['costs']['0']['cost']['0']['value'];
                    $estimasi = $result['0']['costs']['0']['cost']['0']['etd'];

                    Session::put('ongkir', $value);
                    Session::put('ongkir-etd', $estimasi);

                } else {
                    Session::put('ongkir', "Cannot calculate shipping, please check your input");
                    Session::put('ongkir-etd', "Cannot calculate shipping, please check your input");
                }
            } elseif ($request->label == "Manual") {
                $requestAddr = null;

                $option = '2';

                Session::forget('ongkir');
                Session::forget('ongkir-etd');
            } else {
                $requestAddr = Address::where('rowPointer', $request->label)->first();
                //dd($requestAddr);
                $ongkir = RajaOngkir::getCost(444, $requestAddr->city, Session::get('weightRow'), "jne");

                $option = '1';

                if ($ongkir['rajaongkir']['status']['code'] == 200) {

                    $result = $ongkir['rajaongkir']['results'];

                    $value = $result['0']['costs']['0']['cost']['0']['value'];
                    $estimasi = $result['0']['costs']['0']['cost']['0']['etd'];

                    Session::put('ongkir', $value);
                    Session::put('ongkir-etd', $estimasi);

                } else {
                    Session::put('ongkir', "Cannot calculate shipping, please check your input");
                    Session::put('ongkir-etd', "Cannot calculate shipping, please check your input");
                }
            }
        } else {
            $requestAddr = null;

            $option = '2';
        }

        return view('pages.checkout', [
            'datas' => $datas,
            'provinces' => $provinces,
            'citys' => $city,
            'addresses' => $address,
            'reqaddr' => $requestAddr,
            'option' => $option,
        ]);
    }

    public function calculateOngkir(Request $request)
    {
        $curl = curl_init();
        $from = '444';
        $to = $request->billCity;
        $weight = Session::get('weightRow');
        $courier = 'jne';
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "origin=" . $from . "&destination=" . $to . "&weight=" . $weight . "&courier=" . $courier . "",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "Key: aac33b3e10e26c386501903a3ff9bdb5",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

//            echo "cURL Error #:" . $err;
            $result = "Connection problem";

        } else {

            $result = json_decode($response, true);
            $results = $result['rajaongkir']['results'];
            $value = $results['0']['costs']['0']['cost']['0']['value'];
            $estimasi = $results['0']['costs']['0']['cost']['0']['etd'];

            Session::put('ongkir', $value);
            Session::put('ongkir-etd', $estimasi);
            $total = Session::get('getTotal') + $value;
            $subtotal = Session::get('getTotal');
        }

        return response()->json([
            'ongkir' => number_format($value),
            'ongkirraw' => $value,
            'ongkir-etd' => $estimasi,
            'total' => number_format($total),
            'totalraw' => $total,
            'subtotal' => $subtotal,
        ]);

        // $ongkir = RajaOngkir::getCost(444, $request->billCity, Session::get('weightRow'), "jne");

        // $option = '1';

        // if ($ongkir['rajaongkir']['status']['code'] == 200) {

        //     $result = $ongkir['rajaongkir']['results'];

        // $value = $result['0']['costs']['0']['cost']['0']['value'];
        // $estimasi = $result['0']['costs']['0']['cost']['0']['etd'];

        // Session::put('ongkir', $value);
        // Session::put('ongkir-etd', $estimasi);
        // $total = Session::get('getTotal') + $value;
        // $subtotal = Session::get('getTotal');

        // } else {
        //     Session::put('ongkir', "Cannot calculate shipping, please check your input");
        //     Session::put('ongkir-etd', "Cannot calculate shipping, please check your input");
        // }

        // return response()->json([
        //     'ongkir' => number_format($value),
        //     'ongkirraw' => $value,
        //     'ongkir-etd' => $estimasi,
        //     'total' => number_format($total),
        //     'totalraw' => $total,
        //     'subtotal' => $subtotal,
        //     ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    public function calculateOngkirDeliv(Request $request)
    {
        $curl = curl_init();
        $from = '444';
        $to = $request->billCity;
        $weight = Session::get('weightRow');
        $courier = 'jne';
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://api.rajaongkir.com/starter/cost",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "origin=" . $from . "&destination=" . $to . "&weight=" . $weight . "&courier=" . $courier . "",
            CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "Key: aac33b3e10e26c386501903a3ff9bdb5",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

//            echo "cURL Error #:" . $err;
            $result = "Connection problem";

        } else {

            $result = json_decode($response, true);
            $results = $result['rajaongkir']['results'];
            $value = $results['0']['costs']['0']['cost']['0']['value'];
            $estimasi = $results['0']['costs']['0']['cost']['0']['etd'];

            Session::put('ongkirDeliv', $value);
            Session::put('ongkir-etdDeliv', $estimasi);
            $total = Session::get('getTotal') + $value;
            $subtotal = Session::get('getTotal');
        }

        return response()->json([
            'ongkir' => number_format($value),
            'ongkirraw' => $value,
            'ongkir-etd' => $estimasi,
            'total' => number_format($total),
            'totalraw' => $total,
            'subtotal' => $subtotal,
        ]);

        // $ongkir = RajaOngkir::getCost(444, $request->billCity, Session::get('weightRow'), "jne");

        // $option = '1';

        // if ($ongkir['rajaongkir']['status']['code'] == 200) {

        //     $result = $ongkir['rajaongkir']['results'];

        // $value = $result['0']['costs']['0']['cost']['0']['value'];
        // $estimasi = $result['0']['costs']['0']['cost']['0']['etd'];

        // Session::put('ongkir', $value);
        // Session::put('ongkir-etd', $estimasi);
        // $total = Session::get('getTotal') + $value;
        // $subtotal = Session::get('getTotal');

        // } else {
        //     Session::put('ongkir', "Cannot calculate shipping, please check your input");
        //     Session::put('ongkir-etd', "Cannot calculate shipping, please check your input");
        // }

        // return response()->json([
        //     'ongkir' => number_format($value),
        //     'ongkirraw' => $value,
        //     'ongkir-etd' => $estimasi,
        //     'total' => number_format($total),
        //     'totalraw' => $total,
        //     'subtotal' => $subtotal,
        //     ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }
public function setSession(Request $request){
    Session::put('kirimdropship',  $request->name);
    if ($request->name == "on") {
        Session::put('kirimdropship',  $request->name);
    } else {
        Session::forget('ongkirDeliv');
        Session::forget('ongkir-etdDeliv');
    }
    
    return response()->json([
        'isi' => $request->name,
    ]);
}
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
