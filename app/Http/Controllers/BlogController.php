<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use DB;
use App\Models\Blog;
use App\Helpers\CharozaLog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::where('isObsolete',0)->get();

        return view('admin.pages.blog.index')->with('blogs', $blogs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'category' => ['required', 'string', 'max:255'],
            'subtitle' => ['required', 'string', 'max:255'],
            'article' => ['required', 'string', 'max:4000'],
            'image' => ['required', 'image', 'max:2048'],
            'video' => ['max:5048'],
        ]);

        DB::beginTransaction();

        try {

            $video =  $request->video;

            if ($video != null) {
                $ext = $video->getMimeType();

                if ($ext != 'video/mp4') {
                    return redirect()->route('blog.create')->with(['status' => 'danger', 'message' => 'Video must be .mp4']);
                }

                $videoName = time() . '.' . request()->video->getClientOriginalExtension();

                request()->video->move(public_path() . '/upload/users/blogs/video', $videoName);
            } else {
                $videoName = null;
            }


            $imageName = time() . '.' . request()->image->getClientOriginalExtension();

            request()->image->move(public_path() . '/upload/users/blogs/', $imageName);

            $id = Blog::max('id') + 1;

            Blog::insert([
                'id' => $id,
                'userId' => Auth::user()->id,
                'category' => $request->category,
                'title' => $request->title,
                'subtitle' => $request->subtitle,
                'article' => $request->article,
                'image' => $imageName,
                'video' => $videoName,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 1,
                'isObsolete' => 0,
            ]);
            CharozaLog::add('blog', $request->title, 'created', 'Create Blog');
            DB::commit();

            return redirect()->route('blog.create')->with(['status' => 'success', 'message' => 'Blog saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('blog.create')->with(['status' => 'danger', 'message' => 'Blog failed to save' . $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Blog::where('rowPointer', $id)->first();

        return view('admin.pages.blog.show', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Blog::where('rowPointer', $id)->first();

        return view('admin.pages.blog.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'category' => ['required', 'string', 'max:255'],
            'subtitle' => ['required', 'string', 'max:255'],
            'article' => ['required', 'string', 'max:4000'],
            'image' => [ 'image', 'max:2048'],
            'video' => ['max:5048'],
        ]);

        DB::beginTransaction();

        try {

            $data = Blog::where('rowPointer', $id)->first();

            $video =  $request->video;

            if ($video != null) {
                $ext = $video->getMimeType();

                if ($ext != 'video/mp4') {
                    return redirect()->route('blog.edit', $id)->with(['status' => 'danger', 'message' => 'Video must be .mp4']);
                }

                $videoName = time() . '.' . request()->video->getClientOriginalExtension();

                request()->video->move(public_path() . '/upload/users/blogs/video', $videoName);
            } else {
                $videoName = $data->video;
            }

            $images = $request->image;
            if ($images != null) {

                $imageName = time() . '.' . request()->image->getClientOriginalExtension();

                request()->image->move(public_path() . '/upload/users/blogs/', $imageName);
            } else {
                $imageName = $data->image;
            }


            $data = Blog::where('rowPointer', $id);

            $data->update([
                'category' => $request->category,
                'title' => $request->title,
                'subtitle' => $request->subtitle,
                'article' => $request->article,
                'image' => $imageName,
                'video' => $videoName,
                'dateCreated' => Carbon::now(),
                'isActive' => $request->isActive
            ]);
            CharozaLog::add('blog', $request->title, 'edit', 'Edit Blog');
            DB::commit();

            return redirect()->route('blog.edit', $id)->with(['status' => 'success', 'message' => 'Blog saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('blog.edit', $id)->with(['status' => 'danger', 'message' => 'Blog failed to save' . $e]);
        }
    }

    public function statusActive($id)
    {
        DB::beginTransaction();

        try {

            $data = Blog::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0
                ]);
            } else {
                $data->update([
                    'isActive' => 1
                ]);
            }
            CharozaLog::add('blog', $datas->title, 'update', 'Change Status Active Blog');
            DB::commit();

            return redirect()->route('admin.blog')->with(['status' => 'success', 'message' => 'Success update blog']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.blog')->with(['status' => 'danger', 'message' => 'Failed update blog' . $e]);
        }
    }

    public function statusDelete($id)
    {
        DB::beginTransaction();

        try {


            $data = Blog::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isObsolete == 0) {
                $data->update([
                    'isObsolete' => 1
                ]);
            } else {
                $data->update([
                    'isObsolete' => 1
                ]);
            }
            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0
                ]);
            } else {
                $data->update([
                    'isActive' => 0
                ]);
            }
            CharozaLog::add('blog', $datas->title, 'update', 'Change Status Obsulete Blog');
            DB::commit();

            return redirect()->route('admin.blog')->with(['status' => 'success', 'message' => 'Success delete Artikel']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.blog')->with(['status' => 'danger', 'message' => 'Artikel failed to delete' . $e]);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $data = Blog::where('id',$id)->first();
            $model = Blog::find($id);
            $model->delete();
            CharozaLog::add('blog', $data->title, 'delete', 'Delete Blog');
            return redirect()->route('admin.blog')->with(['status' => 'success', 'message' => 'Success delete Artikel']);

        }catch (Exception $e){
            DB::rollback();

            return redirect()->route('admin.blog', $id)->with(['status' => 'danger', 'message' => 'Artikel failed to delete' . $e]);
        }
    }
}
