<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use DB;
use App\Helpers\CharozaLog;
use Illuminate\Support\Facades\File;

class SliderCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('slider_categories')->where('isObsolete', 0)->join('categories','categories.id','slider_categories.category')->select('slider_categories.*','categories.name')->get();

        return view('admin.pages.slider_categories.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = DB::table('categories')->get();

        return view('admin.pages.slider_categories.create', [
            'categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        DB::beginTransaction();

        try {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();

            request()->image->move(public_path() . '/upload/users/slider_category/', $imageName);

            $id = DB::table('slider_categories')->max('id') + 1;

            DB::table('slider_categories')->insert([
                'id' => $id,
                'category' => $request->category,
                'image' => $imageName,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 1,
                'isObsolete' => 0,
            ]);
            CharozaLog::add('slider_category', $request->category, 'created', 'Create slider_category');
            DB::commit();

            return redirect()->route('sliderCat.create')->with(['status' => 'success', 'message' => 'slider category saved']);

        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('sliderCat.create')->with(['status' => 'danger', 'message' => 'slider category failed to save' . $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = DB::table('categories')->get();
        $data = DB::table('slider_categories')->where('rowPointer', $id)->first();
        return view('admin.pages.slider_categories.edit', [
            'categories' => $categories, 'data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        try {

            $slider = DB::table('slider_categories')->where('rowPointer', $id);
            $data = $slider->first();

            $images1 = $request->image;

            if ($images1 != null) {
                $file_path1 = app_path("/upload/users/slider_category/" . $images1);
                if (File::exists($file_path1)) {
                    File::delete($file_path1);
                }
                $imageName1 = time() . '.' . request()->image->getClientOriginalExtension();

                request()->image->move(public_path() . '/upload/users/slider_category/', $imageName1);
            } else {
                $imageName1 = $data->image;
            }

            $slider->update([
                'category' => $request->category,
                'image' => $imageName1,
                'dateCreated' => Carbon::now(),
                'isActive' => $request->isActive,

            ]);
            CharozaLog::add('slider_categories', $request->category, 'edit', 'Edit slider_categories');
            DB::commit();

            return redirect()->route('sliderCat.edit', $id)->with(['status' => 'success', 'message' => 'slider_categories saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('sliderCat.edit' . $id)->with(['status' => 'danger', 'message' => 'slider_categories failed to save' . $e]);
        }
    }
    public function statusActive($id)
    {
        DB::beginTransaction();

        try {

            $data = DB::table('slider_categories')->where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0,
                ]);
            } else {
                $data->update([
                    'isActive' => 1,
                ]);
            }
            CharozaLog::add('slider category', $datas->category, 'update', 'Change Status Active slider category');
            DB::commit();

            return redirect()->route('admin.sliderCat')->with(['status' => 'success', 'message' => 'Success update slider category']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.sliderCat')->with(['status' => 'danger', 'message' => 'Failed update slider category' . $e]);
        }
    }
    public function statusDelete($id)
    {
        DB::beginTransaction();

        try {

            $data = DB::table('slider_categories')->where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isObsolete == 0) {
                $data->update([
                    'isObsolete' => 1,
                ]);
            } else {
                $data->update([
                    'isObsolete' => 1,
                ]);
            }
            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0,
                ]);
            } else {
                $data->update([
                    'isActive' => 0,
                ]);
            }
            CharozaLog::add('slider category', $datas->title, 'update', 'Change Status Obsulete slider category');
            DB::commit();

            return redirect()->route('admin.sliderCat')->with(['status' => 'success', 'message' => 'Success delete slider category']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.sliderCat')->with(['status' => 'danger', 'message' => 'Failed delete slider category' . $e]);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
