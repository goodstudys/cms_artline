<?php

namespace App\Http\Controllers;

use App\Models\Promotion;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use DB;
use App\Helpers\CharozaLog;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotions = Promotion::where('isObsolete',0)->get();

        return view('admin.pages.promotion.index')->with('promotions', $promotions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.promotion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'link' => ['required', 'string', 'max:255'],
            'image' => ['required', 'image', 'max:2048'],
        ]);

        DB::beginTransaction();

        try {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();

            request()->image->move(public_path() . '/upload/users/promotions', $imageName);

            $id = Promotion::max('id') + 1;

            Promotion::insert([
                'id' => $id,
                'title' => $request->title,
                'link' => $request->link,
                'image' => $imageName,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 1,
                'isObsolete' => 0,
            ]);
            CharozaLog::add('promotion', $request->title, 'created', 'Create Promotion');
            DB::commit();

            return redirect()->route('promotion.create')->with(['status' => 'success', 'message' => 'Promotion saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('promotion.create')->with(['status' => 'danger', 'message' => 'Promotion failed to save' . $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Promotion::where('rowPointer', $id)->first();

        return view('admin.pages.promotion.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => ['required', 'string', 'max:255'],
            'link' => ['required', 'string', 'max:255'],

        ]);

        DB::beginTransaction();

        try {
            $data = Promotion::where('rowPointer', $id);

            $imageDb = Promotion::where('rowPointer', $id)->first();

            $images1 = $request->image;

            if ($images1 != null) {

                $imageName1 = time() . '.' . request()->image->getClientOriginalExtension();

                request()->image->move(public_path() . '/upload/users/promotions', $imageName1);
            } else {
                $imageName1 = $imageDb->image;
            }


            $data->update([
                'title' => $request->title,
                'link' => $request->link,
                'image' => $imageName1,
                'dateCreated' => Carbon::now(),
                'isActive' => $request->isActive,

            ]);
            CharozaLog::add('promotion', $request->title, 'edit', 'Edit Promotion');
            DB::commit();

            return redirect()->route('promotion.edit', $id)->with(['status' => 'success', 'message' => 'Promotion saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('promotion.edit', $id)->with(['status' => 'danger', 'message' => 'Promotion failed to save' . $e]);
        }
    }

    public function statusActive($id)
    {
        DB::beginTransaction();

        try {


            $data = Promotion::where('rowPointer', $id);

            $datas = $data->first();


            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0
                ]);
            } else {
                $data->update([
                    'isActive' => 1
                ]);
            }
            CharozaLog::add('promotion', $datas->title, 'update', 'Change Status Active Promotion');
            DB::commit();

            return redirect()->route('admin.promotion')->with(['status' => 'success', 'message' => 'Success update carousel']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.promotion')->with(['status' => 'danger', 'message' => 'Failed update carousel' . $e]);
        }
    }
    public function statusDelete($id)
    {
        DB::beginTransaction();

        try {


            $data = Promotion::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isObsolete == 0) {
                $data->update([
                    'isObsolete' => 1
                ]);
            } else {
                $data->update([
                    'isObsolete' => 1
                ]);
            }
            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0
                ]);
            } else {
                $data->update([
                    'isActive' => 0
                ]);
            }
            CharozaLog::add('promotion', $datas->title, 'update', 'Change Status Obsulete Promotion');
            DB::commit();

            return redirect()->route('admin.promotion')->with(['status' => 'success', 'message' => 'Success delete carousel']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.promotion')->with(['status' => 'danger', 'message' => 'Failed delete carousel' . $e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{

            $data = Promotion::where('rowPointer', $id);

            $datas = $data->first();

            // if ($datas->isActive == 1) {
            //     $data->update([
            //         'isActive' => 0
            //     ]);
            // } else {
            //     $data->update([
            //         'isActive' => 1
            //     ]);
            // }
            $model = Promotion::find($id);
            $model->delete();
            CharozaLog::add('promotion', $datas->title, 'delete', 'Delete Promotion');
            return redirect()->route('admin.promotion')->with(['status' => 'success', 'message' => 'Success delete promotion']);

        }catch (Exception $e){
            DB::rollback();

            return redirect()->route('admin.promotion', $id)->with(['status' => 'danger', 'message' => 'promotion failed to delete' . $e]);
        }
    }
}
