<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Promotion;

class PromotionController extends Controller
{
    public function getPromotion()
    {
        $data = Promotion::where('isActive', 1)->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get Promotion',
            'method' => 'GET',
            'data' => $data,
        ]);
    }
}
