<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Promotion;
use App\Models\Slider;
use DB;
use App\Models\Product;

class SliderController extends Controller
{
    public function getSlider()
    {
        $data = Slider::where('isActive', 1)->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get Slider',
            'method' => 'GET',
            'data' => $data,
        ]);
    }
    public function getHome()
    {
        $slider = Slider::where('isActive', 1)->get();
        $promotion = Promotion::where('isActive', 1)->get();
        $categories = DB::table('categories')->get();
        $recomProducts = Product::where('isActive', 1)->where('flag', 'HOTDEAL')->inRandomOrder()->limit(6)->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get Slider',
            'method' => 'GET',
            'slider' => $slider,
            'promotion' => $promotion,
            'categories' => $categories,
            'recomProducts' => $recomProducts,
        ]);
    }
}
