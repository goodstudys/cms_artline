<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid;

class RegisterController extends Controller
{
    public function register(Request $request) {

        DB::beginTransaction();

        try {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();

            request()->image->move(base_path() . '/public/upload/user/', $imageName);

            $id = DB::table('users')->max('id') + 1;

            DB::table('users')->insert([
                'id' => $id,
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'displayName' => $request->displayName,
                'company' => $request->company,
                'phone' => $request->phone,
                'email' => $request->email,
                'image' => $imageName,
                'level' => '3',
                'isActive' => '1',
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'password' => Hash::make($request->password),
            ]);

            DB::commit();

            return response()->json([
               'status' => 'success',
               'message' => 'Registration successful',
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'Registration failed '.$e,
            ]);
        }

    }
}
