<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Group;
use App\Models\Product;

class ProductController extends Controller
{
    public function getRecom()
    {
        $recomProducts = Product::where('isActive', 1)->where('flag', 'HOTDEAL')->inRandomOrder()->limit(6)->get();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get recomProducts',
            'method' => 'GET',
            'data' => $recomProducts,
        ]);
    }
    public function getDetailProduct($id)
    {
        try {

            $data = Product::where('rowPointer', $id)->with('review')->first();
            $group = Group::where('productId', $id)
                ->join('groupsdetails', 'groupId', 'groups.id')
                ->select('productId', 'groups.id as groupId1', 'groupsdetails.id as detailId', 'groupId as groupDetail', 'name', 'price', 'discAmount', 'discPercent', 'groupsdetails.rowPointer as Pointer')
                ->get();
            $products = Product::where('isActive', 1)->where('flag', 'HOTDEAL')->inRandomOrder()->limit(4)->get();
            return response()->json([
                'status' => 'success',
                'method' => 'GET',
                'message' => 'Successfully get debt data',
                'data' => $data,
                'products' => $products,
                'group' => $group,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'error ' . $e,
            ]);

        }
    }
    public function groupSelected(Request $request, $id)
    {

        try {
            $groupselect = Group::where('productId', $id)
                ->join('groupsdetails', 'groupId', 'groups.id')
                ->where('groupsdetails.rowPointer', $request->selectedgroup)
                ->select('productId', 'groups.id as groupId1', 'groupsdetails.id as detailId', 'groupId as groupDetail', 'name', 'price', 'discAmount', 'discPercent', 'groupsdetails.rowPointer as Pointer')
                ->first();

            return response()->json([
                'status' => 'success',
                'method' => 'GET',
                'message' => 'Successfully get debt data',
                'groupselect' => $groupselect,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'error ' . $e,
            ]);

        }

    }
    public function search(Request $request)
	{
        try {
		$cari = $request->cari;
 
		$products = Product::where('name','like',"%".$cari."%")
		->get();

            return response()->json([
                'status' => 'success',
                'method' => 'GET',
                'message' => 'Successfully get data',
                'products' => $products,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'error ' . $e,
            ]);

        }
 
	}
}
