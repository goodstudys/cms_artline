<?php

namespace App\Http\Controllers\Api;

use App\Helpers\CharozaLog;
use App\Helpers\RajaOngkir;
use App\Http\Controllers\Controller;
use App\Models\Confirmation;
use App\Models\Order;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use Session;

class OrderController extends Controller
{
    public function formDelivery()
    {

        try {

            $provinces = RajaOngkir::getProvince();
            $city = RajaOngkir::getCity();
            return response()->json([
                'status' => 'success',
                'method' => 'GET',
                'message' => 'Successfully get debt data',
                'city' => $city,
                'provinces' => $provinces,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'error ' . $e,
            ]);

        }
    }
    public function getService(Request $request)
    {
        $tujuan = $request->tujuan;
        $berat = $request->berat;
        $kurir = $request->kurir;
        try {

            $data = RajaOngkir::getCost(444, $tujuan, $berat, $kurir);
            $result = $data['rajaongkir']['results'];

            return response()->json([
                'status' => 'success',
                'method' => 'GET',
                'message' => 'Successfully get service data',
                'result' => $result,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'error ' . $e,
            ]);

        }
    }
    public function getCost(Request $request)
    {
        $tujuan = $request->tujuan;
        $berat = $request->berat;
        try {

            $data = RajaOngkir::getCost(444, $tujuan, $berat, "jne");
            $result = $data['rajaongkir']['results'];
            $value = $result['0']['costs']['0']['cost']['0']['value'];

            return response()->json([
                'status' => 'success',
                'method' => 'GET',
                'message' => 'Successfully get debt data',
                'value' => $value,
                'result' => $result,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'error ' . $e,
            ]);

        }
    }
    public function getLastOrder()
    {
        try {

            $id = Order::where('userId', Auth::user()->id)->max('id');

            $data = Order::where('id', $id)->with('detail')->first();

            return response()->json([
                'status' => 'success',
                'message' => 'Successfully get order',
                'method' => 'GET',
                'data' => $data,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'error ' . $e,
            ]);

        }
    }
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {

            $id = (Order::max('id') + 1);

            $total = $request->total;

            $paid = $request->paid;

            if ($paid == $total) {
                $status = 'LUNAS';
            } elseif ($paid < $total) {
                $status = 'BELUM LUNAS';
            }

            $date = date('dmY', strtotime(Carbon::now()));

            $code = CharozaLog::code('orders', 'orderCode', 7, 'ATLN' . $date);
            if ($request->reseller == "on") {
                Order::insert([
                    'id' => $id,
                    'userId' => Auth::user()->id,
                    'orderCode' => $code,
                    'billName' => $request->billName,
                    'billCompany' => $request->billCompany,
                    'billProvince' => $request->billProvince,
                    'billCity' => $request->cityname,
                    'billAddress' => $request->billAddress,
                    'billKecamatan' => $request->billKecamatan,
                    'billKelurahan' => $request->billKelurahan,
                    'billPhone' => $request->billPhone,
                    'billKodePos' => $request->billKodePos,
                    'billEmail' => $request->billEmail,
                    'billNotes' => $request->billNotes,
                    'deliveryName' => $request->deliveryName,
                    'deliveryCompany' => $request->deliveryCompany,
                    'deliveryProvince' => $request->deliveryProvince,
                    'deliveryCity' => $request->deliveryCity,
                    'deliveryAddress' => $request->deliveryAddress,
                    'deliveryKecamatan' => $request->deliveryKecamatan,
                    'deliveryKelurahan' => $request->deliveryKelurahan,
                    'deliveryPhone' => $request->deliveryPhone,
                    'deliveryKodePos' => $request->deliveryKodePos,
                    'kurirName' => $request->kurirName,
                    'kurirRate' => $request->ongkir,
                    'payment' => $request->payment,
                    'bookingCode' => $request->kodeBooking,
                    'status' => 'NEW',
                    'promocode' => Session::get('kode'),
                    'quantity' => $request->totalqtys,
                    'subtotal' => $request->gettotal,
                    'total' => $request->totalorders,
                    'type' => 'RESELLER',
                    'dateCreated' => Carbon::now(),
                    'rowPointer' => Uuid::uuid4()->getHex(),
                    'isObsolete' => 0,
                ]);
            } else {
                Order::insert([
                    'id' => $id,
                    'userId' => Auth::user()->id,
                    'orderCode' => $code,
                    'billName' => $request->billName,
                    'billCompany' => $request->billCompany,
                    'billProvince' => $request->billProvince,
                    'billCity' => $request->cityname,
                    'billAddress' => $request->billAddress,
                    'billKecamatan' => $request->billKecamatan,
                    'billKelurahan' => $request->billKelurahan,
                    'billPhone' => $request->billPhone,
                    'billKodePos' => $request->billKodePos,
                    'billEmail' => $request->billEmail,
                    'billNotes' => $request->billNotes,
                    'deliveryName' => $request->deliveryName,
                    'deliveryCompany' => $request->deliveryCompany,
                    'deliveryProvince' => $request->deliveryProvince,
                    'deliveryCity' => $request->deliveryCity,
                    'deliveryAddress' => $request->deliveryAddress,
                    'deliveryKecamatan' => $request->deliveryKecamatan,
                    'deliveryKelurahan' => $request->deliveryKelurahan,
                    'deliveryPhone' => $request->deliveryPhone,
                    'deliveryKodePos' => $request->deliveryKodePos,
                    'kurirName' => $request->kurirName,
                    'kurirRate' => $request->ongkir,
                    'payment' => $request->payment,
                    'bookingCode' => $request->kodeBooking,
                    'status' => 'NEW',
                    'promocode' => Session::get('kode'),
                    'quantity' => $request->totalqtys,
                    'subtotal' => $request->gettotal,
                    'total' => $request->totalorders,
                    'type' => 'DEFAULT',
                    'dateCreated' => Carbon::now(),
                    'rowPointer' => Uuid::uuid4()->getHex(),
                    'isObsolete' => 0,
                ]);
            }

            $pokoe = json_decode($request->product, true);
            $feeder = [];
            $ids = DB::table('orderdetails')->max('id') + 1;
            $product = $request->get('product');
            $qty = $request->get('qty');
            $total = $request->get('total');

            for ($i = 0; $i < count($pokoe); $i++) {
                array_push($feeder, [
                    'id' => $ids++,
                    'orderId' => $id,
                    'name' => $pokoe[$i]['title'],
                    'colorHex' => $pokoe[$i]['color'],
                    'colorName' => $pokoe[$i]['colorName'],
                    'sku' => $pokoe[$i]['sku'],
                    'category' => $pokoe[$i]['category'],
                    'subcategory' => $pokoe[$i]['subcategory'],
                    'tags' => $pokoe[$i]['tags'],
                    'brand' => $pokoe[$i]['brand'],
                    'weight' => $pokoe[$i]['size'],
                    'dimensions' => $pokoe[$i]['dimension'],
                    'price' => $pokoe[$i]['price'],
                    'discAmount' => $pokoe[$i]['discAmount'],
                    'discPercent' => $pokoe[$i]['discPercent'],
                    'quantity' => $pokoe[$i]['qty'],
                    'image' => $pokoe[$i]['img'],
                    'dateCreated' => Carbon::now(),
                    'rowPointer' => $pokoe[$i]['rowPointer'],
                    'isObsolete' => 0,
                ]);
            }

            DB::table('orderdetails')->insert($feeder);

            DB::commit();

            return response()->json([
                'status' => 'success',
                'method' => 'POST',
                'message' => 'Successfully order',
            ]);

        } catch (Exception $e) {

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'message' => 'error ' . $e,
            ]);

        }
    }

    public function confirmation(Request $request)
    {

        DB::beginTransaction();

        try {

            $id = Confirmation::max('id') + 1;

            Confirmation::insert([
                'id' => $id,
                'orderId' => $request->order,
                'tujuan' => $request->tujuan,
                'bank' => $request->bank,
                'akun' => $request->akun,
                'nama' => $request->nama,
                'jumlah' => $request->jumlah,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 1,
                'isObsolete' => 0,
            ]);
            $datas = Order::where('id', $request->order);

            $datas->update([
                'confirmationId' => $id,
            ]);
            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Confirm order successfully',
            ]);

        } catch (Exception $e) {

            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Confirmation failed' . $e,
            ]);
        }
    }

    // khusus admin
    public function changeConfirm(Request $request)
    {

        DB::beginTransaction();

        try {

            $id = Confirmation::max('id') + 1;

            Confirmation::insert([
                'id' => $id,
                'orderId' => $request->order,
                'tujuan' => $request->tujuan,
                'bank' => $request->bank,
                'akun' => $request->akun,
                'nama' => $request->nama,
                'jumlah' => $request->jumlah,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 1,
                'isObsolete' => 0,
            ]);
            CharozaLog::add('confirmation', $request->nama, 'create', 'Input Confirmation');
            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Confirm order successfully',
            ]);

        } catch (Exception $e) {

            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Confirmation failed ' . $e,
            ]);
        }
    }
        public function statusCancel($id)
    {
        DB::beginTransaction();

        try {

            $data = Order::where('rowPointer', $id);

            $data->update([
                'status' => "CANCEL",
            ]);

            CharozaLog::add('order', $id, 'update', 'Update Status Cancel');
            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Cancel order successfully',
            ]);
        } catch (Exception $e) {

            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Confirmation failed ' . $e,
            ]);
        }
    }

}
