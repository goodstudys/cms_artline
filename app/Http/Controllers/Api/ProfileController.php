<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Order;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Models\JobRequest;
use Ramsey\Uuid\Uuid;

class ProfileController extends Controller
{
    // address
    public function getAddress(Request $request)
    {

        try {

            if ($request->type == null) {
                $data = Address::where('userId', Auth::user()->id)->get();
            } else {
                $data = Address::where('userId', Auth::user()->id)
                    ->where('type', $request->type)
                    ->get();
            }

            return response()->json([
                'status' => 'success',
                'address' => $data,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'error' . $e,
            ]);
        }
    }
    public function getAddressDetail($id)
    {

        try {

            $data = Address::where('userId', Auth::user()->id)
                ->where('rowPointer', $id)
                ->first();

            return response()->json([
                'status' => 'success',
                'address' => $data,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'error' . $e,
            ]);
        }
    }

    public function createAddress(Request $request)
    {
        DB::beginTransaction();

        try {

            $id = Address::max('id') + 1;

            if ($request->type == 'billing') {

                Address::insert([
                    'id' => $id,
                    'userId' => Auth::user()->id,
                    'label' => $request->label,
                    'type' => 'BILLING',
                    'name' => $request->name,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'province' => $request->province,
                    'city' => $request->city,
                    'kecamatan' => $request->kecamatan,
                    'kelurahan' => $request->kelurahan,
                    'kodepos' => $request->kodepos,
                    'dateCreated' => Carbon::now(),
                    'rowPointer' => Uuid::uuid4()->getHex(),
                    'isActive' => 1,
                    'isObsolete' => 0,
                ]);

                DB::commit();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Address saved',
                ]);

            } elseif ($request->type == 'shopping') {

                Address::insert([
                    'id' => $id,
                    'userId' => Auth::user()->id,
                    'label' => $request->label,
                    'type' => 'DELIVERY',
                    'name' => $request->name,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'province' => $request->province,
                    'city' => $request->city,
                    'kecamatan' => $request->kecamatan,
                    'kelurahan' => $request->kelurahan,
                    'kodepos' => $request->kodepos,
                    'dateCreated' => Carbon::now(),
                    'rowPointer' => Uuid::uuid4()->getHex(),
                    'isActive' => 1,
                    'isObsolete' => 0,
                ]);

                DB::commit();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Address saved',
                ]);

            } else {

                return response()->json([
                    'status' => 'danger',
                    'message' => 'Ups, Something went wrong!',
                ]);

            }

        } catch (Exception $e) {

            DB::rollback();

            return response()->json([
                'status' => 'danger',
                'message' => 'Address failed to save' . $e,
            ]);

        }
    }

    public function updateAddress(Request $request, $id)
    {

        DB::beginTransaction();

        try {

            $data = Address::where('rowPointer', $id);

            $data->update([
                'label' => $request->label,
                'address' => $request->address,
                'province' => $request->province,
                'city' => $request->city,
                'kecamatan' => $request->kecamatan,
                'kelurahan' => $request->kelurahan,
                'kodepos' => $request->kodepos,
                'dateCreated' => Carbon::now(),
            ]);

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Address saved',
            ]);

        } catch (Exception $e) {

            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Address failed to save' . $e,
            ]);
        }
    }

    // edit photo
    public function updatePhoto(Request $request, $id)
    {

        DB::beginTransaction();
        try {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();

            request()->image->move(public_path() . '/upload/user/', $imageName);

            $imageUrl = url('/upload/user/' . $imageName);

            $data = User::where('rowPointer', $id);

            $data->update([
                'image' => $imageName,
            ]);

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Photo saved successfully',
                'url' => $imageUrl,
                'imageName' => $imageName,
            ], 200);

        } catch (Exception $e) {

            DB::rollBack();

            return response()->json([
                'status' => 'error',
                'message' => 'Photo failed to update' . $e,
            ]);
        }
    }

    // order
    public function getOrder(Request $request)
    {

        try {

            if ($request->orderPointer == null) {
                $matchThese = ['status' => 'PACKING', 'status' => 'SENT'];
                $news = Order::where('userId', Auth::user()->id)->with('detail')
                    ->where('type', 'DEFAULT')->where('status', 'NEW')->get();
                $proses = Order::where('userId', Auth::user()->id)->with('detail')
                    ->where('type', 'DEFAULT')->whereIn('status', ['PACKING', 'SENT'])->get();
                $selesai = Order::where('userId', Auth::user()->id)->with('detail')
                    ->where('type', 'DEFAULT')->where('status', 'DELIVERED')->get();
                $cancel = Order::where('userId', Auth::user()->id)->with('detail')
                    ->where('type', 'DEFAULT')->where('status', 'CANCEL')->get();
            } else {
                $data = Order::where('userId', Auth::user()->id)
                    ->where('rowPointer', $request->orderPointer)
                    ->with('detail')
                    ->first();
            }

            return response()->json([
                'status' => 'success',
                'new' => $news,
                'proses' => $proses,
                'selesai' => $selesai,
                'cancel' => $cancel,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'order' => 'Failed get order ' . $e,
            ]);

        }
    }
    public function getSalesOrder(Request $request)
    {

        try {

            if ($request->orderPointer == null) {
                $matchThese = ['status' => 'PACKING', 'status' => 'SENT'];
                $news = Order::where('userId', Auth::user()->id)->with('detail')
                    ->where('type', 'SALES')->where('status', 'NEW')->get();
                $proses = Order::where('userId', Auth::user()->id)->with('detail')
                    ->where('type', 'SALES')->whereIn('status', ['PACKING', 'SENT'])->get();
                $selesai = Order::where('userId', Auth::user()->id)->with('detail')
                    ->where('type', 'SALES')->where('status', 'DELIVERED')->get();
                $cancel = Order::where('userId', Auth::user()->id)->with('detail')
                    ->where('type', 'SALES')->where('status', 'CANCEL')->get();
            } else {
                $data = Order::where('userId', Auth::user()->id)
                    ->where('rowPointer', $request->orderPointer)
                    ->with('detail')
                    ->first();
            }

            return response()->json([
                'status' => 'success',
                'new' => $news,
                'proses' => $proses,
                'selesai' => $selesai,
                'cancel' => $cancel,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'order' => 'Failed get order ' . $e,
            ]);

        }
    }

    public function getPreOrder(Request $request)
    {

        try {

            if ($request->orderPointer == null) {
                $matchThese = ['status' => 'PACKING', 'status' => 'SENT'];
                $news = Order::where('userId', Auth::user()->id)->with('detail')
                    ->where('type', 'RESELLER')->where('status', 'NEW')->get();
                $proses = Order::where('userId', Auth::user()->id)->with('detail')
                    ->where('type', 'RESELLER')->whereIn('status', ['PACKING', 'SENT'])->get();
                $selesai = Order::where('userId', Auth::user()->id)->with('detail')
                    ->where('type', 'RESELLER')->where('status', 'DELIVERED')->get();
                $cancel = Order::where('userId', Auth::user()->id)->with('detail')
                    ->where('type', 'RESELLER')->where('status', 'CANCEL')->get();
            } else {
                $data = Order::where('userId', Auth::user()->id)
                    ->where('rowPointer', $request->orderPointer)
                    ->with('detail')
                    ->first();
            }

            return response()->json([
                'status' => 'success',
                'new' => $news,
                'proses' => $proses,
                'selesai' => $selesai,
                'cancel' => $cancel,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'order' => 'Failed get order ' . $e,
            ]);

        }
    }

    public function getOrderDetail($id)
    {
        try {

            $data = Order::with('detail')
                ->leftJoin('confirmations', 'orders.id', '=', 'confirmations.orderId')
                ->select('orders.id',
                    'orders.userId', 'confirmations.id as confirmationsid',
                    'orders.billName', 'confirmations.orderId as confirmationsorderId', 'confirmations.tujuan as confirmationstujuan',
                    'orders.billCompany', 'confirmations.bank as confirmationsbank',
                    'orders.billProvince', 'confirmations.akun as confirmationsakun',
                    'orders.billCity', 'confirmations.nama as confirmationsnama',
                    'orders.billAddress', 'confirmations.jumlah as confirmationsjumlah',
                    'orders.billKecamatan', 'confirmations.dateCreated as confirmationsdateCreated',
                    'orders.billKelurahan', 'confirmations.rowPointer as confirmationsrowPointer',
                    'orders.billPhone', 'confirmations.isActive as confirmationsisActive',
                    'orders.billKodePos',
                    'orders.orderCode',
                    'orders.billEmail',
                    'orders.kurirName',
                    'orders.quantity',
                    'orders.kurirRate',
                    'orders.subtotal',
                    'orders.noResi',
                    'orders.total',
                    'orders.status',
                    'orders.dateCreated',
                    'orders.rowPointer', 'orders.bookingCode')->where('orders.rowPointer', $id)
                ->first();
            return response()->json([
                'status' => 'success',
                'method' => 'GET',
                'message' => 'Successfully get debt data',
                'data' => $data,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'error ' . $e,
            ]);

        }
    }
    public function trackingOrder($id)
    {

        try {

            $data = Order::where('rowPointer', $id)->first();

            if ($data->status == 'NEW') {
                $step = '1';
                $text = 'Menunggu konfirmasi';
            } elseif ($data->status == 'PACKING') {
                $step = '2';
                $text = 'Pesanan anda sedang disiapkan';
            } elseif ($data->status == 'SENT') {
                $step = '3';
                $text = 'Pesanan anda sedang dikirim ke alamat penerima';
            } elseif ($data->status == 'DELIVERED') {
                $step = '4';
                $text = 'Pesanan telah sampai';
            }

            return response()->json([
                'status' => 'success',
                'step' => $step,
                'resi' => $data->noResi,
                'message' => $text,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'Failed get tracking order ' . $e,
            ]);
        }
    }
    // user
    public function changePassword(Request $request, $id)
    {

        DB::beginTransaction();

        try {

            $data = User::where('id', $id);

            $check = User::where('id', $id)->first();

            if (!Hash::check($request->cur_pass, $check->password)) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Password lama anda salah',
                ]);
            } else {
                $data->update([
                    'password' => Hash::make($request->password),
                ]);

                DB::commit();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Password berhasil dirubah',
                ]);
            }

        } catch (Exception $e) {

            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Password failed to save' . $e,
            ]);
        }
    }
    public function editProfile($id)
    {

        DB::beginTransaction();

        try {

            $data = User::where('rowPointer', $id)->first();

            // $data->update([
            //     'firstName' => $request->firstName,
            //     'lastName' => $request->lastName,
            //     'displayName' => $request->displayName,
            //     'company' => $request->company,
            //     'phone' => $request->phone,
            //     'email' => $request->email
            // ]);

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'get Profile successfully',
                'data' => $data,
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'Failed updated profile ' . $e,
            ]);
        }
    }
    public function updateProfile(Request $request, $id)
    {

        DB::beginTransaction();

        try {

            $data = User::where('rowPointer', $id);

            $data->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'displayName' => $request->displayName,
                'company' => $request->company,
                'phone' => $request->phone,
                'email' => $request->email,
            ]);

            DB::commit();

            return response()->json([
                'status' => 'success',
                'message' => 'Profile successfully changed',
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status' => 'error',
                'message' => 'Failed updated profile ' . $e,
            ]);
        }
    }
    public function pengajuanReseller(Request $request)
    {
        DB::beginTransaction();
        $reseller = DB::table('resellers')->where('user_id', Auth::user()->id)->first();
        try {
            if ($reseller == null) {

                $ktpimageName = time() . '.' . request()->ktp_image->getClientOriginalExtension();

                request()->ktp_image->move(base_path() . '/public/upload/reseller/', $ktpimageName);
                if ($request->npwp_image != null) {
                    $npwpimageName = time() . '.' . request()->npwp_image->getClientOriginalExtension();

                    request()->npwp_image->move(base_path() . '/public/upload/reseller/', $npwpimageName);
                } else {
                    $npwpimageName = null;
                }

                $id = DB::table('resellers')->max('id') + 1;

                DB::table('resellers')->insert([
                    'id' => $id,
                    'user_id' => Auth::user()->id,
                    'shopfor' => $request->shopfor,
                    'ktp' => $request->ktp,
                    'ktp_image' => $ktpimageName,
                    'npwp' => $request->npwp,
                    'npwp_image' => $npwpimageName,
                    'work_address' => $request->work_address,
                    'work_city' => $request->work_city,
                    'work_phone' => $request->work_phone,
                    'position' => $request->position,
                    'isActive' => '0',
                    'isObsolete' => '0',
                    'dateCreated' => Carbon::now(),
                    'rowPointer' => Uuid::uuid4()->getHex(),
                ]);
                $idX = JobRequest::max('id') + 1;
                JobRequest::insert([
                    'id' => $idX,
                    'user_id' => Auth::user()->id,
                    'job' => 'RESELLER',
                    'created_at' => Carbon::now()
                ]);
                DB::commit();

                return response()->json([
                    'status' => 'success',
                    'message' => 'Pengajuan Reseller successfully saved',
                ]);
            } else {
                return response()->json([
                    'status' => 'failed',
                    'message' => 'sudah mengajukan Reseller',
                ]);
            }

        } catch (Exception $e) {

            DB::rollback();

            return response()->json([
                'status' => 'error',
                'message' => 'Failed saved reseller ' . $e,
            ]);
        }
    }
}
