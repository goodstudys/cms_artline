<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Product;
use DB;

class CategoriesController extends Controller
{
    public function getCategories()
    {
        $data = DB::table('categories')->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get Categories',
            'method' => 'GET',
            'data' => $data,
        ]);
    }
    public function getDetailCategories($id)
    {
        $subCategories = DB::table('sub_categories')->where('categories_id', $id)->get();
        $productsSale = Product::where('category', $id)->where('flag', 'SALE')->where('isActive', 1)->inRandomOrder()->limit(4)->get();
        $productsNew = Product::where('category', $id)->where('flag', 'NEW')->where('isActive', 1)->inRandomOrder()->limit(4)->get();
        $productsHotDeal = Product::where('category', $id)->where('flag', 'HOTDEAL')->where('isActive', 1)->inRandomOrder()->limit(4)->get();
        if (count($productsSale) == 0) {
            $productsSale = null;
        }
        if (count($productsNew) == 0) {
            $productsNew = null;
        }
        if (count($subCategories) == 0) {
            $subCategories = null;
        }
        if (count($productsHotDeal) == 0) {
            $productsHotDeal = null;
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get Categories',
            'method' => 'GET',
            'subCategories' => $subCategories,
            'productsSale' => $productsSale,
            'productsNew' => $productsNew,
            'productsHotDeal' => $productsHotDeal,
        ]);
    }
    public function getProductsCategories($id)
    {
        $subCategories = DB::table('sub_categories')->where('categories_id', $id)->get();
        $productsSale = Product::where('category', $id)->where('flag', 'SALE')->where('isActive', 1)->get();
        $productsNew = Product::where('category', $id)->where('flag', 'NEW')->where('isActive', 1)->get();
        $productsHotDeal = Product::where('category', $id)->where('flag', 'HOTDEAL')->where('isActive', 1)->get();
        if (count($productsSale) == 0) {
            $productsSale = null;
        }
        if (count($productsNew) == 0) {
            $productsNew = null;
        }
        if (count($subCategories) == 0) {
            $subCategories = null;
        }
        if (count($productsHotDeal) == 0) {
            $productsHotDeal = null;
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get Categories',
            'method' => 'GET',
            'subCategories' => $subCategories,
            'productsSale' => $productsSale,
            'productsNew' => $productsNew,
            'productsHotDeal' => $productsHotDeal,
        ]);
    }
    public function getProductsSubCategories($id)
    {

        $products = Product::where('subcategory', $id)->where('isActive', 1)->get();

        if (count($products) == 0) {
            $products = null;
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get Categories',
            'method' => 'GET',
            'data' => $products,
        ]);
    }
    public function getAllProductsCategory($id)
    {
        $products = Product::where('category', $id)->where('isActive', 1)->get();

        if (count($products) == 0) {
            $products = null;
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get products',
            'method' => 'GET',
            'data' => $products,
        ]);
    }
    public function getSlider($id)
    {
        $data = DB::table('slider_categories')->where('category',$id)->get();
        if (count($data) == 0) {
            $data = null;
        }
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully get slider Categories',
            'method' => 'GET',
            'data' => $data,
        ]);
    }
}
