<?php

namespace App\Http\Controllers;

// use App\Helpers\CharozaApi;
use App\Helpers\CharozaLog;
use App\Models\Coupon;
use Carbon\Carbon;
use Cart;
use DB;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use Session;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Coupon::where('isObsolete', 0)->get();
        return view('admin.pages.coupon.index', ['data' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = DB::table('categories')->get();

        return view('admin.pages.coupon.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'kode' => ['required', 'string', 'max:255'],
            'discAmount' => ['required', 'string', 'max:255'],
            'discPercent' => ['required', 'string', 'max:255'],
            'date_start' => ['required', 'string', 'max:255'],
            'date_end' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();

        $start = date('Y-m-d', strtotime($request->date_start));

        $end = date('Y-m-d', strtotime($request->date_end));

        try {

            $ids = Coupon::max('id') + 1;

            Coupon::insert([
                'id' => $ids,
                'kode' => $request->kode,
                'category_id' => $request->category_id,
                'discAmount' => $request->discAmount,
                'discPercent' => $request->discPercent,
                'date_start' => $start,
                'date_end' => $end,
                'date_end' => $end,
                'minOrder' => $request->min,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 1,
                'isObsolete' => 0,
            ]);

            CharozaLog::add('coupon', $request->kode, 'created', 'Create Coupon');
            DB::commit();

            return redirect()->route('coupon.create')->with(['status' => 'success', 'message' => 'Coupon saved']);

        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('coupon.create')->with(['status' => 'danger', 'message' => 'Coupon failed to save' . $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = DB::table('categories')->get();

        $data = Coupon::where('rowPointer', $id)->first();

        return view('admin.pages.coupon.edit', ['data' => $data, 'categories' => $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kode' => ['required', 'string', 'max:255'],
            'category_id' => ['required', 'string', 'max:4500'],
            'discAmount' => ['required', 'string', 'max:255'],
            'discPercent' => ['required', 'string', 'max:255'],
            'isActive' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();

        try {

            $data = Coupon::where('rowPointer', $id);

            $imageDb = Coupon::where('rowPointer', $id)->first();

            $date_start = $request->date_start;
            $date_end = $request->date_end;

            if ($date_start != null) {

                $date_start1 = date('Y-m-d', strtotime($request->date_start));
            } else {
                $date_start1 = date('Y-m-d', strtotime($imageDb->date_start));
            }

            if ($date_end != null) {

                $date_end2 = $end = date('Y-m-d', strtotime($request->date_end));
            } else {
                $date_end2 = date('Y-m-d', strtotime($imageDb->date_end));
            }

            $data->update([
                'kode' => $request->kode,
                'category_id' => $request->category_id,
                'discAmount' => $request->discAmount,
                'discPercent' => $request->discPercent,
                'date_start' => $date_start1,
                'date_end' => $date_end2,
                'isActive' => $request->isActive,
            ]);
            CharozaLog::add('coupon', $request->kode, 'edit', 'Edit Coupon');
            DB::commit();

            return redirect()->route('coupon.edit', $id)->with(['status' => 'success', 'message' => 'Coupon saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('coupon.edit', $id)->with(['status' => 'danger', 'message' => 'Coupon failed to save' . $e]);
        }
    }

    public function statusActive($id)
    {
        DB::beginTransaction();

        try {

            $data = Coupon::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0,
                ]);
            } else {
                $data->update([
                    'isActive' => 1,
                ]);
            }
            CharozaLog::add('coupon', $datas->kode, 'update', 'Change Status Active Coupon');
            DB::commit();

            return redirect()->route('admin.coupon')->with(['status' => 'success', 'message' => 'Success update coupon']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.coupon')->with(['status' => 'danger', 'message' => 'Failed update coupon' . $e]);
        }
    }

    public function statusDelete($id)
    {
        DB::beginTransaction();

        try {

            $data = Coupon::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isObsolete == 0) {
                $data->update([
                    'isObsolete' => 1,
                ]);
            } else {
                $data->update([
                    'isObsolete' => 1,
                ]);
            }
            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0,
                ]);
            } else {
                $data->update([
                    'isActive' => 0,
                ]);
            }
            CharozaLog::add('coupon', $datas->kode, 'update', 'Change Status Obsulete Coupon');
            DB::commit();

            return redirect()->route('admin.coupon')->with(['status' => 'success', 'message' => 'Success delete coupon']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.coupon')->with(['status' => 'danger', 'message' => 'Failed delete coupon' . $e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $model = Coupon::where('rowPointer', $id);

            $model->delete();
            CharozaLog::add('Coupon', $id, 'delete', 'Delete Coupon');
            return redirect()->route('admin.coupon')->with(['status' => 'success', 'message' => 'Success delete Kupon']);

        } catch (Exception $e) {
            DB::rollback();

            return redirect()->route('admin.coupon', $id)->with(['status' => 'danger', 'message' => 'Coupon failed to delete' . $e]);
        }
    }

    public function useCoupon(Request $request)
    {
        $data = Coupon::where('kode', $request->coupon)->leftjoin('categories', 'category_id', 'categories.id')->select('coupons.*', 'categories.name as categoryName')->first();
        $dataCart1 = Cart::getContent();
        foreach ($dataCart1 as $key) {
            if ($key['attributes']['category'] == $data->categoryName) {
                $nama = $key['attributes']['category'];
            } else {
                $nama = 'beda';
            }
        }
       

        if ($data != null) {

            $start = date('Y-m-d', strtotime($data->date_start));
            $end = date('Y-m-d', strtotime($data->date_end));

            $today = Carbon::now()->format('Y-m-d');

            if ($today >= $start && $today <= $end) {
                if ($data->minOrder > $request->minOrder) {
                    return redirect()->route('cart')->with(['status' => 'danger', 'message' => 'Belum memenuhi syarat minimal pembelian']);
                } else if ($nama == 'beda') {
                    return redirect()->route('cart')->with(['status' => 'danger', 'message' => 'Belum memenuhi syarat Kategori Produk']);
                } else {
                    Session::put('kode', $data->kode);
                    Session::put('category', $data->categoryName);
                    Session::put('discAmount', $data->discAmount);
                    Session::put('discPercent', $data->discPercent);
                    return redirect()->route('cart')->with(['status' => 'success', 'message' => 'Kupon berhasil digunakan']);
                }
            } else {
                return redirect()->route('cart')->with(['status' => 'danger', 'message' => 'Masa berlaku kupon sudah habis']);
            }

        } else {

            return redirect()->route('cart')->with(['status' => 'danger', 'message' => 'Kode kupon yang anda gunakan salah']);

        }

    }

    public function cancelUse()
    {

        Session::forget('kode');
        Session::forget('category');
        Session::forget('discAmount');
        Session::forget('discPercent');
        Session::forget('getTotal');

        return back();
    }
}
