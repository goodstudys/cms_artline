<?php

namespace App\Http\Controllers;

use App\Models\Testimonial;
use App\Models\Address;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use DB;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Testimonial::get();

        return view('admin.pages.testimonial.index', ['testimonials' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'rate' => ['required', 'integer','max:255'],
            'message' => ['required', 'string', 'max:150'],
        ]);

        DB::beginTransaction();

        $address = Address::where('userId',Auth::user()->id)->first();

        if ($address != null)
        {
            $id = Testimonial::max('id') + 1;

            Testimonial::insert([
                'id' => $id,
                'userId' => Auth::user()->id,
                'rate' => $request->rate,
                'message' => $request->message,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 0,
                'isObsolete' => 0,
            ]);

            DB::commit();

            return back()->with(['status' => 'success', 'message' => 'Testimonial saved']);
        } else {
            return back()->with(['status' => 'danger', 'message' => 'Please input billing address first']);
        }

        try {



        }catch (Exception $e) {

            DB::rollback();

            return back()->with(['status' => 'danger', 'message' => 'Testimonial failed to save'.$e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Testimonial::where('rowPointer', $id)->first();

        return view('admin.pages.testimonial.show', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function statusActive($id)
    {
        DB::beginTransaction();

        try {


            $data = Testimonial::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0
                ]);
            } else {
                $data->update([
                    'isActive' => 1
                ]);
            }

            DB::commit();

            return redirect()->route('admin.testimonial')->with(['status' => 'success', 'message' => 'Success update testimonial']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.testimonial')->with(['status' => 'danger', 'message' => 'Failed update testimonial' . $e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
