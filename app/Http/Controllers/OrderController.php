<?php

namespace App\Http\Controllers;

use App\Helpers\CharozaLog;
use App\Helpers\RajaOngkir;
use App\Models\Address;
use App\Models\Order;
use App\Models\Product;
use Carbon\Carbon;
use Cart;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use Session;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = DB::table('orders')
            ->where('type', 'DEFAULT')
            ->leftJoin('confirmations', 'orders.id', '=', 'confirmations.orderId')
            ->select('orders.id',
                'orders.userId', 'confirmations.id as confirmationsid',
                'orders.billName', 'confirmations.orderId as confirmationsorderId', 'confirmations.tujuan as confirmationstujuan',
                'orders.billCompany', 'confirmations.bank as confirmationsbank',
                'orders.billProvince', 'confirmations.akun as confirmationsakun',
                'orders.billCity', 'confirmations.nama as confirmationsnama',
                'orders.billAddress', 'confirmations.jumlah as confirmationsjumlah',
                'orders.billKecamatan', 'confirmations.dateCreated as confirmationsdateCreated',
                'orders.billKelurahan', 'confirmations.rowPointer as confirmationsrowPointer',
                'orders.billPhone', 'confirmations.isActive as confirmationsisActive',
                'orders.billKodePos',
                'orders.orderCode',
                'orders.billEmail',
                'orders.kurirName',
                'orders.quantity',
                'orders.subtotal',
                'orders.total',
                'orders.status',
                'orders.dateCreated',
                'orders.rowPointer', 'orders.bookingCode')
            ->get();

        return view('admin.pages.order.index')->with('orders', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'billName' => ['required', 'string', 'max:255'],
            'billAddress' => ['required', 'string', 'max:255'],
            'billKecamatan' => ['required', 'string', 'max:255'],
            'billKelurahan' => ['required', 'string', 'max:255'],
            'billPhone' => ['required', 'string', 'max:255'],
            'billKodePos' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();
//dd($request->total_order);
        try {

            $id = Order::max('id') + 1;

            $date = date('dmY', strtotime(Carbon::now()));
            $code = CharozaLog::code('orders', 'orderCode', 7, 'ATLN' . $date);

            if (Session::get('discAmount') != '0') {
                $discount = Session::get('discAmount');
            } elseif (Session::get('discPercent') != '0') {
                $discount = Session::get('discPercent');
            } else {
                $discount = '0';
            }
            $label = 'Alamat ' . $request->billName;
            // dd($request->total_order);
            $idAddress = Address::max('id') + 1;
            if ($request->savealamat == "on") {
                Address::insert([
                    'id' => $idAddress,
                    'userId' => Auth::user()->id,
                    'label' => $label,
                    'type' => 'BILLING',
                    'name' => $request->billName,
                    'phone' => $request->billPhone,
                    'address' => $request->billAddress,
                    'province' => $request->billProvince,
                    'city' => $request->billCity,
                    'kecamatan' => $request->billKecamatan,
                    'kelurahan' => $request->billKelurahan,
                    'kodepos' => $request->billKodePos,
                    'dateCreated' => Carbon::now(),
                    'rowPointer' => Uuid::uuid4()->getHex(),
                    'isActive' => 1,
                    'isObsolete' => 0,
                ]);
            }
            if ($request->reseller == "reseller") {
                if ($request->kirimdropship == "on") {
                    // dd('1');
                    Order::insert([
                        'id' => $id,
                        'userId' => Auth::user()->id,
                        'orderCode' => $code,
                        'billName' => $request->billName,
                        'billCompany' => $request->billCompany,
                        'billProvince' => $request->billProvince,
                        'billCity' => $request->billcityname,
                        'billAddress' => $request->billAddress,
                        'billKecamatan' => $request->billKecamatan,
                        'billKelurahan' => $request->billKelurahan,
                        'billPhone' => $request->billPhone,
                        'billKodePos' => $request->billKodePos,
                        'billEmail' => $request->billEmail,
                        'billNotes' => $request->billNotes,
                        'deliveryName' => $request->deliveryName,
                        'deliveryCompany' => $request->deliveryCompany,
                        'deliveryProvince' => $request->deliveryProvince,
                        'deliveryCity' => $request->deliveryCity,
                        'deliveryAddress' => $request->deliveryAddress,
                        'deliveryKecamatan' => $request->deliveryKecamatan,
                        'deliveryKelurahan' => $request->deliveryKelurahan,
                        'deliveryPhone' => $request->deliveryPhone,
                        'deliveryKodePos' => $request->deliveryKodePos,
                        'kurirName' => 'jne',
                        'kurirRate' => $request->ongkirDeliv,
                        'payment' => $request->payment,
                        'bookingCode' => $request->kodeBooking,
                        'status' => 'NEW',
                        'promocode' => Session::get('kode'),
                        'quantity' => $request->totalqtys,
                        'subtotal' => $request->gettotal,
                        'total' => $request->total_order_reseller_deliv,
                        'type' => 'RESELLER',
                        'dateCreated' => Carbon::now(),
                        'rowPointer' => Uuid::uuid4()->getHex(),
                        'isObsolete' => 0,
                    ]);
                } else {
                    // dd('2');
                    Order::insert([
                        'id' => $id,
                        'userId' => Auth::user()->id,
                        'orderCode' => $code,
                        'billName' => $request->billName,
                        'billCompany' => $request->billCompany,
                        'billProvince' => $request->billProvince,
                        'billCity' => $request->billcityname,
                        'billAddress' => $request->billAddress,
                        'billKecamatan' => $request->billKecamatan,
                        'billKelurahan' => $request->billKelurahan,
                        'billPhone' => $request->billPhone,
                        'billKodePos' => $request->billKodePos,
                        'billEmail' => $request->billEmail,
                        'billNotes' => $request->billNotes,
                        'deliveryName' => $request->billName,
                        'deliveryCompany' => $request->billCompany,
                        'deliveryProvince' => $request->billProvince,
                        'deliveryCity' => $request->billcityname,
                        'deliveryAddress' => $request->billAddress,
                        'deliveryKecamatan' => $request->billKecamatan,
                        'deliveryKelurahan' => $request->billKelurahan,
                        'deliveryPhone' => $request->billPhone,
                        'deliveryKodePos' => $request->billKodePos,
                        'kurirName' => 'jne',
                        'kurirRate' => $request->ongkir,
                        'payment' => $request->payment,
                        'bookingCode' => $request->kodeBooking,
                        'status' => 'NEW',
                        'promocode' => Session::get('kode'),
                        'quantity' => $request->totalqtys,
                        'subtotal' => $request->gettotal,
                        'total' => $request->total_order_reseller,
                        'type' => 'RESELLER',
                        'dateCreated' => Carbon::now(),
                        'rowPointer' => Uuid::uuid4()->getHex(),
                        'isObsolete' => 0,
                    ]);
                }

            } else {
                if ($request->kirimdropship == "on") {
                    // dd('3');
                    Order::insert([
                        'id' => $id,
                        'userId' => Auth::user()->id,
                        'orderCode' => $code,
                        'billName' => $request->billName,
                        'billCompany' => $request->billCompany,
                        'billProvince' => $request->billProvince,
                        'billCity' => $request->billcityname,
                        'billAddress' => $request->billAddress,
                        'billKecamatan' => $request->billKecamatan,
                        'billKelurahan' => $request->billKelurahan,
                        'billPhone' => $request->billPhone,
                        'billKodePos' => $request->billKodePos,
                        'billEmail' => $request->billEmail,
                        'billNotes' => $request->billNotes,
                        'deliveryName' => $request->deliveryName,
                        'deliveryCompany' => $request->deliveryCompany,
                        'deliveryProvince' => $request->deliveryProvince,
                        'deliveryCity' => $request->deliveryCity,
                        'deliveryAddress' => $request->deliveryAddress,
                        'deliveryKecamatan' => $request->deliveryKecamatan,
                        'deliveryKelurahan' => $request->deliveryKelurahan,
                        'deliveryPhone' => $request->deliveryPhone,
                        'deliveryKodePos' => $request->deliveryKodePos,
                        'kurirName' => 'jne',
                        'kurirRate' => $request->ongkirDeliv,
                        'payment' => $request->payment,
                        'bookingCode' => $request->kodeBooking,
                        'status' => 'NEW',
                        'promocode' => Session::get('kode'),
                        'quantity' => $request->totalqtys,
                        'subtotal' => $request->gettotal,
                        'total' => $request->total_order_deliv,
                        'type' => 'DEFAULT',
                        'dateCreated' => Carbon::now(),
                        'rowPointer' => Uuid::uuid4()->getHex(),
                        'isObsolete' => 0,
                    ]);
                } else {
                    // dd('4');
                    Order::insert([
                        'id' => $id,
                        'userId' => Auth::user()->id,
                        'orderCode' => $code,
                        'billName' => $request->billName,
                        'billCompany' => $request->billCompany,
                        'billProvince' => $request->billProvince,
                        'billCity' => $request->billcityname,
                        'billAddress' => $request->billAddress,
                        'billKecamatan' => $request->billKecamatan,
                        'billKelurahan' => $request->billKelurahan,
                        'billPhone' => $request->billPhone,
                        'billKodePos' => $request->billKodePos,
                        'billEmail' => $request->billEmail,
                        'billNotes' => $request->billNotes,
                        'deliveryName' => $request->billName,
                        'deliveryCompany' => $request->billCompany,
                        'deliveryProvince' => $request->billProvince,
                        'deliveryCity' => $request->billcityname,
                        'deliveryAddress' => $request->billAddress,
                        'deliveryKecamatan' => $request->billKecamatan,
                        'deliveryKelurahan' => $request->billKelurahan,
                        'deliveryPhone' => $request->billPhone,
                        'deliveryKodePos' => $request->billKodePos,
                        'kurirName' => 'jne',
                        'kurirRate' => $request->ongkir,
                        'payment' => $request->payment,
                        'bookingCode' => $request->kodeBooking,
                        'status' => 'NEW',
                        'promocode' => Session::get('kode'),
                        'quantity' => $request->totalqtys,
                        'subtotal' => $request->gettotal,
                        'total' => $request->total_order,
                        'type' => 'DEFAULT',
                        'dateCreated' => Carbon::now(),
                        'rowPointer' => Uuid::uuid4()->getHex(),
                        'isObsolete' => 0,
                    ]);
                }

            }

            $objs = Cart::getContent();

            $feeder = [];
            $ids = DB::table('orderdetails')->max('id') + 1;
            foreach ($objs as $obj) {
                foreach ($obj as $key => $value) {
                    $insertArr[$key] = $value;

                    $data[$key] = Product::where('rowPointer', $obj->attributes->default)->get();
                }
                if ($request->reseller == "reseller") {
                    foreach ($data[$key] as $idx => $prod) {
                        $prodcheck = Product::where('sku', $data[$key][$idx]['sku'])->where('colorName', $insertArr['attributes']['colorname'])->count();

                        if ($prodcheck == 1) {
                            array_push($feeder, [
                                'id' => $ids++,
                                'orderId' => $id,
                                'name' => $data[$key][$idx]['name'],
                                'colorHex' => $insertArr['attributes']['color'],
                                'colorName' => $insertArr['attributes']['colorname'],
                                'sku' => $data[$key][$idx]['sku'],
                                'category' => $data[$key][$idx]['category'],
                                'subcategory' => $data[$key][$idx]['subcategory'],
                                'tags' => $data[$key][$idx]['tags'],
                                'brand' => $data[$key][$idx]['brand'],
                                'weight' => $data[$key][$idx]['weight'],
                                'dimensions' => $data[$key][$idx]['dimensions'],
                                'price' => $insertArr['attributes']['wholesale_price'],
                                'discAmount' => $data[$key][$idx]['discAmount'],
                                'discPercent' => $data[$key][$idx]['discPercent'],
                                'quantity' => $insertArr['quantity'],
                                'image' => $data[$key][$idx]['image1'],
                                'dateCreated' => Carbon::now(),
                                'rowPointer' => $data[$key][$idx]['rowPointer'],
                                'isObsolete' => 0,
                            ]);
                        } else {
                            array_push($feeder, [
                                'id' => $ids++,
                                'orderId' => $id,
                                'name' => $data[$key][$idx]['name'],
                                'colorHex' => $insertArr['attributes']['color'],
                                'colorName' => $insertArr['attributes']['colorname'],
                                'sku' => $data[$key][$idx]['sku'] . '-' . $insertArr['attributes']['colorname'],
                                'category' => $data[$key][$idx]['category'],
                                'subcategory' => $data[$key][$idx]['subcategory'],
                                'tags' => $data[$key][$idx]['tags'],
                                'brand' => $data[$key][$idx]['brand'],
                                'weight' => $data[$key][$idx]['weight'],
                                'dimensions' => $data[$key][$idx]['dimensions'],
                                'price' => $insertArr['attributes']['wholesale_price'],
                                'discAmount' => $data[$key][$idx]['discAmount'],
                                'discPercent' => $data[$key][$idx]['discPercent'],
                                'quantity' => $insertArr['quantity'],
                                'image' => $data[$key][$idx]['image1'],
                                'dateCreated' => Carbon::now(),
                                'rowPointer' => $data[$key][$idx]['rowPointer'],
                                'isObsolete' => 0,
                            ]);
                        }
                    }
                } else {
                    foreach ($data[$key] as $idx => $prod) {
                        $prodcheck = Product::where('sku', $data[$key][$idx]['sku'])->where('colorName', $insertArr['attributes']['colorname'])->count();

                        if ($prodcheck == 1) {
                            array_push($feeder, [
                                'id' => $ids++,
                                'orderId' => $id,
                                'name' => $data[$key][$idx]['name'],
                                'colorHex' => $insertArr['attributes']['color'],
                                'colorName' => $insertArr['attributes']['colorname'],
                                'sku' => $data[$key][$idx]['sku'],
                                'category' => $data[$key][$idx]['category'],
                                'subcategory' => $data[$key][$idx]['subcategory'],
                                'tags' => $data[$key][$idx]['tags'],
                                'brand' => $data[$key][$idx]['brand'],
                                'weight' => $data[$key][$idx]['weight'],
                                'dimensions' => $data[$key][$idx]['dimensions'],
                                'price' => $insertArr['price'],
                                'discAmount' => $data[$key][$idx]['discAmount'],
                                'discPercent' => $data[$key][$idx]['discPercent'],
                                'quantity' => $insertArr['quantity'],
                                'image' => $data[$key][$idx]['image1'],
                                'dateCreated' => Carbon::now(),
                                'rowPointer' => $data[$key][$idx]['rowPointer'],
                                'isObsolete' => 0,
                            ]);
                        } else {
                            array_push($feeder, [
                                'id' => $ids++,
                                'orderId' => $id,
                                'name' => $data[$key][$idx]['name'],
                                'colorHex' => $insertArr['attributes']['color'],
                                'colorName' => $insertArr['attributes']['colorname'],
                                'sku' => $data[$key][$idx]['sku'] . '-' . $insertArr['attributes']['colorname'],
                                'category' => $data[$key][$idx]['category'],
                                'subcategory' => $data[$key][$idx]['subcategory'],
                                'tags' => $data[$key][$idx]['tags'],
                                'brand' => $data[$key][$idx]['brand'],
                                'weight' => $data[$key][$idx]['weight'],
                                'dimensions' => $data[$key][$idx]['dimensions'],
                                'price' => $insertArr['price'],
                                'discAmount' => $data[$key][$idx]['discAmount'],
                                'discPercent' => $data[$key][$idx]['discPercent'],
                                'quantity' => $insertArr['quantity'],
                                'image' => $data[$key][$idx]['image1'],
                                'dateCreated' => Carbon::now(),
                                'rowPointer' => $data[$key][$idx]['rowPointer'],
                                'isObsolete' => 0,
                            ]);
                        }
                    }
                }
            }

            DB::table('orderdetails')->insert($feeder);

            Session::forget('ongkir');
            Session::forget('ongkir-etd');
            Session::forget('ongkirDeliv');
            Session::forget('ongkir-etdDeliv');
            Session::forget('qtys');
            Session::forget('weightRow');
            Session::forget('kode');
            Session::forget('category');
            Session::forget('discAmount');
            Session::forget('discPercent');
            Session::forget('getTotal');
            Session::forget('kirimdropship');
            Cart::clear();

            DB::commit();

            if ($request->reseller == "reseller") {
                if ($request->kirimdropship == "on") {
                    return redirect()->route('checkout')->with(['status' => 'success', 'message' => 'Order Succesfuly Placed', 'total' => $request->total_order_reseller_deliv]);
                } else {
                    return redirect()->route('checkout')->with(['status' => 'success', 'message' => 'Order Succesfuly Placed', 'total' => $request->total_order_reseller]);
                }

            } else {
                if ($request->kirimdropship == "on") {
                    return redirect()->route('checkout')->with(['status' => 'success', 'message' => 'Order Succesfuly Placed', 'total' => $request->total_order_deliv]);
                } else {
                    return redirect()->route('checkout')->with(['status' => 'success', 'message' => 'Order Succesfuly Placed', 'total' => $request->total_order]);
                }

            }

        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('checkout')->with(['status' => 'danger', 'message' => 'Order failed' . $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provinces = RajaOngkir::getProvince();

        $city = RajaOngkir::getCity();

        $detail = DB::table('orders')
            ->join('orderdetails', 'orderdetails.orderId', 'orders.id')
            ->where('orders.rowPointer', $id)->get();
        $confirm = DB::table('orders')
            ->join('confirmations', 'confirmations.orderId', 'orders.id')
            ->where('orders.rowPointer', $id)
            ->select('tujuan', 'bank', 'akun', 'nama', 'jumlah')
            ->get();
        $order = Order::where('rowPointer', $id)->first();
        return view('admin.pages.order.show')->with([
            'order' => $order,
            'detail' => $detail,
            'confirm' => $confirm,
            'provinces' => $provinces,
            'citys' => $city,
        ]);
    }

    public function showprint($id)
    {
        $provinces = RajaOngkir::getProvince();

        $city = RajaOngkir::getCity();

        $detail = DB::table('orders')
            ->join('orderdetails', 'orderdetails.orderId', 'orders.id')
            ->where('orders.rowPointer', $id)->get();
        $confirm = DB::table('orders')
            ->join('confirmations', 'confirmations.orderId', 'orders.id')
            ->where('orders.rowPointer', $id)
            ->select('tujuan', 'bank', 'akun', 'nama', 'jumlah')
            ->get();
        $order = Order::where('rowPointer', $id)
            ->first();

        $getcity = RajaOngkir::specificCity($order->billCity);

        return view('admin.pages.order.print')->with([
            'order' => $order,
            'detail' => $detail,
            'confirm' => $confirm,
            'provinces' => $provinces,
            'citys' => $city,
            'getcity' => $getcity,
        ]);
    }

    public function detail($id)
    {
        $detail = DB::table('orders')
            ->join('orderdetails', 'orderdetails.orderId', 'orders.id')
            ->where('orders.rowPointer', $id)->get();

        $order = Order::where('rowPointer', $id)->first();
        return view('components.account.order.show')->with([
            'order' => $order,
            'detail' => $detail,
        ]);
    }

    public function checkResi($id)
    {
        $data = Order::where('rowPointer', $id)->first();

        return view('components.account.order.resi.show', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function statusChange(Request $request, $id)
    {

        DB::beginTransaction();

        try {

            $data = Order::where('rowPointer', $id);

            $ResiValid = Order::where('noResi', $request->resi);

            $datas = $data->first();

            if ($datas->status == "NEW") {
                $data->update([
                    'status' => "PACKING",
                ]);
                CharozaLog::add('order', $id, 'update', 'Update Status Order Packing');
            } elseif ($datas->status == "PACKING") {

                if ($ResiValid->count() != 0) {

                    return redirect()->route('admin.order')->with(['status' => 'danger', 'message' => 'No. Resi sudah terpakai']);

                } else {

                    $data->update([
                        'kurirName' => $request->courier,
                        'noResi' => $request->resi,
                        'status' => "SENT",
                    ]);
                    CharozaLog::add('order', $id, 'update', 'Update Status Order Sent');

                }

            } elseif ($datas->status == "SENT") {
                $data->update([
                    'status' => "DELIVERED",
                ]);
                CharozaLog::add('order', $id, 'update', 'Update Status Order Delivered');
            }

            DB::commit();

            return redirect()->route('admin.order')->with(['status' => 'success', 'message' => 'Success update status order']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.order')->with(['status' => 'danger', 'message' => 'Failed update status order' . $e]);
        }
    }

    public function statusCancel($id)
    {
        DB::beginTransaction();

        try {

            $data = Order::where('rowPointer', $id);

            $data->update([
                'status' => "CANCEL",
            ]);

            CharozaLog::add('order', $id, 'update', 'Update Status Cancel');
            DB::commit();

            return redirect()->route('admin.order')->with(['status' => 'success', 'message' => 'Success update status order']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.order')->with(['status' => 'danger', 'message' => 'Failed update status order' . $e]);
        }
    }

    public function bookingCode(Request $request)
    {

        $data = Order::where('id', $request->order);

        $data->update([
            'bookingCode' => $request->booking,
        ]);
        CharozaLog::add('order', $request->order, 'update', 'Update Booking Code');
        return redirect()->route('admin.order')->with(['status' => 'success', 'message' => 'Success update kode booking']);
    }

    public function printMultirple(Request $request)
    {
        $data = $request->input('prints');
        $order = array();
//        $detail = array();
        if ($request->input('prints') == null) {
            return redirect()->route('admin.order')->with(['status' => 'danger', 'message' => 'Checklist data order yang akan di print']);
        } else {
            foreach ($request->input('prints') as $idx => $print) {
                $provinces = RajaOngkir::getProvince();

                $city = RajaOngkir::getCity();

                $detail[] = DB::table('orders')
                    ->join('orderdetails', 'orderdetails.orderId', 'orders.id')
                    ->where('orders.id', $data[$idx])->get();
                $confirm = DB::table('orders')
                    ->join('confirmations', 'confirmations.orderId', 'orders.id')
                    ->where('orders.id', $print)
                    ->select('tujuan', 'bank', 'akun', 'nama', 'jumlah')
                    ->get();
                $order[] = Order::where('id', $data[$idx])->with('detail')->get();

//            $getcity = RajaOngkir::specificCity($order->billCity);
            }
        }

        return view('admin.pages.order.prints')->with([
            'order' => $order,
        ]);

    }
}
