<?php

namespace App\Http\Controllers;

// use App\Helpers\CharozaApi;
use App\Helpers\CharozaLog;
use App\Imports\ProductImport;
use App\Models\Product;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Ramsey\Uuid\Uuid;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = Product::where('isObsolete', 0)->leftjoin('categories','category','categories.id')->select('products.*','categories.name as categoryName')->get();
        return view('admin.pages.product.index')->with('products', $products);
    }
    public function showVariasi($id)
    {
        $group = DB::table('products')
            ->leftjoin('groups', 'products.rowPointer', 'groups.productId')
            ->leftjoin('groupsdetails', 'groups.id', 'groupsdetails.groupId')
            ->select(
                'products.id',
                'products.name',
                'products.brand',
                'products.category',
                'products.rowPointer',
                'products.price',
                'products.discAmount',
                'products.discPercent',
                'products.image1',
                'groups.id as groupsId',
                'groups.productId as groupsProductId',
                'groupsdetails.id as detailsId',
                'groupsdetails.name as detailsName',
                'groupsdetails.price as detailsPrice',
                'groupsdetails.discAmount as detailsDiscAmount',
                'groupsdetails.discPercent as detailsDiscPercent',
                'groupsdetails.rowPointer as detailsRowPointer'
            )->where('products.rowPointer', $id)->get();
        // dd($group);
        return view('admin.pages.product.variasi', ['groups' => $group]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $data = CharozaApi::getGroup();
        $categories = DB::table('categories')->get();
        $subcategories = DB::table('sub_categories')->get();

        // if ($data == 'Connection problem') {
        //     $categorys = "Connection problem";
        // } else {
        //     $categorys = $data['_masterGroupTable'];
        // }

        return view('admin.pages.product.create', [
            'categories' => $categories,
            'subcategories' => $subcategories]);
    }
    public function subCat(Request $request)
    {
        $id = $request->categories_id;
        $sub = DB::table('sub_categories')->where('categories_id', '=', $id)->get();
        return response()->json($sub);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:4500'],
            'colorhex' => ['required', 'string', 'max:255'],
            'colorname' => ['required', 'string', 'max:255'],
            'sku' => ['required', 'string', 'max:255'],
            'category' => ['required', 'string', 'max:255'],
            // 'subcategory' => ['required', 'string', 'max:255'],
            // 'tags' => ['required', 'string', 'max:255'],
            'brand' => ['required', 'string', 'max:255'],
            'weight' => ['required', 'string', 'max:255'],
            'dimensions' => ['required', 'string', 'max:255'],
            'price' => ['required', 'string', 'max:255'],
            'discAmount' => ['required', 'string', 'max:255'],
            'discPercent' => ['required', 'string', 'max:255'],
            'image1' => ['required', 'image', 'max:2048'],
            // 'image2' => ['required', 'image', 'max:2048'],
            // 'image3' => ['required', 'image', 'max:2048'],
            // 'image4' => ['required', 'image', 'max:2048'],
        ]);

        DB::beginTransaction();

        try {

           $imageName1 = time() . '1' . '.' . request()->image1->getClientOriginalExtension();
            // $imageName2 = time() . '2' . '.' . request()->image2->getClientOriginalExtension();
            // $imageName3 = time() . '3' . '.' . request()->image3->getClientOriginalExtension();
            

            request()->image1->move(public_path() . '/upload/users/products', $imageName1);
            // request()->image2->move(public_path() . '/upload/users/products', $imageName2);
            // request()->image3->move(public_path() . '/upload/users/products', $imageName3);
            

            if ($request->image2 == null) {
                $imageName2 = null;
            } else {
                $imageName2 = time() . '2' . '.' . request()->image2->getClientOriginalExtension();
                request()->image2->move(public_path() . '/upload/users/products', $imageName2);
            }

            if ($request->image3 == null) {
                $imageName3 = null;
            } else {
                $imageName3 = time() . '3' . '.' . request()->image3->getClientOriginalExtension();
                request()->image3->move(public_path() . '/upload/users/products', $imageName3);
            }

            if ($request->image4 == null) {
                $imageName4 = null;
            } else {
                $imageName4 = time() . '4' . '.' . request()->image4->getClientOriginalExtension();
                request()->image4->move(public_path() . '/upload/users/products', $imageName4);
            }

            $id = Product::max('id') + 1;

            Product::insert([
                'id' => $id,
                'name' => $request->name,
                'description' => $request->description,
                'colorHex' => $request->colorhex,
                'colorName' => $request->colorname,
                'sku' => $request->sku,
                'category' => $request->category,
                'subcategory' => $request->subcategory,
                'tags' => $request->tags,
                'brand' => $request->brand,
                'weight' => $request->weight,
                'dimensions' => $request->dimensions,
                'price' => $request->price,
                'wholesale_price' => $request->wholesale_price,
                'wholesale_type' => $request->wholesaleType,
                'discAmount' => $request->discAmount,
                'discPercent' => $request->discPercent,
                'image1' => $imageName1,
                'image2' => $imageName2,
                'image3' => $imageName3,
                'image4' => $imageName4,
                'flag' => $request->flag,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 1,
                'isObsolete' => 0,
            ]);

            CharozaLog::add('products', $request->name, 'created', 'Create Product');

            DB::commit();

            return redirect()->route('product.create')->with(['status' => 'success', 'message' => 'Product saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('product.create')->with(['status' => 'danger', 'message' => 'Product failed to save' . $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Product::where('rowPointer', $id)->first();

        return view('admin.pages.product.show', ['data' => $data]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Product::where('rowPointer', $id)->first();
        $categories = DB::table('categories')->get();
        $subcategories = DB::table('sub_categories')->get();

        // $get = CharozaApi::getGroup();

        // if ($get == 'Connection problem') {
        //     $categorys = "Connection problem";
        // } else {
        //     $categorys = $get['_masterGroupTable'];
        // }

        return view('admin.pages.product.edit', [
            'data' => $data,
            'categories' => $categories,
            'sub_categories' => $subcategories
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:4500'],
            'colorhex' => ['required', 'string', 'max:255'],
            'colorname' => ['required', 'string', 'max:255'],
            'sku' => ['required', 'string', 'max:255'],
            'category' => ['required', 'string', 'max:255'],
            // 'subcategory' => ['required', 'string', 'max:255'],
            // 'tags' => ['required', 'string', 'max:255'],
            'brand' => ['required', 'string', 'max:255'],
            'weight' => ['required', 'string', 'max:255'],
            'dimensions' => ['required', 'string', 'max:255'],
            'price' => ['required', 'string', 'max:255'],
            'discAmount' => ['required', 'string', 'max:255'],
            'discPercent' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();

        try {

            $data = Product::where('rowPointer', $id);

            $imageDb = Product::where('rowPointer', $id)->first();

            $images1 = $request->image1;
            $images2 = $request->image2;
            $images3 = $request->image3;
            $images4 = $request->image4;

            if ($images1 != null) {

                $imageName1 = time() . '1' . '.' . request()->image1->getClientOriginalExtension();

                request()->image1->move(public_path() . '/upload/users/products', $imageName1);
            } else {
                $imageName1 = $imageDb->image1;
            }

            if ($images2 != null) {

                $imageName2 = time() . '2' . '.' . request()->image2->getClientOriginalExtension();

                request()->image2->move(public_path() . '/upload/users/products', $imageName2);
            } else {
                $imageName2 = $imageDb->image2;
            }

            if ($images3 != null) {

                $imageName3 = time() . '3' . '.' . request()->image3->getClientOriginalExtension();

                request()->image3->move(public_path() . '/upload/users/products', $imageName3);
            } else {
                $imageName3 = $imageDb->image3;
            }

            if ($images4 != null) {

                $imageName4 = time() . '4' . '.' . request()->image4->getClientOriginalExtension();

                request()->image4->move(public_path() . '/upload/users/products', $imageName4);
            } else {
                $imageName4 = $imageDb->image4;
            }

            $data->update([
                'name' => $request->name,
                'description' => $request->description,
                'colorHex' => $request->colorhex,
                'colorName' => $request->colorname,
                'sku' => $request->sku,
                'category' => $request->category,
                'subcategory' => $request->subcategory,
                'tags' => $request->tags,
                'brand' => $request->brand,
                'weight' => $request->weight,
                'dimensions' => $request->dimensions,
                'price' => $request->price,
                'wholesale_price' => $request->wholesale_price,
                'wholesale_type' => $request->wholesaleType,
                'discAmount' => $request->discAmount,
                'discPercent' => $request->discPercent,
                'image1' => $imageName1,
                'image2' => $imageName2,
                'image3' => $imageName3,
                'image4' => $imageName4,
                'flag' => $request->flag,
                'isActive' => $request->isActive,
            ]);

            CharozaLog::add('products', $request->name, 'edit', 'Edit Product');

            DB::commit();

            return redirect()->route('product.edit', $id)->with(['status' => 'success', 'message' => 'Product saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('product.edit', $id)->with(['status' => 'danger', 'message' => 'Product failed to save' . $e]);
        }
    }

    public function quickUpdate(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'brand' => ['required', 'string', 'max:255'],
            'price' => ['required', 'string', 'max:255'],
            'discAmount' => ['required', 'string', 'max:255'],
            'discPercent' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();

        try {

            $data = Product::where('rowPointer', $id);

            $data->update([
                'name' => $request->name,
                'brand' => $request->brand,
                'price' => $request->price,
                'discAmount' => $request->discAmount,
                'discPercent' => $request->discPercent,
            ]);

            DB::commit();

            return redirect()->route('admin.product', $id)->with(['status' => 'success', 'message' => 'Product update']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.product', $id)->with(['status' => 'danger', 'message' => 'Product failed to update' . $e]);
        }
    }

    public function statusActive($id)
    {
        DB::beginTransaction();

        try {

            $data = Product::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0,
                ]);
            } else {
                $data->update([
                    'isActive' => 1,
                ]);
            }

            CharozaLog::add('products', $datas->name, 'update', 'Change Status Active Product');

            DB::commit();

            return redirect()->route('admin.product')->with(['status' => 'success', 'message' => 'Success update product']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.product')->with(['status' => 'danger', 'message' => 'Failed update product' . $e]);
        }
    }

    public function statusDelete($id)
    {
        DB::beginTransaction();

        try {

            $data = Product::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isObsolete == 0) {
                $data->update([
                    'isObsolete' => 1,
                ]);
            } else {
                $data->update([
                    'isObsolete' => 1,
                ]);
            }
            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0,
                ]);
            } else {
                $data->update([
                    'isActive' => 0,
                ]);
            }

            CharozaLog::add('products', $datas->name, 'update', 'Change Status Obsulete Product');

            DB::commit();

            return redirect()->route('admin.product')->with(['status' => 'success', 'message' => 'Success update product']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('admin.product')->with(['status' => 'danger', 'message' => 'Failed update product' . $e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $data = Product::where('rowPointer', $id);

            $datas = $data->first();

            // if ($datas->isActive == 1) {
            //     $data->update([
            //         'isActive' => 0
            //     ]);
            // } else {
            //     $data->update([
            //         'isActive' => 1
            //     ]);
            // }
            $model = Product::find($id);
            $model->delete();
            CharozaLog::add('products', $datas->name, 'delete', 'Delete Product');
            return redirect()->route('admin.product')->with(['status' => 'success', 'message' => 'Success delete Produk']);

        } catch (Exception $e) {
            DB::rollback();

            return redirect()->route('admin.product', $id)->with(['status' => 'danger', 'message' => 'Produk failed to delete' . $e]);
        }
    }

    public function import()
    {

        // $datas = CharozaApi::getProduct();

        // $data = $datas['_masterBarangTable'];

        // $feeder = [];

        // foreach ($data as $prdct) {
        //     array_push($feeder, [
        //         'name' => $prdct['NamaBarang'],
        //         'sku' => $prdct['KodeBarang'],
        //         'dateCreated' => Carbon::now(),
        //         'rowPointer' => Uuid::uuid4()->getHex(),
        //         'isActive' => 1,
        //         'isObsolete' => 0,
        //     ]);
        // }

        // Product::insert($feeder);

        // return redirect()->route('admin.product')->with(['status' => 'success', 'message' => 'Success import product']);

    }

    public function importExcel(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|mimes:csv,xls,xlsx',
        ]);

        $file = $request->file('file');

        $name = rand() . $file->getClientOriginalName();

        $file->move('upload/file_product', $name);

        Excel::import(new ProductImport, public_path('upload/file_product/' . $name));

        return redirect()->route('admin.product')->with(['status' => 'success', 'message' => 'Success import excel product']);
    }
}
