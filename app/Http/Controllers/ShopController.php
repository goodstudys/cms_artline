<?php

namespace App\Http\Controllers;

// use App\Helpers\CharozaApi;
use App\Models\Group;
use App\Models\Product;
use App\Models\Rating;
use DB;
use Illuminate\Http\Request;
use Session;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Product::where('isActive', 1)->leftjoin('categories', 'category', 'categories.id')->select('products.*', 'categories.name as categoryName');
        $products = $data->paginate(20);
        $koleksi = json_encode($data->get());
        // $data = CharozaApi::getGroup();
        $categories = DB::table('categories')->get();
        $subcategories = DB::table('sub_categories')->get();
        // if ($data == 'Connection problem') {
        //     $categorys = "Connection problem";
        // } else {
        //     $categorys = $data['_masterGroupTable'];
        // }

        return view('pages.shop.index', [
            'koleksi' => $koleksi,
            'products' => $products,
            'categories' => $categories,
            'subcategories' => $subcategories,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        if (Session::has('previous')) {
            if ($id != Session::get('previous')) {
                Session::forget('prodPrice');
                Session::forget('proddiscAmount');
                Session::forget('proddiscPercent');
                Session::forget('prodHex');
                Session::forget('prodName');
                Session::forget('prodPointer');
                Session::forget('prodSku');
                // Session::forget('prodQty');
            } else {
                Session::put('previous', $id);
            }
        } else {
            Session::put('previous', $id);
        }

        $products = Product::where('isActive', 1)->get();
        $data = Product::where('rowPointer', $id)->first();
        $rating = Rating::where('productId', $id)
            ->join('users', 'users.id', 'userId')
            ->select('productId', 'userId', 'rate', 'comments', 'users.id as idUser', 'displayName', 'image', 'ratings.dateCreated as createdAt')
            ->get();
        $star = Rating::where('productId', $id)
            ->leftjoin('users', 'users.id', 'userId')
            ->select('productId', 'userId', 'rate', 'comments', 'users.id as idUser', 'displayName', 'image', 'ratings.dateCreated as createdAt')
            ->avg('rate');
//  dd($star);
        $group = Group::where('productId', $id)
            ->join('groupsdetails', 'groupId', 'groups.id')
            ->where('groupsdetails.isActive', 1)
            ->select('productId', 'groups.id as groupId1', 'groupsdetails.id as detailId', 'groupId as groupDetail', 'name', 'price', 'discAmount', 'discPercent', 'groupsdetails.rowPointer as Pointer')
            ->get();

        $checkgroup = Group::where('productId', $id)->count();

        $related = Product::where('isActive', 1)->inRandomOrder()->limit(5)->get();

        return view('pages.shop.show', [
            'product' => $data,
            'related' => $related,
            'ratings' => $rating,
            'star' => $star,
            'groups' => $group,
            'checkgroup' => $checkgroup,
        ]);
    }

    public function groupSelected(Request $request, $id)
    {

        Session::put('previous', $id);

        if ($request->selectedgroup == $id) {
            $groupselect = Product::where('rowPointer', $id)->first();

            Session::put('prodPrice', $groupselect->price);
            Session::put('proddiscAmount', $groupselect->discAmount);
            Session::put('proddiscPercent', $groupselect->discPercent);
            Session::put('prodHex', $groupselect->colorHex);
            Session::put('prodName', $groupselect->colorName);
            Session::put('prodPointer', $groupselect->Pointer);
            Session::put('prodColor', $groupselect->colorName);
            // Session::put('prodQty', $groupselect->quantity);
            Session::put('prodSku', $groupselect->sku);

        } else {
            $groupselect = Group::where('productId', $id)
                ->join('groupsdetails', 'groupId', 'groups.id')
                ->where('groupsdetails.rowPointer', $request->selectedgroup)
                ->select('productId', 'groups.id as groupId1', 'groupsdetails.id as detailId', 'groupId as groupDetail', 'name', 'price', 'discAmount', 'discPercent', 'groupsdetails.rowPointer as Pointer')
                ->first();

            Session::put('prodPrice', $groupselect->price);
            Session::put('proddiscAmount', $groupselect->discAmount);
            Session::put('proddiscPercent', $groupselect->discPercent);
            Session::put('prodHex', $groupselect->name);
            Session::put('prodName', $groupselect->name);
            Session::put('prodPointer', $groupselect->Pointer);
            Session::put('prodColor', $groupselect->name);
            // Session::put('prodQty', $groupselect->groupqty);
            Session::put('prodSku', $groupselect->groupsku);
        }

        return back();

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
