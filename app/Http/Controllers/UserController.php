<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Hash;
use App\Helpers\CharozaLog;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if (Auth::user()->level == 1) {

        //     $users = User::get();

        //     return view('admin.pages.user.index')->with('data', $users);
        // } else {

            $users = User::where('level', '!=', 1)->where('level', '!=', 2)->get();

            return view('admin.pages.user.index')->with('data', $users);
        // }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();

            request()->image->move(base_path() . '/public/upload/user/', $imageName);

            $id = DB::table('users')->max('id') + 1;

            DB::table('users')->insert([
                'id' => $id,
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'displayName' => $request->displayName,
                'company' => $request->company,
                'phone' => $request->phone,
                'email' => $request->email,
                'image' => $imageName,
                'level' => $request->level,
                'isActive' => '1',
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'password' => Hash::make($request->password),
            ]);

            DB::commit();

            return redirect()->route('user.create')->with(['status' => 'success', 'message' => 'User saved']);

        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('user.create')->with(['status' => 'danger', 'message' => 'User failed to save' . $e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::where('rowPointer', $id)->first();

        return view('admin.pages.user.edit', ['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        DB::beginTransaction();

        try {

            $data = User::where('rowPointer', $id)->first();

            $images = $request->image;
            if ($images != null) {

                $imageName = time() . '.' . request()->image->getClientOriginalExtension();

                request()->image->move(public_path() . '/upload/user/', $imageName);
            } else {
                $imageName = $data->image;
            }


            $user = User::where('rowPointer', $id);

            $user->update([
                'firstName' => $request->firstName,
                'lastName' => $request->lastName,
                'displayName' => $request->displayName,
                'company' => $request->company,
                'phone' => $request->phone,
                'email' => $request->email,
                'image' => $imageName,
                'level' => $request->level,
            ]);
            CharozaLog::add('user', $request->firstName, 'edit', 'Edit User');
            DB::commit();

            return redirect()->route('user.edit', $id)->with(['status' => 'success', 'message' => 'User saved']);
        } catch (Exception $e) {

            DB::rollback();

            return redirect()->route('user.edit', $id)->with(['status' => 'danger', 'message' => 'User failed to save' . $e]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
