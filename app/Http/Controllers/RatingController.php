<?php

namespace App\Http\Controllers;

use App\Models\Rating;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use DB;
use Session;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('components.account.order.rating.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'rate' => ['required', 'string', 'max:255'],
            'comments' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();

        try {

            $ids = Rating::max('id') + 1;

            Rating::insert([
                'id' => $ids,
                'productId' => $request->product,
                'userId' => Auth::user()->id,
                'orderId' => $request->order,
                'rate' => $request->rate,
                'comments' => $request->comments,
                'dateCreated' => Carbon::now(),
                'rowPointer' => Uuid::uuid4()->getHex(),
                'isActive' => 1,
                'isObsolete' => 0,
            ]);

            Session::forget('item');
            DB::commit();

            return back()->with(['status' => 'success', 'message' => 'Rating saved']);

        }catch (Exception $e) {

            DB::rollback();

            return back()->with(['status' => 'danger', 'message' => 'Rating failed to save'.$e]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'rate' => ['required', 'string', 'max:255'],
            'comments' => ['required', 'string', 'max:255'],
        ]);

        DB::beginTransaction();

        try {

            $data = Rating::where('orderId', $id);

            $data->insert([
                'rate' => $request->rate,
                'comments' => $request->comments,
                'dateCreated' => Carbon::now(),
            ]);

            DB::commit();

            return back()->with(['status' => 'success', 'message' => 'Success update rating']);

        }catch (Exception $e) {

            DB::rollback();

            return back()->with(['status' => 'danger', 'message' => 'Failed update rating'.$e]);
        }
    }

    public function statusObsolete($id) {
        DB::beginTransaction();

        try {


            $data = User::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isObsolete == 1) {
                $data->update([
                    'isObsolete' => 0
                ]);
            } else {
                $data->update([
                    'isObsolete' => 1
                ]);
            }

            DB::commit();

            return back()->with(['status' => 'success', 'message' => 'Success update rating']);

        } catch (Exception $e) {

            DB::rollback();

            return back()->with(['status' => 'danger', 'message' => 'Failed update rating'.$e ]);

        }
    }

    public function statusActive($id) {
        DB::beginTransaction();

        try {


            $data = User::where('rowPointer', $id);

            $datas = $data->first();

            if ($datas->isActive == 1) {
                $data->update([
                    'isActive' => 0
                ]);
            } else {
                $data->update([
                    'isActive' => 1
                ]);
            }

            DB::commit();

            return back()->with(['status' => 'success', 'message' => 'Success update rating']);

        } catch (Exception $e) {

            DB::rollback();

            return back()->with(['status' => 'danger', 'message' => 'Failed update rating'.$e ]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
