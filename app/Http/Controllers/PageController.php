<?php

namespace App\Http\Controllers;

use App;
use App\Faq;
use App\About;
use App\Models\Blog;
use Illuminate\Http\Request;
use App\Models\Carousel;
use App\Models\Product;
use App\Models\Slider;
use App\Models\Testimonial;
use Illuminate\Support\Facades\DB;
use App\Mail\SendEmail;
use Illuminate\Support\Facades\Mail;

class PageController extends Controller
{
    public function index()
    {

        $sliders = Slider::where('isActive', 1)->get();
        $blogs = Blog::where('isActive', 1)->get();
        $carousel = Carousel::where('isActive', 1)->get();

        $testimonials = Testimonial::where('testimonials.isActive', 1)
            ->join('users', 'users.id', 'testimonials.userId')->select('testimonials.id as testimonialsId',
            'users.id as usersId',
            'users.displayName as usersName',
            'users.image as usersImage',
            'testimonials.message as testiMessage',
            'testimonials.rate as testiRate'
        )->get()->take(3);
        $blog = $blogs->take(2);
        $hotproducts = Product::where('isActive', 1)->where('flag', 'HOTDEAL')->inRandomOrder()->limit(5)->get();
        $newproducts = Product::where('isActive', 1)->where('flag', 'NEW')->orderBy('id', 'desc')->get()->take(5);
        // $products = Product::where('isActive',1)->orderBy('id', 'desc')->get();
        return view('pages.home', [
            'sliders' => $sliders,
            // 'products' => $products,
            'blog' => $blog,
            'carousel' => $carousel,
            'hotproducts' => $hotproducts,
            'newproducts' => $newproducts,
            'testimonials' => $testimonials]);

    }
    public function show($id)
    {

        $data = Product::where('rowPointer', $id)->first();

        return view('pages.shop.show', ['data' => $data]);

    }

    public function shop()
    {

        $products = Product::get();

        return view('pages.shop.index')->with('products', $products);

    }

    public function promotions()
    {
        return view('pages.promotions');
    }
    public function blog()
    {
        return view('pages.blog.index');
    }
    public function newlook()
    {
        return view('pages.newlook');
    }
    public function productDetail()
    {
        return view('pages.shop.detail');
    }
    public function blogDetail()
    {
        return view('pages.blog.show');
    }
    public function faqs()
    {
        $general = Faq::where('type', 1)->get();
        $payment = Faq::where('type', 2)->get();
        return view('pages.faqs')->with(['general' => $general, 'payment' => $payment]);
    }
    public function about()
    {
        $data = About::first();
        return view('pages.about')->with(['data' => $data]);
    }

    public function connectionPdo()
    {
        try {
            DB::connection()->getPdo();
            echo "Connected successfully to: " . DB::connection()->getDatabaseName();
        } catch (\Exception $e) {
            die("Could not connect to the database. Please check your configuration. error:" . $e);
        }
    }
    public function sendCoba(Request $request)
    {
        try {
            $nama = $request->name;
            $email = $request->email;
            $phone = $request->phone;
            $message = $request->message;
            $isi = ['nama' => $nama,
                'email' => $email,
                'message' => $message,
                'phone' => $phone,
            ];

            $kirim = Mail::to('emailanda@gmail.com')->send(new SendEmail($isi));
            return redirect()->route('contact')->with(['status' => 'success', 'message' => 'Your message has been sent!']);
        } catch (Exception $e) {

            return redirect()->route('contact')->with(['status' => 'danger', 'message' => 'Your message failed sent!' . $e]);
        }

    }
}
