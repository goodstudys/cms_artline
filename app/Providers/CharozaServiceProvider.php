<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class CharozaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // require_once app_path() . '/Helpers/CharozaApi.php';
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
