<?php
namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Auth;

class CharozaLog {
    public static function add($table, $id, $type, $information){

        $ids = (DB::table('logs')->max('id') + 1);

//        DB::unprepared('SET IDENTITY_INSERT logs ON');
        DB::table('logs')->insert([
            'id'			=> $ids,
            'user'		    => Auth::user()->id,
            'table'		    => $table,
            'konteks'		=> $id,
            'type'		    => $type,
            'information'	=> $information,
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);
//        DB::unprepared('SET IDENTITY_INSERT logs OFF');

    }

    public static function code($table, $field, $lebar = 0, $awalan)
    {
        //        CodeGenerator::code('m_company', 'c_id', 7, 'MB');
        $code = DB::table($table)->select($field)->orderBy($field, 'desc')->limit(1);
        $countData = $code->count();
        if ($countData == 0) {
            $nomor = 1;
        } else {
            $getData = $code->get();
            $row = array();
            foreach ($getData as $value) {
                $row = array($value->$field);
            }
            $nomor = intval(substr($row[0], strlen($awalan))) + 1;
        }

        if ($lebar > 0) {
            $angka = $awalan . str_pad($nomor, $lebar, "0", STR_PAD_LEFT);
        } else {
            $angka = $awalan . $nomor;
        }

        return $angka;
    }
}
