<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'address';

    protected $primaryKey = 'id';

    protected $fillable = [
        'label', 'type', 'address', 'province', 'city', 'kecamatan', 'kelurahan', 'kodePos'
    ];

    public $timestamps = false;
}
