<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'id', 'name', 'description', 'colorHex', 'colorName', 'sku', 'category', 'subcategory', 'tags', 'brand', 'weight', 'dimensions', 'discAmount', 'discPercent', 'image1', 'image2', 'image3', 'image4', 'dateCreated', 'rowPointer','isActive','isObsolete'
    ];

    public $timestamps = false;

    public function review() {
        return $this->hasMany('App\Models\Rating', 'productId','rowPointer');
    }

    public function group() {
        return $this->belongsTo('App\Models\Group');
    }
}
