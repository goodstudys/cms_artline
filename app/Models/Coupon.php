<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'kode', 'catorgory_id', 'discAmount', 'discPercent', 'date_start', 'date_end', 'minOrder',
    ];

    public $timestamps = false;
}
