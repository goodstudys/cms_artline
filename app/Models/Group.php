<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'productId', 'type'
    ];

    public $timestamps = false;

    public function detail()
    {
        return $this->hasMany('App\Models\GroupDetail', 'groupId','id');
    }
}
