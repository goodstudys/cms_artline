<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Confirmation extends Model
{
    protected $fillable = [
        'tujuan', 'bank', 'akun', 'nama', 'jumlah',
    ];

    public $timestamps = false;
}
