<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
        'category', 'title', 'subtitle', 'article', 'image', 'video',
    ];

    public $timestamps = false;
}
