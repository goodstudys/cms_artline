<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'userId','orderCode','billFirst', 'billLast', 'billCompany', 'billProvince', 'billCity', 'billAddress', 'billKecamatan', 'billKelurahan', 'billPhone', 'billKodePos', 'billEmail', 'billNotes',
        'deliveryFirst', 'deliveryLast', 'deliveryCompany', 'deliveryProvince', 'deliveryCity', 'deliveryAddress', 'deliveryKecamatan', 'deliveryKelurahan', 'deliveryPhone', 'deliveryKodePos', 'status',
        'subtotal', 'total', 'discount', 'type',
    ];

    public $timestamps = false;

    public function detail() {
        return $this->hasMany('App\Models\OrderDetail', 'orderId','id');
    }
}
