<?php

namespace App\Imports;

use App\Models\Product;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Ramsey\Uuid\Uuid;

class ProductImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $id = Product::max('id') + 1;

        return new Product([
            'id' => $id,
            'sku' => $row[0],
            'name' => $row[1],
            'dateCreated' => Carbon::now(),
            'rowPointer' => Uuid::uuid4()->getHex(),
            'isActive' => 1,
            'isObsolete' => 0
            ]);
    }
}
