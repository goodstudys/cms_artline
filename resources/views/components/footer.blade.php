        <!-- Footer Start -->
        <footer class="footer footer-3">
            <div class="footer-top pt--80 pt-md--60">
                <div class="container-fluid">
                    <div class="row footer-row">
                        <div class="footer-column footer-column-1 order-first mb-xl--35 mb-md--30 mb-sm--20">
                            <!-- Widget Start Here -->
                            <div class="footer-widget text-center text-sm-left">
                                <div class="textwidget mb--30 mb-sm--20">
                                    <a href="{{ Route('index') }}" class="footer-logo">
                                        <img src="{{asset('assets/img/logo/logo.png')}}" alt="Logo" class="logo-img">
                                    </a>
                                </div>
                                <div class="textwidget mr--40 mr-sm--0">
                                    <p>Integer ut ligula quis lectus fringilla elementum porttitor sed est. Duis fringilla efficitur ligula sed lobortis.</p>
                                </div>
                            </div>
                            <!-- Widget End Here -->
                        </div>
                        <div class="footer-column footer-column-2 order-xl-2 order-3 mb-sm--30 mb-xs--25">
                            <!-- Widget Start Here -->
                            <div class="footer-widget text-center text-sm-left">
                                <h3 class="widget-title mb--25 mb-xs--20">Helful Link</h3>
                                <ul class="widget-menu">
                                    <li><a href="{{ Route('account') }}">Payment Confirmation</a></li>
                                    {{-- <li><a href="#">Size Guide</a></li> --}}
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="{{ Route('faqs') }}">F.A.Q</a></li>
                                </ul>
                            </div>
                            <!-- Widget End Here -->
                        </div>
                        <div class="footer-column footer-column-3 order-xl-3 order-4 mb-sm--30">
                            <!-- Widget Start Here -->
                            <div class="footer-widget text-center text-sm-left">
                                <h3 class="widget-title mb--25">Company</h3>
                                <ul class="widget-menu">
                                    <li><a href="{{ Route('about') }}">About Us</a></li>
                                    <li><a href="#">Dropship Program</a></li>
                                    <li><a href="#">Work for Artline</a></li>
                                    <li><a href="{{ Route('contact') }}">Contact Us</a></li>
                                </ul>
                            </div>
                            <!-- Widget End Here -->
                        </div>
                        <div class="footer-column footer-column-4 order-xl-4 order-5 mb-xs--25">
                            <!-- Widget Start Here -->
                            <div class="footer-widget text-center text-sm-left">
                                <h3 class="widget-title mb--25">Work Hour</h3>
                                <ul class="widget-menu">
                                    <li><span>Mon - Sat (7:00 – 22:00)</span></li>
                                    <li><span>Sun (9:00 – 20:00)</span></li>
                                </ul>
                            </div>
                            <!-- Widget End Here -->
                        </div>
                        <div class="footer-column footer-column-5 order-xl-5 order-6">
                            <!-- Contact Widget Start Here -->
                            <div class="footer-widget text-center text-sm-left">
                                <h3 class="widget-title mb--25 mb-xs--20">Contact Info</h3>
                                <div class="widget_contact_info">
                                    <ul>
                                        <li><i class="fa fa-phone"></i><span>+62.31.745.88899, +62.81.223.889988</span></li>
                                        <li><i class="fa fa-envelope"></i><a href="mailto:info@artline.com">support@artline.co.id</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Contact Widget End Here -->
                        </div>
                        <div class="footer-column footer-column-6 order-xl-6 order-2 mb-xl--35 mb-md--30 mb-xs--25">
                            <div class="footer-widget text-center text-sm-left">
                                <h3 class="widget-title mb--25 mb-xs--20">Newsletter</h3>
                                <!-- Subscribe widget Start Here -->
                                <div class="subscribe-widget mb--30">
                                    <form action="{{ route('newsletter.store') }}" method="POST" enctype="multipart/form-data" class="form form--account">
                                        @csrf
                                        <div class="row mb--20">
                                            <div class="col-12">
                                                <div class="form__group">
                                                    <input type="email" name="newsletter" id="newsletter" class="form__input">
                                                    @if (Session::has('status'))
                                                        <p>{{ session('message') }}</p>
                                                    @endif

                                                    @if (count($errors) > 0)

                                                        @foreach ($errors->all() as $error)

                                                            <p>{{ $error }}</p>

                                                        @endforeach

                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form__group">
                                                    <input type="submit" value="Submit" class="btn btn-style-1 btn-submit">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <!-- Subscribe widget End Here -->

                                <!-- Social Icons Start Here -->
                                <ul class="social social-medium">
                                    <li class="social__item">
                                        <a href="https://twitter.com" class="social__link">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li class="social__item">
                                        <a href="https://plus.google.com" class="social__link">
                                            <i class="fa fa-google-plus"></i>
                                        </a>
                                    </li>
                                    <li class="social__item">
                                        <a href="https://facebook.com" class="social__link">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li class="social__item">
                                        <a href="https://youtube.com" class="social__link">
                                            <i class="fa fa-youtube"></i>
                                        </a>
                                    </li>
                                    <li class="social__item">
                                        <a href="https://instagram.com" class="social__link">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                </ul>
                                <!-- Social Icons End Here -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-bottom pt--45 pb--50 pb-xs--45">
                <div class="container">
                    <div class="row">
                        <div class="col-12 text-center">
                            <p class="copyright-text">&copy; 2020 ARTLINE. Developed by <a href="http://mygoodnews.id/"> GoodNews</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer End -->
