<form class="form" method="post" action="{{route('sendEmail')}}">
    @csrf
    <div class="form__group mb--20">
        <input type="text" id="contact_name" name="name" class="form__input" placeholder="Your name*" required>
    </div>
    <div class="form__group mb--20">
        <input type="email" id="contact_email" name="email" class="form__input" placeholder="Email Address*" required>
    </div>
    <div class="form__group mb--20">
        <input type="text" id="contact_phone" name="phone" class="form__input" placeholder="Your Phone*" required>
    </div>
    <div class="form__group mb--20">
        <textarea class="form__input form__input--textarea" id="contact_message" name="message" placeholder="Message*"
            required></textarea>
    </div>
    <div class="form__group">
        <button type="submit" class="btn btn-submit btn-large btn-shape-round btn-style-1">Send</button> </div>
    <div class="form__output"></div>
</form>