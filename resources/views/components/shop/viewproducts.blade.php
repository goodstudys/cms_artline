<div class="shop-products">
    <div class="row xl-block-grid-5 sm-block-grid-2 grid-space-20">
        @foreach ($products as $item)
        <div class="col-xl-3 col-lg-4 col-sm-6 ptb--10 mb--20">
            <div class="charoza-product thumb-has-effect">
                <div class="product-inner">
                    <figure class="product-image">
                        <div class="product-image--holder">
                            <a href="{{ route('shop.show', $item->rowPointer) }}">
                                <img src="{{ asset('/upload/users/products/'. $item->image1) }}" alt="Product Image"
                                    class="primary-image">
                                <img src="{{ asset('/upload/users/products/'. $item->image2) }}" alt="Product Image"
                                    class="secondary-image">
                            </a>
                        </div>
                        <div class="charoza-product-action">
                            <div class="product-action d-flex flex-column align-items-end">
                                <a class="quickview-btn action-btn" data-toggle="tooltip" data-placement="top" title=""
                                    data-original-title="Quick Shop">
                                    <span data-toggle="modal" data-target="#productModal{{$item->id}}">
                                        <i class="dl-icon-view"></i>
                                    </span>
                                </a>
{{--                                <a class="add_wishlist action-btn" href="{{ Route('wishlist') }}" data-toggle="tooltip"--}}
{{--                                    data-placement="top" title="" data-original-title="Add to Wishlist">--}}
{{--                                    <i class="dl-icon-heart4"></i>--}}
{{--                                </a>--}}
                                {{-- <a class="add_compare action-btn" href="#" data-toggle="tooltip" data-placement="top"
                                    title="" data-original-title="Add to Compare">
                                    <i class="dl-icon-compare"></i>
                                </a> --}}
                                <form method="post" class="form--action">
                                    @csrf
                                    <div class="product-action d-flex align-items-center">
                                        <input type="hidden" class="quantity-input" name="qty" id="qty" value="1"
                                            min="1">

                                        <input type="hidden" name="default" value="{{ $item->rowPointer }}">
                                        <input type="hidden" name="row" value="{{ $item->rowPointer }}">
                                        <input type="hidden" name="name" value="{{ $item->name }}">
                                        <input type="hidden" name="colorss" id="colorss" value="{{$item->colorHex}}">
                                        <input type="hidden" name="colorname" id="colorname" value="{{$item->colorName}}">
                                        <input type="hidden" name="wholesale_price" value="{{ $item->wholesale_price }}">
                                        <input type="hidden" name="category" value="{{ $item->categoryName }}">
                                        @if ($item->discAmount === '0' && $item->discPercent === '0')
                                        <input class="money" type="hidden" name="prices" value="{{ $item->price, 2 }}">
                                        @elseif($item->discAmount === '0')
                                        <input class="money" type="hidden" name="prices"
                                            value="{{ $item->price - ($item->price*$item->discPercent/100), 2 }}">
                                        @elseif($item->discPercent === '0')
                                        <input class="money" type="hidden" name="prices"
                                            value="{{ $item->price-$item->discAmount, 2 }}">
                                        @endif

                                    </div>
                                    <a data-toggle="tooltip" data-placement="top" title=""
                                        data-original-title="Add to Cart">
                                        <button type="submit" class="add_to_cart_btn action-btn"
                                            formaction="{{ route('cart.store') }}" data-toggle="tooltip"
                                            data-placement="top" title="" data-original-title="Add to Cart">

                                            <i class="dl-icon-cart29"></i>

                                        </button>
                                    </a>
                                </form>
                                {{-- <a class="add_to_cart_btn action-btn" href="{{Route('cart')}}"
                                data-toggle="tooltip"
                                data-placement="top" title="" data-original-title="Add to Cart">
                                <i class="dl-icon-cart29"></i>
                                </a> --}}
                            </div>
                        </div>
                    </figure>
                    <div class="product-info text-center">
                        {{-- <div class="star-rating star-three">
                            <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                        </div> --}}
                        <h3 class="product-title">
                            <a href="{{ route('shop.show', $item->rowPointer) }}">{{$item->name}}</a>
                        </h3>
                        <span class="product-price-wrapper">
                            @if ($item->discAmount === '0' && $item->discPercent === '0')
                            <span class="product-price">
                                <span class="money" name="price">Rp.{{number_format($item->price, 2)}}</span>
                            </span>
                            @elseif($item->discAmount === '0')
                            <span class="product-price-old">
                                <span class="money" name="price">Rp.{{number_format($item->price, 2)}}</span>
                            </span>
                            <span class="product-price-new">
                                <span class="money"
                                    name="price">Rp.{{number_format($item->price - ($item->price*$item->discPercent/100), 2)}}</span>
                            </span>
                            @elseif($item->discPercent === '0')
                            <span class="product-price-old">
                                <span class="money" name="price">Rp.{{number_format($item->price, 2)}}</span>
                            </span>
                            <span class="product-price-new">
                                <span class="money"
                                    name="price">Rp.{{number_format($item->price-$item->discAmount, 2)}}</span>
                            </span>
                            @endif
                        </span>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
