<div class="col-lg-3 order-lg-1 mt--25 mt-md--40" id="primary-sidebar">
    <div class="sidebar-widget">
        <div class="product-widget tag-widget mb--35 mb-md--30">
            {{-- <form action="{{route('filter.price')}}" method="get">--}}
            <h3 class="widget-title">Categories</h3>
            <div class="tagcloud">
                @foreach ($categories as $item)

                {{-- <a href="{{route('filter.product',['data'=>$koleksi])}}" class="category"
                style="border:1px solid #ccc">{{ $item->name }}
                </a> --}}

                {{-- <input style="display: none" type="hidden" name="data" value="{{$item->name}}" id=""> --}}
                <form style="display: inline" @if (empty($subkategori))
                    action="{{route('filter.price',['kode'=>$item->id,'sub'=> 'kosong'])}}" @else
                    action="{{route('filter.price',['kode'=>$item->id,'sub'=> $subkategori])}}" @endif method="get">
                    <button class="category" type="submit" id="{{$item->id}}" @if (empty($kategori) || $kategori
                        !=$item->id)
                        style="font-size: 11px;
                        border: 1px solid #ccc;
                        padding: 5px 15px;
                        display: inline-block;
                        background-color:white;
                        margin: 0 0 5px;
                        border-radius: 30px;
                        line-height: 1.7;"
                        @else
                        style="font-size: 11px;
                        border: 1px solid #3f89f5;
                        padding: 5px 15px;
                        display: inline-block;
                        background-color:white;
                        margin: 0 0 5px;
                        border-radius: 30px;
                        line-height: 1.7;"
                        @endif >{{ $item->name }}</button>
                </form>
                @endforeach
                {{-- <input type="hidden" id="kode" name="kode" value="1"> --}}
                {{-- <input style="display: none" type="hidden" name="data" value="{{$koleksi}}" id=""> --}}
            </div>

        </div>
        <div class="product-widget tag-widget mb--35 mb-md--30">
            <h3 class="widget-title">Sub Categories</h3>
            <div class="tagcloud">
                @foreach ($subcategories as $item)
                {{-- <a href='{{$item->id}}' class="subcategory">{{ $item->name }}
                </a> --}}
                <form style="display: inline" @if (empty($kategori))
                    action="{{route('filter.price',['kode'=>'kosong','sub'=> $item->id])}}" @else
                    action="{{route('filter.price',['kode'=>$kategori,'sub'=> $item->id])}}" @endif method="get">
                    <button class="subcategory" type="submit" id="{{$item->id}}" @if (empty($subkategori) ||
                        $subkategori !=$item->id)
                        style="font-size: 11px;
                        border: 1px solid #ccc;
                        padding: 5px 15px;
                        display: inline-block;
                        background-color:white;
                        margin: 0 0 5px;
                        border-radius: 30px;
                        line-height: 1.7;"
                        @else
                        style="font-size: 11px;
                        border: 1px solid #3f89f5;
                        padding: 5px 15px;
                        display: inline-block;
                        background-color:white;
                        margin: 0 0 5px;
                        border-radius: 30px;
                        line-height: 1.7;"
                        @endif >{{ $item->name }}</button>
                </form>
                @endforeach
                {{-- <input type="hidden" id="sub" name="sub" value=""> --}}
            </div>
        </div>
        <div class="product-widget product-price-widget mb--40 mb-md--35">
            <form style="display: inline" @if (empty($subkategori) && empty($kategori))
                action="{{route('filter.price',['kode'=>'kosong','sub'=> 'kosong'])}}" @elseif(!empty($subkategori) &&
                empty($kategori)) action="{{route('filter.price',['kode'=>'kosong','sub'=> $subkategori])}}"
                @elseif(empty($subkategori) && !empty($kategori))
                action="{{route('filter.price',['kode'=>$kategori,'sub'=> 'kosong'])}}" @else
                action="{{route('filter.price',['kode'=>$kategori,'sub'=> $subkategori])}}" @endif method="get">
                <h3 class="widget-title">Price</h3>
                <div class="widget_content">
                    <div id="slider-range"
                        class="price-slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                        <div class="ui-slider-range ui-widget-header ui-corner-all">
                        </div>
                        <span class="ui-slider-handle ui-state-default ui-corner-all">
                        </span>
                        <span class="ui-slider-handle ui-state-default ui-corner-all">
                        </span>
                    </div>
                    <div class="filter-price">
                        <div class="filter-price__count">
                            <div class="filter-price__input-group">
                                <span>Price: </span>
                                <input type="text" id="amount" class="amount-range" name="amount" readonly="">
                                <input type="hidden" id="min-range" @if (empty($min)) value="0" @else value={{$min}}
                                    @endif name="minamount">
                                <input type="hidden" id="max-range" @if (empty($max)) value="1000000" @else
                                    value={{$max}} @endif name="maxamount">
                            </div>
                            <button type="submit" class="btn btn-style-1 btn-shape-round sidebar-btn">
                                filter
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        {{-- <form action="{{ route('filter.price') }}" method="get">
        <!-- Category Widget Start -->
        <div class="product-widget tag-widget mb--35 mb-md--30">
            <h3 class="widget-title">Categories</h3>
            <div class="tagcloud">
                @foreach ($categories as $item)

                <a href='{{route('filter.product',$products)}}' class="category"
                    style="border:1px solid #ccc">{{ $item->name }}
                </a>
                @endforeach
                <input type="hidden" id="kode" name="kode" value="">
            </div>
        </div>
        <div class="product-widget tag-widget mb--35 mb-md--30">
            <h3 class="widget-title">Sub Categories</h3>
            <div class="tagcloud">
                @foreach ($subcategories as $item)
                <a href='{{$item->id}}' class="subcategory">{{ $item->name }}
                </a>
                @endforeach
                <input type="hidden" id="sub" name="sub" value="">
            </div>
        </div>
        <!-- Price Filter Widget Start -->
        <div class="product-widget product-price-widget mb--40 mb-md--35">
            <h3 class="widget-title">Price</h3>
            <div class="widget_content">


                <div id="slider-range"
                    class="price-slider ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                    <div class="ui-slider-range ui-widget-header ui-corner-all">

                    </div>
                    <span class="ui-slider-handle ui-state-default ui-corner-all">

                    </span>
                    <span class="ui-slider-handle ui-state-default ui-corner-all">

                    </span>
                </div>
                <div class="filter-price">
                    <div class="filter-price__count">
                        <div class="filter-price__input-group">
                            <span>Price: </span>
                            <input type="text" id="amount" class="amount-range" name="amount" readonly="">
                            <input type="hidden" id="min-range" name="minamount">
                            <input type="hidden" id="max-range" name="maxamount">
                        </div>
                        <button type="submit" class="btn btn-style-1 btn-shape-round sidebar-btn">
                            filter
                        </button>
                    </div>
                </div>
                </form>
            </div>--}}
        </div>
        <!-- Price Filter Widget End -->

        <!-- Promo Widget Start -->
        <div class="product-widget promo-widget">
            <div class="banner-box banner-type-3 banner-type-3-2 banner-hover-3">
                <div class="banner-inner">
                    <div class="banner-image">
                        <img src="{{asset('assets/img/banner/shop_banner_sidebar_01.jpg')}}" alt="Banner">
                    </div>
                    <div class="banner-info">
                        <div class="banner-info--inner">
                            <p class="banner-title-2 font-normal color--white">Man Fashion</p>
                            <p class="banner-title-7 color--white">Gravity</p>
                        </div>
                    </div>
                    <a class="banner-link banner-overlay" href="{{ Route('shop') }}">Shop Now</a>
                </div>
            </div>
        </div>
        <!-- Promo Widget End -->
    </div>
</div>