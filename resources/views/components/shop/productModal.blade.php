@foreach ($products as $item)
<div class="modal fade product-modal" id="productModal{{$item->id}}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="dl-icon-close"></i></span>
                </button>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="charoza-element-carousel product-image-carousel nav-vertical-center nav-style-1"
                            data-slick-options='{
                                              "slidesToShow": 1,
                                              "slidesToScroll": 1,
                                              "arrows": true,
                                              "prevArrow": {"buttonClass": "slick-btn slick-prev", "iconClass": "dl-icon-left" },
                                              "nextArrow": {"buttonClass": "slick-btn slick-next", "iconClass": "dl-icon-right" }
                                          }'>
                            <div class="product-image">
                                <div class="product-image--holder">
                                    <a href="{{ route('shop.show', $item->rowPointer) }}">
                                        <img src="{{ asset('/upload/users/products/'. $item->image1) }}"
                                            alt="Product Image" class="primary-image">
                                    </a>
                                </div>
                                @if ($item->discAmount !=='0' || $item->discPercent !=='0')
                                <span class="product-badge sale">Sale</span>
                                @endif
                            </div>
                            <div class="product-image">
                                <div class="product-image--holder">
                                    <a href="{{ route('shop.show', $item->rowPointer) }}">
                                        <img src="{{ asset('/upload/users/products/'. $item->image2) }}"
                                            alt="Product Image" class="primary-image">
                                    </a>
                                </div>
                                @if ($item->discAmount !=='0' || $item->discPercent !=='0')
                                <span class="product-badge sale">Sale</span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="modal-box product-summary">
                            <div
                                class="product-top-meta d-flex justify-content-between flex-sm-row flex-column-reverse">
                                <div class="product-rating d-flex">
                                    <div class="star-rating star-five">
                                        <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                                    </div>
                                    {{--                                        <a href="" class="review-link">({{ $ratings->count() }}
                                    customer review)</a>--}}
                                </div>
                                <div class="product-navigation">
                                    <span data-toggle="modal" data-dismiss="modal"
                                        data-target="#productModal{{$item->id-1}}">
                                        <i class="dl-icon-left"></i>
                                    </span>
                                    <span data-toggle="modal" data-dismiss="modal"
                                        data-target="#productModal{{$item->id+1}}">
                                        <i class="dl-icon-right"></i>
                                    </span>
                                    {{-- <a href="#" class="prev"><i class="dl-icon-left"></i></a> --}}
                                    {{-- <a href="#" class="next"><i class="dl-icon-right"></i></a> --}}
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <h3 class="product-title mb--30 mb-md--20">{{$item->name}}</h3>
                            <div class="product-price-wrapper mb--30 mb-md--20">
                                @if ($item->discAmount === '0' && $item->discPercent === '0')
                                <span class="product-price-new">
                                    <span class="money" name="price">Rp.{{number_format($item->price, 2)}}</span>
                                    @if (Auth::check() == 1)
                                    @if (Auth::user()->level != 3)
                                    <div class="row">
                                        <div class="col-12">
                                            <span name="wholesale_price" id="singleprice" style="font-size: 12pt">Harga
                                                Grosir Rp.{{number_format($item->wholesale_price, 2)}}</span>
                                        </div>
                                    </div>
                                    @endif
                                    @endif
                                </span>
                                @elseif($item->discAmount === '0')
                                <span class="product-price-old">
                                    <span class="money" name="price">Rp.{{number_format($item->price, 2)}}</span>
                                    @if (Auth::check() == 1)
                                    @if (Auth::user()->level != 3)
                                    <div class="row">
                                        <div class="col-12">
                                            <span name="wholesale_price" id="singleprice" style="font-size: 12pt">
                                                {{-- Harga
                                                Grosir Rp.{{number_format($item->wholesale_price, 2)}} --}}
                                            </span>
                                        </div>
                                    </div>
                                    @endif
                                    @endif
                                </span>
                                <span class="product-price-new">
                                    <span class="money"
                                        name="price">Rp.{{number_format($item->price - ($item->price*$item->discPercent/100), 2)}}</span>
                                    {{-- @if (Auth::check() == 1)
                                    @if (Auth::user()->level != 3)
                                    <div class="row">
                                        <div class="col-12">
                                            <span name="wholesale_price" id="singleprice" style="font-size: 12pt">Harga
                                                Grosir Rp.{{number_format($item->wholesale_price, 2)}}</span>
                                        </div>
                                    </div>
                                    @endif
                                    @endif --}}
                                </span>
                                <span class="product-price-new">
                                    @if (Auth::check() == 1)
                                    @if (Auth::user()->level != 3)
                                    <div class="row">
                                        <div class="col-12">
                                            <span name="wholesale_price" id="singleprice" style="font-size: 12pt">Harga
                                                Grosir Rp.{{number_format($item->wholesale_price, 2)}}</span>
                                        </div>
                                    </div>
                                    @endif
                                    @endif
                                </span>
                                @elseif($item->discPercent === '0')
                                <span class="product-price-old">
                                    <span class="money" name="price">Rp.{{number_format($item->price, 2)}}</span>
                                    @if (Auth::check() == 1)
                                    @if (Auth::user()->level != 3)
                                    <div class="row">
                                        <div class="col-12">
                                            <span name="wholesale_price" id="singleprice" style="font-size: 12pt">
                                                {{-- Harga
                                                Grosir Rp.{{number_format($item->wholesale_price, 2)}} --}}
                                            </span>
                                        </div>
                                    </div>
                                    @endif
                                    @endif
                                </span>
                                <span class="product-price-new">
                                    <span class="money"
                                        name="price">Rp.{{number_format($item->price-$item->discAmount, 2)}}</span>
                                    
                                </span>
                                <span class="product-price-new">
                                    @if (Auth::check() == 1)
                                    @if (Auth::user()->level != 3)
                                    <div class="row">
                                        <div class="col-12">
                                            <span name="wholesale_price" id="singleprice" style="font-size: 12pt">Harga
                                                Grosir Rp.{{number_format($item->wholesale_price, 2)}}</span>
                                        </div>
                                    </div>
                                    @endif
                                    @endif
                                </span>
                                @endif
                            </div>
                            <p class="product-short-description mb--30 mb-md--20">{!! $item->description !!}</p>
                            <div class="product-action d-flex flex-row align-items-center mb--20">
                                <form action="{{ route('cart.store') }}" method="post" class="form--action">
                                    @csrf
                                    <div class="product-action d-flex align-items-center">
                                        <div class="quantity">
                                            <input type="number" class="quantity-input" name="qty" id="qty" value="1"
                                                min="1">
                                        </div>
                                        <input type="hidden" name="default" value="{{ $item->rowPointer }}">
                                        <input type="hidden" name="row" value="{{ $item->rowPointer }}">
                                        <input type="hidden" name="name" value="{{ $item->name }}">
                                        <input type="hidden" name="colorss" id="colorss" value="{{$item->colorHex}}">
                                        <input type="hidden" name="colorname" id="colorname"
                                            value="{{$item->colorName}}">
                                        <input type="hidden" name="wholesale_price"
                                            value="{{ $item->wholesale_price }}">
                                        @if ($item->discAmount === '0' && $item->discPercent === '0')
                                        <input class="money" type="hidden" name="prices" value="{{ $item->price, 2 }}">
                                        @elseif($item->discAmount === '0')
                                        <input class="money" type="hidden" name="prices"
                                            value="{{ $item->price - ($item->price*$item->discPercent/100), 2 }}">
                                        @elseif($item->discPercent === '0')
                                        <input class="money" type="hidden" name="prices"
                                            value="{{ $item->price-$item->discAmount, 2 }}">
                                        @endif
                                        {{--                                <button type="submit" class="btn btn-shape-round btn-style-1 btn-large add-to-cart">--}}
                                        {{--                                    Add To Cart--}}
                                        {{--                                </button>--}}
                                        <input type="submit" value="Add To Cart"
                                            class="btn btn-shape-round btn-style-1 btn-large add-to-cart">
                                        {{--                                            <a href="{{ Route('wishlist') }}"
                                        class="action-btn action-btn-square"><i--}}
                                            {{--                                                    class="dl-icon-heart2"></i></a>--}}
                                            {{--                                            <a href="#" class="action-btn action-btn-square"><i class="dl-icon-compare2"></i></a>--}}
                                            </div> </form> </div> <div class="product-extra mb--20">
                                            {{--                                    <a href="#" class="font-size-12"><i class="fa fa-map-marker"></i>Find store near--}}
                                            {{--                                        you</a>--}}
                                            {{--                                    <a href="#" class="font-size-12"><i class="fa fa-exchange"></i>Delivery and--}}
                                            {{--                                        return</a>--}}
                                    </div>
                                    <div class="product-summary-footer">
                                        <div class="product-meta mb--20">
                                            <span class="sku_wrapper font-size-12">SKU: <span
                                                    class="sku">{{$item->sku}}</span></span>
                                            <span class="posted_in font-size-12">Categories: <a
                                                    href="{{ Route('shop') }}"
                                                    rel="tag">{{$item->categoryName}}</a></span>
                                        </div>
                                        <div class="product-share-box">
                                            <span class="font-size-12">Share With</span>
                                            <!-- Social Icons Start Here -->
                                            <ul class="social social-small">
                                                <li class="social__item">
                                                    <a href="facebook.com" class="social__link">
                                                        <i class="fa fa-facebook"></i>
                                                    </a>
                                                </li>
                                                <li class="social__item">
                                                    <a href="twitter.com" class="social__link">
                                                        <i class="fa fa-twitter"></i>
                                                    </a>
                                                </li>
                                                <li class="social__item">
                                                    <a href="plus.google.com" class="social__link">
                                                        <i class="fa fa-google-plus"></i>
                                                    </a>
                                                </li>
                                                <li class="social__item">
                                                    <a href="plus.google.com" class="social__link">
                                                        <i class="fa fa-pinterest-p"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                            <!-- Social Icons End Here -->
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach