<div class="row pt--65 pt-md--50 pt-sm--30 pb--75 pb-md--55 pb-sm--35 mt--3 mt-md--2">
    <div class="col-12">
        <div class="row mb--40 mb-sm--30">
            <div class="col-12 text-center">
                <h2 class="heading-secondary">Related Products</h2>
                <hr class="separator separator-3 separator-color-2 center mt--25 mt-md--15">
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="charoza-element-carousel nav-vertical-center nav-style-1" data-slick-options='{
                "spaceBetween": 30,
                "slidesToShow": 4,
                "slidesToScroll": 1,
                "arrows": true,
                "prevArrow": {"buttonClass": "slick-btn slick-prev", "iconClass": "dl-icon-left" },
                "nextArrow": {"buttonClass": "slick-btn slick-next", "iconClass": "dl-icon-right" }
                }' data-slick-responsive='[
                    {"breakpoint":1200, "settings": {"slidesToShow": 3} },
                    {"breakpoint":991, "settings": {"slidesToShow": 2} },
                    {"breakpoint":450, "settings": {"slidesToShow": 1} }
                ]'>
                    @foreach ($related as $item)
                    <div class="item">
                        <div class="charoza-product thumb-has-effect">
                            <div class="product-inner">
                                <figure class="product-image">
                                    <div class="product-image--holder">
                                        <a href="{{ route('shop.show', $item->rowPointer) }}">
                                            <img src="{{ asset('/upload/users/products/'. $item->image1) }}" alt="Product Image"
                                                class="primary-image">
                                            <img src="{{ asset('/upload/users/products/'. $item->image2) }}" alt="Product Image"
                                                class="secondary-image">
                                        </a>
                                    </div>
                                    <div class="charoza-product-action">
                                        <div class="product-action d-flex flex-column align-items-end">
                                            <a class="quickview-btn action-btn" data-toggle="tooltip"
                                                data-placement="top" title="" data-original-title="Quick Shop">
                                                <span data-toggle="modal" data-target="#productModal{{$item->id}}">
                                                    <i class="dl-icon-view"></i>
                                                </span>
                                            </a>
                                            {{-- <a class="add_wishlist action-btn" href="{{ Route('wishlist') }}"
                                                data-toggle="tooltip" data-placement="top" title=""
                                                data-original-title="Add to Wishlist">
                                                <i class="dl-icon-heart4"></i>
                                            </a>
                                            <a class="add_compare action-btn" href="#" data-toggle="tooltip"
                                                data-placement="top" title="" data-original-title="Add to Compare">
                                                <i class="dl-icon-compare"></i>
                                            </a> --}}
                                            <form method="post" class="form--action">
                                                @csrf
                                                <div class="product-action d-flex align-items-center">
                                                    <input type="hidden" class="quantity-input" name="qty" id="qty"
                                                        value="1" min="1">
                                                    <input type="hidden" name="row" value="{{ $item->rowPointer }}">
                                                    <input type="hidden" name="name" value="{{ $item->name }}">
                                                    @if ($item->discAmount === '0' && $item->discPercent === '0')
                                                    <input class="money" type="hidden" name="prices"
                                                        value="{{ $item->price, 2 }}">
                                                    @elseif($item->discAmount === '0')
                                                    <input class="money" type="hidden" name="prices"
                                                        value="{{ $item->price - ($item->price*$item->discPercent/100), 2 }}">
                                                    @elseif($item->discPercent === '0')
                                                    <input class="money" type="hidden" name="prices"
                                                        value="{{ $item->price-$item->discAmount, 2 }}">
                                                    @endif

                                                </div>
                                                <a data-toggle="tooltip" data-placement="top" title=""
                                                    data-original-title="Add to Cart">
                                                    <button type="submit" class="add_to_cart_btn action-btn"
                                                        formaction="{{ route('cart.store') }}" data-toggle="tooltip"
                                                        data-placement="top" title="" data-original-title="Add to Cart">

                                                        <i class="dl-icon-cart29"></i>

                                                    </button>
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </figure>
                                <div class="product-info text-center">
                                    {{-- <div class="star-rating star-five">
                                        <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                                    </div> --}}
                                    <h3 class="product-title">
                                        <a href="{{ route('shop.show', $item->rowPointer) }}">{{$item->name}}</a>
                                    </h3>
                                    <span class="product-price-wrapper">
                                        @if ($item->discAmount === '0' && $item->discPercent === '0')
                                        <span class="product-price">
                                            <span class="money" name="price">Rp.{{number_format($item->price, 2)}}</span>
                                        </span>
                                        @elseif($item->discAmount === '0')
                                        <span class="product-price-old">
                                            <span class="money" name="price">Rp.{{number_format($item->price, 2)}}</span>
                                        </span>
                                        <span class="product-price-new">
                                            <span class="money"
                                                name="price">Rp.{{number_format($item->price - ($item->price*$item->discPercent/100), 2)}}</span>
                                        </span>
                                        @elseif($item->discPercent === '0')
                                        <span class="product-price-old">
                                            <span class="money" name="price">Rp.{{number_format($item->price, 2)}}</span>
                                        </span>
                                        <span class="product-price-new">
                                            <span class="money"
                                                name="price">Rp.{{number_format($item->price-$item->discAmount, 2)}}</span>
                                        </span>
                                        @endif
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
