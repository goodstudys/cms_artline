<div class="comment-respond">
    <h3 class="comment-reply-title">Leave a Reply</h3>
    <form action="#" class="form comment-form">
        <div class="form-notes mb--20">
            <p><em>Your email address will not be published. Required fields are marked</em>
                <span class="required">*</span></p>
        </div>
        <div class="form__group mb--30 mb-sm--20">
            <div class="form-row">
                <div class="col-md-4 mb-sm--20">
                    <label class="form__label form__label--3" for="comment_name">Name<span
                            class="required">*</span></label>
                    <input type="text" name="comment_name" id="comment_name" class="form__input">
                </div>
                <div class="col-md-4 mb-sm--20">
                    <label class="form__label form__label--3" for="comment_email">Email<span
                            class="required">*</span></label>
                    <input type="email" name="comment_email" id="comment_email" class="form__input">
                </div>
                <div class="col-md-4">
                    <label class="form__label form__label--3" for="comment_website">Website</label>
                    <input type="url" name="comment_website" id="comment_website" class="form__input">
                </div>
            </div>
        </div>
        <div class="form__group mb--30 mb-sm--20">
            <div class="form-row">
                <div class="col-12">
                    <label class="form__label form__label--3" for="review">Your Review<span
                            class="required">*</span></label>
                    <textarea name="review" id="review" class="form__input form__input--textarea"></textarea>
                </div>
            </div>
        </div>
        <div class="form__group">
            <div class="form-row">
                <div class="col-12">
                    <input type="submit" value="Submit" class="btn btn-style-1 btn-submit">
                </div>
            </div>
        </div>
    </form>
</div>