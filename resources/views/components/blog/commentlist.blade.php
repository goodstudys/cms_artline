<ul class="comment-list">
    <li>
        <div class="single-comment">
            <div class="comment-avatar">
                <img src="{{asset('assets/img/others/comment-1.jpg')}}" alt="comment">
            </div>
            <div class="comment-info">
                <div class="comment-meta">
                    <h5 class="comment-author"><a href="#">Julia Rebeca</a></h5>
                    <span class="comment-date">30 Janurary, 2019</span>
                    <a href="" class="reply">Reply</a>
                </div>
                <div class="comment-content">
                    <p>enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut
                        fugit, sed quia cntur magn lores eos qui ratione voluptatem
                        sequi nesciunt. Neque porro</p>
                </div>
            </div>
        </div>
        <ul class="children">
            <li>
                <div class="single-comment">
                    <div class="comment-avatar">
                        <img src="{{asset('assets/img/others/comment-2.jpg')}}" alt="comment">
                    </div>
                    <div class="comment-info">
                        <div class="comment-meta">
                            <h5 class="comment-author"><a href="#">Admin</a></h5>
                            <span class="comment-date">30 Janurary, 2019</span>
                            <a href="" class="reply">Reply</a>
                        </div>
                        <div class="comment-content">
                            <p>enim ipsam voluptatem quia voluptas sit aspernatur aut
                                odit aut fugit, sed quia cntur magn lores eos qui
                                ratione voluptatem sequi nesciunt. Neque porro</p>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </li>
    <li>
        <div class="single-comment">
            <div class="comment-avatar">
                <img src="{{asset('assets/img/others/comment-3.jpg')}}" alt="comment">
            </div>
            <div class="comment-info">
                <div class="comment-meta">
                    <h5 class="comment-author"><a href="#">Julia Rebeca</a></h5>
                    <span class="comment-date">30 Janurary, 2019</span>
                    <a href="" class="reply">Reply</a>
                </div>
                <div class="comment-content">
                    <p>enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut
                        fugit, sed quia cntur magn lores eos qui ratione voluptatem
                        sequi nesciunt. Neque porro</p>
                </div>
            </div>
        </div>
    </li>
</ul>