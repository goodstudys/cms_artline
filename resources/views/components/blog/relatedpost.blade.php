<div class="container">
    <div class="row pt--60 pt-md--45 pt-sm--25 pb--80 pb-md--60 pb-sm--40">
        <div class="col-12">
            <div class="row mb--35">
                <div class="col-12 text-center">
                    <h2 class="heading-secondary">Related Post</h2>
                    <hr class="separator center mt--25 mt-md--20">
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="charoza-element-carousel related-post" data-slick-options='{
                                "spaceBetween": 30,
                                "slidesToShow": 3,
                                "slidesToScroll": 3
                            }' data-slick-responsive='[
                                {"breakpoint":991, "settings": {"slidesToShow": 2} },
                                {"breakpoint":479, "settings": {"slidesToShow": 1} }
                            ]'>
                        @foreach ($blogs as $item)
                        <div class="item">
                            <article class="post">
                                <div class="post-media">
                                    <figure class="image text-center">
                                        <a href="{{ route('blogs.show', $item->blogrowPointer) }}">
                                            <img src="{{ asset('/upload/users/blogs/'. $item->blogImage) }}" alt="Post">
                                            <span class="link-overlay"></span>
                                        </a>
                                    </figure>
                                </div>
                                <div class="post-info">
                                    <div class="post-meta category-link">
                                        <a href="{{ Route('blogs') }}">{{$item->blogCategory}}</a>
                                    </div>
                                    <h3 class="post-title"><a href="{{ route('blogs.show', $item->blogrowPointer) }}">{{$item->blogTitle}}</a></h3>
                                    <div class="post-meta d-flex align-items-center justify-content-center mb--20">
                                        <a href="{{ Route('blogs') }}" class="posted-on">
                                            <span class="entry-date">{{Carbon\Carbon::parse($item->blogDate)->format('M d,  Y ')}}</span>
                                        </a>
                                        <span class="meta-separator">-</span>
                                        <a href="{{ Route('blogs') }}" class="posted-by">by {{$item->userDisplayname}}</a>
                                    </div>
                                    <a href="{{ route('blogs.show', $item->blogrowPointer) }}" class="read-more">Read More <i
                                            class="fa fa-caret-right"></i></a>
                                </div>
                            </article>
                        </div>
                        @endforeach
<!--
                        <div class="item">
                            <article class="post">
                                <div class="post-media">
                                    <figure class="image text-center">
                                        <a href="{{ Route('blog-detail') }}">
                                            <img src="{{asset('assets/img/blog/blog_beuty.jpg')}}" alt="Post">
                                            <span class="link-overlay"></span>
                                        </a>
                                    </figure>
                                </div>
                                <div class="post-info">
                                    <div class="post-meta category-link">
                                        <a href="{{ Route('blogs') }}">Beauty</a>
                                    </div>
                                    <h3 class="post-title"><a href="{{ Route('blog-detail') }}">10 ways care
                                            your skin in home</a></h3>
                                    <div class="post-meta d-flex align-items-center justify-content-center mb--20">
                                        <a href="{{ Route('blogs') }}" class="posted-on">
                                            <span class="entry-date">September 15, 2019</span>
                                        </a>
                                        <span class="meta-separator">-</span>
                                        <a href="{{ Route('blogs') }}" class="posted-by">by admin</a>
                                    </div>
                                    <a href="{{ Route('blog-detail') }}" class="read-more">Read More <i
                                            class="fa fa-caret-right"></i></a>
                                </div>
                            </article>
                        </div>
                        <div class="item">
                            <article class="post">
                                <div class="post-media">
                                    <figure class="image text-center">
                                        <a href="{{ Route('blog-detail') }}">
                                            <img src="{{asset('assets/img/blog/blog-9.jpg')}}" alt="Post">
                                            <span class="link-overlay"></span>
                                        </a>
                                    </figure>
                                </div>
                                <div class="post-info">
                                    <div class="post-meta category-link">
                                        <a href="{{ Route('blogs') }}">Beauty</a>
                                    </div>
                                    <h3 class="post-title"><a href="{{ Route('blog-detail') }}">10 ways care
                                            your skin in home</a></h3>
                                    <div class="post-meta d-flex align-items-center justify-content-center mb--20">
                                        <a href="{{ Route('blogs') }}" class="posted-on">
                                            <span class="entry-date">September 15, 2019</span>
                                        </a>
                                        <span class="meta-separator">-</span>
                                        <a href="{{ Route('blogs') }}" class="posted-by">by admin</a>
                                    </div>
                                    <a href="{{ Route('blog-detail') }}" class="read-more">Read More <i
                                            class="fa fa-caret-right"></i></a>
                                </div>
                            </article>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
