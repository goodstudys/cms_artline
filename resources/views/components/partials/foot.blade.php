<!-- ************************* JS Files ************************* -->

<!-- All Plugins Js -->
<script src="{{asset('assets/js/plugins.js')}}"></script>

<!-- Main JS -->
<script src="{{asset('assets/js/main.js')}}"></script>

<!-- REVOLUTION JS FILES -->
<script src="{{asset('assets/js/revoulation/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('assets/js/revoulation/jquery.themepunch.revolution.min.js')}}"></script>

<!-- REVOLUTION ACTIVE JS FILES -->
<script src="{{asset('assets/js/revoulation.js')}}"></script>
<script src="{{asset('assets/cart/js/util.js')}}"></script> <!-- util functions included in the CodyHouse framework -->
<script src="{{asset('assets/cart/js/main.js')}}"></script>