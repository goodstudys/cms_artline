        <!-- ************************* CSS Files ************************* -->
        <link rel="stylesheet" href="{{asset('assets/cart/css/style.css')}}">
        <!-- All Plugins CSS  -->
        <link rel="stylesheet" href="{{asset('assets/css/plugins.css')}}">

        <!-- Revoulation CSS  -->
        <link rel="stylesheet" href="{{asset('assets/css/revoulation.css')}}">

        <!-- style CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/main.css')}}">

        <!-- custom style CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">

        <!-- modernizr JS
        ============================================ -->
        <script src="{{asset('assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>
        <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js')}}"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js')}}"></script>
        <![endif]-->
