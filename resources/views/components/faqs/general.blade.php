<div class="row mb--40 mb-md--20 mb-sm--10">
    @foreach ($general as $index => $item)
    <div style="display: none">{{$urutan = $index+1}}</div>
    @if ($urutan %2 == 0)
    <div class="col-lg-6">
        <div class="accordion__single mb--30 mb-sm--20">
            <div class="accordion__header" id="heading{{$item->id}}">
                <h4 class="accordion__link" data-target="#accordion{{$item->id}}">
                    {{$item->title}}
                </h4>
            </div>
            <div id="accordion{{$item->id}}" class="accordion__body hide-in-default">
                <div class="accordion__text">
                    {{$item->description}}
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="col-lg-6">
        <div class="accordion__single mb--30 mb-sm--20">
            <div class="accordion__header" id="heading{{$item->id}}">
                <h4 class="accordion__link" data-target="#accordion{{$item->id}}">
                    {{$item->title}}
                </h4>
            </div>
            <div id="accordion{{$item->id}}" class="accordion__body hide-in-default">
                <div class="accordion__text">
                    {{$item->description}}
                </div>
            </div>
        </div>

    </div>
    @endif
    @endforeach
</div>