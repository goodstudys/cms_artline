<div class="user-actions user-actions__coupon">
    <div class="message-box mb--30 mb-sm--20">
        <p><i class="fa fa-exclamation-circle"></i> Have A Coupon? <a class="expand-btn" href="#coupon_info">Click Here To Enter Your Code.</a></p>
    </div>
    <div id="coupon_info" class="user-actions__form hide-in-default">
        <form action="#" class="form">
            <p>If you have a coupon code, please apply it below.</p>
            <div class="form__group d-sm-flex">
                <input type="text" name="coupon" id="coupon" class="form__input form__input--2 mr--20 mr-xs--0" placeholder="Coupon Code">
                <button type="submit" class="btn btn-medium btn-style-1">Apply Coupon</button>
            </div>
        </form>
    </div>
</div>