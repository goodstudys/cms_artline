<div class="col-lg-6">
    <div class="checkout-title mt--10">
        <h2>Alamat Penagihan</h2>

    </div>
    @if (Session::has('status'))
    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
    @endif

    @if (count($errors) > 0)

    <div class="alert alert-danger">

        <strong>Whoops!</strong> There were some problems with your input.

        <ul>

            @foreach ($errors->all() as $error)

            <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

    @endif
    @if ($option == '1')
    <div>

        <form action="{{ route('checkout') }}" method="get" class="form form--checkout">
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="label" class="form__label form__label--2">Label <span class="required">*</span></label>
                    <div class="row">
                        <div id="opsi1label" class="col-8">
                            <select id="label" name="label" class="form__input form__input--2 nice-select bacot">
                                <option value="Manual">Input Manual</option>
                                @foreach($addresses as $address)
                                <option value="{{ $address->rowPointer }}"
                                    {{ ( $reqaddr->rowPointer == $address->rowPointer ) ? ' selected' : '' }}>
                                    {{ $address->label }} </option>
                                @endforeach
                            </select>
                        </div>
                        @foreach ($datas as $item)
                        <span style="display: none">{{$totalberat = 0}}</span>
                        <span
                            style="display: none">{{ $perweight = $item->attributes->weight * $item->quantity }}</span>
                        <span style="display: none">{{ $allweight = $totalberat += $perweight  }}</span>
                        <span style="display: none">{{ Session::put('weightRow',  $allweight)  }}</span>
                        @endforeach
                        <div class="col-4">
                            <a href="{{ url('/address/edit/shopping/'. $reqaddr->rowPointer) }}"
                                class="btn btn-fullwidth btn-style-1"> Edit </a>
                        </div>
                    </div>
                </div>
            </div>
            <button type="submit" class="btn btn-fullwidth btn-style-1" id="labelsubmit" style="display: none;">hidden
                button</button>
        </form>
    </div>
    <div class="checkout-form">
        <form action="{{ route('order.store') }}" method="post" class="form form--checkout">
            @csrf
            <input type="hidden" name="ongkir" value="{{ Session::get('ongkir') }}">
            <input type="hidden" name="gettotal" id="subtotalorderinput">
            @if (Auth::check() == 1)
            @if (Auth::user()->level != 3)
            @if (Request::get('reseller') == 'reseller')
            <div class="custom-checkbox mb--20">
                <input type="checkbox" name="reseller" id="reseller" class="form__checkbox" value="reseller" checked>

                <label for="reseller" class="form__label form__label--2 shipping-label">Pesan Sebagai Reseller </label>
            </div>
            @else
            <div class="custom-checkbox mb--20">
                <input type="checkbox" name="reseller" id="reseller" class="form__checkbox" value="reseller">

                <label for="reseller" class="form__label form__label--2 shipping-label">Pesan Sebagai Reseller </label>
            </div>
            @endif
            @endif
            @endif
            <div class="form-row mb--30">
                @if ($addresses->count() != 0)
                <div class="form__group col-12">
                    <label for="billing_fname" class="form__label form__label--2">Nama <span
                            class="required">*</span></label>
                    <input type="text" name="billName" id="billing_fname" class="form__input form__input--2"
                        value="{{ $reqaddr->name }}" required>
                </div>
                @endif
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_company" class="form__label form__label--2">Nama perusahaan (Optional)</label>
                    <input type="text" name="billCompany" id="billing_company" class="form__input form__input--2">
                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_province" class="form__label form__label--2">Provinsi <span
                            class="required">*</span></label>
                    <input type="hidden" id="provinceRow" name="provinceRow" readonly>
                    <input type="hidden" id="provinceRow" name="billProvince" value="$reqaddr->province" readonly>
                    <select id="billing_province" name="billProvincetest"
                        class="form__input form__input--2 nice-select provinces" disabled>
                        @if ($provinces != 'Connection problem')
                        <option value="">Pilih provinsi…</option>
                        @foreach($provinces['rajaongkir']['results'] as $province)
                        <option value="{{ $province['province_id'] }}"
                            {{ ( $reqaddr->province == $province['province_id'] ) ? ' selected' : '' }}>
                            {{ $province['province'] }} </option>
                        @endforeach
                        @else
                        <option value="">Internet connection problem</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_streetAddress" class="form__label form__label--2">Alamat <span
                            class="required">*</span></label>

                    <input type="text" name="billAddress" id="billing_streetAddress"
                        class="form__input form__input--2 mb--30" placeholder="House number and street name"
                        value="{{ $reqaddr->address }}" required>

                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_city" class="form__label form__label--2">Kota <span
                            class="required">*</span></label>
                    <input type="hidden" id="cityRow" name="cityRow">
                    <select id="billing_city2" name="billCity"
                        class="form__input form__input--2 citys nice-select appendcity" readonly="" disabled>
                        @if ($citys != 'Connection problem')
                        <option value="">Pilih kota…</option>
                        @foreach($citys['rajaongkir']['results'] as $city)
                        <option value="{{ $city['city_id'] }}"
                            {{ ( $reqaddr->city == $city['city_id'] ) ? ' selected' : '' }}>{{ $city['city_name'] }}
                        </option>
                        @endforeach
                        @else
                        <option value="">Internet connection problem</option>
                        @endif
                    </select>
                </div>
            </div>
            {{-- <input type="text" name="billcityname" id="placecitynamehere2" class="d-none" readonly> --}}
            @foreach ($citys['rajaongkir']['results'] as $city)
                @if ($reqaddr->city == $city['city_id'])
                    <input type="text" name="billcityname" value="{{ $city['city_name'] }}" id="placecitynamehere2" class="d-none" readonly>
                @endif
            @endforeach
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_state" class="form__label form__label--2">Kecamatan <span
                            class="required">*</span></label>
                    <input type="text" name="billKecamatan" id="billing_state" class="form__input form__input--2"
                        value="{{ $reqaddr->kecamatan }}" readonly required>
                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_state" class="form__label form__label--2">Kelurahan <span
                            class="required">*</span></label>
                    <input type="text" name="billKelurahan" id="billing_state" class="form__input form__input--2"
                        value="{{ $reqaddr->kelurahan }}" readonly required>
                </div>
            </div>
            <div  class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_state" class="form__label form__label--2">Total Order <span
                            class="required">*</span></label>
                    <input type="text" name="total_order" id="total_order" class="form__input form__input--2" value="{{ $totalorder = Session::get('getTotal') + Session::get('ongkir')  }}">
                </div>
            </div>
            <div  class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_state" class="form__label form__label--2">Total Order Reseller<span
                            class="required">*</span></label>
                    <input type="text" name="total_order_reseller" id="total_order_reseller"
                        class="form__input form__input--2" value="{{ $totalorder = Session::get('getTotal') + Session::get('ongkir') }}">
                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_state" class="form__label form__label--2">Kode Pos <span
                            class="required">*</span></label>
                    <input type="text" name="billKodePos" id="billing_state" class="form__input form__input--2"
                        value="{{ $reqaddr->kodePos }}" readonly required>
                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_phone" class="form__label form__label--2">Telepon <span
                            class="required">*</span></label>
                    <input type="text" name="billPhone" id="billing_phone" class="form__input form__input--2"
                        value="{{ $reqaddr->phone }}" required>
                    <input type="hidden" id="paymentmethod" value="1" name="payment">
                    @if (Request::get('reseller') == 'reseller')
                    <input type="hidden" id="totalorders1" name="totalorders">
                    @else
                    <input type="hidden" id="totalorders" name="totalorders">
                    @endif
                    <input type="hidden" id="totalqtys" name="totalqtys" value="{{ Session::get('qtys')  }}">
                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_email" class="form__label form__label--2">Alamat email </label>
                    <input type="email" name="billEmail" id="billing_email" class="form__input form__input--2">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="form-row">
                <div class="form__group col-12">
                    <div class="row">
                        <div class="col-6 ml-2">
                            {{-- <div class="custom-checkbox mb--20">
                                <input type="checkbox" name="savealamat" id="savealamat" class="form__checkbox">

                                <label for="savealamat" class="form__label form__label--2 shipping-label">Simpan Alamat
                                </label>
                            </div> --}}
                            <div class="custom-checkbox mb--20">
                                <input type="checkbox" name="shipdifferetads" id="shipdifferetadsisi"
                                    class="form__checkbox">
                                <input style="display: none" type="text" name="kirimdropship" id="kirimdropshipopsi1">
                                <label for="shipdifferetads" class="form__label form__label--2 shipping-label">Kirim
                                    sebagai Dropshipper</label>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="custom-checkbox mb--20">
                                <input type="checkbox" name="kodebooking" id="kodebooking" class="form__checkbox">

                                <label for="kodebooking" class="form__label form__label--2 shipping-label">Kode
                                    Booking</label>
                            </div>
                        </div>
                    </div>
                    <div class="kode-box-info hide-in-default mt--30">
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="shipping_fname" class="form__label form__label--2">Kode Booking <span
                                        class="required">*</span></label>
                                <input type="text" name="kodeBooking" id="kodeBooking"
                                    class="form__input form__input--2">
                            </div>
                        </div>
                    </div>
                    <div class="ship-box-info-isi hide-in-default mt--30">
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="shipping_fname" class="form__label form__label--2">Nama<span
                                        class="required">*</span></label>
                                <input type="text" name="deliveryName" id="delivery_name"
                                    class="form__input form__input--2">
                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_company" class="form__label form__label--2">Nama perusahaan
                                    (Optional)</label>
                                <input type="text" name="deliveryCompany" id="delivery_company"
                                    class="form__input form__input--2">
                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_province" class="form__label form__label--2">Provinsi <span
                                        class="required">*</span></label>
                                <input type="hidden" id="deliveryprovinceRow" name="deliveryprovinceRow" readonly>
                                <select id="delivery_province" name="deliveryProvince"
                                    class="form__input form__input--2 nice-select deliveryprovinces">
                                    @if ($provinces != 'Connection problem')
                                    <option value="">Pilih provinsi…</option>
                                    @foreach($provinces['rajaongkir']['results'] as $province)
                                    <option value="{{ $province['province_id'] }}">{{ $province['province'] }} </option>
                                    @endforeach
                                    @else
                                    <option value="">Internet connection problem</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_streetAddress" class="form__label form__label--2">Alamat <span
                                        class="required">*</span></label>

                                <input type="text" name="deliveryAddress" id="delivery_streetAddress"
                                    class="form__input form__input--2 mb--30" placeholder="House number and street name"
                                    value="">

                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_city" class="form__label form__label--2">Kota <span
                                        class="required">*</span></label>
                                <input type="hidden" id="deliverycityRow" name="deliverycityRow">
                                <select id="deliverycalc_shipping_city" name="deliveryCity"
                                    class="select-custom deliverycitys appendcity" disabled>
                                    <option value="">Pilih provinsi terlebih dahulu…</option>
                                </select>
                            </div>
                        </div>
                        <input type="text" name="cityname" id="deliveryplacecitynamehere" class="d-none" readonly>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_state" class="form__label form__label--2">Kecamatan <span
                                        class="required">*</span></label>
                                <input type="text" name="deliveryKecamatan" id="delivery_kecamatan"
                                    class="form__input form__input--2" value="">
                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_state" class="form__label form__label--2">Kelurahan <span
                                        class="required">*</span></label>
                                <input type="text" name="deliveryKelurahan" id="delivery_kelurahan"
                                    class="form__input form__input--2" value="">
                            </div>
                        </div>

                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_state" class="form__label form__label--2">Kode Pos <span
                                        class="required">*</span></label>
                                <input type="text" name="deliveryKodePos" id="delivery_kodepos"
                                    class="form__input form__input--2" value="">
                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_phone" class="form__label form__label--2">Telepon <span
                                        class="required">*</span></label>
                                <input type="text" name="deliveryPhone" id="delivery_phone"
                                    class="form__input form__input--2" value="">
                                <input type="hidden" id="paymentmethod" value="1" name="payment">
                                @if (Request::get('reseller') == 'reseller')
                                <input type="hidden" id="totalorders1" name="totalorders1">
                                <input type="hidden" id="totalorders1Deliv" name="totalorders1Deliv">
                                @else
                                <input type="hidden" id="totalorders" name="totalorders">
                                <input type="hidden" id="totalordersDeliv" name="totalordersDeliv">
                                @endif
                                <input type="hidden" id="totalqtys" name="totalqtys"
                                    value="{{ Session::get('qtys')  }}">
                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_email" class="form__label form__label--2">Alamat email </label>
                                <input type="email" name="deliveryEmail" id="delivery_email"
                                    class="form__input form__input--2">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div style="display: none" class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="billing_state" class="form__label form__label--2">Total Order <span
                                        class="required">*</span></label>
                                <input type="text" name="total_order_deliv" id="total_order_deliv"
                                    class="form__input form__input--2" value="">
                            </div>
                        </div>
                        <div style="display: none" class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="billing_state" class="form__label form__label--2">Total Order Reseller<span
                                        class="required">*</span></label>
                                <input type="text" name="total_order_reseller_deliv" id="total_order_reseller_deliv"
                                    class="form__input form__input--2" value="">
                            </div>
                        </div>
                        <div style="display: none" class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="billing_state" class="form__label form__label--2">Ongkir Deliv<span
                                        class="required">*</span></label>
                                <input type="text" name="ongkir_deliv" id="ongkir_deliv"
                                    class="form__input form__input--2" value="{{Session::get('ongkirDeliv')}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form__group col-12">
                    <label for="orderNotes" class="form__label form__label--2">Pesan</label>
                    <textarea class="form__input form__input--2 form__input--textarea" id="orderNotes" name="billNotes"
                        placeholder="Pesan terkait order."></textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-fullwidth btn-style-1" id="orderbutton" style="display: none;">Place
                Order</button>
        </form>
    </div>
    @else
    <div>
        <form action="{{ route('checkout') }}" method="get" class="form form--checkout">
            @csrf
            <div class="form-row mb--30">
                
                <div id="opsi2label" class="form__group col-12">
                    <label for="label" class="form__label form__label--2">Label <span class="required">*</span></label>
                    <select id="label" name="label" class="form__input form__input--2 nice-select bacot">
                        <option value="Manual" selected>Input Manual</option>
                        @foreach($addresses as $address)
                        <option value="{{ $address->rowPointer }}">{{ $address->label }} </option>
                        @endforeach
                    </select>
                </div>
                
                {{-- <div id="y">
                    <select id="label" name="label" class="form__input form__input--2 nice-select bacot">
                        <option>O1</option>
                        <option>O2</option>
                    </select>
                </div> --}}
            </div>
            <button type="submit" class="btn btn-fullwidth btn-style-1" id="labelsubmitopsi2" style="display: none;">hidden
                button</button>
        </form>
    </div>
    <div class="checkout-form">
        <form action="{{ route('order.store') }}" method="post" class="form form--checkout">
            @csrf
            <input type="hidden" name="ongkir" id="ongkirinput">
            <input type="hidden" name="ongkirDeliv" id="ongkirinputDeliv">
            <input style="display: none" type="text" class="form__label form__label--2 shipping-label" name="gettotal"
                id="subtotalorderinput">
            @if (Auth::check() == 1)
            @if (Auth::user()->level != 3)
            @if (Request::get('reseller') == 'reseller')
            <div class="custom-checkbox mb--20">
                <input type="checkbox" name="reseller" id="reseller" class="form__checkbox" value="reseller" checked>

                <label for="reseller" class="form__label form__label--2 shipping-label">Pesan Sebagai Reseller </label>
            </div>
            @else
            <div class="custom-checkbox mb--20">
                <input type="checkbox" name="reseller" id="reseller" class="form__checkbox" value="reseller">

                <label for="reseller" class="form__label form__label--2 shipping-label">Pesan Sebagai Reseller </label>
            </div>
            @endif
            @endif
            @endif
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_fname" class="form__label form__label--2">Nama <span
                            class="required">*</span></label>
                    <input type="text" name="billName" id="billing_fname" class="form__input form__input--2" value=""
                        required>
                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_company" class="form__label form__label--2">Nama perusahaan (Optional)</label>
                    <input type="text" name="billCompany" id="billing_company" class="form__input form__input--2">
                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_province" class="form__label form__label--2">Provinsi <span
                            class="required">*</span></label>
                    <input type="hidden" id="provinceRow" name="provinceRow" readonly>
                    <select id="billing_province" name="billProvince"
                        class="form__input form__input--2 nice-select provinces" required>
                        @if ($provinces != 'Connection problem')
                        <option value="">Pilih provinsi…</option>
                        @foreach($provinces['rajaongkir']['results'] as $province)
                        <option value="{{ $province['province_id'] }}">{{ $province['province'] }} </option>
                        @endforeach
                        @else
                        <option value="">Internet connection problem</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_streetAddress" class="form__label form__label--2">Alamat <span
                            class="required">*</span></label>

                    <input type="text" name="billAddress" id="billing_streetAddress"
                        class="form__input form__input--2 mb--30" placeholder="House number and street name" value=""
                        required>

                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_city" class="form__label form__label--2">Kota <span
                            class="required">*</span></label>
                    <input type="hidden" id="cityRow" name="cityRow">
                    <select id="calc_shipping_city" name="billCity" class="select-custom citys appendcity" disabled
                        required>
                        <option value="">Pilih provinsi terlebih dahulu…</option>
                    </select>
                </div>
            </div>
            {{-- <input type="text" name="cityname" id="placecitynamehere" class="d-none" readonly> --}}
            <div style="display: none" class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_state" class="form__label form__label--2">kotanya <span
                            class="required">*</span></label>
                    <input type="text" name="billcityname" id="billcityname" class="form__input form__input--2"
                        value="">
                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_state" class="form__label form__label--2">Kecamatan <span
                            class="required">*</span></label>
                    <input type="text" name="billKecamatan" id="billing_state" class="form__input form__input--2"
                        value="" required>
                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_state" class="form__label form__label--2">Kelurahan <span
                            class="required">*</span></label>
                    <input type="text" name="billKelurahan" id="billing_state" class="form__input form__input--2"
                        value="" required>
                </div>
            </div>

            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_state" class="form__label form__label--2">Kode Pos <span
                            class="required">*</span></label>
                    <input type="text" name="billKodePos" id="billing_state" class="form__input form__input--2" value=""
                        required>
                </div>
            </div>
            <div style="display: none" class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_state" class="form__label form__label--2">Total Order <span
                            class="required">*</span></label>
                    <input type="text" name="total_order" id="total_order" class="form__input form__input--2" value="">
                </div>
            </div>
            <div style="display: none" class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_state" class="form__label form__label--2">Total Order Reseller<span
                            class="required">*</span></label>
                    <input type="text" name="total_order_reseller" id="total_order_reseller"
                        class="form__input form__input--2" value="">
                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_phone" class="form__label form__label--2">Telepon <span
                            class="required">*</span></label>
                    <input type="text" name="billPhone" id="billing_phone" class="form__input form__input--2" value=""
                        required>
                    <input type="hidden" id="paymentmethod" value="1" name="payment">
                    @if (Request::get('reseller') == 'reseller')
                    <input style="display: none" type="text" id="totalorders1" class="form__input form__input--2"
                        name="totalorders1">
                    <input style="display: none" type="text" id="totalorders1Deliv" class="form__input form__input--2"
                        name="totalorders1Deliv">
                    @else
                    <input style="display: none" type="text" id="totalorders" class="form__input form__input--2"
                        value="" name="totalorders">
                    <input style="display: none" type="text" id="totalordersDeliv" class="form__input form__input--2"
                        value="" name="totalordersDeliv">
                    @endif
                    <input style="display: none" type="text" id="totalqtys" name="totalqtys"
                        value="{{ Session::get('qtys')  }}">
                </div>
            </div>
            <div class="form-row mb--30">
                <div class="form__group col-12">
                    <label for="billing_email" class="form__label form__label--2">Alamat email </label>
                    <input type="email" name="billEmail" id="billing_email" class="form__input form__input--2">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="form-row">
                <div class="form__group col-12">
                    <div class="row">
                        <div class="col-6 ml-2">
                            <div class="custom-checkbox mb--20">
                                <input type="checkbox" name="savealamat" id="savealamat" class="form__checkbox">

                                <label for="savealamat" class="form__label form__label--2 shipping-label">Simpan Alamat
                                </label>
                            </div>
                            <div class="custom-checkbox mb--20">
                                <input type="checkbox" name="shipdifferetads" id="shipdifferetads"
                                    class="form__checkbox">
                                <input style="display: none" type="text" name="kirimdropship" id="kirimdropship">
                                <label for="shipdifferetads" class="form__label form__label--2 shipping-label">Kirim
                                    sebagai Dropshipper</label>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="custom-checkbox mb--20">
                                <input type="checkbox" name="kodebooking" id="kodebooking" class="form__checkbox">

                                <label for="kodebooking" class="form__label form__label--2 shipping-label">Kode
                                    Booking</label>
                            </div>
                        </div>
                    </div>
                    <div class="kode-box-info hide-in-default mt--30">
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="shipping_fname" class="form__label form__label--2">Kode Booking <span
                                        class="required">*</span></label>
                                <input type="text" name="kodeBooking" id="kodeBooking"
                                    class="form__input form__input--2">
                            </div>
                        </div>
                    </div>
                    <div class="ship-box-info hide-in-default mt--30">
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="shipping_fname" class="form__label form__label--2">Nama<span
                                        class="required">*</span></label>
                                <input type="text" name="deliveryName" id="delivery_name"
                                    class="form__input form__input--2">
                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_company" class="form__label form__label--2">Nama perusahaan
                                    (Optional)</label>
                                <input type="text" name="deliveryCompany" id="delivery_company"
                                    class="form__input form__input--2">
                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_province" class="form__label form__label--2">Provinsi <span
                                        class="required">*</span></label>
                                <input type="hidden" id="deliveryprovinceRow" name="deliveryprovinceRow" readonly>
                                <select id="delivery_province" name="deliveryProvince"
                                    class="form__input form__input--2 nice-select deliveryprovinces">
                                    @if ($provinces != 'Connection problem')
                                    <option value="">Pilih provinsi…</option>
                                    @foreach($provinces['rajaongkir']['results'] as $province)
                                    <option value="{{ $province['province_id'] }}">{{ $province['province'] }} </option>
                                    @endforeach
                                    @else
                                    <option value="">Internet connection problem</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_streetAddress" class="form__label form__label--2">Alamat <span
                                        class="required">*</span></label>

                                <input type="text" name="deliveryAddress" id="delivery_streetAddress"
                                    class="form__input form__input--2 mb--30" placeholder="House number and street name"
                                    value="">

                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_city" class="form__label form__label--2">Kota <span
                                        class="required">*</span></label>
                                <input type="hidden" id="deliverycityRow" name="deliverycityRow">
                                <select id="deliverycalc_shipping_city" name="deliveryCity"
                                    class="select-custom deliverycitys appendcity" disabled>
                                    <option value="">Pilih provinsi terlebih dahulu…</option>
                                </select>
                            </div>
                        </div>
                        <input type="text" name="cityname" id="deliveryplacecitynamehere" class="d-none" readonly>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_state" class="form__label form__label--2">Kecamatan <span
                                        class="required">*</span></label>
                                <input type="text" name="deliveryKecamatan" id="delivery_kecamatan"
                                    class="form__input form__input--2" value="">
                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_state" class="form__label form__label--2">Kelurahan <span
                                        class="required">*</span></label>
                                <input type="text" name="deliveryKelurahan" id="delivery_kelurahan"
                                    class="form__input form__input--2" value="">
                            </div>
                        </div>

                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_state" class="form__label form__label--2">Kode Pos <span
                                        class="required">*</span></label>
                                <input type="text" name="deliveryKodePos" id="delivery_kodepos"
                                    class="form__input form__input--2" value="">
                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_phone" class="form__label form__label--2">Telepon <span
                                        class="required">*</span></label>
                                <input type="text" name="deliveryPhone" id="delivery_phone"
                                    class="form__input form__input--2" value="">
                                <input type="hidden" id="paymentmethod" value="1" name="payment">
                                @if (Request::get('reseller') == 'reseller')
                                <input type="hidden" id="totalorders1" name="totalorders1">
                                <input type="hidden" id="totalorders1Deliv" name="totalorders1Deliv">
                                @else
                                <input type="hidden" id="totalorders" name="totalorders">
                                <input type="hidden" id="totalordersDeliv" name="totalordersDeliv">
                                @endif
                                <input type="hidden" id="totalqtys" name="totalqtys"
                                    value="{{ Session::get('qtys')  }}">
                            </div>
                        </div>
                        <div class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="delivery_email" class="form__label form__label--2">Alamat email </label>
                                <input type="email" name="deliveryEmail" id="delivery_email"
                                    class="form__input form__input--2">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div style="display: none" class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="billing_state" class="form__label form__label--2">Total Order <span
                                        class="required">*</span></label>
                                <input type="text" name="total_order_deliv" id="total_order_deliv"
                                    class="form__input form__input--2" value="">
                            </div>
                        </div>
                        <div style="display: none" class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="billing_state" class="form__label form__label--2">Total Order Reseller<span
                                        class="required">*</span></label>
                                <input type="text" name="total_order_reseller_deliv" id="total_order_reseller_deliv"
                                    class="form__input form__input--2" value="">
                            </div>
                        </div>
                        <div style="display: none" class="form-row mb--30">
                            <div class="form__group col-12">
                                <label for="billing_state" class="form__label form__label--2">Ongkir Deliv<span
                                        class="required">*</span></label>
                                <input type="text" name="ongkir_deliv" id="ongkir_deliv"
                                    class="form__input form__input--2" value="{{Session::get('ongkirDeliv')}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form__group col-12">
                    <label for="orderNotes" class="form__label form__label--2">Pesan</label>
                    <textarea class="form__input form__input--2 form__input--textarea" id="orderNotes"
                        name="deliveryNotes" placeholder="Pesan terkait order."></textarea>
                </div>
            </div>
            <button type="submit" class="btn btn-fullwidth btn-style-1" id="orderbutton" style="display: none;">Place
                Order</button>
        </form>
    </div>
    @endif
</div>