@if ($blog->count() != '0')
    <section class="blog-area pt--75 pt-md--55 pt-sm--35 pb--75 pb-md--55 pb-sm--35">
        <div class="container">
            <div class="row mb--35 mb-md--30 mb-sm--20">
                <div class="col-12 text-center">
                    <h4 class="text-uppercase font-size-18 color--tertiary lts-10 lts-md-5 lts-sm-2 mb--15 mb-md--10">
                        Artline/h4>
                    <h2 class="heading-secondary-2">Our Journal</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="charoza-element-carousel" data-slick-options='{
                    "spaceBetween": 30,
                    "adaptiveHeight": true,
                    "slidesToShow": 2,
                    "slidesToScroll": 1
                }' data-slick-responsive='[
                    {"breakpoint":991, "settings": {"slidesToShow": 1}}
                ]'>
                        @foreach ($blog as $item)
                            <div class="item">
                                <article class="post post-layout-2">
                                    <div class="post-media">
                                        <figure class="image text-center">
                                            <a href="{{ route('blogs.show', $item->rowPointer) }}">
                                                <img src="{{ asset('/upload/users/blogs/'. $item->image) }}" alt="Post">
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="post-info">
                                        <div class="post-meta category-link">
                                            <a href="{{ Route('blogs') }}">{{$item->category}}</a>
                                        </div>
                                        <h3 class="post-title"><a href="{{ route('blogs.show', $item->rowPointer) }}">{{$item->title}}</a></h3>
                                        <div class="post-footer-meta">
                                            <a href="{{ Route('blogs') }}" class="posted-on">
                                                <span class="date">{{Carbon\Carbon::parse($item->blogDate)->format('d')}}</span>
                                                <span class="month">{{Carbon\Carbon::parse($item->blogDate)->format('M')}}</span>
                                            </a>
                                        </div>
                                        <div class="post-content">
                                            <p>{!!  substr(strip_tags($item->subtitle), 0, 100) !!}</p>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
