<!-- Slider area Start -->
<div class="slider-area">
    <!-- START REVOLUTION SLIDER 5.4.7 auto mode -->
    <div id="rev_slider_8_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="home-08"
        data-source="gallery"
        style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
        <div id="rev_slider_8_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.7">
            <ul>
                @foreach ($sliders as $item)
                <!-- SLIDE  -->
                <li data-index="rs-5{{$item->id-1}}" data-transition="random-premium" data-slotamount="default"
                    data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default"
                    data-masterspeed="default" data-thumb="{{ asset('/upload/users/sliders/'. $item->image) }}" data-rotate="0"
                    data-saveperformance="off" data-title="0{{$item->id}}" data-param1="" data-param2="" data-param3=""
                    data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
                    data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ asset('/upload/users/sliders/'. $item->image) }}" alt="" data-bgposition="center center"
                        data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="2" class="rev-slidebg"
                        data-no-retina>
                    <!-- LAYERS -->

                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption     rev_group" id="slide-50-layer-1"
                        data-x="['left','left','center','center']" data-hoffset="['164','164','2','0']"
                        data-y="['bottom','bottom','middle','middle']" data-voffset="['119','119','6','0']"
                        data-width="['800','800','613','437']" data-height="['289','289','279','202']"
                        data-whitespace="nowrap" data-type="group" data-basealign="slide" data-responsive_offset="on"
                        data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                        data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                        data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                        data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                        style="z-index: 5; min-width: 800px; max-width: 800px; max-width: 289px; max-width: 289px; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: #ffffff; letter-spacing: 0px;">
                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption   tp-resizeme" id="slide-50-layer-2"
                            data-x="['left','left','center','center']" data-hoffset="['0','0','13','0']"
                            data-y="['top','top','middle','middle']" data-voffset="['0','0','-124','-61']"
                            data-fontsize="['24','24','22','18']" data-lineheight="['30','30','50','40']"
                            data-letterspacing="['14','14','14','8']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-type="text" data-responsive_offset="on"
                            data-frames='[{"delay":"+390","speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                            data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                            data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            style="z-index: 6; white-space: nowrap; font-size: 24px; line-height: 30px; font-weight: 400; color: #ffffff; letter-spacing: 14px;font-family:Poppins;">
                            {{$item->subtitle}}</div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption   tp-resizeme" id="slide-50-layer-3"
                            data-x="['left','left','center','center']" data-hoffset="['0','0','0','0']"
                            data-y="['middle','middle','middle','middle']" data-voffset="['-22','-22','-31','0']"
                            data-fontsize="['100','100','80','60']" data-lineheight="['120','120','100','60']"
                            data-width="none" data-height="none" data-whitespace="nowrap" data-type="text"
                            data-responsive_offset="on"
                            data-frames='[{"delay":"+790","speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                            data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                            data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]"
                            data-paddingleft="[0,0,0,0]"
                            style="z-index: 7; white-space: nowrap; font-size: 100px; line-height: 120px; font-weight: 400; color: #ffffff; letter-spacing: 0px;font-family:Prata;">
                            {{$item->title}}</div>

                        <!-- LAYER NR. 4 -->
                        <a class="tp-caption rev-btn " href="{{ url($item->link) }}" id="slide-50-layer-4"
                            data-x="['left','left','center','center']" data-hoffset="['0','0','0','0']"
                            data-y="['middle','middle','middle','middle']" data-voffset="['94','94','69','62']"
                            data-fontweight="['700','700','400','400']" data-width="none" data-height="none"
                            data-whitespace="nowrap" data-type="button" data-responsive_offset="on"
                            data-responsive="off"
                            data-frames='[{"delay":"+1510","speed":1500,"frame":"0","from":"x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"300","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(228,88,89);bs:solid;bw:0 0 0 0;"}]'
                            data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]" data-marginbottom="[0,0,0,0]"
                            data-marginleft="[0,0,0,0]" data-textAlign="['inherit','inherit','inherit','inherit']"
                            data-paddingtop="[16,16,15,12]" data-paddingright="[45,45,45,35]"
                            data-paddingbottom="[16,16,15,12]" data-paddingleft="[45,45,45,35]"
                            style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 20px; font-weight: 700; color: #282828;font-family:Poppins;background-color:rgb(255,255,255);border-color:rgba(0,0,0,1);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">{{$item->button}}</a>
                    </div>
                </li>
                @endforeach
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
    <!-- END REVOLUTION SLIDER -->
</div>
<!-- Slider area End -->
