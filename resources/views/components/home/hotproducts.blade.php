@if ($hotproducts->count() != '0')
    <section class="product-tab-area pt--75 pt-md--55 pt-sm--35 pb--80 pb-md--60 pb-sm--40">
        <div class="container-fluid">
            <div class="row mb--35 mb-md--30 mb-sm--5 pb-sm--2">
                <div class="col-12 text-center">
                    <h4 class="text-uppercase font-size-18 color--tertiary lts-10 lts-md-5 lts-sm-2 mb--15 mb-md--10">
                        ARTLINE INDONESIA</h4>
                    <h2 class="heading-secondary-2">Hot Deal</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="product-tab tab-style-1">
                        {{-- <div class="nav nav-tabs product-tab__head mb--30 mb-md--10" id="deal-product-tab" role="tablist">
                            <a class="product-tab__link nav-link active" id="nav-deal-all-tab" data-toggle="tab"
                                href="#nav-deal-all" role="tab" aria-selected="true">
                                <span>All Products</span>
                            </a>
                            <a class="product-tab__link nav-link" id="nav-deal-makeup-tab" data-toggle="tab"
                                href="#nav-deal-makeup" role="tab" aria-selected="true">
                                <span>Foundation Makeup</span>
                            </a>
                            <a class="product-tab__link nav-link" id="nav-deal-scrubs-tab" data-toggle="tab"
                                href="#nav-deal-scrubs" role="tab" aria-selected="true">
                                <span>Body Scrubs</span>
                            </a>
                            <a class="product-tab__link nav-link" id="nav-deal-masks-tab" data-toggle="tab"
                                href="#nav-deal-masks" role="tab" aria-selected="true">
                                <span>Masks</span>
                            </a>
                            <a class="product-tab__link nav-link" id="nav-deal-skin-tab" data-toggle="tab"
                                href="#nav-deal-skin" role="tab" aria-selected="true">
                                <span>Skin Care</span>
                            </a>
                            <a class="product-tab__link nav-link" id="nav-deal-hair-tab" data-toggle="tab"
                                href="#nav-deal-hair" role="tab" aria-selected="true">
                                <span>Hair Care</span>
                            </a>
                            <a class="product-tab__link nav-link" id="nav-deal-tool-tab" data-toggle="tab"
                                href="#nav-deal-tool" role="tab" aria-selected="true">
                                <span>Tool &amp; Accessories</span>
                            </a>
                            <a class="product-tab__link nav-link" id="nav-deal-foot-tab" data-toggle="tab"
                                href="#nav-deal-foot" role="tab" aria-selected="true">
                                <span>Foot, Hand &amp; Nail</span>
                            </a>
                        </div> --}}
                        <div class="tab-content product-tab__content" id="deal-product-tabcontent">
                            <div class="tab-pane fade show active" id="nav-deal-all-room" role="tabpanel"
                                 aria-labelledby="nav-deal-all-tab">
                                <div class="row xl-block-grid-5 grid-space-20">
                                    @foreach ($hotproducts as $item)
                                        <div class="col-xl-3 col-lg-4 col-sm-6 ptb--10 mb--20">
                                            <div class="charoza-product thumb-has-effect">
                                                <div class="product-inner">
                                                    <figure class="product-image">
                                                        <div class="product-image--holder">
                                                            <a href="{{ route('shop.show', $item->rowPointer) }}">
                                                                <img src="{{ asset('/upload/users/products/'. $item->image1) }}"
                                                                     alt="Product Image" class="primary-image">
                                                                <img src="{{ asset('/upload/users/products/'. $item->image2) }}"
                                                                     alt="Product Image" class="secondary-image">
                                                            </a>
                                                        </div>
                                                        <div class="charoza-product-action">
                                                            <div class="product-action d-flex flex-column align-items-end">
                                                                <a class="quickview-btn action-btn" data-toggle="tooltip"
                                                                   data-placement="top" title=""
                                                                   data-original-title="Quick Shop">
                                                            <span data-toggle="modal"
                                                                  data-target="#productModal{{$item->id}}hot">
                                                                <i class="dl-icon-view"></i>
                                                            </span>
                                                                </a>
                                                                {{-- <a class="add_wishlist action-btn"
                                                                    href="{{ Route('wishlist') }}"
         data-toggle="tooltip"
                                                                    data-placement="top" title=""
                                                                    data-original-title="Add to Wishlist">
                                                                    <i class="dl-icon-heart4"></i>
                                                                </a>
         <a class="add_compare action-btn" href="#" data-toggle="tooltip"
             data-placement="top" title=""
             data-original-title="Add to Compare">
             <i class="dl-icon-compare"></i>
         </a> --}}
                                                                <form method="post" class="form--action">
                                                                    @csrf
                                                                    <div class="product-action d-flex align-items-center">
                                                                        <input type="hidden" class="quantity-input" name="qty"
                                                                               id="qty" value="1" min="1">

                                                                        <input type="hidden" name="default" value="{{ $item->rowPointer }}">
                                                                        <input type="hidden" name="row" value="{{ $item->rowPointer }}">
                                                                        <input type="hidden" name="name" value="{{ $item->name }}">
                                                                        <input type="hidden" name="colorss" id="colorss" value="{{$item->colorHex}}">
                                                                        <input type="hidden" name="colorname" id="colorname" value="{{$item->colorName}}">
                                                                        <input type="hidden" name="wholesale_price" value="{{ $item->wholesale_price }}">
                                                                        <input type="hidden" name="category" value="{{ $item->category }}">
                                                                        @if ($item->discAmount === '0' && $item->discPercent ===
                                                                        '0')
                                                                            <input class="money" type="hidden" name="prices"
                                                                                   value="{{ $item->price, 2 }}">
                                                                        @elseif($item->discAmount === '0')
                                                                            <input class="money" type="hidden" name="prices"
                                                                                   value="{{ $item->price - ($item->price*$item->discPercent/100), 2 }}">
                                                                        @elseif($item->discPercent === '0')
                                                                            <input class="money" type="hidden" name="prices"
                                                                                   value="{{ $item->price-$item->discAmount, 2 }}">
                                                                        @endif

                                                                    </div>
                                                                    <a data-toggle="tooltip" data-placement="top" title=""
                                                                       data-original-title="Add to Cart">
                                                                        <button type="submit" class="add_to_cart_btn action-btn"
                                                                                formaction="{{ route('cart.store') }}"
                                                                                data-toggle="tooltip" data-placement="top" title=""
                                                                                data-original-title="Add to Cart">

                                                                            <i class="dl-icon-cart29"></i>

                                                                        </button>
                                                                    </a>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </figure>
                                                    <div class="product-info text-center">
                                                        {{-- <div class="product-countdown-wrap mb--20">
                                                           <div class="product-countdown product-countdown-3"
                                                               data-countdown="2020/05/01"></div>
                                                       </div>  --}}
                                                        {{-- <div class="star-rating star-five">
                                                            <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                                                        </div> --}}
                                                        <h3 class="product-title">
                                                            <a
                                                                href="{{ route('shop.show', $item->rowPointer) }}">{{$item->name}}</a>
                                                        </h3>
                                                        <span class="product-price-wrapper">
                                                    @if ($item->discAmount === '0' && $item->discPercent === '0')
                                                                <span class="product-price">
                                                        <span class="money"
                                                              name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                    </span>
                                                            @elseif($item->discAmount === '0')
                                                                <span class="product-price-old">
                                                        <span class="money"
                                                              name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                    </span>
                                                                <span class="product-price-new">
                                                        <span class="money"
                                                              name="price">Rp.{{number_format($item->price - ($item->price*$item->discPercent/100), 2)}}</span>
                                                    </span>
                                                            @elseif($item->discPercent === '0')
                                                                <span class="product-price-old">
                                                        <span class="money"
                                                              name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                    </span>
                                                                <span class="product-price-new">
                                                        <span class="money"
                                                              name="price">Rp.{{number_format($item->price-$item->discAmount, 2)}}</span>
                                                    </span>
                                                            @endif
                                                </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-deal-makeup" role="tabpanel"
                                 aria-labelledby="nav-deal-makeup-tab">
                                <div class="row xl-block-grid-5 grid-space-20">
                                    {{-- @foreach ($hotproducts as $item)
                                    <div class="col-xl-3 col-lg-4 col-sm-6 ptb--10 mb--20">
                                        <div class="charoza-product thumb-has-effect">
                                            <div class="product-inner">
                                                <figure class="product-image">
                                                    <div class="product-image--holder">
                                                        <a href="{{ route('shop.show', $item->rowPointer) }}">
                                                            <img src="/upload/users/products/{{$item->image1}}"
                                                                alt="Product Image" class="primary-image">
                                                            <img src="/upload/users/products/{{$item->image2}}"
                                                                alt="Product Image" class="secondary-image">
                                                        </a>
                                                    </div>
                                                    <div class="charoza-product-action">
                                                        <div class="product-action d-flex flex-column align-items-end">
                                                            <a class="quickview-btn action-btn" data-toggle="tooltip"
                                                                data-placement="top" title=""
                                                                data-original-title="Quick Shop">
                                                                <span data-toggle="modal"
                                                                    data-target="#productModal{{$item->id}}">
                                                                    <i class="dl-icon-view"></i>
                                                                </span>
                                                            </a>
                                                             <a class="add_wishlist action-btn"
                                                                href="{{ Route('wishlist') }}" data-toggle="tooltip"
                                                            data-placement="top" title=""
                                                            data-original-title="Add to Wishlist">
                                                            <i class="dl-icon-heart4"></i>
                                                            </a>
                                                            <a class="add_compare action-btn" href="#" data-toggle="tooltip"
                                                                data-placement="top" title=""
                                                                data-original-title="Add to Compare">
                                                                <i class="dl-icon-compare"></i>
                                                            </a>
                                                            <form method="post" class="form--action">
                                                                @csrf
                                                                <div class="product-action d-flex align-items-center">
                                                                    <input type="hidden" class="quantity-input" name="qty"
                                                                        id="qty" value="1" min="1">
                                                                    <input type="hidden" name="row"
                                                                        value="{{ $item->rowPointer }}">
                                                                    <input type="hidden" name="name"
                                                                        value="{{ $item->name }}">
                                                                    @if ($item->discAmount === '0' && $item->discPercent ===
                                                                    '0')
                                                                    <input class="money" type="hidden" name="prices"
                                                                        value="{{ $item->price, 2 }}">
                                                                    @elseif($item->discAmount === '0')
                                                                    <input class="money" type="hidden" name="prices"
                                                                        value="{{ $item->price - ($item->price*$item->discPercent/100), 2 }}">
                                                                    @elseif($item->discPercent === '0')
                                                                    <input class="money" type="hidden" name="prices"
                                                                        value="{{ $item->price-$item->discAmount, 2 }}">
                                                                    @endif

                                                                </div>
                                                                <a data-toggle="tooltip" data-placement="top" title=""
                                                                    data-original-title="Add to Cart">
                                                                    <button type="submit" class="add_to_cart_btn action-btn"
                                                                        formaction="{{ route('cart.store') }}"
                                                                        data-toggle="tooltip" data-placement="top" title=""
                                                                        data-original-title="Add to Cart">

                                                                        <i class="dl-icon-cart29"></i>

                                                                    </button>
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </figure>
                                                <div class="product-info text-center">
                                                     <div class="product-countdown-wrap mb--20">
                                                        <div class="product-countdown product-countdown-3"
                                                            data-countdown="2020/05/01"></div>
                                                    </div>
                                                    <div class="star-rating star-five">
                                                        <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                                                    </div>
                                                    <h3 class="product-title">
                                                        <a
                                                            href="{{ route('shop.show', $item->rowPointer) }}">{{$item->name}}</a>
                                                    </h3>
                                                    <span class="product-price-wrapper">
                                                        @if ($item->discAmount === '0' && $item->discPercent === '0')
                                                        <span class="product-price">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                        </span>
                                                        @elseif($item->discAmount === '0')
                                                        <span class="product-price-old">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                        </span>
                                                        <span class="product-price-new">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price - ($item->price*$item->discPercent/100), 2)}}</span>
                                                        </span>
                                                        @elseif($item->discPercent === '0')
                                                        <span class="product-price-old">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                        </span>
                                                        <span class="product-price-new">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price-$item->discAmount, 2)}}</span>
                                                        </span>
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach --}}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-deal-scrubs" role="tabpanel"
                                 aria-labelledby="nav-deal-scrubs-tab">
                                <div class="row xl-block-grid-5 grid-space-20">
                                    {{-- @foreach ($hotproducts as $item)
                                    <div class="col-xl-3 col-lg-4 col-sm-6 ptb--10 mb--20">
                                        <div class="charoza-product thumb-has-effect">
                                            <div class="product-inner">
                                                <figure class="product-image">
                                                    <div class="product-image--holder">
                                                        <a href="{{ route('shop.show', $item->rowPointer) }}">
                                                            <img src="/upload/users/products/{{$item->image1}}"
                                                                alt="Product Image" class="primary-image">
                                                            <img src="/upload/users/products/{{$item->image2}}"
                                                                alt="Product Image" class="secondary-image">
                                                        </a>
                                                    </div>
                                                    <div class="charoza-product-action">
                                                        <div class="product-action d-flex flex-column align-items-end">
                                                            <a class="quickview-btn action-btn" data-toggle="tooltip"
                                                                data-placement="top" title=""
                                                                data-original-title="Quick Shop">
                                                                <span data-toggle="modal"
                                                                    data-target="#productModal{{$item->id}}">
                                                                    <i class="dl-icon-view"></i>
                                                                </span>
                                                            </a>
                                                             <a class="add_wishlist action-btn"
                                                                href="{{ Route('wishlist') }}" data-toggle="tooltip"
                                                            data-placement="top" title=""
                                                            data-original-title="Add to Wishlist">
                                                            <i class="dl-icon-heart4"></i>
                                                            </a>
                                                            <a class="add_compare action-btn" href="#" data-toggle="tooltip"
                                                                data-placement="top" title=""
                                                                data-original-title="Add to Compare">
                                                                <i class="dl-icon-compare"></i>
                                                            </a>
                                                            <form method="post" class="form--action">
                                                                @csrf
                                                                <div class="product-action d-flex align-items-center">
                                                                    <input type="hidden" class="quantity-input" name="qty"
                                                                        id="qty" value="1" min="1">
                                                                    <input type="hidden" name="row"
                                                                        value="{{ $item->rowPointer }}">
                                                                    <input type="hidden" name="name"
                                                                        value="{{ $item->name }}">
                                                                    @if ($item->discAmount === '0' && $item->discPercent ===
                                                                    '0')
                                                                    <input class="money" type="hidden" name="prices"
                                                                        value="{{ $item->price, 2 }}">
                                                                    @elseif($item->discAmount === '0')
                                                                    <input class="money" type="hidden" name="prices"
                                                                        value="{{ $item->price - ($item->price*$item->discPercent/100), 2 }}">
                                                                    @elseif($item->discPercent === '0')
                                                                    <input class="money" type="hidden" name="prices"
                                                                        value="{{ $item->price-$item->discAmount, 2 }}">
                                                                    @endif

                                                                </div>
                                                                <a data-toggle="tooltip" data-placement="top" title=""
                                                                    data-original-title="Add to Cart">
                                                                    <button type="submit" class="add_to_cart_btn action-btn"
                                                                        formaction="{{ route('cart.store') }}"
                                                                        data-toggle="tooltip" data-placement="top" title=""
                                                                        data-original-title="Add to Cart">

                                                                        <i class="dl-icon-cart29"></i>

                                                                    </button>
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </figure>
                                                <div class="product-info text-center">
                                                     <div class="product-countdown-wrap mb--20">
                                                        <div class="product-countdown product-countdown-3"
                                                            data-countdown="2020/05/01"></div>
                                                    </div>
                                                    <div class="star-rating star-five">
                                                        <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                                                    </div>
                                                    <h3 class="product-title">
                                                        <a
                                                            href="{{ route('shop.show', $item->rowPointer) }}">{{$item->name}}</a>
                                                    </h3>
                                                    <span class="product-price-wrapper">
                                                        @if ($item->discAmount === '0' && $item->discPercent === '0')
                                                        <span class="product-price">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                        </span>
                                                        @elseif($item->discAmount === '0')
                                                        <span class="product-price-old">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                        </span>
                                                        <span class="product-price-new">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price - ($item->price*$item->discPercent/100), 2)}}</span>
                                                        </span>
                                                        @elseif($item->discPercent === '0')
                                                        <span class="product-price-old">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                        </span>
                                                        <span class="product-price-new">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price-$item->discAmount, 2)}}</span>
                                                        </span>
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach --}}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-deal-masks" role="tabpanel"
                                 aria-labelledby="nav-deal-masks-tab">
                                <div class="row xl-block-grid-5 grid-space-20">
                                    {{-- @foreach ($hotproducts as $item)
                                   <div class="col-xl-3 col-lg-4 col-sm-6 ptb--10 mb--20">
                                       <div class="charoza-product thumb-has-effect">
                                           <div class="product-inner">
                                               <figure class="product-image">
                                                   <div class="product-image--holder">
                                                       <a href="{{ route('shop.show', $item->rowPointer) }}">
                                                           <img src="/upload/users/products/{{$item->image1}}"
                                                               alt="Product Image" class="primary-image">
                                                           <img src="/upload/users/products/{{$item->image2}}"
                                                               alt="Product Image" class="secondary-image">
                                                       </a>
                                                   </div>
                                                   <div class="charoza-product-action">
                                                       <div class="product-action d-flex flex-column align-items-end">
                                                           <a class="quickview-btn action-btn" data-toggle="tooltip"
                                                               data-placement="top" title=""
                                                               data-original-title="Quick Shop">
                                                               <span data-toggle="modal"
                                                                   data-target="#productModal{{$item->id}}">
                                                                   <i class="dl-icon-view"></i>
                                                               </span>
                                                           </a>
                                                            <a class="add_wishlist action-btn"
                                                               href="{{ Route('wishlist') }}" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="Add to Wishlist">
                                                           <i class="dl-icon-heart4"></i>
                                                           </a>
                                                           <a class="add_compare action-btn" href="#" data-toggle="tooltip"
                                                               data-placement="top" title=""
                                                               data-original-title="Add to Compare">
                                                               <i class="dl-icon-compare"></i>
                                                           </a>
                                                           <form method="post" class="form--action">
                                                               @csrf
                                                               <div class="product-action d-flex align-items-center">
                                                                   <input type="hidden" class="quantity-input" name="qty"
                                                                       id="qty" value="1" min="1">
                                                                   <input type="hidden" name="row"
                                                                       value="{{ $item->rowPointer }}">
                                                                   <input type="hidden" name="name"
                                                                       value="{{ $item->name }}">
                                                                   @if ($item->discAmount === '0' && $item->discPercent ===
                                                                   '0')
                                                                   <input class="money" type="hidden" name="prices"
                                                                       value="{{ $item->price, 2 }}">
                                                                   @elseif($item->discAmount === '0')
                                                                   <input class="money" type="hidden" name="prices"
                                                                       value="{{ $item->price - ($item->price*$item->discPercent/100), 2 }}">
                                                                   @elseif($item->discPercent === '0')
                                                                   <input class="money" type="hidden" name="prices"
                                                                       value="{{ $item->price-$item->discAmount, 2 }}">
                                                                   @endif

                                                               </div>
                                                               <a data-toggle="tooltip" data-placement="top" title=""
                                                                   data-original-title="Add to Cart">
                                                                   <button type="submit" class="add_to_cart_btn action-btn"
                                                                       formaction="{{ route('cart.store') }}"
                                                                       data-toggle="tooltip" data-placement="top" title=""
                                                                       data-original-title="Add to Cart">

                                                                       <i class="dl-icon-cart29"></i>

                                                                   </button>
                                                               </a>
                                                           </form>
                                                       </div>
                                                   </div>
                                               </figure>
                                               <div class="product-info text-center">
                                                    <div class="product-countdown-wrap mb--20">
                                                       <div class="product-countdown product-countdown-3"
                                                           data-countdown="2020/05/01"></div>
                                                   </div>
                                                   <div class="star-rating star-five">
                                                       <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                                                   </div>
                                                   <h3 class="product-title">
                                                       <a
                                                           href="{{ route('shop.show', $item->rowPointer) }}">{{$item->name}}</a>
                                                   </h3>
                                                   <span class="product-price-wrapper">
                                                       @if ($item->discAmount === '0' && $item->discPercent === '0')
                                                       <span class="product-price">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                       </span>
                                                       @elseif($item->discAmount === '0')
                                                       <span class="product-price-old">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                       </span>
                                                       <span class="product-price-new">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price - ($item->price*$item->discPercent/100), 2)}}</span>
                                                       </span>
                                                       @elseif($item->discPercent === '0')
                                                       <span class="product-price-old">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                       </span>
                                                       <span class="product-price-new">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price-$item->discAmount, 2)}}</span>
                                                       </span>
                                                       @endif
                                                   </span>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   @endforeach  --}}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-deal-skin" role="tabpanel"
                                 aria-labelledby="nav-deal-skin-tab">
                                <div class="row xl-block-grid-5 grid-space-20">
                                    {{-- @foreach ($hotproducts as $item)
                                    <div class="col-xl-3 col-lg-4 col-sm-6 ptb--10 mb--20">
                                        <div class="charoza-product thumb-has-effect">
                                            <div class="product-inner">
                                                <figure class="product-image">
                                                    <div class="product-image--holder">
                                                        <a href="{{ route('shop.show', $item->rowPointer) }}">
                                                            <img src="/upload/users/products/{{$item->image1}}"
                                                                alt="Product Image" class="primary-image">
                                                            <img src="/upload/users/products/{{$item->image2}}"
                                                                alt="Product Image" class="secondary-image">
                                                        </a>
                                                    </div>
                                                    <div class="charoza-product-action">
                                                        <div class="product-action d-flex flex-column align-items-end">
                                                            <a class="quickview-btn action-btn" data-toggle="tooltip"
                                                                data-placement="top" title=""
                                                                data-original-title="Quick Shop">
                                                                <span data-toggle="modal"
                                                                    data-target="#productModal{{$item->id}}">
                                                                    <i class="dl-icon-view"></i>
                                                                </span>
                                                            </a>
                                                            <a class="add_wishlist action-btn"
                                                                href="{{ Route('wishlist') }}" data-toggle="tooltip"
                                                            data-placement="top" title=""
                                                            data-original-title="Add to Wishlist">
                                                            <i class="dl-icon-heart4"></i>
                                                            </a>
                                                            <a class="add_compare action-btn" href="#" data-toggle="tooltip"
                                                                data-placement="top" title=""
                                                                data-original-title="Add to Compare">
                                                                <i class="dl-icon-compare"></i>
                                                            </a>
                                                            <form method="post" class="form--action">
                                                                @csrf
                                                                <div class="product-action d-flex align-items-center">
                                                                    <input type="hidden" class="quantity-input" name="qty"
                                                                        id="qty" value="1" min="1">
                                                                    <input type="hidden" name="row"
                                                                        value="{{ $item->rowPointer }}">
                                                                    <input type="hidden" name="name"
                                                                        value="{{ $item->name }}">
                                                                    @if ($item->discAmount === '0' && $item->discPercent ===
                                                                    '0')
                                                                    <input class="money" type="hidden" name="prices"
                                                                        value="{{ $item->price, 2 }}">
                                                                    @elseif($item->discAmount === '0')
                                                                    <input class="money" type="hidden" name="prices"
                                                                        value="{{ $item->price - ($item->price*$item->discPercent/100), 2 }}">
                                                                    @elseif($item->discPercent === '0')
                                                                    <input class="money" type="hidden" name="prices"
                                                                        value="{{ $item->price-$item->discAmount, 2 }}">
                                                                    @endif

                                                                </div>
                                                                <a data-toggle="tooltip" data-placement="top" title=""
                                                                    data-original-title="Add to Cart">
                                                                    <button type="submit" class="add_to_cart_btn action-btn"
                                                                        formaction="{{ route('cart.store') }}"
                                                                        data-toggle="tooltip" data-placement="top" title=""
                                                                        data-original-title="Add to Cart">

                                                                        <i class="dl-icon-cart29"></i>

                                                                    </button>
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </figure>
                                                <div class="product-info text-center">
                                                     <div class="product-countdown-wrap mb--20">
                                                        <div class="product-countdown product-countdown-3"
                                                            data-countdown="2020/05/01"></div>
                                                    </div>
                                                    <div class="star-rating star-five">
                                                        <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                                                    </div>
                                                    <h3 class="product-title">
                                                        <a
                                                            href="{{ route('shop.show', $item->rowPointer) }}">{{$item->name}}</a>
                                                    </h3>
                                                    <span class="product-price-wrapper">
                                                        @if ($item->discAmount === '0' && $item->discPercent === '0')
                                                        <span class="product-price">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                        </span>
                                                        @elseif($item->discAmount === '0')
                                                        <span class="product-price-old">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                        </span>
                                                        <span class="product-price-new">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price - ($item->price*$item->discPercent/100), 2)}}</span>
                                                        </span>
                                                        @elseif($item->discPercent === '0')
                                                        <span class="product-price-old">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                        </span>
                                                        <span class="product-price-new">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price-$item->discAmount, 2)}}</span>
                                                        </span>
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach  --}}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-deal-hair" role="tabpanel"
                                 aria-labelledby="nav-deal-hair-tab">
                                <div class="row xl-block-grid-5 grid-space-20">
                                    {{-- @foreach ($hotproducts as $item)
                                   <div class="col-xl-3 col-lg-4 col-sm-6 ptb--10 mb--20">
                                       <div class="charoza-product thumb-has-effect">
                                           <div class="product-inner">
                                               <figure class="product-image">
                                                   <div class="product-image--holder">
                                                       <a href="{{ route('shop.show', $item->rowPointer) }}">
                                                           <img src="/upload/users/products/{{$item->image1}}"
                                                               alt="Product Image" class="primary-image">
                                                           <img src="/upload/users/products/{{$item->image2}}"
                                                               alt="Product Image" class="secondary-image">
                                                       </a>
                                                   </div>
                                                   <div class="charoza-product-action">
                                                       <div class="product-action d-flex flex-column align-items-end">
                                                           <a class="quickview-btn action-btn" data-toggle="tooltip"
                                                               data-placement="top" title=""
                                                               data-original-title="Quick Shop">
                                                               <span data-toggle="modal"
                                                                   data-target="#productModal{{$item->id}}">
                                                                   <i class="dl-icon-view"></i>
                                                               </span>
                                                           </a>
                                                           <a class="add_wishlist action-btn"
                                                               href="{{ Route('wishlist') }}" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="Add to Wishlist">
                                                           <i class="dl-icon-heart4"></i>
                                                           </a>
                                                           <a class="add_compare action-btn" href="#" data-toggle="tooltip"
                                                               data-placement="top" title=""
                                                               data-original-title="Add to Compare">
                                                               <i class="dl-icon-compare"></i>
                                                           </a>
                                                           <form method="post" class="form--action">
                                                               @csrf
                                                               <div class="product-action d-flex align-items-center">
                                                                   <input type="hidden" class="quantity-input" name="qty"
                                                                       id="qty" value="1" min="1">
                                                                   <input type="hidden" name="row"
                                                                       value="{{ $item->rowPointer }}">
                                                                   <input type="hidden" name="name"
                                                                       value="{{ $item->name }}">
                                                                   @if ($item->discAmount === '0' && $item->discPercent ===
                                                                   '0')
                                                                   <input class="money" type="hidden" name="prices"
                                                                       value="{{ $item->price, 2 }}">
                                                                   @elseif($item->discAmount === '0')
                                                                   <input class="money" type="hidden" name="prices"
                                                                       value="{{ $item->price - ($item->price*$item->discPercent/100), 2 }}">
                                                                   @elseif($item->discPercent === '0')
                                                                   <input class="money" type="hidden" name="prices"
                                                                       value="{{ $item->price-$item->discAmount, 2 }}">
                                                                   @endif

                                                               </div>
                                                               <a data-toggle="tooltip" data-placement="top" title=""
                                                                   data-original-title="Add to Cart">
                                                                   <button type="submit" class="add_to_cart_btn action-btn"
                                                                       formaction="{{ route('cart.store') }}"
                                                                       data-toggle="tooltip" data-placement="top" title=""
                                                                       data-original-title="Add to Cart">

                                                                       <i class="dl-icon-cart29"></i>

                                                                   </button>
                                                               </a>
                                                           </form>
                                                       </div>
                                                   </div>
                                               </figure>
                                               <div class="product-info text-center">
                                                    <div class="product-countdown-wrap mb--20">
                                                       <div class="product-countdown product-countdown-3"
                                                           data-countdown="2020/05/01"></div>
                                                   </div>
                                                   <div class="star-rating star-five">
                                                       <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                                                   </div>
                                                   <h3 class="product-title">
                                                       <a
                                                           href="{{ route('shop.show', $item->rowPointer) }}">{{$item->name}}</a>
                                                   </h3>
                                                   <span class="product-price-wrapper">
                                                       @if ($item->discAmount === '0' && $item->discPercent === '0')
                                                       <span class="product-price">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                       </span>
                                                       @elseif($item->discAmount === '0')
                                                       <span class="product-price-old">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                       </span>
                                                       <span class="product-price-new">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price - ($item->price*$item->discPercent/100), 2)}}</span>
                                                       </span>
                                                       @elseif($item->discPercent === '0')
                                                       <span class="product-price-old">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                       </span>
                                                       <span class="product-price-new">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price-$item->discAmount, 2)}}</span>
                                                       </span>
                                                       @endif
                                                   </span>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   @endforeach  --}}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-deal-tool" role="tabpanel"
                                 aria-labelledby="nav-deal-tool-tab">
                                <div class="row xl-block-grid-5 grid-space-20">
                                    {{-- @foreach ($hotproducts as $item)
                                   <div class="col-xl-3 col-lg-4 col-sm-6 ptb--10 mb--20">
                                       <div class="charoza-product thumb-has-effect">
                                           <div class="product-inner">
                                               <figure class="product-image">
                                                   <div class="product-image--holder">
                                                       <a href="{{ route('shop.show', $item->rowPointer) }}">
                                                           <img src="/upload/users/products/{{$item->image1}}"
                                                               alt="Product Image" class="primary-image">
                                                           <img src="/upload/users/products/{{$item->image2}}"
                                                               alt="Product Image" class="secondary-image">
                                                       </a>
                                                   </div>
                                                   <div class="charoza-product-action">
                                                       <div class="product-action d-flex flex-column align-items-end">
                                                           <a class="quickview-btn action-btn" data-toggle="tooltip"
                                                               data-placement="top" title=""
                                                               data-original-title="Quick Shop">
                                                               <span data-toggle="modal"
                                                                   data-target="#productModal{{$item->id}}">
                                                                   <i class="dl-icon-view"></i>
                                                               </span>
                                                           </a>
                                                            <a class="add_wishlist action-btn"
                                                               href="{{ Route('wishlist') }}" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="Add to Wishlist">
                                                           <i class="dl-icon-heart4"></i>
                                                           </a>
                                                           <a class="add_compare action-btn" href="#" data-toggle="tooltip"
                                                               data-placement="top" title=""
                                                               data-original-title="Add to Compare">
                                                               <i class="dl-icon-compare"></i>
                                                           </a>
                                                           <form method="post" class="form--action">
                                                               @csrf
                                                               <div class="product-action d-flex align-items-center">
                                                                   <input type="hidden" class="quantity-input" name="qty"
                                                                       id="qty" value="1" min="1">
                                                                   <input type="hidden" name="row"
                                                                       value="{{ $item->rowPointer }}">
                                                                   <input type="hidden" name="name"
                                                                       value="{{ $item->name }}">
                                                                   @if ($item->discAmount === '0' && $item->discPercent ===
                                                                   '0')
                                                                   <input class="money" type="hidden" name="prices"
                                                                       value="{{ $item->price, 2 }}">
                                                                   @elseif($item->discAmount === '0')
                                                                   <input class="money" type="hidden" name="prices"
                                                                       value="{{ $item->price - ($item->price*$item->discPercent/100), 2 }}">
                                                                   @elseif($item->discPercent === '0')
                                                                   <input class="money" type="hidden" name="prices"
                                                                       value="{{ $item->price-$item->discAmount, 2 }}">
                                                                   @endif

                                                               </div>
                                                               <a data-toggle="tooltip" data-placement="top" title=""
                                                                   data-original-title="Add to Cart">
                                                                   <button type="submit" class="add_to_cart_btn action-btn"
                                                                       formaction="{{ route('cart.store') }}"
                                                                       data-toggle="tooltip" data-placement="top" title=""
                                                                       data-original-title="Add to Cart">

                                                                       <i class="dl-icon-cart29"></i>

                                                                   </button>
                                                               </a>
                                                           </form>
                                                       </div>
                                                   </div>
                                               </figure>
                                               <div class="product-info text-center">
                                                   <div class="product-countdown-wrap mb--20">
                                                       <div class="product-countdown product-countdown-3"
                                                           data-countdown="2020/05/01"></div>
                                                   </div>
                                                   <div class="star-rating star-five">
                                                       <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                                                   </div>
                                                   <h3 class="product-title">
                                                       <a
                                                           href="{{ route('shop.show', $item->rowPointer) }}">{{$item->name}}</a>
                                                   </h3>
                                                   <span class="product-price-wrapper">
                                                       @if ($item->discAmount === '0' && $item->discPercent === '0')
                                                       <span class="product-price">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                       </span>
                                                       @elseif($item->discAmount === '0')
                                                       <span class="product-price-old">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                       </span>
                                                       <span class="product-price-new">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price - ($item->price*$item->discPercent/100), 2)}}</span>
                                                       </span>
                                                       @elseif($item->discPercent === '0')
                                                       <span class="product-price-old">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                       </span>
                                                       <span class="product-price-new">
                                                           <span class="money"
                                                               name="price">Rp.{{number_format($item->price-$item->discAmount, 2)}}</span>
                                                       </span>
                                                       @endif
                                                   </span>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   @endforeach --}}
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-deal-foot" role="tabpanel"
                                 aria-labelledby="nav-deal-foot-tab">
                                <div class="row xl-block-grid-5 grid-space-20">
                                    {{-- @foreach ($hotproducts as $item)
                                    <div class="col-xl-3 col-lg-4 col-sm-6 ptb--10 mb--20">
                                        <div class="charoza-product thumb-has-effect">
                                            <div class="product-inner">
                                                <figure class="product-image">
                                                    <div class="product-image--holder">
                                                        <a href="{{ route('shop.show', $item->rowPointer) }}">
                                                            <img src="/upload/users/products/{{$item->image1}}"
                                                                alt="Product Image" class="primary-image">
                                                            <img src="/upload/users/products/{{$item->image2}}"
                                                                alt="Product Image" class="secondary-image">
                                                        </a>
                                                    </div>
                                                    <div class="charoza-product-action">
                                                        <div class="product-action d-flex flex-column align-items-end">
                                                            <a class="quickview-btn action-btn" data-toggle="tooltip"
                                                                data-placement="top" title=""
                                                                data-original-title="Quick Shop">
                                                                <span data-toggle="modal"
                                                                    data-target="#productModal{{$item->id}}">
                                                                    <i class="dl-icon-view"></i>
                                                                </span>
                                                            </a>
                                                             <a class="add_wishlist action-btn"
                                                                href="{{ Route('wishlist') }}" data-toggle="tooltip"
                                                            data-placement="top" title=""
                                                            data-original-title="Add to Wishlist">
                                                            <i class="dl-icon-heart4"></i>
                                                            </a>
                                                            <a class="add_compare action-btn" href="#" data-toggle="tooltip"
                                                                data-placement="top" title=""
                                                                data-original-title="Add to Compare">
                                                                <i class="dl-icon-compare"></i>
                                                            </a>
                                                            <form method="post" class="form--action">
                                                                @csrf
                                                                <div class="product-action d-flex align-items-center">
                                                                    <input type="hidden" class="quantity-input" name="qty"
                                                                        id="qty" value="1" min="1">
                                                                    <input type="hidden" name="row"
                                                                        value="{{ $item->rowPointer }}">
                                                                    <input type="hidden" name="name"
                                                                        value="{{ $item->name }}">
                                                                    @if ($item->discAmount === '0' && $item->discPercent ===
                                                                    '0')
                                                                    <input class="money" type="hidden" name="prices"
                                                                        value="{{ $item->price, 2 }}">
                                                                    @elseif($item->discAmount === '0')
                                                                    <input class="money" type="hidden" name="prices"
                                                                        value="{{ $item->price - ($item->price*$item->discPercent/100), 2 }}">
                                                                    @elseif($item->discPercent === '0')
                                                                    <input class="money" type="hidden" name="prices"
                                                                        value="{{ $item->price-$item->discAmount, 2 }}">
                                                                    @endif

                                                                </div>
                                                                <a data-toggle="tooltip" data-placement="top" title=""
                                                                    data-original-title="Add to Cart">
                                                                    <button type="submit" class="add_to_cart_btn action-btn"
                                                                        formaction="{{ route('cart.store') }}"
                                                                        data-toggle="tooltip" data-placement="top" title=""
                                                                        data-original-title="Add to Cart">

                                                                        <i class="dl-icon-cart29"></i>

                                                                    </button>
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </figure>
                                                <div class="product-info text-center">
                                                     <div class="product-countdown-wrap mb--20">
                                                        <div class="product-countdown product-countdown-3"
                                                            data-countdown="2020/05/01"></div>
                                                    </div>
                                                    <div class="star-rating star-five">
                                                        <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                                                    </div>
                                                    <h3 class="product-title">
                                                        <a
                                                            href="{{ route('shop.show', $item->rowPointer) }}">{{$item->name}}</a>
                                                    </h3>
                                                    <span class="product-price-wrapper">
                                                        @if ($item->discAmount === '0' && $item->discPercent === '0')
                                                        <span class="product-price">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                        </span>
                                                        @elseif($item->discAmount === '0')
                                                        <span class="product-price-old">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                        </span>
                                                        <span class="product-price-new">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price - ($item->price*$item->discPercent/100), 2)}}</span>
                                                        </span>
                                                        @elseif($item->discPercent === '0')
                                                        <span class="product-price-old">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price, 2)}}</span>
                                                        </span>
                                                        <span class="product-price-new">
                                                            <span class="money"
                                                                name="price">Rp.{{number_format($item->price-$item->discAmount, 2)}}</span>
                                                        </span>
                                                        @endif
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach  --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt--40 mt-md--20 mt-sm--0">
                <div class="col-12 text-center">
                    <a href="{{ Route('shop') }}" class="view-all">View All Products</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Tab Area End -->

@endif
