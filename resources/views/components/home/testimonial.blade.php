@if ($testimonials->count() != '0')
    <section
        class="testimonial-area slick-overflow-visible pt--75 pt-md--55 pt-sm--35 pb--120 pb-lg--80 pb-md--60 pb-sm--40">
        <div class="container">
            <div class="row mb--50 mb-md--40 mb-sm--25">
                <div class="col-12 text-center">
                    <h4 class="text-uppercase font-size-18 color--tertiary lts-10 lts-md-5 lts-sm-2 mb--15 mb-md--10">
                        Artline</h4>
                    <h2 class="heading-secondary-2">What Client Say?</h2>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="charoza-element-carousel nav-vertical-center nav-disabled nav-style-1" data-slick-options='{
                                "spaceBetween": 30,
                                "slidesToShow": 3,
                                "slidesToScroll": 1,
                                "arrows": true,
                                "prevArrow": {"buttonClass": "slick-btn slick-prev", "iconClass": "dl-icon-left" },
                                "nextArrow": {"buttonClass": "slick-btn slick-next", "iconClass": "dl-icon-right" }
                            }' data-slick-responsive='[
                                {"breakpoint":991, "settings": {"slidesToShow": 2}},
                                {"breakpoint":620, "settings": {"slidesToShow": 1}}
                            ]'>
                        @foreach ($testimonials as $item)
                            <div class="item">
                                <div class="testimonial testimonial-style-1">
                                    <div class="testimonial__inner">
                                        @if ($item->testiRate == 5)
                                            <div class="star-rating star-five">
                                                <span>Rated <strong class="rating">{{$item->testiRate}}.00</strong> out of 5</span>
                                            </div>
                                        @elseif($item->testiRate == 4)
                                            <div class="star-rating star-four">
                                                <span>Rated <strong class="rating">{{$item->testiRate}}.00</strong> out of 5</span>
                                            </div>
                                        @elseif($item->testiRate == 3)
                                            <div class="star-rating star-three">
                                                <span>Rated <strong class="rating">{{$item->testiRate}}.00</strong> out of 5</span>
                                            </div>
                                        @elseif($item->testiRate == 2)
                                            <div class="star-rating star-two">
                                                <span>Rated <strong class="rating">{{$item->testiRate}}.00</strong> out of 5</span>
                                            </div>
                                        @elseif($item->testiRate == 1)
                                            <div class="star-rating star-one">
                                                <span>Rated <strong class="rating">{{$item->testiRate}}.00</strong> out of 5</span>
                                            </div>
                                        @endif
                                        <p class="testimonial__desc">{{$item->testiMessage}}</p>
                                        <div class="testimonial__author">
                                            <h3 class="testimonial__author--name">{{$item->usersName}}</h3>
{{--                                            <h5 class="testimonial__author--role">{{$item->addressCity}}</h5>--}}
                                        </div>
                                        <figure class="testimonial__thumbnail">
                                            @if ($item->usersImage != null)
                                                <img src="{{ asset('/upload/user/'.$item->usersImage) }}" alt="Client" style="object-fit: cover;">
                                            @else
                                                <img src="{{ asset('assets/img/profile.webp') }}" alt="Client" style="object-fit: cover;">
                                            @endif
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif
