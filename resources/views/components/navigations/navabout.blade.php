        <!-- Header Area Start -->
        <header class="header header-transparent header-style-1">
                <div class="header-inner">
                    <div class="header-top headroom headroom--fixed">
                        <div class="container-fluid">
                            <div class="row align-items-center">
                                <div class="col-lg-2 col-6">
                                    <!-- Logo Start Here -->
                                    <a href="{{ Route('index') }}" class="logo-box">
                                        <figure class="logo--normal">
                                            <img src="{{asset('assets/img/logo/transparent-logo.png')}}" alt="Logo" />
                                        </figure>
                                        <figure class="logo--sticky">
                                            <img src="{{asset('assets/img/logo/transparent-logo.png')}}" alt="Logo" />
                                        </figure>
                                    </a>
                                    <!-- Logo End Here -->
                                </div>
                                <div class="col-lg-8 d-none d-lg-block">
                                    <!-- Main Navigation Start Here -->
                                    @include('components.navigations._navitem')
                                    <!-- Main Navigation End Here -->
                                </div>
                                <div class="col-lg-2 col-6">
                                    <ul class="header-toolbar white-color text-right">
                                        <li class="header-toolbar__item d-lg-none">
                                            <a href="#mobileMenu" class="toolbar-btn menu-btn">
                                                <span class="hamburger-icon">
                                                </span>
                                            </a>
                                        </li>
                                        <li class="header-toolbar__item">
                                            <a href="#searchForm" class="search-btn toolbar-btn">
                                                <i class="dl-icon-search1"></i>
                                            </a>
                                        </li>
                                        <li class="header-toolbar__item">
                                            <a href="#miniCart" class="mini-cart-btn toolbar-btn">
                                                <i class="dl-icon-cart25"></i>
                                                <sup class="mini-cart-count">{{ Cart::getContent()->count() }}</sup>
                                            </a>
                                        </li>
                                        <li class="header-toolbar__item d-none d-lg-block">
                                            <a href="#sideNav" class="toolbar-btn">
                                                <span class="hamburger-icon">
                                                </span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mobile-menu-wrapper" id="mobileMenu">
                    <div class="mobile-menu-inner">
                        <a href="" class="btn-close">
                            <span class="hamburger-icon">
                            </span>
                        </a>
                        <nav class="mobile-navigation">
                            <ul class="mobile-menu">
                                <li class="mainmenu__item active">
                                    <a href="{{ Route('index') }}" class="mainmenu__link">
                                        <span class="mm-text">Home</span>
                                    </a>
                                </li>
                                <li class="mainmenu__item">
                                    <a href="{{ Route('newlook') }}" class="mainmenu__link">
                                        <span class="mm-text">New Products</span>
                                        <span class="badge">Hot</span>
                                    </a>
                                </li>
                                <li class="mainmenu__item menu-item-has-children megamenu-holder">
                                    <a href="{{ Route('shop') }}" class="mainmenu__link">
                                        <span class="mm-text">Shop</span>
                                    </a>
                                    {{-- <ul class="megamenu four-column">
                                        <li>
                                            <a class="megamenu-title" href="#">
                                                <span class="mm-text">Eyes</span>
                                            </a>
                                            <ul>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 1</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 2</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 3</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 4</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 5</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a class="megamenu-title" href="#">
                                                <span class="mm-text">Lips</span>
                                            </a>
                                            <ul>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 1</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 2</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 3</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 4</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 5</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 6</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 7</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a class="megamenu-title" href="#">
                                                <span class="mm-text">Face</span>
                                            </a>
                                            <ul>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 1</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 2</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 3</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="mm-text">Sub Category 4</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="banner-holder">
                                            <div class="megamenu-banner">
                                                <div class="megamenu-banner-image"></div>
                                                <div class="megamenu-banner-info">
                                                    <span>Autumn Winter 2019</span>
                                                    <h3>new <strong>arrival</strong></h3>
                                                </div>
                                                <a href="{{ Route('shop') }}" class="megamenu-banner-link"></a>
                                            </div>
                                        </li>
                                    </ul> --}}
                                </li>
                                <li class="mainmenu__item">
                                    <a href="{{ Route('promotions') }}" class="mainmenu__link">
                                        <span class="mm-text">Promotions</span>
                                    </a>
                                </li>
                                <li class="mainmenu__item">
                                    <a href="{{ Route('blogs') }}" class="mainmenu__link">
                                        <span class="mm-text">Blog</span>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>
            <!-- Header Area End -->
