<!-- Main Navigation Start Here -->
<nav
    class="main-navigation {{ ( Request::is('/') || Request::is('/*') ) ? 'main-navigation-3 white-color' : '' }} {{ ( Request::is('about') || Request::is('about') ) ? ' white-color' : '' }}">
    <ul class="mainmenu {{ ( Request::is('/') || Request::is('/*') ) ? '' : 'mainmenu--centered' }} ">
        <li class="mainmenu__item {{ ( Request::is('/') || Request::is('/*') ) ? 'active' : '' }}">
            <a href="{{ Route('index') }}" class="mainmenu__link">
                <span class="mm-text">Home</span>
            </a>
        </li>
        <li class="mainmenu__item  {{ ( Request::is('newlook') || Request::is('newlook') ) ? 'active' : '' }}">
            <a href="{{ Route('newlook') }}" class="mainmenu__link">
                <span class="mm-text">New Products</span>
                <span class="badge">Hot</span>
            </a>
        </li>
        <li
            class="mainmenu__item menu-item-has-children megamenu-holder {{ ( Request::is('shop') || Request::is('shop') ) ? 'active' : '' }}">
            <a href="{{ Route('shop') }}" class="mainmenu__link">
                <span class="mm-text">Shop</span>
            </a>
            {{-- <ul class="megamenu four-column">
                <li>
                    <a class="megamenu-title" href="#">
                        <span class="mm-text">Eyes</span>
                    </a>
                    <ul>
                        {{-- @foreach ($subeyes as $item)
                        <li>
                            <a href="{{ Route('shop') }}">
            <span class="mm-text">{{$item}}</span>
            </a>
        </li>
        @endforeach
        <li>
            <a href="{{ Route('shop') }}">
                <span class="mm-text">Conclear</span>
            </a>
        </li>
        <li>
            <a href="{{ Route('shop') }}">
                <span class="mm-text">Eye Brow</span>
            </a>
        </li>
        <li>
            <a href="{{ Route('shop') }}">
                <span class="mm-text">Eye Liner</span>
            </a>
        </li>
        <li>
            <a href="{{ Route('shop') }}">
                <span class="mm-text">Eye Shadow</span>
            </a>
        </li>
        <li>
            <a href="{{ Route('shop') }}">
                <span class="mm-text">Mascara</span>
            </a>
        </li>
    </ul>
    </li>
    <li>
        <a class="megamenu-title" href="#">
            <span class="mm-text">Lips</span>
        </a>
        <ul>
            {{-- @foreach ($sublips as $item)
                <li>
                    <a href="{{ Route('shop') }}">
            <span class="mm-text">{{$item}}</span>
            </a>
    </li>
    @endforeach
    <li>
        <a href="{{ Route('shop') }}">
            <span class="mm-text">Lip Balm</span>
        </a>
    </li>
    <li>
        <a href="{{ Route('shop') }}">
            <span class="mm-text">Lip Gloss</span>
        </a>
    </li>
    <li>
        <a href="{{ Route('shop') }}">
            <span class="mm-text">Lip Liner</span>
        </a>
    </li>
    <li>
        <a href="{{ Route('shop') }}">
            <span class="mm-text">Lip Matte</span>
        </a>
    </li>
    <li>
        <a href="{{ Route('shop') }}">
            <span class="mm-text">Lipstick</span>
        </a>
    </li>

    </ul>
    </li>
    <li>
        <a class="megamenu-title" href="#">
            <span class="mm-text">Face</span>
        </a>
        <ul>
            {{-- @foreach ($subface as $item)
            <li>
                <a href="{{ Route('shop') }}">
            <span class="mm-text">{{$item}}</span>
            </a>
    </li>
    @endforeach
    <li>
        <a href="{{ Route('shop') }}">
            <span class="mm-text">BB Cream</span>
        </a>
    </li>
    <li>
        <a href="{{ Route('shop') }}">
            <span class="mm-text">Blush On</span>
        </a>
    </li>
    <li>
        <a href="{{ Route('shop') }}">
            <span class="mm-text">DD Cream</span>
        </a>
    </li>
    <li>
        <a href="{{ Route('shop') }}">
            <span class="mm-text">Face Powder</span>
        </a>
    </li>
    <li>
        <a href="{{ Route('shop') }}">
            <span class="mm-text">Foundation</span>
        </a>
    </li>
    </ul>
    </li>
    <li class="banner-holder">
        <div class="megamenu-banner">
            <div class="megamenu-banner-image"></div>
            <div class="megamenu-banner-info">
                <span>Autumn Winter 2019</span>
                <h3>new <strong>arrival</strong></h3>
            </div>
            <a href="{{ Route('shop') }}" class="megamenu-banner-link"></a>
        </div>
    </li>
    </ul> --}}
    </li>
    <li class="mainmenu__item {{ ( Request::is('promotions') || Request::is('promotions') ) ? 'active' : '' }}">
        <a href="{{ Route('promotions') }}" class="mainmenu__link">
            <span class="mm-text">Promotions</span>
        </a>
    </li>
    <li class="mainmenu__item {{ ( Request::is('blogs') || Request::is('blogs') ) ? 'active' : '' }}">
        <a href="{{ Route('blogs') }}" class="mainmenu__link">
            <span class="mm-text">Blog</span>
        </a>
    </li>
    </ul>
</nav>
<!-- Main Navigation End Here -->