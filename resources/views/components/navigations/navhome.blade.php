<!-- Header Area Start -->
<header class="header header-style-3">
    <div class="header-inner headroom headroom--fixed">
        <div class="top-bar d-none d-lg-block">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-xl-4 d-none d-xl-block">
                        <!-- Social Icons Start Here -->
                        <ul class="social social-medium dark-color">
                            <li class="social__item">
                                <a href="https://www.facebook.com" class="social__link">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li class="social__item">
                                <a href="https://www.twitter.com" class="social__link">
                                    <i class="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li class="social__item">
                                <a href="https://www.pinterest.com" class="social__link">
                                    <i class="fa fa-pinterest"></i>
                                </a>
                            </li>
                            <li class="social__item">
                                <a href="https://www.youtube.com" class="social__link">
                                    <i class="fa fa-youtube"></i>
                                </a>
                            </li>
                            <li class="social__item">
                                <a href="https://www.plus.google.com" class="social__link">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- Social Icons End Here -->
                    </div>
                    <div class="col-xl-4 text-center">
                        <div class="header__text">
                            {{-- <span>Flash sale! Off 20% All Items This Week <a href="{{ Route('shop') }}">SHOP
                            NOW</a></span> --}}
                        </div>
                    </div>
                    <div class="col-xl-4 text-right d-none d-xl-block">
                        <div class="contact-info">
                            <span>
                                <i class="fa fa-phone"></i>
                                +62.31.745.88899, +62.81.223.889988
                            </span>
                            <span>
                                <i class="fa fa-envelope"></i>
                                support@artline.co.id
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-top">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-3 col-6">
                        <!-- Logo Start Here -->
                        <a href="{{ Route('index') }}" class="logo-box">
                            <figure class="logo--normal">
                                <img src="{{asset('assets/img/logo/logo.png')}}" alt="Logo" />
                            </figure>
                        </a>
                        <!-- Logo End Here -->
                    </div>
                    <div class="col-lg-6 d-lg-block d-none">
                        <form action="{{Route('filter.search')}}" method="get" class="searchform searchform-3">
                            <input type="text" name="q" id="search" class="searchform__input"
                                placeholder="Search Products">
                            <button type="submit" class="searchform__submit"><i class="dl-icon-search1"></i></button>
                        </form>
                    </div>
                    <div class="col-lg-3 col-6">
                        <ul class="header-toolbar header-toolbar-3 text-right">
                            {{-- <li class="header-toolbar__item d-lg-block d-none">
                                <a href="{{ Route('wishlist') }}">
                            <i class="fa fa-heart-o"></i>
                            </a>
                            </li> --}}
                            @if(Auth::check())
                            <li class="header-toolbar__item user-info-menu-btn d-lg-block d-none">
                                @else
                            <li class="header-toolbar__item  d-lg-block d-none">
                                @endif
                                <a href="{{ Route('account') }}">
                                    <i class="fa fa-user-circle-o"></i>
                                </a>
                                <ul class="user-info-menu">
                                    <li>
                                        <a href="{{ Route('account') }}">My Account</a>
                                    </li>
                                    {{-- <li>
                                        <a href="order-tracking.html">Tracking Order</a>
                                    </li>
                                    <li>
                                        <a href="#">compare</a>
                                    </li>
                                    <li>
                                        <a href="{{ Route('wishlist') }}">Wishlist</a>
                            </li> --}}
                            <li>
                                <a href="lost-password.html">Lost Password</a>
                            </li>
                        </ul>
                        </li>
                        <li class="header-toolbar__item">
                            <a href="#miniCart" class="mini-cart-btn mini-cart-btn-3 toolbar-btn">
                                <i class="dl-icon-cart25"></i>
                                <sup class="mini-cart-count">{{ Cart::getContent()->count() }}</sup>
                            </a>
                        </li>
                        <li class="header-toolbar__item d-lg-none">
                            <a href="#searchForm" class="search-btn toolbar-btn">
                                <i class="dl-icon-search1"></i>
                            </a>
                        </li>
                        <li class="header-toolbar__item d-lg-none">
                            <a href="#mobileMenu" class="toolbar-btn menu-btn">
                                <span class="hamburger-icon">
                                </span>
                            </a>
                        </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-bottom d-lg-block d-none">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-xl-8 col-lg-7">
                        <div class="header-bottom__left">
                            <!-- Secondary Navigation Start Here -->
                            <nav class="secondary-navigation d-xl-block d-none">
                                <span class="secondary-nav-btn">
                                    {{--                                    <i class="fa fa-navicon"></i>--}}
                                    Artline online shopping
                                </span>
                                {{-- <ul class="secondary-menu">
                                    <li><a href="">Eyes</a></li>
                                    <li><a href="">Face</a></li>
                                    <li><a href="">Lips</a></li>
                                    <li><a href="">Accessories</a></li>
                                    <li><a href="">Skin Care</a></li>
                                    <li><a href="">Parfumes</a></li>
                                </ul> --}}
                            </nav>
                            <!-- Secondary Navigation End Here -->

                            <!-- Main Navigation Start Here -->
                            @include('components.navigations._navitem')
                            <!-- Main Navigation Start Here -->
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5 text-right">
                        <ul class="header-component">
                            {{-- <li>
                                <a href="#">
                                    <i class="fa fa-language"></i>
                                    <span>Language</span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="submenu">
                                    <li><a href="#">English</a></li>
                                    <li><a href="#">Bahasa Indonesia</a></li>
                                </ul>
                            </li> --}}
                            <li>
                                <a href="{{ Route('faqs') }}">
                                    <i class="fa fa-comment-o"></i>
                                    <span>Help &amp; FAQs</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="mobile-menu-wrapper" id="mobileMenu">
        <div class="mobile-menu-inner">
            <a href="" class="btn-close">
                <span class="hamburger-icon">
                </span>
            </a>
            <nav class="mobile-navigation">
                <ul class="mobile-menu">
                    <li class="menu-item-has-children active">
                        <a href="{{ Route('index') }}">
                            <span class="mm-text">Home</span>
                        </a>
                        {{-- <ul class="sub-menu">
                            <li class="menu-item-has-children">
                                <a href="#">
                                    <span class="mm-text">Demo Group 01</span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="{{ Route('index') }}">
                        <span class="mm-text">Demo 01</span>
                        </a>
                    </li>
                    <li>
                        <a href="index-02.html">
                            <span class="mm-text">Demo 02</span>
                        </a>
                    </li>
                    <li>
                        <a href="index-03.html">
                            <span class="mm-text">Demo 03</span>
                        </a>
                    </li>
                    <li>
                        <a href="index-04.html">
                            <span class="mm-text">Demo 04</span>
                        </a>
                    </li>
                    <li>
                        <a href="index-05.html">
                            <span class="mm-text">Demo 05</span>
                        </a>
                    </li>
                </ul>
                </li>
                <li class="menu-item-has-children">
                    <a href="#">
                        <span class="mm-text">Demo Group 02</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="index-06.html">
                                <span class="mm-text">Demo 06</span>
                            </a>
                        </li>
                        <li>
                            <a href="index-07.html">
                                <span class="mm-text">Demo 07</span>
                            </a>
                        </li>
                        <li>
                            <a href="index-08.html">
                                <span class="mm-text">Demo 08</span>
                            </a>
                        </li>
                        <li>
                            <a href="index-09.html">
                                <span class="mm-text">Demo 09</span>
                            </a>
                        </li>
                        <li>
                            <a href="index-10.html">
                                <span class="mm-text">Demo 10</span>
                            </a>
                        </li>
                    </ul>
                </li>
                </ul> --}}
                </li>
                <li class="menu-item-has-children">
                    <a href="{{ Route('shop') }}">
                        <span class="mm-text">Shop</span>
                        <span class="badge">Hot</span>
                    </a>
                    {{-- <ul class="sub-menu">
                            <li class="menu-item-has-children">
                                <a href="#">
                                    <span class="mm-text">Shop Layout</span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="{{ Route('newlook') }}">
                    <span class="mm-text">FullWidth</span>
                    </a>
                </li>
                <li>
                    <a href="{{ Route('shop') }}">
                        <span class="mm-text">with Sidebar</span>
                    </a>
                </li>
                <li>
                    <a href="shop-two-columns.html">
                        <span class="mm-text">Two columns</span>
                    </a>
                </li>
                <li>
                    <a href="shop-three-columns.html">
                        <span class="mm-text">Three columns</span>
                    </a>
                </li>
                <li>
                    <a href="{{ Route('promotions') }}">
                        <span class="mm-text">With collections</span>
                    </a>
                </li>
                <li>
                    <a href="shop-instagram.html">
                        <span class="mm-text">Shop Instagram</span>
                    </a>
                </li>
                </ul>
                </li>
                <li class="menu-item-has-children">
                    <a href="#">
                        <span class="mm-text">Single Product</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ Route('product-detail') }}">
                                <span class="mm-text">Simple 01</span>
                            </a>
                        </li>
                        <li>
                            <a href="product-details-02.html">
                                <span class="mm-text">Simple 02</span>
                            </a>
                        </li>
                        <li>
                            <a href="product-details-sticky.html">
                                <span class="mm-text">Sticky Info</span>
                            </a>
                        </li>
                        <li>
                            <a href="product-details-gallery.html">
                                <span class="mm-text">Thumbnail Gallery</span>
                            </a>
                        </li>
                        <li>
                            <a href="product-details-sidebar.html">
                                <span class="mm-text">Sidebar</span>
                            </a>
                        </li>
                        <li>
                            <a href="product-details-grouped.html">
                                <span class="mm-text">Grouped</span>
                            </a>
                        </li>
                        <li>
                            <a href="product-details-affiliate.html">
                                <span class="mm-text">Affiliate</span>
                            </a>
                        </li>
                        <li>
                            <a href="product-details-configurable.html">
                                <span class="mm-text">Configurable</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children">
                    <a href="#">
                        <span class="mm-text">Shop Pages</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="my-account.html">
                                <span class="mm-text">My Account</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('cart')}}">
                                <span class="mm-text">Shopping Cart</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{Route('checkout')}}">
                                <span class="mm-text">Check Out</span>
                            </a>
                        </li>
                        {{-- <li>
                                        <a href="{{ Route('wishlist') }}">
                        <span class="mm-text">Wishlist</span>
                        </a>
                </li>
                <li>
                    <a href="order-tracking.html">
                        <span class="mm-text">Order tracking</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span class="mm-text">compare</span>
                    </a>
                </li>
                </ul>
                </li>
                </ul> --}}
                </li>

                <li class="mobile-menu__item">
                    <a href="{{ Route('promotions') }}">
                        <span class="mm-text">Promotions</span>
                    </a>
                </li>
                <li class="menu-item-has-children">
                    <a href="{{ Route('blogs') }}">
                        <span class="mm-text">Blog</span>
                    </a>
                    {{-- <ul class="sub-menu">
                            <li class="menu-item-has-children has-children">
                                <a href="#">
                                    <span class="mm-text">Blog Grid</span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="blog-02-columns.html">
                                            <span class="mm-text">Blog 02 Columns</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="blog-03-columns.html">
                                            <span class="mm-text">Blog 03 Columns</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ Route('blogs') }}">
                    <span class="mm-text">Blog Sidebar</span>
                    </a>
                </li>
                </ul>
                </li>
                <li class="menu-item-has-children has-children">
                    <a href="#">
                        <span class="mm-text">Blog List</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="blog-classic.html">
                                <span class="mm-text">Blog Classic</span>
                            </a>
                        </li>
                        <li>
                            <a href="blog-no-sidebar.html">
                                <span class="mm-text">Blog No Sidebar</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="blog-masonary.html">
                        <span class="mm-text">Blog Masonary</span>
                    </a>
                </li>
                <li class="menu-item-has-children has-children">
                    <a href="#">
                        <span class="mm-text">Blog Details</span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ Route('blog-detail') }}">
                                <span class="mm-text">Single Post</span>
                            </a>
                        </li>
                        <li>
                            <a href="single-post-sidebar.html">
                                <span class="mm-text">Single Post Sidebar</span>
                            </a>
                        </li>
                    </ul>
                </li>
                </ul> --}}
                </li>
                <li>
                    <a href="{{ Route('newlook') }}">
                        <span class="mm-text">New Products</span>
                    </a>
                </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<!-- Header Area End -->