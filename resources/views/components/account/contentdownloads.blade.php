<div class="tab-pane fade" id="downloads">
    <div class="message-box mb--30 d-none">
        <p><i class="fa fa-exclamation-circle"></i>No downloads available yet.</p>
        <a href="{{ Route('shop') }}">Go Shop</a>
    </div>
    <div class="table-content table-responsive">
        <table class="table text-center">
            <thead>
                <tr>
                    <th>Product</th>
                    <th>Downloads</th>
                    <th>Expires</th>
                    <th>Download</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="wide-column">Guide Make Up 2019</td>
                    <td>August 10, 2019 </td>
                    <td class="wide-column">Never</td>
                    <td><a href="#" class="btn btn-medium btn-style-1">Download File</a></td>
                </tr>
                <tr>
                    <td class="wide-column">Guide Make Up 2018</td>
                    <td>August 10, 2018 </td>
                    <td class="wide-column">Never</td>
                    <td><a href="#" class="btn btn-medium btn-style-1">Download File</a></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>