@extends('master')
{{-- @section('nav')
    @include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')
    <!-- Main Content Wrapper Start -->
    <div id="content" class="main-content-wrapper">
        <div class="page-content-inner" style="margin-top: 50px;">
            <div class="container">
                @if (Session::has('status'))
                    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                @endif

                @if (count($errors) > 0)

                    <div class="alert alert-danger">

                        <strong>Whoops!</strong> There were some problems with your input.

                        <ul>

                            @foreach ($errors->all() as $error)

                                <li>{{ $error }}</li>

                            @endforeach

                        </ul>

                    </div>

                @endif
                <form action="{{ route('address.update', $address->rowPointer)  }}" method="POST" enctype="multipart/form-data" class="form form--account">
                    @csrf
                    <div class="row mb--20">
                        <div class="col-12">
                            <div class="form__group">
                                <label class="form__label" for="label">Label <span class="required">*</span></label>
                                <input type="text" name="label" id="label" class="form__input" value="{{ $address->label }}">
                                <input type="hidden" name="type" id="type" class="form__input" value="{{ Request()->type }}">
                                <span class="suggestion"><em>This will be how your billing address will be labeled</em></span>
                            </div>
                        </div>
                    </div>
                    <div class="row grid-space-30 mb--20">
                        <div class="col-md-6 mb-sm--20">
                            <div class="form__group">
                                <label class="form__label" for="province">Province <span class="required">*</span></label>
                                <select id="calc_shipping_province" name="province" class="nice-select provinces">
                                    @if ($provinces != 'Connection problem')
                                        <option value="">Select a province…</option>
                                        @foreach($provinces['rajaongkir']['results'] as $province)
                                            <option value="{{ $province['province_id'] }}" name="province" id="province" {{ ( $address->province == $province['province_id'] ) ? ' selected' : '' }}>{{ $province['province'] }} </option>
                                        @endforeach
                                    @else
                                        <option value="">Internet connection problem</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form__group">
                                <label class="form__label" for="city">City <span class="required">*</span></label>
                                <select id="calc_shipping_city" name="city" class="select-custom citys"></select>
                                <select id="calc_shipping_citys" name="city" class="select-custom citys" disabled>
                                    @if ($citys != 'Connection problem')
                                        <option value="">Select a City…</option>
                                        @foreach($citys['rajaongkir']['results'] as $city)
                                            <option value="{{ $city['city_id'] }}"{{ ( $address->city == $city['city_id'] ) ? ' selected' : '' }}>{{ $city['city_name'] }} </option>
                                        @endforeach
                                    @else
                                        <option value="">Internet connection problem</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row grid-space-30 mb--20">
                        <div class="col-md-6 mb-sm--20">
                            <div class="form__group">
                                <label class="form__label" for="kelurahan">Kelurahan <span class="required">*</span></label>
                                <input type="text" name="kelurahan" id="kelurahan" class="form__input" value="{{ $address->kelurahan }}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form__group">
                                <label class="form__label" for="kecamatan">Kecamatan <span class="required">*</span></label>
                                <input type="text" name="kecamatan" id="kecamatan" class="form__input" value="{{ $address->kecamatan }}">
                            </div>
                        </div>
                    </div>
                    <div class="row grid-space-30 mb--20">
                        <div class="col-md-8 mb-sm--20">
                            <div class="form__group">
                                <label class="form__label" for="address">Address <span class="required">*</span></label>
                                <input type="text" name="address" id="address" class="form__input" value="{{ $address->address }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form__group">
                                <label class="form__label" for="kodepos">Kode pos <span class="required">*</span></label>
                                <input type="text" name="kodepos" id="kodepos" class="form__input" value="{{ $address->kodePos }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <div class="form__group">
                                <input type="submit" value="Simpan perubahan" class="btn btn-style-1 btn-submit">
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form__group">
                                <a href="{{ route('account') }}" class="btn btn-style-1 btn-submit">Kembali</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Main Content Wrapper Start -->
@endsection
@section('extra_script')
    <script>
        $("#calc_shipping_city").hide();

        $("select.provinces").change(function(){
            var pointer = $(this).children("option:selected").val();

            $.ajax({
                url:'{{url('/ongkir/city/')}}',
                data:'province=' + pointer,
                type:'get',
                dataType: 'json',
                success:function (response) {
                    console.log(response['rajaongkir']['results']);

                    $("#calc_shipping_citys").hide();
                    $("#calc_shipping_city").show();

                    $("#calc_shipping_city").attr('disabled', false);

                    var $select = $('#calc_shipping_city');

                    $select.find('option').remove();
                    $.each(response['rajaongkir']['results'],function(key, value)
                    {
                        $select.append('<option value=' + value.city_id + '>' + value.city_name + '</option>');
                    });
                }
            });
        });
    </script>
@stop
