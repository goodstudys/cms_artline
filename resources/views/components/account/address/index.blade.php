<div class="tab-pane fade" id="addresses">
    <p class="mb--20">Alamat berikut akan digunakan pada halaman checkout secara default.</p>
    <div class="row">
        <div class="col-md-6 mb-sm--20">
            <div class="text-block">
                <div class="row">
                    <div class="col-9">
                        <h4 class="mb--20">Alamat Tagihan</h4>
                    </div>
                    @if ($billings->count() == '0')
                        <div class="col-3"></div>
                    @elseif ($billings->count() == '5')
                        <div class="col-3"></div>
                    @else
                        <div class="col-3">
                            <a class="mt--15 font-bold text-danger" href="{{ url('/address/create/'.'billing') }}">Tambah baru</a>
                        </div>
                    @endif
                </div>
                @if($billings->count() == '0')
                    <a href="{{ url('/address/create/'.'billing') }}">Buat sekarang</a>
                    <p>Kamu belum memiliki alamat Penagihan sama sekali.</p>
                @else
                    <div class="table-content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="text-left">Label</th>
                                <th class="text-right">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($billings as $billing)
                                <tr>
                                    <td class="wide-column">{{ $billing->label }}</td>
                                    <td class="text-center" ><a href="{{ route('address.show', $billing->rowPointer) }}">Lihat</a></td>
                                    <td class="text-center" ><a href="{{ url('/address/edit/'.'billing', $billing->rowPointer) }}">Edit</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="text-block">
                <div class="row">
                    <div class="col-9">
                        <h4 class="mb--20">Alamat Pengiriman</h4>
                    </div>
                    @if ($deliverys->count() == '0')
                        <div class="col-3"></div>
                    @elseif ($deliverys->count() == '5')
                        <div class="col-3"></div>
                    @else
                        <div class="col-3">
                            <a class="mt--15 font-bold text-danger" href="{{ url('/address/create/'.'shopping') }}">Tambah baru</a>
                        </div>
                    @endif
                </div>
                @if ($deliverys->count() == '0')
                    <a href="{{ url('/address/create/'.'shopping') }}">Buat Sekarang</a>
                    <p>Kamu belum memiliki alamat pengiriman sama sekali.</p>
                @else
                    <div class="table-content">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="text-left">Label</th>
                                <th class="text-right">Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($deliverys as $delivery)
                                <tr>
                                    <td class="wide-column">{{ $delivery->label }}</td>
                                    <td class="text-center" ><a href="{{ route('address.show', $delivery->rowPointer) }}">Lihat</a></td>
                                    <td class="text-center" ><a href="{{ url('/address/edit/'.'shopping', $delivery->rowPointer) }}">Edit</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
