<div class="tab-pane fade show active" id="dashboard">
    <p>Hallo <strong>{{ Auth::user()->displayName }}</strong> (bukan <strong>{{ Auth::user()->displayName }}</strong>?
        <a href="{{ route('logout') }}"
            onclick="event.preventDefault();document.getElementById('logout-form').submit();">Keluar</a>)</p>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
    <p>Dari dasbord akun Anda. Anda dapat dengan mudah memeriksa & melihat <a href="">pesanan terakhir Anda</a>,
        mengelola <a href="">alamat pengiriman dan penagihan Anda</a> dan <a href="">serta mengedit kata sandi dan
            detail akun Anda</a>.</p>
    @if (Session::has('status'))
        <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
    @endif
     <div class="">
        @if (Auth::user()->level == 3)
        <div class="order-details">
            <div class="row">
                <div class="col">
                    <div class="mx-auto text-black text-center">
                        <h4 class="text-justify">Ayo ajukan dirimu dan jadilah reseller kami mulai sekarang juga!</h4>
                    </div>
                    <br>
                    {{-- <form action="{{ route('jobrequest.store') }}" method="post">
                        @csrf
                        <input type="text" name="job" class="d-none" value="RESELLER" readonly>
                        <button type="submit" class="btn btn-style-1 btn-submit">Ajukan Sekarang!</button>
                    </form> --}}
                    {{-- <a class="nav-link" data-toggle="pill" role="tab" href="#pengajuanreseller" aria-controls="pengajuanreseller"
                    aria-selected="true">Pengajuan Reseller</a>
                <a href="#pengajuanreseller" class="btn btn-style-1 btn-submit">Ajukan Sekarang!</a> --}}
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
