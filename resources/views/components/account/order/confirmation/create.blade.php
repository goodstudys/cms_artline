@extends('master')
{{-- @section('nav')
    @include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')
    <!-- Main Content Wrapper Start -->
    <div id="content" class="main-content-wrapper">
        <div class="page-content-inner" style="margin-top: 50px;">
            <div class="container">
                @if (Session::has('status'))
                    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                @endif

                @if (count($errors) > 0)

                    <div class="alert alert-danger">

                        <strong>Whoops!</strong> There were some problems with your input.

                        <ul>

                            @foreach ($errors->all() as $error)

                                <li>{{ $error }}</li>

                            @endforeach

                        </ul>

                    </div>

                @endif
                <fieldset class="form__fieldset mb--20">
                    <legend class="form__legend">Confirmation form</legend>
                    <form action="{{ route('confirmation.store') }}" method="POST" enctype="multipart/form-data" class="form form--account">
                        @csrf
                        <div class="row mb--20">
                            <input type="hidden" name="order" id="order" class="form__input" value="{{ Request()->id }}">
                            <div class="col-12">
                                <div class="form__group">
                                    <label class="form__label" for="dropship_name">Nama Pengirim di Rekening Bank<span class="required">*</span></label>
                                    <input type="text" name="nama" id="nama" class="form__input" >
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form__group">
                                    <label class="form__label" for="dropship_name">Transfer dari Bank<span class="required">*</span></label>
                                    <input type="text" name="bank" id="bank" class="form__input" >
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form__group">
                                    <label class="form__label" for="dropship_name">Masukkan No. Rekening Anda<span class="required">*</span></label>
                                    <input type="text" name="akun" id="akun" class="form__input" >
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form__group">
                                    <label class="form__label" for="dropship_name">Tujuan Transfer<span class="required">*</span></label>
                                    <input type="text" name="tujuan" id="tujuan" class="form__input" >
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form__group">
                                    <label class="form__label" for="dropship_name">Jumlah Transfer (Rp)<span class="required">*</span></label>
                                    <input type="text" name="jumlah" id="jumlah" class="form__input" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-2">
                                <div class="form__group">
                                    <input type="submit" value="Kirim" class="btn btn-style-1 btn-submit">
                                </div>
                            </div>
                            <div class="col-2">
                                <div class="form__group">
                                    <a href="{{ route('account') }}" class="btn btn-style-1 btn-submit">Kembali</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </fieldset>
            </div>
        </div>
    </div>
    <!-- Main Content Wrapper Start -->
@endsection


