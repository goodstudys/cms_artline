@extends('master')
{{-- @section('nav')
@include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')
    <!-- Main Content Wrapper Start -->
    <div id="content" class="main-content-wrapper">
        <div class="page-content-inner" style="margin-top: 50px;">
            <div class="container">
                <div class="row pt--50 pt-md--40 pt-sm--20 pb--65 pb-md--45 pb-sm--25">
                    <div class="col-12" id="main-content">
                        <div class="table-content table-responsive">
                            <table class="table table-style-2 wishlist-table text-center">
                                <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th class="text-left">Nama</th>
                                    <th>Jumlah order</th>
                                    <th>Total</th>
                                    <th>Kurir</th>
                                    <th>No. Resi</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>

                                    </td>
                                    <td></td>
                                    <td class="product-name text-left wide-column">
                                        {{ $data->billName }}
                                    </td>
                                    <td class="product-name">
                                        {{ $data->quantity }}
                                    </td>
                                    <td class="product-price">
                                        Rp.{{number_format($data->total, 2)}}
                                    </td>
                                    <td class="product-name">
                                        {{ $data->kurirName }}
                                    </td>
                                    <td class="product-price">
                                        {{ $data->noResi }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-2">
                        <div class="form__group">
                            <a href="{{ route('account') }}" class="btn btn-style-1 btn-submit">Kembali</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Content Wrapper Start -->
@endsection
