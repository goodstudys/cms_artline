@extends('master')
{{-- @section('nav')
@include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')
<!-- Main Content Wrapper Start -->
<div id="content" class="main-content-wrapper">
    <div class="page-content-inner" style="margin-top: 50px;">
        <div class="container">
            <div class="row pt--50 pt-md--40 pt-sm--20 pb--65 pb-md--45 pb-sm--25">
                <div class="col-12" id="main-content">
                    <div class="table-content table-responsive">
                        <table class="table table-style-2 wishlist-table text-center">
                            <thead>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th class="text-left">Produk</th>
                                    <th>Jumlah</th>
                                    <th>Harga</th>
                                    {{-- <th>Diskon</th> --}}
                                    {{-- <th>Harga akhir</th> --}}
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($detail as $index => $item)


                                <tr>
                                    <td>{{$index+1}}</td>
                                    <td class="product-thumbnail text-left">
                                        <img src="{{ asset('/upload/users/products/'. $item->image) }}" alt="Product Thumnail">
                                    </td>
                                    <td class="product-name text-left wide-column">
                                        <h3>
                                            <a href="{{ route('shop.show', $item->rowPointer) }}">{{ $item->name }}</a>
                                        </h3>
                                    </td>
                                    <td class="product-stock">
                                        {{$item->quantity}}
                                    </td>

                                    <td class="product-price">
                                        <span class="product-price-wrapper">
                                            <span class="money">Rp.{{number_format($item->price, 2)}}</span>
                                        </span>
                                    </td>
                                    {{-- <td class="product-price">
                                        <span class="product-price-wrapper">
                                            <span class="money">@if ($item->discAmount !== '0' || $item->discPercent !==
                                                '0')
                                                yes
                                                @else
                                                No
                                                @endif</span>
                                        </span>
                                    </td> --}}
                                    {{-- <td class="product-price">
                                        @if ($item->discAmount === '0' && $item->discPercent === '0')
                                        <span class="product-price">
                                            <span class="money"
                                                name="price">Rp.{{number_format($item->price * $item->quantity, 2)}}</span>
                                        </span>
                                        @elseif($item->discAmount === '0')
                                        <span class="product-price-new">
                                            <span class="money"
                                                name="price">Rp.{{number_format(($item->price - ($item->price*$item->discPercent/100))*$item->quantity, 2)}}</span>
                                        </span>
                                        @elseif($item->discPercent === '0')
                                        <span class="product-price-new">
                                            <span class="money"
                                                name="price">Rp.{{number_format(($item->price-$item->discAmount)*$item->quantity, 2)}}</span>
                                        </span>
                                        @endif
                                    </td> --}}
                                    <td class="product-action-btn">
                                        <div style="display: none;">
                                            {{ Session::put('item', $item->rowPointer) }}
                                            {{ Session::get('item') }}
                                        </div>
                                        {{--                                            <span>{{ $item->rowPointer }}</span>--}}
                                        <a href="{{Route('rating.create', ['id' => Request()->id, 'order' => $item->rowPointer])}}"
                                            id="addrating"> Kasih rating</a>
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td>Total</td>
                                    <td></td>
                                    <td>
                                    </td>
                                    <td>

                                    </td>
                                    <td>

                                    </td>
                                    <td>

                                    </td>

                                    <td class="product-price">
                                        <span class="product-price">
                                            <span class="money"
                                                name="price">Rp.{{number_format($order->subtotal, 2)}}</span>
                                        </span>

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-2">
                    <div class="form__group">
                        <a href="{{ route('account') }}" class="btn btn-style-1 btn-submit">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main Content Wrapper Start -->
@endsection
