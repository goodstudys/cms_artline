<div class="tab-pane fade" id="orders">
    <div class="message-box mb--30 d-none">
        <p><i class="fa fa-check-circle"></i>No order has been made yet.</p>
        <a href="{{ Route('shop') }}">Go Shop</a>
    </div>
    <div class="table-content table-responsive">
        @if ($orders->count() == 0)
        <h4>Anda belum memesan apa pun.</h4>
        @else
        <table class="table text-center">
            <thead>
                <tr>
                    <th>Order</th>
                    <th>Tanggal</th>
                    <th>Status</th>
                    <th>Total</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $index => $item)
                <tr>
                    <td>{{$index+1}}</td>
                    <td class="wide-column">{{$item->dateCreated}}</td>
                    <td>{{ $item->status }}</td>
                    <td class="wide-column">Rp.{{number_format($item->total, 2)}}</td>
                    <td>
                        <a href="{{ route('order.detail', $item->rowPointer) }}"
                            class="btn btn-small btn-style-1">Lihat</a>
                        @if($item->status == 'CANCEL')
                        <a href="{{ route('confirmation.create', $item->id) }}"
                            class="btn btn-small btn-style-1 disabled">Konfirmasi</a>
                        @elseif ($item->status == 'NEW')
                        <a href="{{ route('confirmation.create', $item->id) }}"
                            class="btn btn-small btn-style-1">Konfirmasi</a>
                        @elseif ($item->status == 'PACKING')
                        <a href="{{ route('confirmation.create', $item->id) }}"
                            class="btn btn-small btn-style-1">Konfirmasi</a>
                        @else
                        <a href="{{ route('order.checkResi', $item->rowPointer) }}"
                            class="btn btn-small btn-style-1">Check No. Resi</a>
                        @endif
                    </td>
                </tr>

                @endforeach
            </tbody>
        </table>
        @endif
        {{--
                    <tr>
                        <td>2</td>
                        <td class="wide-column">September 19, 2019</td>
                        <td>Processing</td>
                        <td class="wide-column">$49.00 for 1 item</td>
                        <td><a href="{{ route('#') }}" class="btn btn-medium btn-style-1">View</a></td>
        </tr> --}}

    </div>
</div>