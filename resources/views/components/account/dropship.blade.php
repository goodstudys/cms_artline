<div class="tab-pane fade" id="dropship">
    @if (!empty($user->dropName))
        @if ($user->isDropship == 1)
            <div class="row">
                <div class="mb--20 col-md-3 col-sm-12">
                    <img class="dropship-image" src="{{ asset('upload/users/dropships').'/'.$user->dropImage }}" alt="dropship image">
                </div>
                <div class="row mb--20 col-md-9">
                    <div class="col-12">
                        <div class="form__group">
                            <label class="form__label" for="dropship_name">Dropship name</label>
                            <input type="text" value="{{ $user->dropName }}" class="form__input" readonly="">
                        </div>
                    </div>
{{--                    <div class="row ml-4">--}}
{{--                        <div>--}}
{{--                            <a class="btn btn-style-1" href="{{ route('dropship.update') }}">Disable</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        @endif
    @endif
    @if (Session::has('status'))
        <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
    @endif

    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif

        <fieldset class="form__fieldset mb--20">
            @if ($user->isDropship == 1)
                <legend class="form__legend">Ubah dropship</legend>
            @else
                <legend class="form__legend">Buat dropship</legend>
            @endif
            <form action="{{ route('dropship.store') }}" method="POST" enctype="multipart/form-data" class="form form--account">
                @csrf
                <div class="row mb--20">
                    <div class="col-12">
                        <div class="form__group">
                            <label class="form__label" for="dropship_name">Nama dropship<span class="required">*</span></label>
                            <input type="text" name="dropship_name" id="dropship_name" class="form__input">
                        </div>
                    </div>
                </div>
                <div class="row mb--20">
                    <div class="col-12">
                        <div class="form__group">
                            <label class="form__label" for="dropship_image">Gambar dropship <span class="required">*</span></label>
                            <input type="file" name="dropship_image" id="dropship_image" class="form__input--2">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="form__group">
                            <input type="submit" value="Simpan" class="btn btn-style-1 btn-submit">
                        </div>
                    </div>
                </div>
            </form>
        </fieldset>
</div>
