<div class="tab-pane fade" id="accountdetails">
    @if (Session::has('status'))
        <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
    @endif

    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif
    <form action="{{ route('account.changepswd', Auth::user()->id) }}" class="form form--account">
        <div class="row grid-space-30 mb--20">
            <div class="col-md-6 mb-sm--20">
                <div class="form__group">
                    <label class="form__label" for="f_name">Nama depan <span class="required">*</span></label>
                    <input type="text" name="f_name" id="f_name" class="form__input" value="{{ $user->firstName }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form__group">
                    <label class="form__label" for="l_name">Nama belakang <span class="required">*</span></label>
                    <input type="text" name="l_name" id="l_name" class="form__input" value="{{ $user->lastName }}">
                </div>
            </div>
        </div>
        <div class="row mb--20">
            <div class="col-12">
                <div class="form__group">
                    <label class="form__label" for="d_name">Nama tampilan <span class="required">*</span></label>
                    <input type="text" name="d_name" id="d_name" class="form__input" value="{{ $user->displayName }}">
                    <span class="suggestion"><em>Nama yang akan ditampilkan di bagian akun dan ulasan</em></span>
                </div>
            </div>
        </div>
        <div class="row mb--20">
            <div class="col-12">
                <div class="form__group">
                    <label class="form__label" for="email">Alamat email<span class="required">*</span></label>
                    <input type="email" name="email" id="email" class="form__input" value="{{ $user->email }}">
                </div>
            </div>
        </div>
        <fieldset class="form__fieldset mb--20">
            <legend class="form__legend">Ubah password</legend>
            <div class="row mb--20">
                <div class="col-12">
                    <div class="form__group">
                        <label class="form__label" for="cur_pass">Password lama (biarkan kosong jika tidak ingin mengganti)</label>
                        <input type="password" name="cur_pass" id="cur_pass" class="form__input">
                    </div>
                </div>
            </div>
            <div class="row mb--20">
                <div class="col-12">
                    <div class="form__group">
                        <label class="form__label" for="new_pass">Password baru (biarkan kosong jika tidak ingin mengganti)</label>
                        <input id="password" type="password" placeholder="Password"
                               class="form__input @error('password') is-invalid @enderror" name="password" required
                               autocomplete="new-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form__group">
                        <label class="form__label" for="conf_new_pass">Konfirmasi password baru</label>
                        <input id="password-confirm" placeholder="Konfirmasi Password" type="password" class="form__input"
                               name="password_confirmation" required autocomplete="new-password">
                    </div>
                </div>
            </div>
        </fieldset>
        <div class="row">
            <div class="col-12">
                <div class="form__group">
                    <input type="submit" value="Save Changes" class="btn btn-style-1 btn-submit">
                </div>
            </div>
        </div>
    </form>
</div>
