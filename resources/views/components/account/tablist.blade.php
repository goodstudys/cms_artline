<div class="user-dashboard-tab__head nav flex-md-column" role="tablist" aria-orientation="vertical">
    <a class="nav-link active" data-toggle="pill" role="tab" href="#dashboard" aria-controls="dashboard"
        aria-selected="true">Dashboard</a>
    {{--    <a class="nav-link" data-toggle="pill" role="tab" href="#dropship" aria-controls="dropship"--}}
    {{--        aria-selected="true">Dropship</a>--}}
    {{-- <a class="nav-link" data-toggle="pill" role="tab" href="#confirmation" aria-controls="confirmation"
        aria-selected="true">Confirmation</a> --}}
    <a class="nav-link" data-toggle="pill" role="tab" href="#orders" aria-controls="orders"
        aria-selected="true">Pesanan</a>
    @if (Auth::user()->level == 4)
    <a class="nav-link" data-toggle="pill" role="tab" href="#ordersresseller" aria-controls="orders"
        aria-selected="true">Pre-Order</a>
    @endif
    <a class="nav-link" data-toggle="pill" role="tab" href="#testimonial" aria-controls="orders"
        aria-selected="true">Testimoni</a>
    {{--    <a class="nav-link" data-toggle="pill" role="tab" href="#downloads" aria-controls="downloads"--}}
    {{--        aria-selected="true">Downloads</a>--}}
    <a class="nav-link" data-toggle="pill" role="tab" href="#addresses" aria-controls="addresses"
        aria-selected="true">Alamat</a>
    <a class="nav-link" data-toggle="pill" role="tab" href="#accountdetails" aria-controls="accountdetails"
        aria-selected="true">Detail Akun</a>
        @if (Auth::user()->level == 3)
        <a class="nav-link" data-toggle="pill" role="tab" href="#pengajuanreseller" aria-controls="pengajuanreseller"
        aria-selected="true">Pengajuan Reseller</a>
        @endif

    <a class="nav-link" data-toggle="pill" role="tab" href="#newsletter" aria-controls="newsletter"
        aria-selected="true">Newsletter</a>
    <a class="nav-link" href="{{ route('logout') }}"
        onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</div>