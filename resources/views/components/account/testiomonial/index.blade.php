<div class="tab-pane fade" id="testimonial">
    @if (Session::has('status'))
    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
    @endif

    @if (count($errors) > 0)

    <div class="alert alert-danger">

        <strong>Whoops!</strong> There were some problems with your input.

        <ul>

            @foreach ($errors->all() as $error)

            <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

    @endif
    @if ($address != null)
    <fieldset class="form__fieldset mb--20">
        <legend class="form__legend">Testimonial form</legend>
        <form action="{{ route('testimonial.store') }}" method="POST" enctype="multipart/form-data"
            class="form form--account">
            @csrf
            <div class="row mb--20">
                <div class="col-12">
                    <div class="form__group mb--20">
                        <label class="form__label" for="dropship_name">Rating<span class="required">*</span></label>
                        <select name="rate-option" id="rate-option" class="form__input rate-option">
                            <option value="5">
                                <div class="star-rating star-five"><span>Rated <strong class="rating">5</strong></span>
                                </div>
                            </option>
                            <option value="4">
                                <div class="star-rating star-four"><span>Rated <strong class="rating">4</strong></span>
                                </div>
                            </option>
                            <option value="3">
                                <div class="star-rating star-three"><span>Rated <strong class="rating">3</strong></span>
                                </div>
                            </option>
                            <option value="2">
                                <div class="star-rating star-two"><span>Rated <strong class="rating">2</strong></span>
                                </div>
                            </option>
                            <option value="1">
                                <div class="star-rating star-one"><span>Rated <strong class="rating">1</strong></span>
                                </div>
                            </option>
                        </select>
                        <input type="hidden" name="rate" id="rate" value="5">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form__group">
                        <label class="form__label" for="dropship_name">Pesan<span class="required">*</span></label>
                        <textarea type="text" name="message" id="message" class="form__input--textarea"
                            style="width: 100%"></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form__group">
                        <input type="submit" value="Simpan" class="btn btn-style-1 btn-submit">
                    </div>
                </div>
            </div>
        </form>
    </fieldset>
    @else
    <h4>Silahkan Melakukan order barang terlebih dahulu untuk bisa mengisi testimoni pada website artline</h4>
    @endif

</div>