<div class="tab-pane fade" id="pengajuanreseller">
    @if (Session::has('status'))
    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
    @endif

    @if (count($errors) > 0)

    <div class="alert alert-danger">

        <strong>Whoops!</strong> There were some problems with your input.

        <ul>

            @foreach ($errors->all() as $error)

            <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

    @endif
    @if ($reseller != null)
    <div class="mx-auto text-black text-center">
        <h4 class="text-justify">Anda Sudah Melakukan Pengajuan, Mohon Menunggu Konfirmasi dari Admin Kami</h4>
    </div>
    @else
    <form action="{{ route('reseller.store.user')  }}" method="POST" enctype="multipart/form-data" class="form">
        @csrf
        {{-- <div class="form-group">
            <label for="inputText3" class="col-form-label">Nama User</label>
            <select name="user_id" id="level" class="form__input" required>
                <option value="" selected="true" disabled="disabled">Pilih User</option>
                @foreach ($data as $item)
                <option value="{{$item->id}}">{{$item->firstName}}</option>
        @endforeach

        </select>
</div> --}}
<input style="display: none" type="text" name="user_id" value="{{$user->id}}" id="">
<div class="form-group">
    <label for="inputText3" class="col-form-label">Keperluan Belanja </label>
    <select name="shopfor" id="level" class="form__input" onchange="checkAlert(event)">
        <option value="" selected="true" disabled="disabled">Pilih Keperluan</option>
        <option value="1">Toko Offline</option>
        <option value="2">Toko Online</option>
        <option value="3">Supplier/Perusahaan
            pengadaan
            barang</option>
        <option value="4">Lembaga Pendidikan</option>
        <option id="other" value="5">Other</option>
    </select>
</div>
<input id="inputother" placeholder="Other" class="form__input" type="text" name="othertext"
    onchange="changeradioother()">
<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="inputText3" class="col-form-label">KTP</label>
            <input id="subtitle" name="ktp" type="text" class="form__input" required>
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="inputText3" class="col-form-label">Foto KTP</label>
            <input type='file' name="ktp_image" class="form__input" onchange="readURL(this,'#ktp_image');" required />
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="exampleFormControlInput1">Preview Image</label><br>
            <img id="ktp_image" style="width: 20%" src="{{asset('assets/img/default.png')}}" alt="your image" />
        </div>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="form-group">
            <label for="inputText3" class="col-form-label">NPWP (optional)</label>
            <input id="subtitle" name="npwp" type="text" class="form__input">
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="inputText3" class="col-form-label">Foto NPWP</label>
            <input type='file' name="npwp_image" class="form__input" onchange="readURL(this,'#npwp_image');" />
        </div>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="exampleFormControlInput1">Preview Image</label><br>
            <img id="npwp_image" style="width: 20%" src="{{asset('assets/img/default.png')}}" alt="your image" />
        </div>
    </div>
</div>
<div class="form-group">
    <label for="inputText3" class="col-form-label">Alamat Usaha</label>
    <input id="link" name="work_address" type="text" class="form__input" required>
</div>
<div class="form-group">
    <label for="inputText3" class="col-form-label">Kota Usaha</label>
    <input id="email" name="work_city" type="text" class="form__input" required>
</div>
<div class="form-group">
    <label for="inputText3" class="col-form-label">Nomor telepon perusahaan</label>
    <input id="link" name="work_phone" type="text" class="form__input" required>
</div>
<div class="form-group">
    <label for="inputText3" class="col-form-label">Jabatan</label>
    <input id="isi" name="position" type="text" class="form__input" required>
</div>

<div class="form__group">
    <input type="submit" value="Save" class="btn btn-style-1 btn-submit">
</div>
</form>
@endif

</div>