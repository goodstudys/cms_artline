<div class="tab-pane fade" id="newsletter">
    @if (Session::has('status'))
        <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
    @endif

    @if (count($errors) > 0)

        <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.

            <ul>

                @foreach ($errors->all() as $error)

                    <li>{{ $error }}</li>

                @endforeach

            </ul>

        </div>

    @endif

    <fieldset class="form__fieldset mb--20">
        @if ($user->newsletter == null)
            <legend class="form__legend">Add newsletter</legend>
        @else
            <legend class="form__legend">Change newsletter</legend>
        @endif
        <form action="{{ route('newsletter.update') }}" method="POST" enctype="multipart/form-data" class="form form--account">
            @csrf
            <div class="row mb--20">
                <div class="col-12">
                    <div class="form__group">
                        <label class="form__label" for="dropship_name">Newsletter email<span class="required">*</span></label>
                        <input type="text" name="newsletter" id="newsletter" class="form__input" value="{{ $user->newsletter }}">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form__group">
                        <input type="submit" value="Simpan" class="btn btn-style-1 btn-submit">
                    </div>
                </div>
            </div>
        </form>
    </fieldset>
</div>
