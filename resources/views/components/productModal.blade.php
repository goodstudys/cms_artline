<!-- Modal Start -->
<div class="modal fade product-modal" id="productModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="dl-icon-close"></i></span>
                </button>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="charoza-element-carousel product-image-carousel nav-vertical-center nav-style-1"
                            data-slick-options='{
                                      "slidesToShow": 1,
                                      "slidesToScroll": 1,
                                      "arrows": true,
                                      "prevArrow": {"buttonClass": "slick-btn slick-prev", "iconClass": "dl-icon-left" },
                                      "nextArrow": {"buttonClass": "slick-btn slick-next", "iconClass": "dl-icon-right" }
                                  }'>
                            <div class="product-image">
                                <div class="product-image--holder">
                                    <a href="single-product.html">
                                        <img src="{{asset('assets/img/products/prod-7-1.jpg')}}" alt="Product Image"
                                            class="primary-image">
                                    </a>
                                </div>
                                <span class="product-badge sale">sale</span>
                            </div>
                            <div class="product-image">
                                <div class="product-image--holder">
                                    <a href="single-product.html">
                                        <img src="{{asset('assets/img/products/prod-7-2.jpg')}}" alt="Product Image"
                                            class="primary-image">
                                    </a>
                                </div>
                                <span class="product-badge sale">sale</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="modal-box product-summary">
                            <div
                                class="product-top-meta d-flex justify-content-between flex-sm-row flex-column-reverse">
                                <div class="product-rating d-flex">
                                    <div class="star-rating star-five">
                                        <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                                    </div>
                                    <a href="" class="review-link">(1 customer review)</a>
                                </div>
                                <div class="product-navigation">
                                    <a href="#" class="prev"><i class="dl-icon-left"></i></a>
                                    <a href="#" class="next"><i class="dl-icon-right"></i></a>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <h3 class="product-title mb--30 mb-md--20">Waxed-effect pleated skirt</h3>
                            <div class="product-price-wrapper mb--30 mb-md--20">
                                <span class="product-price-old">
                                    <span class="money">&pound;60.00</span>
                                </span>
                                <span class="money">&pound;49.00</span>
                            </div>
                            <p class="product-short-description mb--30 mb-md--20">Donec accumsan auctor iaculis. Sed
                                suscipit arcu ligula, at egestas magna molestie a. Proin ac ex maximus, ultrices justo
                                eget, sodales orci. Aliquam egestas libero ac turpis pharetra, in vehicula lacus
                                scelerisque. Vestibulum ut sem laoreet, feugiat tellus at, hendrerit arcu.</p>
                            <div class="product-action d-flex flex-row align-items-center mb--20">
                                <div class="quantity mr--20">
                                    <input type="number" class="quantity-input" name="qty" id="quick-qty" value="1"
                                        min="1">
                                </div>
                                <button type="button" class="btn btn-style-1 btn-semi-large btn-shape-round add-to-cart"
                                    onclick="window.location.href='{{Route('cart')}}'">
                                    Add To Cart
                                </button>
                                {{-- <a href="{{ Route('wishlist') }}" class="action-btn action-btn-square"><i
                                    class="dl-icon-heart2"></i></a>
                                <a href="#" class="action-btn action-btn-square"><i class="dl-icon-compare2"></i></a>
                                --}}
                            </div>
                            <div class="product-extra mb--20">
{{--                                <a href="#" class="font-size-12"><i class="fa fa-map-marker"></i>Find store near you</a>--}}
{{--                                <a href="#" class="font-size-12"><i class="fa fa-exchange"></i>Delivery and return</a>--}}
                            </div>
                            <div class="product-summary-footer">
                                <div class="product-meta mb--20">
                                    <span class="sku_wrapper font-size-12">SKU: <span class="sku">REF.
                                            LA-887</span></span>
                                    <span class="posted_in font-size-12">Categories: <a href="{{ Route('shop') }}"
                                            rel="tag">Fashions</a></span>
                                </div>
                                <div class="product-share-box">
                                    <span class="font-size-12">Share With</span>
                                    <!-- Social Icons Start Here -->
                                    <ul class="social social-small">
                                        <li class="social__item">
                                            <a href="facebook.com" class="social__link">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="social__item">
                                            <a href="twitter.com" class="social__link">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="social__item">
                                            <a href="plus.google.com" class="social__link">
                                                <i class="fa fa-google-plus"></i>
                                            </a>
                                        </li>
                                        <li class="social__item">
                                            <a href="plus.google.com" class="social__link">
                                                <i class="fa fa-pinterest-p"></i>
                                            </a>
                                        </li>
                                    </ul>
                                    <!-- Social Icons End Here -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal End -->
