@if ($paginator->hasPages())
<ul class="pagination">
    {{-- Previous Page Link --}}
    @if ($paginator->onFirstPage())
    <li><a href="#" class="prev page-number disabled"><i class="fa fa-long-arrow-left"></i></a>
    </li>
    {{-- <li class="disabled"><span>← Previous</span></li> --}}
    @else
    <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev" class="prev page-number "><i
                class="fa fa-long-arrow-left"></i></a></li>
    {{-- <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">← Previous</a></li> --}}
    @endif


    {{-- Pagination Elements --}}
    @foreach ($elements as $element)
    {{-- "Three Dots" Separator --}}
    @if (is_string($element))
    <li class="page-number disabled"><span class="page-number ">{{ $element }}</span></li>
    @endif


    {{-- Array Of Links --}}
    @if (is_array($element))
    @foreach ($element as $page => $url)
    @if ($page == $paginator->currentPage())
    <li> <span class="current page-number active">{{ $page }}</span></li>
    {{-- <li class="active my-active"><span>{{ $page }}</span></li> --}}
    @else
    <li><a href="{{ $url }}" class="page-number">{{ $page }}</a></li>
    {{-- <li><a href="{{ $url }}">{{ $page }}</a></li> --}}
    @endif
    @endforeach
    @endif
    @endforeach


    {{-- Next Page Link --}}
    @if ($paginator->hasMorePages())
    <li><a href="{{ $paginator->nextPageUrl() }}" class=" next page-number"><i class="fa fa-long-arrow-right"></i></a>
    </li>
    {{-- <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">Next →</a></li> --}}
    @else
    <li><a href="#" class=" next page-number"><i class="fa fa-long-arrow-right disabled"></i></a>
    </li>
    {{-- <li class="disabled"><span>Next →</span></li> --}}
    @endif
</ul>
@endif