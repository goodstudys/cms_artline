@extends('master')
{{-- @section('nav')
@include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')
<!-- Breadcrumb area Start -->
<div class="breadcrumb-area breadcrumb-style-2 pt--75 pt-md--55 pt-sm--35">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <ul class="breadcrumb">
                    <li><a href="{{ Route('index') }}">Home</a></li>
                    <li><a href="{{ Route('shop') }}">Shop</a></li>
                    <li class="current"><span>{{$product->name}}</span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb area End -->

<!-- Main Content Wrapper Start -->
<div id="content" class="main-content-wrapper">
    <div class="page-content-inner enable-full-width">
        <div class="container-fluid">
            <div class="row pt--40 pt-sm--30">
                <div class="col-md-6 product-main-image">
                    <div class="product-image">
                        <div class="product-gallery vertical-slide-nav">
                            <div class="product-gallery__nav-image">
                                <div class="charoza-element-carousel nav-slider slick-vertical product-slide-nav"
                                    data-slick-options='{
                                        "slidesToShow": 3,
                                        "slidesToScroll": 1,
                                        "vertical": true,
                                        "swipe": true,
                                        "verticalSwiping": true,
                                        "infinite": true,
                                        "focusOnSelect": true,
                                        "asNavFor": ".main-slider",
                                        "arrows": true,
                                        "prevArrow": {"buttonClass": "slick-btn slick-prev", "iconClass": "fa fa-angle-up" },
                                        "nextArrow": {"buttonClass": "slick-btn slick-next", "iconClass": "fa fa-angle-down" }
                                    }' data-slick-responsive='[
                                        {
                                            "breakpoint":991,
                                            "settings": {
                                                "slidesToShow": 3,
                                                "vertical": false
                                            }
                                        },
                                        {
                                            "breakpoint":767,
                                            "settings": {
                                                "slidesToShow": 4,
                                                "vertical": false
                                            }
                                        },
                                        {
                                            "breakpoint":575,
                                            "settings": {
                                                "slidesToShow": 3,
                                                "vertical": false
                                            }
                                        },
                                        {
                                            "breakpoint":480,
                                            "settings": {
                                                "slidesToShow": 2,
                                                "vertical": false
                                            }
                                        }
                                    ]'>
                                    <figure class="product-gallery__nav-image--single">
                                        <img src="{{ asset('/upload/users/products/'. $product->image1) }}"
                                            alt="Products">
                                    </figure>
                                    @if ($product->image2)
                                    <figure class="product-gallery__nav-image--single">
                                        <img src="{{ asset('/upload/users/products/'. $product->image2) }}"
                                            alt="Products">
                                    </figure>
                                    @endif
                                    @if ($product->image3)
                                    <figure class="product-gallery__nav-image--single">
                                        <img src="{{ asset('/upload/users/products/'. $product->image3) }}"
                                            alt="Products">
                                    </figure>
                                    @endif
                                    @if ($product->image4)
                                    <figure class="product-gallery__nav-image--single">
                                        <img src="{{ asset('/upload/users/products/'. $product->image4) }}"
                                            alt="Products">
                                    </figure>
                                    @endif
                                </div>
                            </div>
                            <div class="product-gallery__large-image mb-md--30">
                                <div class="product-gallery__wrapper">
                                    <div class="charoza-element-carousel main-slider image-popup" data-slick-options='{
                                            "slidesToShow": 1,
                                            "slidesToScroll": 1,
                                            "infinite": true,
                                            "arrows": false,
                                            "asNavFor": ".nav-slider"
                                        }'>
                                        <figure class="product-gallery__image zoom">
                                            <img src="{{ asset('/upload/users/products/'. $product->image1) }}"
                                                alt="Products">
                                        </figure>
                                        @if ($product->image2 != null)
                                        <figure class="product-gallery__image zoom">
                                            <img src="{{ asset('/upload/users/products/'. $product->image2) }}"
                                                alt="Products">
                                        </figure>
                                        @endif
                                        @if ($product->image3 != null)
                                        <figure class="product-gallery__image zoom">
                                            <img src="{{ asset('/upload/users/products/'. $product->image3) }}"
                                                alt="Products">
                                        </figure>
                                        @endif
                                        @if ($product->image4 != null)
                                        <figure class="product-gallery__image zoom">
                                            <img src="{{ asset('/upload/users/products/'. $product->image4) }}"
                                                alt="Products">
                                        </figure>
                                        @endif
                                    </div>
                                    <div class="product-gallery__actions">
                                        <button class="action-btn-2 btn-zoom-popup"><i
                                                class="dl-icon-zoom-in"></i></button>
                                        {{--                                        <a href="https://www.youtube.com/watch?v=M7pnuxkcLXo"--}}
                                        {{--                                            class="action-btn-2 video-popup"><i class="dl-icon-format-video"></i></a>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($product->discAmount !=='.00' || $product->discPercent !=='0')
                        <span class="product-badge sale">Sale</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-6 product-main-details mt--10 mt-sm--25">
                    <div class="product-summary">
                        <div class="product-top-meta">
                            <div class="product-rating mr-auto">
                                @if ($star > '1.0000' && $star < '2.0000' ) <div class="star-rating star-one-half">
                                    <span>Rated <strong class="rating">1.00</strong> out of 5</span>
                            </div>
                            @elseif($star == '2.0000')
                            <div class="star-rating star-two">
                                <span>Rated <strong class="rating">2.00</strong> out of 5</span>
                            </div>
                            @elseif ($star > '2.0000' && $star < '3.0000' ) <div class="star-rating star-two-half">
                                <span>Rated <strong class="rating">2.00</strong> out of 5</span>
                        </div>
                        @elseif($star == '3.0000' )
                        <div class="star-rating star-three">
                            <span>Rated <strong class="rating">3.00</strong> out of 5</span>
                        </div>
                        @elseif($star > '3.0000' && $star < '4.0000' ) <div class="star-rating star-three-half">
                            <span>Rated <strong class="rating">3.00</strong> out of 5</span>
                    </div>
                    @elseif($star == '4.0000')
                    <div class="star-rating star-four">
                        <span>Rated <strong class="rating">4.00</strong> out of 5</span>
                    </div>
                    @elseif($star > '4.0000' && $star < '5.0000' ) <div class="star-rating star-four-half">
                        <span>Rated <strong class="rating">4.00</strong> out of 5</span>
                </div>
                @elseif($star == '5.0000 ')
                <div class="star-rating star-five">
                    <span>Rated <strong class="rating">5</strong> out of 5</span>
                </div>
                @endif


                {{-- <div class="star-rating star-four-half">
                                        <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                                    </div> --}}
                <a href="" class="review-link">({{ $ratings->count() }} customer review)</a>
            </div>
        </div>
        <div class="clearfix"></div>
        <h3 class="product-title mb--30 mb-lg--20 mb-sm--10">{{$product->name}}</h3>
        @if (Session::has('prodPrice'))
        <div class="product-price-wrapper float-left mb--30 mb-lg--20 mb-sm--10">
            @if (Session::get('proddiscAmount') === '0' && Session::get('proddiscPercent') === '0')
            <span class="product-price">
                <span class="money" name="price"
                    id="singleprice">Rp.{{number_format(Session::get('prodPrice'), 0)}}</span>
                @if (Auth::check() == 1)
                @if (Auth::user()->level != 3)
                <div class="row">
                    <div class="col-12">
                        <span name="wholesale_price" id="singleprice" style="font-size: 12pt">
                            @if ($product->wholesale_price == null || $product->wholesale_price == 0 )
            
                            @else
                            Harga @if($product->wholesale_type == null)
            
                            @else
                            @if ($product->wholesale_type == 'PACK')
                            Grosir Pack
                            @elseif($product->wholesale_type == 'GROSS')
                            Grosir GROSS
                            @elseif($product->wholesale_type == 'TOPLES')
                            Grosir TOPLES
                            @elseif($product->wholesale_type == 'LUSINAN')
                            Grosir Lusinan
                            @else
                            Grosir Set
                            @endif
                            @endif
                            Rp.{{number_format($product->wholesale_price, 0)}}
                            @endif
                        </span>
                    </div>
                </div>
                @endif
                @endif
            </span>
            @elseif(Session::get('proddiscAmount') === '0')
            <span class="product-price-old">
                <span class="money" name="price"
                    id="doublepriceold">Rp.{{number_format(Session::get('prodPrice'), 0)}}</span>
                @if (Auth::check() == 1)
                @if (Auth::user()->level != 3)
                <div class="row">
                    <div class="col-12">
                        <span name="wholesale_price" id="singleprice" style="font-size: 12pt">
                            {{-- Harga Grosir
                            Rp.{{number_format($product->wholesale_price, 0)}} --}}
                        </span>
                    </div>
                </div>
                @endif
                @endif
            </span>
            <span class="product-price-new">
                <span class="money" name="price"
                    id="doublepricenew">Rp.{{number_format(Session::get('prodPrice') - (Session::get('prodPrice')*Session::get('proddiscPercent')/100), 0)}}</span>
            </span>
            <span class="product-price-new">
                @if (Auth::check() == 1)
                @if (Auth::user()->level != 3)
                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <span name="wholesale_price" id="singleprice" style="font-size: 12pt">
                                @if ($product->wholesale_price == null || $product->wholesale_price == 0 )
                
                                @else
                                Harga @if($product->wholesale_type == null)
                
                                @else
                                @if ($product->wholesale_type == 'PACK')
                                Grosir Pack
                                @elseif($product->wholesale_type == 'GROSS')
                                Grosir GROSS
                                @elseif($product->wholesale_type == 'TOPLES')
                                Grosir TOPLES
                                @elseif($product->wholesale_type == 'LUSINAN')
                                Grosir Lusinan
                                @else
                                Grosir Set
                                @endif
                                @endif
                                Rp.{{number_format($product->wholesale_price, 0)}}
                                @endif
                            </span>
                        </div>
                </div>
                @endif
                @endif
            </span>
            @elseif(Session::get('proddiscPercent') === '0')
            <span class="product-price-old">
                <span class="money" name="price"
                    id="doublepriceold">Rp.{{number_format(Session::get('prodPrice'), 0)}}</span>
                @if (Auth::check() == 1)
                @if (Auth::user()->level != 3)
                <div class="row">
                    <div class="col-12">
                        <span name="wholesale_price" id="singleprice" style="font-size: 12pt">
                            <!--Harga Grosir-->
                            <!--Rp.{{number_format($product->wholesale_price, 0)}} -->
                        </span>
                    </div>
                </div>
                @endif
                @endif
            </span>
            <span class="product-price-new">
                <span class="money" name="price"
                    id="doublepricenew">Rp.{{number_format(Session::get('prodPrice')-Session::get('proddiscAmount'), 0)}}</span>

            </span><br>
            <span class="product-price-new">

                @if (Auth::check() == 1)
                @if (Auth::user()->level != 3)
                <div class="row">
                    <div class="col-12">
                            <span name="wholesale_price" id="singleprice" style="font-size: 12pt">
                                @if ($product->wholesale_price == null || $product->wholesale_price == 0 )
                
                                @else
                                Harga @if($product->wholesale_type == null)
                
                                @else
                                @if ($product->wholesale_type == 'PACK')
                                Grosir Pack
                                @elseif($product->wholesale_type == 'GROSS')
                                Grosir GROSS
                                @elseif($product->wholesale_type == 'TOPLES')
                                Grosir TOPLES
                                @elseif($product->wholesale_type == 'LUSINAN')
                                Grosir Lusinan
                                @else
                                Grosir Set
                                @endif
                                @endif
                                Rp.{{number_format($product->wholesale_price, 0)}}
                                @endif
                            </span>
                    </div>
                </div>
                @endif
                @endif
            </span>
            @endif
        </div>
        @else
        <div class="product-price-wrapper float-left mb--30 mb-lg--20 mb-sm--10">
            @if ($product->discAmount === '0' && $product->discPercent === '0')
            <span class="product-price">
                <span class="money" name="price" id="singleprice">Rp.{{number_format($product->price, 0)}}</span>
                @if (Auth::check() == 1)
                @if (Auth::user()->level != 3)
                <div class="row">
                    <div class="col-12">
                        <span name="wholesale_price" id="singleprice" style="font-size: 12pt">
                            @if ($product->wholesale_price == null || $product->wholesale_price == 0 )
            
                            @else
                            Harga @if($product->wholesale_type == null)
            
                            @else
                            @if ($product->wholesale_type == 'PACK')
                            Grosir Pack
                            @elseif($product->wholesale_type == 'GROSS')
                            Grosir GROSS
                            @elseif($product->wholesale_type == 'TOPLES')
                            Grosir TOPLES
                            @elseif($product->wholesale_type == 'LUSINAN')
                            Grosir Lusinan
                            @else
                            Grosir Set
                            @endif
                            @endif
                            Rp.{{number_format($product->wholesale_price, 0)}}
                            @endif
                        </span>
                    </div>
                </div>
                @endif
                @endif
            </span>
            @elseif($product->discAmount === '0')
            <span class="product-price-old">
                <span class="money" name="price" id="doublepriceold">Rp.{{number_format($product->price, 0)}}</span>
                @if (Auth::check() == 1)
                @if (Auth::user()->level != 3)
                <div class="row">
                    <div class="col-12">
                        <span name="wholesale_price" id="singleprice" style="font-size: 12pt">
                            {{-- Harga Grosir
                            Rp.{{number_format($product->wholesale_price, 0)}} --}}
                        </span>
                    </div>
                </div>
                @endif
                @endif
            </span>
            <span class="product-price-new">
                <span class="money" name="price"
                    id="doublepricenew">Rp.{{number_format($product->price - ($product->price*$product->discPercent/100), 0)}}</span>
                {{-- @if (Auth::check() == 1)
                @if (Auth::user()->level != 3)
                <div class="row">
                    <div class="col-12">
                        <span name="wholesale_price" id="singleprice" style="font-size: 12pt">Harga Grosir
                            Rp.{{number_format($product->wholesale_price, 0)}}</span>
        </div>
    </div>
    @endif
    @endif --}}
    </span>
    <span class="product-price-new">

        @if (Auth::check() == 1)
        @if (Auth::user()->level != 3)
        <div class="row">
            <div class="col-12">
                <span name="wholesale_price" id="singleprice" style="font-size: 12pt">
                    @if ($product->wholesale_price == null || $product->wholesale_price == 0 )
    
                    @else
                    Harga @if($product->wholesale_type == null)
    
                    @else
                    @if ($product->wholesale_type == 'PACK')
                    Grosir Pack
                    @elseif($product->wholesale_type == 'GROSS')
                    Grosir GROSS
                    @elseif($product->wholesale_type == 'TOPLES')
                    Grosir TOPLES
                    @elseif($product->wholesale_type == 'LUSINAN')
                    Grosir Lusinan
                    @else
                    Grosir Set
                    @endif
                    @endif
                    Rp.{{number_format($product->wholesale_price, 0)}}
                    @endif
                </span>
            </div>
        </div>
        @endif
        @endif
    </span>
    @elseif($product->discPercent === '0')
    <span class="product-price-old">
        <span class="money" name="price" id="doublepriceold">Rp.{{number_format($product->price, 0)}}</span>
        @if (Auth::check() == 1)
        @if (Auth::user()->level != 3)
        <div class="row">
            <div class="col-12">
                <span name="wholesale_price" id="singleprice" style="font-size: 12pt">
                    {{-- Harga Grosir
                            Rp.{{number_format($product->wholesale_price, 0)}} --}}
                </span>
            </div>
        </div>
        @endif
        @endif
    </span>
    <span class="product-price-new">
        <span class="money" name="price"
            id="doublepricenew">Rp.{{number_format($product->price-$product->discAmount, 0)}}</span>
        {{-- @if (Auth::check() == 1)
                @if (Auth::user()->level != 3)
                <div class="row">
                    <div class="col-12">
                        <span name="wholesale_price" id="singleprice" style="font-size: 12pt">Harga Grosir
                            Rp.{{number_format($product->wholesale_price, 0)}}</span>
</div>
</div>
@endif
@endif --}}
</span>
<span class="product-price-new">
    @if (Auth::check() == 1)
    @if (Auth::user()->level != 3)
    <div class="row">
        <div class="col-12">
            <span name="wholesale_price" id="singleprice" style="font-size: 12pt">
                @if ($product->wholesale_price == null || $product->wholesale_price == 0 )

                @else
                Harga @if($product->wholesale_type == null)

                @else
                @if ($product->wholesale_type == 'PACK')
                Grosir Pack
                @elseif($product->wholesale_type == 'GROSS')
                Grosir GROSS
                @elseif($product->wholesale_type == 'TOPLES')
                Grosir TOPLES
                @elseif($product->wholesale_type == 'LUSINAN')
                Grosir Lusinan
                @else
                Grosir Set
                @endif
                @endif
                Rp.{{number_format($product->wholesale_price, 0)}}
                @endif
            </span>
        </div>
    </div>
    @endif
    @endif
</span>
@endif
</div>
@endif
        <span class="product-stock in-stock float-right">
            <i class="dl-icon-check-circle1"></i>
            {{-- @if (Session::has('prodQty'))
            Stock ({{ Session::get('prodQty') }})
            @else
            Stock ({{ $product->quantity }})
            @endif --}}
            Stock (tersedia)
        </span>
        <div class="clearfix"></div>
        <p class="product-short-description mb--30 mb-lg--20 mb-sm--10 pb-sm--1">
            {!!$product->description!!}</p>
        <form action="{{ route('shop.groupselected', $product->rowPointer) }}" method="post"
            class="variation-form mb--40 mb-lg--30 mb-sm--20">
            @csrf
            <div class="product-size-variations">
                @if (Session::has('prodName'))
                <p class="swatch-label">Color: <strong class="swatch-label">{{ Session::get('prodName') }}</strong></p>
                @else
                @if ($product->min_price != null && $product->max_price != null)
                <p class="swatch-label">Color: <strong class="swatch-label">-</strong></p>
                @else
                <p class="swatch-label">Color: <strong class="swatch-label">{{$product->colorName}}</strong></p>
                @endif
                @endif
                <div class="product-size-swatch variation-wrapper">
                    @if ($checkgroup == 0)
                    @if ($product->min_price != null && $product->max_price != null)
                    <div class="swatch-wrapper pl-3 pr-3 mb-2">
                        <a class="product-size-swatch-btn variation-btn1" data-toggle="tooltip" data-placement="top"
                            title="{{$product->colorName}}" style="width: 100%;">
                            <span class="product-size-swatch-label">{{$product->colorName}}</span>
                            <div style="display: none;">
                                <input type="hidden" id="colors1" value="{{ $product->colorHex }}">
                                <input type="hidden" id="group1" value="{{ $product->rowPointer }}">
                                @if ($product->discAmount === '.00' && $product->discPercent === '0')
                                <span class="product-price">
                                    <span class="money" name="price"><input type="hidden" id="notsingleprice1"
                                            value="Rp.{{number_format($product->price, 2)}}">
                                        <input type="hidden" id="notdoublepricenewss1"
                                            value="{{ $product->price, 2 }}"></span>
                                </span>
                                @elseif($product->discAmount === '.00')
                                <span class="product-price-old">
                                    <span class="money" name="price"><input class="product-price-old" type="hidden"
                                            id="notdoublepriceold1"
                                            value="Rp.{{number_format($product->price, 2)}}"></span>
                                </span>
                                <span class="product-price-new">
                                    <span class="money" name="price"><input type="hidden" id="notdoublepricenew1"
                                            value="Rp.{{number_format($product->price - ($product->price*$product->discPercent/100), 2)}}"><input
                                            type="hidden" id="notdoublepricenewss1"
                                            value="{{ $product->price - ($product->price*$product->discPercent/100), 2 }}"></span>
                                </span>
                                @elseif($product->discPercent === '0')
                                <span class="product-price-old">
                                    <span class="money" name="price"><input class="product-price-old" type="hidden"
                                            id="notdoublepriceold1"
                                            value="Rp.{{number_format($product->price, 2)}}"></span>
                                </span>
                                <span class="product-price-new">
                                    <span class="money" name="price"><input type="hidden" id="notdoublepricenew1"
                                            value="Rp.{{number_format($product->price-$product->discAmount, 2)}}"><input
                                            type="hidden" id="notdoublepricenewss1"
                                            value="{{ $product->price-$product->discAmount, 2 }}"></span>
                                </span>
                                @endif
                            </div>
                        </a>
                    </div>
                    @endif
                    @endif
                    @foreach($groups as $group)
                    @if (Session::get('prodName') == $group->name)
                    <div class="swatch-wrapper pl-3 pr-3 mb-2" style="background-color: #80808024;">
                        <a class="product-size-swatch-btn variation-btn{{ $group->Pointer }}" data-toggle="tooltip"
                            data-placement="top" title="{{$group->name}}" style="width: 100%;">
                            <span class="product-size-swatch-label">{{$group->name}}</span>
                            <div>
                                <input type="hidden" id="colors{{ $group->Pointer }}" value="{{ $group->name }}">
                                <input type="hidden" id="group{{ $group->Pointer }}" value="{{ $group->Pointer }}">
                                @if ($group->discAmount === '.00' && $group->discPercent === '0')
                                <span class="product-price">
                                    <input type="hidden" id="notsingleprice{{ $group->Pointer }}"
                                        value="Rp.{{number_format($group->price, 2)}}">
                                    <input type="hidden" id="notdoublepricenewss{{ $group->Pointer }}"
                                        value="{{ $group->price, 2 }}"></span>
                                </span>
                                @elseif($group->discAmount === '.00')
                                <span class="product-price-old"><span class="money" name="price">
                                        <input class="product-price-old" type="hidden"
                                            id="notdoublepriceold{{ $group->Pointer }}"
                                            value="Rp.{{number_format($group->price, 2)}}"></span>
                                </span>
                                <span class="product-price-new">
                                    <span class="money" name="price">
                                        <input type="hidden" id="notdoublepricenew{{ $group->Pointer }}"
                                            value="Rp.{{number_format($group->price - ($group->price*$group->discPercent/100), 2)}}"><input
                                            type="hidden" id="notdoublepricenewss{{ $group->Pointer }}"
                                            value="{{ $group->price - ($group->price*$group->discPercent/100), 2 }}">
                                    </span>
                                </span>
                                @elseif($group->discPercent === '0')
                                <span class="product-price-old">
                                    <span class="money" name="price"><input class="product-price-old" type="hidden"
                                            id="notdoublepriceold{{ $group->Pointer }}"
                                            value="Rp.{{number_format($group->price, 2)}}"></span>
                                </span>
                                <span class="product-price-new">
                                    <span class="money" name="price">
                                        <input type="hidden" id="notdoublepricenew{{ $group->Pointer }}"
                                            value="Rp.{{number_format($group->price-$group->discAmount, 2)}}">
                                        <input type="hidden" id="notdoublepricenewss{{ $group->Pointer }}"
                                            value="{{ $group->price-$group->discAmount, 2 }}">
                                    </span>
                                </span>
                                @endif
                            </div>
                        </a>
                    </div>
                    @else

                    {{-- @if ($group->groupDetailsQuantity <= 0) <div class="swatch-wrapper pl-3 pr-3 mb-2"
                        style="background-color: #80808024;">
                        <a class="product-size-swatch-btn" data-toggle="tooltip" data-placement="top" title="Stok Habis"
                            style="width: 100%;cursor: not-allowed;" disabled>
                            <span class="product-size-swatch-label">{{$group->name}}</span>
                    <div>
                        <input type="hidden" id="colors{{ $group->Pointer }}" value="{{ $group->name }}">
                        <input type="hidden" id="group{{ $group->Pointer }}" value="{{ $group->Pointer }}">
                        @if ($group->discAmount === '.00' && $group->discPercent === '0')
                        <span class="product-price">
                            <input type="hidden" id="notsingleprice{{ $group->Pointer }}"
                                value="Rp.{{number_format($group->price, 2)}}">
                            <input type="hidden" id="notdoublepricenewss{{ $group->Pointer }}"
                                value="{{ $group->price, 2 }}"></span>
                        </span>
                        @elseif($group->discAmount === '.00')
                        <span class="product-price-old"><span class="money" name="price">
                                <input class="product-price-old" type="hidden"
                                    id="notdoublepriceold{{ $group->Pointer }}"
                                    value="Rp.{{number_format($group->price, 2)}}"></span>
                        </span>
                        <span class="product-price-new">
                            <span class="money" name="price">
                                <input type="hidden" id="notdoublepricenew{{ $group->Pointer }}"
                                    value="Rp.{{number_format($group->price - ($group->price*$group->discPercent/100), 2)}}"><input
                                    type="hidden" id="notdoublepricenewss{{ $group->Pointer }}"
                                    value="{{ $group->price - ($group->price*$group->discPercent/100), 2 }}">
                            </span>
                        </span>
                        @elseif($group->discPercent === '0')
                        <span class="product-price-old">
                            <span class="money" name="price"><input class="product-price-old" type="hidden"
                                    id="notdoublepriceold{{ $group->Pointer }}"
                                    value="Rp.{{number_format($group->price, 2)}}"></span>
                        </span>
                        <span class="product-price-new">
                            <span class="money" name="price">
                                <input type="hidden" id="notdoublepricenew{{ $group->Pointer }}"
                                    value="Rp.{{number_format($group->price-$group->discAmount, 2)}}">
                                <input type="hidden" id="notdoublepricenewss{{ $group->Pointer }}"
                                    value="{{ $group->price-$group->discAmount, 2 }}">
                            </span>
                        </span>
                        @endif
                    </div>
                    </a>
                </div> --}}
                {{-- @else --}}
                <div class="swatch-wrapper pl-3 pr-3 mb-2">
                    <a class="product-size-swatch-btn variation-btn{{ $group->Pointer }}" data-toggle="tooltip"
                        data-placement="top" title="{{$group->name}}" style="width: 100%;">
                        <span class="product-size-swatch-label">{{$group->name}}</span>
                        <div>
                            <input type="hidden" id="colors{{ $group->Pointer }}" value="{{ $group->name }}">
                            <input type="hidden" id="group{{ $group->Pointer }}" value="{{ $group->Pointer }}">
                            @if ($group->discAmount === '.00' && $group->discPercent === '0')
                            <span class="product-price">
                                <input type="hidden" id="notsingleprice{{ $group->Pointer }}"
                                    value="Rp.{{number_format($group->price, 2)}}">
                                <input type="hidden" id="notdoublepricenewss{{ $group->Pointer }}"
                                    value="{{ $group->price, 2 }}"></span>
                            </span>
                            @elseif($group->discAmount === '.00')
                            <span class="product-price-old"><span class="money" name="price">
                                    <input class="product-price-old" type="hidden"
                                        id="notdoublepriceold{{ $group->Pointer }}"
                                        value="Rp.{{number_format($group->price, 2)}}"></span>
                            </span>
                            <span class="product-price-new">
                                <span class="money" name="price">
                                    <input type="hidden" id="notdoublepricenew{{ $group->Pointer }}"
                                        value="Rp.{{number_format($group->price - ($group->price*$group->discPercent/100), 2)}}"><input
                                        type="hidden" id="notdoublepricenewss{{ $group->Pointer }}"
                                        value="{{ $group->price - ($group->price*$group->discPercent/100), 2 }}">
                                </span>
                            </span>
                            @elseif($group->discPercent === '0')
                            <span class="product-price-old">
                                <span class="money" name="price"><input class="product-price-old" type="hidden"
                                        id="notdoublepriceold{{ $group->Pointer }}"
                                        value="Rp.{{number_format($group->price, 2)}}"></span>
                            </span>
                            <span class="product-price-new">
                                <span class="money" name="price">
                                    <input type="hidden" id="notdoublepricenew{{ $group->Pointer }}"
                                        value="Rp.{{number_format($group->price-$group->discAmount, 2)}}">
                                    <input type="hidden" id="notdoublepricenewss{{ $group->Pointer }}"
                                        value="{{ $group->price-$group->discAmount, 2 }}">
                                </span>
                            </span>
                            @endif
                        </div>
                    </a>
                </div>
                {{-- @endif --}}


                @endif
                @endforeach
            </div>
    </div>
    {{--                                <div class="product-color-variations mb--20">--}}
    {{--                                    @if (Session::has('prodName'))--}}
    {{--                                        <p class="swatch-label">Color: <strong class="swatch-label">{{ Session::get('prodName') }}</strong>
    </p>--}}
    {{--                                    @else--}}
    {{--                                        <p class="swatch-label">Color: <strong class="swatch-label">{{$product->colorName}}</strong>
    </p>--}}
    {{--                                    @endif--}}
    {{--                                    <div class="product-color-swatch variation-wrapper">--}}
    {{--                                        <div class="swatch-wrapper">--}}
    {{--                                            <a class="product-color-swatch-btn variation-btn1"--}}
    {{--                                               style="background-color:{{$product->colorHex}};"
    data-toggle="tooltip"--}}
    {{--                                               data-placement="top" title="{{$product->colorName}}">--}}
    {{--                                                <span class="product-color-swatch-label">{{$product->colorName}}</span>--}}
    {{--                                                <div style="display: none;">--}}
    {{--                                                    <input type="hidden" id="colors1" value="{{ $product->colorHex }}">--}}
    {{--                                                    <input type="hidden" id="group1" value="{{ $product->rowPointer }}">--}}
    {{--                                                    @if ($product->discAmount === '0' && $product->discPercent === '0')--}}
    {{--                                                        <span class="product-price">--}}
    {{--                                                        <span class="money" name="price"><input type="hidden" id="notsingleprice1" value="Rp.{{number_format($product->price, 2)}}">--}}
    {{--                                                            <input type="hidden" id="notdoublepricenewss1" value="{{ $product->price, 2 }}"></span>--}}
    {{--                                                    </span>--}}
    {{--                                                    @elseif($product->discAmount === '0')--}}
    {{--                                                        <span class="product-price-old">--}}
    {{--                                                        <span class="money" name="price"><input class="product-price-old" type="hidden" id="notdoublepriceold1" value="Rp.{{number_format($product->price, 2)}}"></span>--}}
    {{--                                                    </span>--}}
    {{--                                                        <span class="product-price-new">--}}
    {{--                                                        <span class="money" name="price"><input type="hidden" id="notdoublepricenew1" value="Rp.{{number_format($product->price - ($product->price*$product->discPercent/100), 2)}}">
    <input--}}
        {{--                                                                type="hidden" id="notdoublepricenewss1" value="{{ $product->price - ($product->price*$product->discPercent/100), 2 }}">
        </span>--}}
        {{--                                                    </span>--}}
        {{--                                                    @elseif($product->discPercent === '0')--}}
        {{--                                                        <span class="product-price-old">--}}
        {{--                                                        <span class="money" name="price"><input class="product-price-old" type="hidden" id="notdoublepriceold1" value="Rp.{{number_format($product->price, 2)}}"></span>--}}
        {{--                                                    </span>--}}
        {{--                                                        <span class="product-price-new">--}}
        {{--                                                        <span class="money" name="price"><input type="hidden" id="notdoublepricenew1" value="Rp.{{number_format($product->price-$product->discAmount, 2)}}">
        <input--}}
            {{--                                                                type="hidden" id="notdoublepricenewss1" value="{{ $product->price-$product->discAmount, 2 }}">
            </span>--}}
            {{--                                                    </span>--}}
            {{--                                                    @endif--}}
            {{--                                                </div>--}}
            {{--                                            </a>--}}
            {{--                                        </div>--}}
            {{--                                        @foreach($groups as $group)--}}
            {{--                                            <div class="swatch-wrapper">--}}
            {{--                                                <a class="product-color-swatch-btn variation-btn{{ $group->Pointer }}"--}}
            {{--                                                   style="background-color:{{$group->colorHex}};"
            data-toggle="tooltip"--}}
            {{--                                                   data-placement="top" title="{{$group->colorName}}">--}}
            {{--                                                    <span class="product-color-swatch-label">{{$group->colorName}}</span>--}}
            {{--                                                    <div>--}}
            {{--                                                        <input type="hidden" id="colors{{ $group->Pointer }}"
            value="{{ $group->colorHex }}">--}}
            {{--                                                        <input type="hidden" id="group{{ $group->Pointer }}"
            value="{{ $group->Pointer }}">--}}
            {{--                                                        @if ($group->discAmount === '0' && $group->discPercent === '0')--}}
            {{--                                                            <span class="product-price">--}}
            {{--                                                                <input type="hidden" id="notsingleprice{{ $group->Pointer }}"
            value="Rp.{{number_format($group->price, 2)}}">--}}
            {{--                                                                <input type="hidden" id="notdoublepricenewss{{ $group->Pointer }}"
            value="{{ $group->price, 2 }}"></span>--}}
            {{--                                                            </span>--}}
            {{--                                                        @elseif($group->discAmount === '0')--}}
            {{--                                                            <span class="product-price-old"><span class="money" name="price">--}}
            {{--                                                                    <input class="product-price-old" type="hidden" id="notdoublepriceold{{ $group->Pointer }}"
            value="Rp.{{number_format($group->price, 2)}}"></span>--}}
            {{--                                                            </span>--}}
            {{--                                                            <span class="product-price-new">--}}
            {{--                                                                <span class="money" name="price">--}}
            {{--                                                                    <input type="hidden" id="notdoublepricenew{{ $group->Pointer }}"
            value="Rp.{{number_format($group->price - ($group->price*$group->discPercent/100), 2)}}"><input
                type="hidden" id="notdoublepricenewss{{ $group->Pointer }}"
                value="{{ $group->price - ($group->price*$group->discPercent/100), 2 }}">--}}
            {{--                                                                </span>--}}
            {{--                                                            </span>--}}
            {{--                                                        @elseif($group->discPercent === '0')--}}
            {{--                                                            <span class="product-price-old">--}}
            {{--                                                                <span class="money" name="price"><input class="product-price-old" type="hidden" id="notdoublepriceold{{ $group->Pointer }}"
            value="Rp.{{number_format($group->price, 2)}}"></span>--}}
            {{--                                                            </span>--}}
            {{--                                                            <span class="product-price-new">--}}
            {{--                                                                <span class="money" name="price">--}}
            {{--                                                                    <input type="hidden" id="notdoublepricenew{{ $group->Pointer }}"
            value="Rp.{{number_format($group->price-$group->discAmount, 2)}}">--}}
            {{--                                                                    <input type="hidden" id="notdoublepricenewss{{ $group->Pointer }}"
            value="{{ $group->price-$group->discAmount, 2 }}">--}}
            {{--                                                                </span>--}}
            {{--                                                            </span>--}}
            {{--                                                        @endif--}}
            {{--                                                    </div>--}}
            {{--                                                </a>--}}
            {{--                                            </div>--}}
            {{--                                        @endforeach--}}
            {{--                                    </div>--}}
</div>
<input type="hidden" name="selectedgroup" id="groupselected" value="{{ $product->rowPointer }}" readonly>
<button type="submit" id="changeGroup" style="display: none;">change</button>
{{--                                <a href="" class="reset_variations">Clear</a>--}}
</form>
{{-- informasi stok --}}
{{-- <p class="product-short-description mb--30 mb-lg--20 mb-sm--10 pb-sm--1" style="color: crimson;font-size: 1.5em">
    @if (Session::has('prodQty'))
    @if (Session::get('prodQty') <= 0) 
    stok habis 
    @endif 
    @endif
</p>  --}}
{{-- informasi stok --}}
<form action="{{ route('cart.store') }}" method="post" class="form--action">
    @csrf
    <div class="product-action d-flex align-items-center">
        <div class="quantity">
            <input type="number" class="quantity-input" name="qty" id="qty" value="1" min="1">
        </div>
        @if (Session::has('prodPrice'))
        <input type="hidden" name="default" value="{{ $product->rowPointer }}">
        <input type="hidden" name="row" value="{{Session::get('prodPointer')}}">
        <input type="hidden" name="name" value="{{ $product->name }}">
        <input type="hidden" name="colorss" id="colorss" value="{{Session::get('prodHex')}}">
        <input type="hidden" name="colorname" id="colorname" value="{{Session::get('prodColor')}}">
        <input type="hidden" name="category" value="{{ $product->category }}">
        <input type="hidden" name="sku" value="{{ Session::get('prodSku') }}">
        @if (Session::get('proddiscAmount') === '.00' && Session::get('proddiscPercent') === '0')
        <input class="money" type="hidden" id="singleprices" name="prices" value="{{ Session::get('prodPrice'), 2 }}">
        @elseif(Session::get('proddiscAmount') === '.00')
        <input class="money" type="hidden" id="doublepricenews" name="prices"
            value="{{ Session::get('prodPrice') - (Session::get('prodPrice')*Session::get('proddiscPercent')/100), 2 }}">
        @elseif(Session::get('proddiscPercent') === '0')
        <input class="money" id="doublepricenews" type="hidden" name="prices"
            value="{{ Session::get('prodPrice')-Session::get('proddiscAmount'), 2 }}">
        @endif
        @else
        <input type="hidden" name="default" value="{{ $product->rowPointer }}">
        <input type="hidden" name="row" value="{{ $product->rowPointer }}">
        <input type="hidden" name="name" value="{{ $product->name }}">
        <input type="hidden" name="colorss" id="colorss" value="{{$product->colorHex}}">
        <input type="hidden" name="colorname" id="colorname" value="{{$product->colorName}}">
        <input type="hidden" name="category" value="{{ $product->category }}">
        <input type="hidden" name="sku" value="{{ $product->sku }}">
        @if ($product->discAmount === '.00' && $product->discPercent === '0')
        <input class="money" type="hidden" id="singleprices" name="prices" value="{{ $product->price, 2 }}">
        @elseif($product->discAmount === '.00')
        <input class="money" type="hidden" id="doublepricenews" name="prices"
            value="{{ $product->price - ($product->price*$product->discPercent/100), 2 }}">
        @elseif($product->discPercent === '0')
        <input class="money" id="doublepricenews" type="hidden" name="prices"
            value="{{ $product->price-$product->discAmount, 2 }}">
        @endif
        @endif

        @if (Session::has('prodName'))
        {{-- @if (Session::get('prodQty') <= 0)
         @else --}}
        <input type="submit" value="Add To Cart" class="btn btn-shape-round btn-style-1 btn-large add-to-cart">
        {{-- @endif --}}
        @else
        @endif
            </a> </div> </form> <div class="product-extra pt--3 mt--35 mt-lg--25 mt-sm--15">
            <a href="#" class="font-size-12"><i class="fa fa-exchange"></i>Delivery and return</a>
    </div>
    <div class="product-meta mt--35 mt-lg--25 mt-sm--15 pt-sm--1">
        <span class="sku_wrapper font-size-12">SKU: <span class="sku">{{$product->sku}}</span></span>
        <span class="posted_in font-size-12">Categories:
            <a href="{{ Route('shop') }}">{{$product->category}}</a>
        </span>

        <span class="posted_in font-size-12">Tags:
            @if ($product->tags != "")
            @foreach(explode(',', $product->tags) as $tag)
            <a href="{{ Route('shop') }}">{{$tag}},</a>
            @endforeach
            @endif
        </span>
    </div>
    <div class="product-share-box mt--40 mt-lg--30 mt-sm--10 pt-sm--1">
        <span class="font-size-12">Share With</span>
        <!-- Social Icons Start Here -->
        <ul class="social social-small">
            <li class="social__item">
                <a href="facebook.com" class="social__link">
                    <i class="fa fa-facebook"></i>
                </a>
            </li>
            <li class="social__item">
                <a href="twitter.com" class="social__link">
                    <i class="fa fa-twitter"></i>
                </a>
            </li>
            <li class="social__item">
                <a href="plus.google.com" class="social__link">
                    <i class="fa fa-google-plus"></i>
                </a>
            </li>
            <li class="social__item">
                <a href="plus.google.com" class="social__link">
                    <i class="fa fa-pinterest-p"></i>
                </a>
            </li>
        </ul>
        <!-- Social Icons End Here -->
    </div>
    </div>
    </div>
    </div>
    <div class="row justify-content-center pt--80 pt-md--60 pt-sm--35 mt-md--1 mt-sm--3">
        <div class="col-12">
            <div class="product-data-tab tab-style-5">
                <div class="nav nav-tabs product-data-tab__head mb--35 mb-sm--25" id="product-tab" role="tablist">
                    {{-- <a class="product-data-tab__link nav-link active" id="nav-description-tab" data-toggle="tab"
                                   href="#nav-description" role="tab" aria-selected="true">
                                    <span>Description</span>
                                </a> --}}
                    <a class="product-data-tab__link nav-link" id="nav-info-tab" data-toggle="tab" href="#nav-info"
                        role="tab" aria-selected="true">
                        <span>Additional Information</span>
                    </a>
                    <a class="product-data-tab__link nav-link" id="nav-reviews-tab" data-toggle="tab"
                        href="#nav-reviews" role="tab" aria-selected="true">
                        <span>Reviews({{ $ratings->count() }})</span>
                    </a>
                </div>
                <div class="tab-content product-data-tab__content" id="product-tabContent">
                    {{-- <div class="tab-pane fade show active" id="nav-description" role="tabpanel"
                                     aria-labelledby="nav-description-tab">
                                    <div class="product-description">
                                        {!!$product->description!!}
                                    </div>
                                </div> --}}
                    <div class="tab-pane" id="nav-info" role="tabpanel" aria-labelledby="nav-info-tab">
                        <div class="table-content table-responsive">
                            <table class="table shop_attributes">
                                <tbody>
                                    <tr>
                                        <th>Weight</th>
                                        <td>{{$product->weight}}</td>
                                    </tr>
                                    <tr>
                                        <th>Dimensions</th>
                                        <td>{{$product->dimensions}}</td>
                                    </tr>
                                    <!--<tr>
                                                <th>Color</th>
                                                <td>
                                                    <a href="{{ Route('shop') }}">Black</a>,
                                                    <a href="{{ Route('shop') }}">Gray</a>,
                                                    <a href="{{ Route('shop') }}">Red</a>,
                                                    <a href="{{ Route('shop') }}">Violet</a>,
                                                    <a href="{{ Route('shop') }}">Yellow</a>
                                                </td>
                                            </tr>-->
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-reviews" role="tabpanel" aria-labelledby="nav-reviews-tab">
                        <div class="product-reviews">
                            <h3 class="review__title">{{ $ratings->count() }} review for {{$product->name}}</h3>
                            <ul class="review__list">
                                @foreach($ratings as $rating)
                                <li class="review__item">
                                    <div class="review__container">
                                        <img src="{{asset('/upload/user/'.$rating->image)}}" alt="Review Avatar"
                                            class="review__avatar" style="object-fit: contain;">
                                        <div class="review__text">
                                            <div class="product-rating float-right">
                                                @if($rating->rate == 5)
                                                <div class="star-rating star-five">
                                                    <span>Rated <strong class="rating">5.00</strong> out of 5</span>
                                                </div>
                                                @elseif ($rating->rate == 4)
                                                <div class="star-rating star-four">
                                                    <span>Rated <strong class="rating">4.00</strong> out of 4</span>
                                                </div>
                                                @elseif ($rating->rate == 3)
                                                <div class="star-rating star-three">
                                                    <span>Rated <strong class="rating">3.00</strong> out of 3</span>
                                                </div>
                                                @elseif ($rating->rate == 2)
                                                <div class="star-rating star-two">
                                                    <span>Rated <strong class="rating">2.00</strong> out of 2</span>
                                                </div>
                                                @elseif ($rating->rate == 1)
                                                <div class="star-rating star-one">
                                                    <span>Rated <strong class="rating">1.00</strong> out of 1</span>
                                                </div>
                                                @endif
                                            </div>
                                            <div class="review__meta">
                                                <strong class="review__author">{{ $rating->displayName }}</strong>
                                                <span class="review__dash">-</span>
                                                <span
                                                    class="review__published-date">{{Carbon\Carbon::parse($rating->createdAt)->format('M d,  Y ')}}</span>
                                            </div>
                                            <div class="clearfix"></div>
                                            <p class="review__description">{{ $rating->comments }}</p>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                            {{--                                    <div class="review-form-wrapper">--}}
                            {{--                                        <span class="reply-title">Add a review</span>--}}
                            {{--                                        <form action="#" class="form">--}}
                            {{--                                            <div class="form-notes mb--20">--}}
                            {{--                                                <p>Your email address will not be published. Required fields are marked--}}
                            {{--                                                    <span class="required">*</span></p>--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="form__group mb--30 mb-sm--20">--}}
                            {{--                                                <div class="revew__rating">--}}
                            {{--                                                    <p class="stars selected">--}}
                            {{--                                                        <a class="star-1 active" href="#">1</a>--}}
                            {{--                                                        <a class="star-2" href="#">2</a>--}}
                            {{--                                                        <a class="star-3" href="#">3</a>--}}
                            {{--                                                        <a class="star-4" href="#">4</a>--}}
                            {{--                                                        <a class="star-5" href="#">5</a>--}}
                            {{--                                                    </p>--}}
                            {{--                                                </div>--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="form__group mb--30 mb-sm--20">--}}
                            {{--                                                <div class="form-row">--}}
                            {{--                                                    <div class="col-sm-6 mb-sm--20">--}}
                            {{--                                                        <label class="form__label" for="name">Name<span--}}
                            {{--                                                                class="required">*</span></label>--}}
                            {{--                                                        <input type="text" name="name" id="name" class="form__input">--}}
                            {{--                                                    </div>--}}
                            {{--                                                    <div class="col-sm-6">--}}
                            {{--                                                        <label class="form__label" for="email">Email<span--}}
                            {{--                                                                class="required">*</span></label>--}}
                            {{--                                                        <input type="email" name="email" id="email" class="form__input">--}}
                            {{--                                                    </div>--}}
                            {{--                                                </div>--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="form__group mb--30 mb-sm--20">--}}
                            {{--                                                <div class="form-row">--}}
                            {{--                                                    <div class="col-12">--}}
                            {{--                                                        <label class="form__label" for="email">Your Review<span--}}
                            {{--                                                                class="required">*</span></label>--}}
                            {{--                                                        <textarea name="review" id="review"--}}
                            {{--                                                            class="form__input form__input--textarea"></textarea>--}}
                            {{--                                                    </div>--}}
                            {{--                                                </div>--}}
                            {{--                                            </div>--}}
                            {{--                                            <div class="form__group">--}}
                            {{--                                                <div class="form-row">--}}
                            {{--                                                    <div class="col-12">--}}
                            {{--                                                        <input type="submit" value="Submit"--}}
                            {{--                                                            class="btn btn-style-1 btn-submit">--}}
                            {{--                                                    </div>--}}
                            {{--                                                </div>--}}
                            {{--                                            </div>--}}
                            {{--                                        </form>--}}
                            {{--                                    </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('components.shop.related')
    </div>
    </div>
    </div>
    <!-- Main Content Wrapper Start -->
    <!-- Modal Start -->
    @include('components.shop.modalrelated')
    <!-- Modal End -->
    @endsection

    @section('extra_script')
    <script>
        $('.variation-btn1').on('click', function(e){
            e.preventDefault();
            var $attr = $(this).data('original-title');
            $(this).parents('.variation-wrapper').siblings().children().text($attr).css('opacity', '1');
            $('.reset_variations').css('display', 'block');
            var singleprice = $('#notsingleprice1').val();
            var selected = $('#group1').val();
            var singleprices = $('#notdoublepricenewss1').val();
            var doublepriceold = $('#notdoublepriceold1').val();
            var doublepricenew = $('#notdoublepricenew1').val();
            var doublepricenews = $('#notdoublepricenewss1').val();
            var color = $('#colors1').val();
            $('#singleprice').text(singleprice)
            $('#singleprices').val(singleprices)
            $('#doublepriceold').text(doublepriceold)
            $('#doublepricenew').text(doublepricenew)
            $('#doublepricenews').val(doublepricenews)
            $('#colorss').val(color)
            $('#groupselected').val(selected)
            $('#changeGroup').click()
        });

        $('.reset_variations').on('click', function(e){
            e.preventDefault();
            $('.swatch-label strong').text('');
            $(this).css('display', 'none');
            window.location.reload();
        });

        @foreach($groups as $group)
        $('.variation-btn{{ $group->Pointer }}').on('click', function(e){
            e.preventDefault();
            var $attr = $(this).data('original-title');
            $(this).parents('.variation-wrapper').siblings().children().text($attr).css('opacity', '1');
            $('.reset_variations').css('display', 'block');
            var singleprice = $('#notsingleprice{{ $group->Pointer }}').val();
            var selected = $('#group{{ $group->Pointer }}').val();
            var singleprices = $('#notdoublepricenewss{{ $group->Pointer }}').val();
            var doublepriceold = $('#notdoublepriceold{{ $group->Pointer }}').val();
            var doublepricenew = $('#notdoublepricenew{{ $group->Pointer }}').val();
            var doublepricenews = $('#notdoublepricenewss{{ $group->Pointer }}').val();
            var colors = $('#colors{{ $group->Pointer }}').val();
            $('#singleprice').text(singleprice)
            $('#singleprices').val(singleprices)
            $('#doublepriceold').text(doublepriceold)
            $('#doublepricenew').text(doublepricenew)
            $('#doublepricenews').val(doublepricenews)
            $('#colorss').val(colors)
            $('#groupselected').val(selected)
            $('#changeGroup').click()
        });

        $('.reset_variations').on('click', function(e){
            e.preventDefault();
            $('.swatch-label strong').text('');
            $(this).css('display', 'none');
            window.location.reload();
        });
        @endforeach
    </script>
    @endsection