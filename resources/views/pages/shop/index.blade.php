@extends('master')
{{-- @section('nav')
@include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')
<!-- Breadcrumb area Start -->
<div class="breadcrumb-area pt--40 pb--30 pb-md--20">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="page-title">Shop</h1>
                <ul class="breadcrumb justify-content-center">
                    <li><a href="{{ Route('index') }}">Home</a></li>
                    <li class="current"><span>Shop</span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb area End -->

<!-- Main Content Wrapper Start -->
{{-- @if ($categorys == 'Connection problem')
    <h1 class="text-center">No internet connection</h1>
@else --}}
<div id="content" class="main-content-wrapper">
    <div class="page-content-inner enable-page-sidebar">
        <div class="container-fluid">
            <div class="row shop-sidebar mt-md-2 pt--30 pt-md--15 pt-sm--10 pb--90 pb-md--70 pb-sm--50">
                <div class="col-lg-9 order-lg-2" id="main-content">
                    <div class="shop-toolbar">
                        <div class="shop-toolbar__inner">
                            <div class="row align-items-center">
                                <div class="col-md-6 text-md-left text-center mb-sm--5">
                                    <div class="shop-toolbar__left">
                                        <p class="product-pages">Showing {{$products->perPage()}} of
                                            {{$products->total()}} results</p>
                                        <div class="product-view-count">
                                            <p>Show</p>
                                            <ul>
                                                <li class="active"><a href="{{ route('shop') }}">20</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="shop-toolbar__right">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="advanced-product-filters">
                            <div class="product-filter">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="product-widget product-widget--price">
                                            <h3 class="widget-title">Price</h3>
                                            <ul class="product-widget__list">
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="ammount">Rp 0,00</span>
                                                        <span> - </span>
                                                        <span class="ammount">Rp 50.000,00</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="ammount">Rp 50.000,00</span>
                                                        <span> - </span>
                                                        <span class="ammount">Rp 100.000,00</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="ammount">Rp 100.000,00</span>
                                                        <span> - </span>
                                                        <span class="ammount">Rp 150.000,00</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="ammount">Rp 150.000,00</span>
                                                        <span> - </span>
                                                        <span class="ammount">Rp 200.000,00</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="ammount">Rp 200.000,00</span>
                                                        <span> - </span>
                                                        <span class="ammount">Rp 250.000,00</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span class="ammount">Rp 250.000,00</span>
                                                        <span> + </span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="product-widget product-widget--brand">
                                            <h3 class="widget-title">Brands</h3>
                                            <ul class="product-widget__list">
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span>Airi</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span>Mango</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span>Valention</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}">
                                                        <span>Zara</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="product-widget product-widget--color">
                                            <h3 class="widget-title">Color</h3>
                                            <ul class="product-widget__list product-color-swatch">
                                                <li>
                                                    <a href="{{ Route('shop') }}" class="product-color-swatch-btn blue">
                                                        <span class="product-color-swatch-label">Blue</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}"
                                                        class="product-color-swatch-btn green">
                                                        <span class="product-color-swatch-label">Green</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}" class="product-color-swatch-btn pink">
                                                        <span class="product-color-swatch-label">Pink</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}" class="product-color-swatch-btn red">
                                                        <span class="product-color-swatch-label">Red</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{ Route('shop') }}" class="product-color-swatch-btn grey">
                                                        <span class="product-color-swatch-label">Grey</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <a href="#" class="btn-close"><i class="dl-icon-close"></i></a>
                            </div>
                        </div>
                    </div>

                    <!-- view products -->
                    @if ($products->isEmpty())
                    <div style="  text-align: center;
                    ">
                        <h1>No items.</h1>
                    </div>
                    {{-- <div class="shop-products">
                        <div class="row text-center">
                            <h1 class="text-center">No items</h1>
                        </div>
                    </div> --}}
                    @else
                    @include('components.shop.viewproducts')
                    @endif
                    <!--view products -->

                    <nav class="pagination-wrap">
                        {{$products->links('pages.shop.pagin')}}
                    </nav>
                </div>
                <!--sidebar-->
                @include('components.shop.sidebar')
                <!--sidebar-->
            </div>
        </div>
    </div>
</div>


<!-- Main Content Wrapper Start -->
<!-- Modal Start -->
@include('components.shop.productModal')
<!-- Modal End -->
{{-- @endif --}}
@endsection
@section('extra_script')
<script>
    // $(function(){
    //         $('.category').click(function(event){
    //             var val = $(this).attr('id');
    //             $('#kode').val(val);
    //             event.preventDefault();

    //         });
    //         $('.subcategory').click(function(event){
    //             var val = $(this).attr('href');
    //             $('#sub').val(val);
    //             event.preventDefault();

    //         });
        //     $('.category').click(function() {
        //         $(this).parent().find('.category').css('border', '1px solid #ccc');
        //     $(this).css('border', '1px solid #3f89f5');
        //     });
        //     $('.subcategory').click(function() {
        //         $(this).parent().find('.subcategory').css('border', '1px solid #ccc');
        //     $(this).css('border', '1px solid #3f89f5');
        //     });
        // });

</script>
@endsection