<!-- Menghubungkan dengan view template master -->
@extends('master')
{{-- @section('nav')
  @include('components.navigations.navhome')
@endsection --}}
@section('bodyclass',"color-variation-2")
@section('main')
<!-- Main Content Wrapper Start -->
<div id="content" class="main-content-wrapper">

    @include('components.home.slider')

    <section class="cta-area bg-color pt--85 pt-md--65 pt-sm--45 pb--90 pb-md--70 pb-sm--50" style="background-color: rgb(212, 230, 255);">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-8 text-center">
                    <h4 class="text-uppercase font-size-18 color--tertiary lts-10 lts-md-5 lts-sm-2 mb--15 mb-md--10">
                        OUR HISTORY</h4>
                    <h2 class="heading-secondary-2 mb--15">Artline Indonesia</h2>
                    {{-- <h4 class="heading-secondary-2 mb--15">Since 2010</h4> --}}
                    <p class="paragraph-2 mb--35">
                        Etiam molestie lacus sit amet augue ultrices, id posuere nunc venenatis. Mauris tincidunt, lorem
                        ac maximus vestibulum, massa mauris consectetur dui, ac aliquet sem ante vel orci. Duis varius
                        posuere sagittis. Fusce consectetur odio non odio euismod imperdiet. Sed vulputate mi id lore
                    </p>
                    <a href="{{ Route('about') }}" class="btn btn-semi-large btn-style-1">Discover Now</a>
                </div>
            </div>
        </div>
    </section>

    @include('components.home.newProducts')

    <!-- Banner Area Start -->
    @if ($carousel->count() == '1')
        @if ($carousel[0]->id == '1')
            <div class="banner-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 mb-sm--30">
                            <div class="banner-box banner-hover-3 banner-type-6 banner-type-6-4">
                                <div class="banner-inner">
                                    <div class="banner-image">
                                        <img src="{{asset('/upload/users/carousels/'.$carousel[0]->image)}}" alt="Banner">
                                    </div>
                                    <div class="banner-info">
                                        <div class="banner-info--inner text-center">
                                            <p class="banner-paragraph heading-color">{{$carousel[0]->subtitle}}</p>
                                            <p class="banner-title-10 font-prata lts-6 lts-lg-3 lts-sm-1 heading-color mb--10">
                                                {{$carousel[0]->title}}
                                            </p>
                                            <a href="{{$carousel[0]->link}}" class="banner-btn banner-btn-5 banner-btn-outline banner-btn-color-red banner-btn-shape-round hover-style-bg">Shop Now</a>
                                        </div>
                                    </div>
                                    <a class="banner-link banner-overlay" href="{{$carousel[0]->link}}">{{$carousel[0]->button}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @elseif ($carousel[0]->id == '2')
            <div class="banner-area">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 mb-sm--30">
                        </div>
                        <div class="col-md-6">
                            <div class="banner-box banner-hover-3 banner-type-6 banner-type-6-5">
                                <div class="banner-inner">
                                    <div class="banner-image">
                                        <img src="{{asset('/upload/users/carousels/'.$carousel[0]->image)}}" alt="Banner">
                                    </div>
                                    <div class="banner-info">
                                        <div class="banner-info--inner text-center">
                                            <p class="banner-paragraph heading-color">{{$carousel[0]->subtitle}}</p>
                                            <p class="banner-title-10 font-prata lts-6 lts-lg-3 lts-sm-1 heading-color mb--10">
                                                {{$carousel[0]->title}}
                                            </p>
                                            <a href="{{$carousel[0]->link}}" class="banner-btn banner-btn-5 banner-btn-outline banner-btn-color-red banner-btn-shape-round hover-style-bg">Shop Now</a>
                                        </div>
                                    </div>
                                    <a class="banner-link banner-overlay" href="{{$carousel[0]->link}}">{{$carousel[0]->button}}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="banner-link banner-overlay" href="{{$carousel[0]->link}}">{{$carousel[0]->button}}</a>
            </div>
        @endif
    @elseif ($carousel->count() == '2')
        <div class="banner-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 mb-sm--30">
                        <div class="banner-box banner-hover-3 banner-type-6 banner-type-6-4">
                            <div class="banner-inner">
                                <div class="banner-image">
                                    <img src="{{asset('/upload/users/carousels/'.$carousel[0]->image)}}" alt="Banner">
                                </div>
                                <div class="banner-info">
                                    <div class="banner-info--inner text-center">
                                        <p class="banner-paragraph heading-color">{{$carousel[0]->subtitle}}</p>
                                        <p class="banner-title-10 font-prata lts-6 lts-lg-3 lts-sm-1 heading-color mb--10">
                                            {{$carousel[0]->title}}
                                        </p>
                                        <a href="{{$carousel[0]->link}}" class="banner-btn banner-btn-5 banner-btn-outline banner-btn-color-red banner-btn-shape-round hover-style-bg">Shop Now</a>
                                    </div>
                                </div>
                                <a class="banner-link banner-overlay" href="{{$carousel[0]->link}}">{{$carousel[0]->button}}</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="banner-box banner-hover-3 banner-type-6 banner-type-6-5">
                            <div class="banner-inner">
                                <div class="banner-image">
                                    <img src="{{asset('/upload/users/carousels/'.$carousel[1]->image)}}" alt="Banner">
                                </div>
                                <div class="banner-info">
                                    <div class="banner-info--inner text-center">
                                        <p class="banner-paragraph heading-color">{{$carousel[1]->subtitle}}</p>
                                        <p class="banner-title-10 font-prata lts-6 lts-lg-3 lts-sm-1 heading-color mb--10">
                                            {{$carousel[1]->title}}
                                        </p>
                                        <a href="{{$carousel[1]->link}}" class="banner-btn banner-btn-5 banner-btn-outline banner-btn-color-red banner-btn-shape-round hover-style-bg">Shop Now</a>
                                    </div>
                                </div>
                                <a class="banner-link banner-overlay" href="{{$carousel[1]->link}}">{{$carousel[1]->button}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else

    @endif
<!-- Banner Area End -->


    <!-- Product Tab Area Start -->
    @include('components.home.hotproducts')
    <!-- Product Tab Area Start -->

    <!-- Testimonial Area Start -->
    @include('components.home.testimonial')
    <!-- Testimonial Area End -->

    <!-- Blog area Start -->
    @include('components.home.blog')
    <!-- Blog area End -->

    <!-- Method Area Start -->
    <section class="method-area bg-color pt--55 pb--55 pt-sm--35 pb-sm--40" style="background-color: rgb(178, 210, 255);">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 mb-md--30">
                    <div class="method-box text-center">
                        <img src="{{asset('assets/img/icons/artline/pengiriman.png')}}" alt="Icon" width="110px">
                        <h4 class="mt--20">Pengiriman</h4>
                        <p>Harga belum termasuk ongkos kirim dari Surabaya</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-md--30">
                    <div class="method-box text-center">
                        <img src="{{asset('assets/img/icons/artline/easy.png')}}" alt="Icon" width="70px">
                        <h4 class="mt--20">Belanja Mudah</h4>
                        <p>Belanja dari rumah saja</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 mb-sm--30">
                    <div class="method-box text-center">
                        <img src="{{asset('assets/img/icons/artline/harga.png')}}" alt="Icon" width="65px">
                        <h4 class="mt--20">Harga Produk</h4>
                        <p>Harga produk sudah termasuk PPN</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="method-box text-center">
                        <img src="{{asset('assets/img/icons/artline/min50.png')}}" alt="Icon" width="70px">
                        <h4 class="mt--20">Minimum Pembelian</h4>
                        <p>Minimum pembelian sebesar Rp. 50.000</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Method Area End -->

    <!-- Instagram Area Start -->

    <!-- Instagram Area End -->

</div>
<!-- Main Content Wrapper Start -->
@include('components.home.productModal')

@endsection
