
@extends('master')
@section('nav')
  @include('components.navigations.navabout')
@endsection
@section('bodyclass',"default-color")
@section('main')

<!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
                <!-- Page Title area Satrt -->
                <section class="page-title-area bg-image pb--80 pb-md--60 pb-sm--40" data-bg-image="{{asset('upload/users/abouts/'.$data->header_image)}}">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="breadcrumb-area breadcrumb-style-3">
                                    <h1 class="page-title">About Us</h1>
                                    <ul class="breadcrumb">
                                        <li><a href="index.html">Home</a></li>
                                        <li class="current"><span>About Us</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Page Title area End -->
    
                <!-- About Area Start -->
                <div class="about-area section-right-full ptb--80 ptb-md--60 ptb-sm--40">
                    <div class="row align-items-center">
                        <div class="left-side-2 col-lg-5">
                            <div class="about-text pl--55 pl-sm--30 mb-md--40 mb-sm--30">
                                <figure class="mb--30">
                                    <img src="{{asset('assets/img/logo/transparent-logo.png')}}" alt="logo">
                                </figure>
                              {!!$data->description!!} </div>
                        </div>
                        <div class="right-side-2 col-lg-7">
                            <div class="banner-box banner-hover-3 banner-type-9">
                                <div class="banner-inner">
                                    <div class="banner-image">
                                        <img src="{{asset('upload/users/abouts/'.$data->banner_image)}}" alt="Banner">
                                    </div>
                                    <div class="banner-info">
                                        <div class="banner-info--inner">
                                            <a href="{{ Route('shop') }}" class="banner-btn-7">Shop Now</a>
                                        </div>
                                    </div>
                                    <a class="banner-link banner-overlay" href="{{ Route('shop') }}">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- About Area End -->
    
                <!-- Brand Area Start -->
                {{-- <div class="brand-area ptb--50 ptb-md--40">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="charoza-element-carousel"                            
                                data-slick-options='{
                                    "slidesToShow": 6,
                                    "slidesToScroll": 1,
                                    "adaptiveHeight": true
                                }'
                                data-slick-responsive='[
                                    {"breakpoint":1499, "settings": {"slidesToShow": 5}},
                                    {"breakpoint":1199, "settings": {"slidesToShow": 4}},
                                    {"breakpoint":767, "settings": {"slidesToShow": 3}},
                                    {"breakpoint":575, "settings": {"slidesToShow": 2}},
                                    {"breakpoint":450, "settings": {"slidesToShow": 1}}
                                ]'>
                                    <div class="item">
                                        <figure class="brand-item">
                                            <img src="{{asset('assets/img/partner/brand_9.png')}}" alt="Brand" class="mx-auto">
                                        </figure>
                                    </div>
                                    <div class="item">
                                        <figure class="brand-item">
                                            <img src="{{asset('assets/img/partner/brand_10.png')}}" alt="Brand" class="mx-auto">
                                        </figure>
                                    </div>
                                    <div class="item">
                                        <figure class="brand-item">
                                            <img src="{{asset('assets/img/partner/brand_11.png')}}" alt="Brand" class="mx-auto">
                                        </figure>
                                    </div>
                                    <div class="item">
                                        <figure class="brand-item">
                                            <img src="{{asset('assets/img/partner/brand_12.png')}}" alt="Brand" class="mx-auto">
                                        </figure>
                                    </div>
                                    <div class="item">
                                        <figure class="brand-item">
                                            <img src="{{asset('assets/img/partner/brand_13.png')}}" alt="Brand" class="mx-auto">
                                        </figure>
                                    </div>
                                    <div class="item">
                                        <figure class="brand-item">
                                            <img src="{{asset('assets/img/partner/brand_14.png')}}" alt="Brand" class="mx-auto">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
                <!-- Brand Area End -->
    
            </div>
            <!-- Main Content Wrapper Start -->
           
@endsection    
