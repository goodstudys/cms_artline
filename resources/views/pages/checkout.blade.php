@extends('master')
{{-- @section('nav')
  @include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')
<!-- Breadcrumb area Start -->
<div class="breadcrumb-area pt--40 pb--30 pb-md--20">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="page-title">Checkout</h1>
                <ul class="breadcrumb justify-content-center">
                    <li><a href="index.html">Home</a></li>
                    <li class="current"><span>Checkout</span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb area End -->

<!-- Main Content Wrapper Start -->
<div id="content" class="main-content-wrapper">
    <div class="page-content-inner">
        <div class="container">
            <div class="row pt--50 pt-md--40 pt-sm--20">
                <div class="col-12">
                    <!-- User Action Start -->
                    {{--                                    @include('components.checkout.couponcode')--}}
                    <!-- User Action End -->
                </div>
            </div>
            <div class="row pb--80 pb-md--60 pb-sm--40">
                <!-- Checkout Area Start -->
                <!-- form billing -->
                @include('components.checkout.formbilling',['datas'=>$datas])
                <!-- form billing -->
                @if (Session::has('status'))

                <div class="col-xl-5 offset-xl-1 col-lg-6 mt-md--40">
                    <div class="order-details">
                        <h6 class="text-justify">Lakukan pembayaran langsung ke rekening bank kami. Pesanan Anda tidak
                            akan dikirim sampai
                            dana telah dibuka di rekening kami. Pesanan Anda akan Dibatalkan Jika tidak
                            Melakukan Pembayaran Selama 1 x 24 Jam, Dan segera konfirmasi pembayaran ke
                            admin kami.</h6><br>
                        <div class="row">
                            <div class="col">
                                <div class="mx-auto text-black text-center">
                                    <h6>Total yang harus Transfer : </h6>
                                </div>

                            </div>
                            <div class="col">
                                <div class="mx-auto text-black text-center font-weight-bold">
                                    <h6 class="font-weight-bold"> Rp {{ number_format(Session::get('total'))}}</h6>
                                </div>

                            </div>
                        </div>

                        <br>
                        <h6>Dibawah ini adalah No.Rekening Artline</h6>
                        <div class="row">
                            <div class="col">
                                <div class=" text-black text-center">
                                    <img src="{{asset('assets/img/logobca.jpg')}}">
                                </div>

                            </div>
                            <div class="col">
                                <div class=" pt-5 text-black text-center font-weight-bold">
                                    <h6 class="font-weight-bold">ARTLINE<br>
                                        ac: 14000886347655</h6>
                                </div>

                            </div>
                        </div>
                        <br>
                        <h6 class="text-justify text-danger">catatan : konfirmasi pembayaran bisa dilakukan di halaman
                            account, lalu masuk ke
                            tab pesanan, kemudian pilih konfirmasi sesuai dengan order yang ingin di konfirmasi.</h6>
                    </div>
                </div>
                @endif

                <div class="col-xl-5 offset-xl-1 col-lg-6 mt-md--40">
                    <div class="order-details">
                        @if (Cart::getContent()->count() != 0)
                        <div class="checkout-title mt--10">
                            <h2>Your Order</h2>
                        </div>
                        <table class="table order-table order-table-2">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th class="text-right">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <span class="d-none">{{ $zero = 0 }}</span>
                                @foreach($datas as $data)
                                @if (Request::get('reseller') == 'reseller')
                                <tr>
                                    <th>{{ $data->name.' ('.$data->attributes->colorname.')' }}
                                        <strong><span>&#10005;</span>{{ $data->quantity }}</strong>
                                    </th>
                                    <span
                                        class="d-none">{{ $prodprice = $data->quantity * $data->attributes->wholesale_price }}</span>
                                    <span class="d-none">{{ $gettotal = $zero += $prodprice }}</span>
                                    <td class="text-right">Rp
                                        {{ number_format($data->quantity * $data->attributes->wholesale_price) }}</td>
                                </tr>
                                @else
                                @if (Session::has('kode'))
                                @if (Session::get('category') == $data->attributes->category)
                                @if (Session::get('discAmount') == '0' && Session::get('discPercent') == '0')
                                <tr>
                                    <th>{{ $data->name.' ('.$data->attributes->colorname.')' }}
                                        <strong><span>&#10005;</span>{{ $data->quantity }}</strong>
                                    </th>
                                    <td class="text-right">Rp {{ number_format($data->quantity * $data->price) }}</td>
                                </tr>
                                @elseif(Session::get('discAmount') == '0')
                                <tr>
                                    <th>{{ $data->name.' ('.$data->attributes->colorname.')' }}
                                        <strong><span>&#10005;</span>{{ $data->quantity }}</strong>
                                        <span
                                            style="display: none;">{{ $newprice = $data->price - ($data->price*Session::get('discPercent')/100) }}</span>
                                    </th>
                                    <td class="text-right">Rp {{ number_format($data->quantity * $newprice) }}</td>
                                </tr>
                                @elseif(Session::get('discPercent') == '0')
                                <tr>
                                    <th>{{ $data->name.' ('.$data->attributes->colorname.')' }}
                                        <strong><span>&#10005;</span>{{ $data->quantity }}</strong>
                                        <span
                                            style="display: none;">{{ $newprice = $data->price-Session::get('discAmount') }}</span>
                                    </th>
                                    <td class="text-right">Rp {{ number_format($data->quantity * $newprice) }}</td>
                                </tr>
                                @endif
                                @else
                                <tr>
                                    <th>{{ $data->name.' ('.$data->attributes->colorname.')' }}
                                        <strong><span>&#10005;</span>{{ $data->quantity }}</strong>
                                    </th>
                                    <td class="text-right">Rp {{ number_format($data->quantity * $data->price) }}</td>
                                </tr>
                                @endif
                                @else
                                <tr>
                                    <th>{{ $data->name.' ('.$data->attributes->colorname.')' }}
                                        <strong><span>&#10005;</span>{{ $data->quantity }}</strong>
                                    </th>
                                    <td class="text-right">Rp {{ number_format($data->quantity * $data->price) }}</td>
                                </tr>
                                @endif
                                @endif
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr class="cart-subtotal">
                                    <th>Subtotal</th>
                                    @if (Request::get('reseller') == 'reseller')
                                    <td class="text-right">Rp {{ number_format($gettotal) }}</td>
                                    <span id="subtotalcheckout" class="d-none">{{ $gettotal }}</span>
                                    @else
                                    <td class="text-right">Rp {{ number_format(Session::get('getTotal')) }}</td>
                                    <span id="subtotalcheckout" class="d-none">{{ Session::get('getTotal') }}</span>
                                    @endif
                                </tr>
                                @if ($option == '1')
                                <div id="dvPassport" >
                                    <tr  class="shipping ajur" id="shippingkirimdropship">
                                        <th>Shipping to delivery</th>
                                        <td class="text-right">
                                            <span id="ongkirssDeliv"></span>
                                        </td>
                                    </tr>
                                    <tr class="order-total" id="ordertotalkirimdropship">
                                        <th>Order Total Delivery</th>
                                        @if (Request::get('reseller') == 'reseller')
                                        <td class="text-right">
                                            <span  class="order-total-ammount" id="totalorderres1Deliv"></span>
                                        </td>
                                        @else
                                        <td class="text-right">
                                            <span class="order-total-ammount" id="totalorderDeliv"></span>
                                        </td>
                                        @endif
                                    </tr>
                                </div>
                                <div id="AddPassport">
                                    <tr style="display: " class="shipping" id="shippingbill">
                                        <th>Shipping to billing </th>
                                        <td class="text-right">
                                            @if(Session::has('ongkir'))
                                            <span>Flat rate: Rp {{ number_format(Session::get('ongkir'))}}</span>
                                            <span style="display: none;"
                                                id="ongkir">{{ $cost = Session::get('ongkir') }}</span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr style="display: " class="order-total" id="ordertotalbill">
                                        <th>Order Total billing</th>
                                        <td class="text-right">
                                            @if (Request::get('reseller') == 'reseller')
                                            @if(Session::has('ongkir'))
                                            <span style="display: none;">{{ $totals = $gettotal + $cost }}</span>
                                            <span class="order-total-ammount">Rp {{ number_format($totals) }}</span>
                                            <span class="order-total-ammount" id="totals"
                                                style="display: none;">{{ $totals }}</span>
                                            @endif
                                            @else
                                            @if(Session::has('ongkir'))
                                            <span
                                                style="display: none;">{{ $totals = Session::get('getTotal') + $cost }}</span>
                                            <span class="order-total-ammount">Rp {{ number_format($totals) }}</span>
                                            <span class="order-total-ammount" id="totals"
                                                style="display: none;">{{ $totals }}</span>
                                            @endif
                                            @endif
                                        </td>
                                    </tr>
                                </div>
                                @else

                                @if (Session::has('kirimdropship') && Session::get('kirimdropship') == "on")
                                <tr class="shipping" id="shippingkirimdropship1">
                                    <th>Shipping </th>
                                    <td class="text-right">
                                        <span id="ongkirssDeliv"></span>
                                    </td>
                                </tr>
                                <tr class="order-total" id="ordertotalkirimdropship1">
                                    <th>Order Total</th>
                                    @if (Request::get('reseller') == 'reseller')
                                    <td class="text-right">
                                        <span class="order-total-ammount" id="totalorderres1Deliv"></span>
                                    </td>
                                    @else
                                    <td class="text-right">
                                        <span class="order-total-ammount" id="totalorderDeliv"></span>
                                    </td>
                                    @endif
                                </tr>
                                @else
                                <tr class="shipping" id="shippingbill1">
                                    <th>Shipping </th>
                                    <td class="text-right">
                                        <span id="ongkirss"></span>
                                    </td>
                                </tr>
                                <tr class="order-total" id="ordertotalbill1">
                                    <th>Order Total</th>
                                    @if (Request::get('reseller') == 'reseller')
                                    <td class="text-right">
                                        <span class="order-total-ammount" id="totalorderres1"></span>
                                    </td>
                                    @else
                                    <td class="text-right">
                                        <span class="order-total-ammount" id="totalorder"></span>
                                    </td>
                                    @endif
                                </tr>
                                @endif
                                @endif
                                {{-- @if ($option == '1')
                                @if (Session::has('kirimdropship') && Session::get('kirimdropship') == "on")
                                <tr class="shipping" id="shippingkirimdropship">
                                    <th>Shipping zz</th>
                                    <td class="text-right">
                                        @if(Session::has('ongkirDeliv'))
                                        <span>Flat rate: Rp {{ number_format(Session::get('ongkirDeliv'))}}</span>
                                <span style="display: none;"
                                    id="ongkir">{{ $cost = Session::get('ongkirDeliv') }}</span>
                                @endif
                                </td>
                                </tr>
                                <tr class="order-total" id="ordertotalkirimdropship">
                                    <th>Order Total</th>
                                    <td class="text-right">
                                        @if (Request::get('reseller') == 'reseller')
                                        @if(Session::has('ongkirDeliv'))
                                        <span style="display: none;">{{ $totals = $gettotal + $cost }}</span>
                                        <span class="order-total-ammount">Rp {{ number_format($totals) }}</span>
                                        <span class="order-total-ammount" id="totals"
                                            style="display: none;">{{ $totals }}</span>
                                        @endif
                                        @else
                                        @if(Session::has('ongkirDeliv'))
                                        <span
                                            style="display: none;">{{ $totals = Session::get('getTotal') + $cost }}</span>
                                        <span class="order-total-ammount">Rp {{ number_format($totals) }}</span>
                                        <span class="order-total-ammount" id="totals"
                                            style="display: none;">{{ $totals }}</span>
                                        @endif
                                        @endif
                                    </td>
                                </tr>
                                @else
                                <tr class="shipping" id="shippingbill">
                                    <th>Shipping zzz</th>
                                    <td class="text-right">
                                        @if(Session::has('ongkir'))
                                        <span>Flat rate: Rp {{ number_format(Session::get('ongkir'))}}</span>
                                        <span style="display: none;"
                                            id="ongkir">{{ $cost = Session::get('ongkir') }}</span>
                                        @endif
                                    </td>
                                </tr>
                                <tr class="order-total" id="ordertotalbill">
                                    <th>Order Total</th>
                                    <td class="text-right">
                                        @if (Request::get('reseller') == 'reseller')
                                        @if(Session::has('ongkir'))
                                        <span style="display: none;">{{ $totals = $gettotal + $cost }}</span>
                                        <span class="order-total-ammount">Rp {{ number_format($totals) }}</span>
                                        <span class="order-total-ammount" id="totals"
                                            style="display: none;">{{ $totals }}</span>
                                        @endif
                                        @else
                                        @if(Session::has('ongkir'))
                                        <span
                                            style="display: none;">{{ $totals = Session::get('getTotal') + $cost }}</span>
                                        <span class="order-total-ammount">Rp {{ number_format($totals) }}</span>
                                        <span class="order-total-ammount" id="totals"
                                            style="display: none;">{{ $totals }}</span>
                                        @endif
                                        @endif
                                    </td>
                                </tr>
                                @endif
                                @else
                                @if (Session::has('kirimdropship') && Session::get('kirimdropship') == "on")
                                <tr class="shipping" id="shippingkirimdropship1">
                                    <th>Shipping zzzz</th>
                                    <td class="text-right">
                                        <span id="ongkirssDeliv"></span>
                                    </td>
                                </tr>
                                <tr class="order-total" id="ordertotalkirimdropship1">
                                    <th>Order Total</th>
                                    @if (Request::get('reseller') == 'reseller')
                                    <td class="text-right">
                                        <span class="order-total-ammount" id="totalorderres1Deliv"></span>
                                    </td>
                                    @else
                                    <td class="text-right">
                                        <span class="order-total-ammount" id="totalorderDeliv"></span>
                                    </td>
                                    @endif
                                </tr>
                                @else
                                <tr class="shipping" id="shippingbill1">
                                    <th>Shipping zzzzz</th>
                                    <td class="text-right">
                                        <span id="ongkirss"></span>
                                    </td>
                                </tr>
                                <tr class="order-total" id="ordertotalbill1">
                                    <th>Order Total</th>
                                    @if (Request::get('reseller') == 'reseller')
                                    <td class="text-right">
                                        <span class="order-total-ammount" id="totalorderres1"></span>
                                    </td>
                                    @else
                                    <td class="text-right">
                                        <span class="order-total-ammount" id="totalorder"></span>
                                    </td>
                                    @endif
                                </tr>
                                @endif

                                @endif --}}
                            </tfoot>
                        </table>
                        <div class="checkout-payment">
                            <a href="{{ route('checkout') }}" id="cryinglightning" class="d-none">aaa</a>
                            <form action="#" class="payment-form">
                                <div class="payment-group mb--10">
                                    <div class="payment-radio">
                                        <input type="radio" value="1" name="payment-method" id="bank" checked>
                                        <label class="payment-label" for="cheque">Direct Bank Transfer</label>
                                    </div>
                                    <div class="payment-info" data-method="bank">
                                        <p>Lakukan pembayaran Anda langsung ke rekening bank kami di bawah ini<br>
                                            <div class="row">
                                                <div class="col">
                                                    <div class=" text-black text-center">
                                                        <img src="{{asset('assets/img/logobca.jpg')}}">
                                                    </div>

                                                </div>
                                                <div class="col">
                                                    <div class="pt-5 text-black text-center">
                                                        ARTLINE<br>
                                                        ac: 14000886347655
                                                    </div>

                                                </div>
                                            </div>
                                            {{-- <div class="container">
                                                <div class="row">
                                                    <div class="col-sm">
                                                        <img src="{{asset('assets/img/bankmandiri.png')}}">
                                    </div>
                                    <div class="col-sm text-center">
                                        PT.CHAROZA<br>
                                        ac: 14000886347655
                                    </div>
                                </div>
                        </div> --}}

                        .
                        </p>
                        {{-- <p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order will not be shipped until the funds have cleared in our account.</p> --}}
                    </div>
                </div>
                {{-- <div class="payment-group mb--10">
                                                        <div class="payment-radio">
                                                            <input type="radio" value="2" name="payment-method" id="cash">
                                                            <label class="payment-label" for="cash">
                                                                CASH ON DELIVERY
                                                            </label>
                                                        </div>
                                                        <div class="payment-info cash hide-in-default" data-method="cash">
                                                            <p>Pay with cash upon delivery.</p>
                                                        </div>
                                                    </div> --}}
                </form>
                <div class="payment-group mt--20">
                    @if ($option == '1' && !empty($totals))
                    <button type="submit" class="btn btn-fullwidth btn-style-1" id="buttonorder">Buat Pesanan</button>
                    @else
                    <button type="submit" class="btn btn-fullwidth btn-style-1 d-none" id="buttonorder">Buat
                        Pesanan</button>
                    <button type="submit" style="cursor: not-allowed !important;" class="btn btn-fullwidth btn-style-1"
                        id="buttonsalah" disabled>Buat Pesanan</button>
                    @endif
                    {{-- <p class="mb--15">Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our privacy policy.</p> --}}

                </div>
            </div>
        </div>
    </div>
    @else
    @if (Session::has('status'))

    @else
    <h1 class="text-center">Your cart is empty</h1>
    @endif
    @endif
</div>
<!-- Checkout Area End -->
</div>
</div>
</div>
</div>
<!-- Main Content Wrapper Start -->
@endsection

@section('extra_script')
<script>
    $( document ).ready(function() {
    $('#shippingkirimdropship').hide();
    $('#ordertotalkirimdropship').hide();
    $("#opsi2label").on('change','select',function () { $('#labelsubmitopsi2').click(); });
    $("#opsi1label").on('change','select',function () { $('#labelsubmit').click(); });
    // $("#shipdifferetadsisi").click(function () {
    //         if ($(this).is(":checked")) {
    //             alert('gak');
    //             $("#dvPassport").show();
    //             $("#AddPassport").hide();
    //         } else {
    //             alert('ya');
    //             $("#dvPassport").hide();
    //             $("#AddPassport").show();
    //         }
    //     });
});
</script>
<script>
    $(document).ready(function () {
            $("#buttonorder").on('click', function () {
                $("#orderbutton").trigger("click");
            })
          //  $('#shipdifferetads').click(function() {
                // if ($('#shipdifferetads').prop('checked') == 'true') {
                //     alert('abc')
                // }
              //  alert()
    // alert("Checkbox state (method 1) = " + $('#shipdifferetads').prop('checked'));
    // alert("Checkbox state (method 2) = " + $('#shipdifferetads').is(':checked'));
//});
$('#shipdifferetads').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
       // alert("tidak centang / kososng");
        document.getElementById("delivery_name").required=false;
        document.getElementById("delivery_phone").required=false;
        document.getElementById("delivery_province").required=false;
        document.getElementById("delivery_streetAddress").required=false;
        document.getElementById("deliverycalc_shipping_city").required=false;
        document.getElementById("delivery_kecamatan").required=false;
        document.getElementById("delivery_kelurahan").required=false;
        document.getElementById("delivery_kodepos").required=false;
        document.getElementById("delivery_email").required=false;
        document.getElementById("deliveryprovinceRow").required=false;
        document.getElementById("deliverycityRow").required=false;
        document.getElementById("delivery_streetAddress").val('');
        document.getElementById("deliverycalc_shipping_city").val('');
        // $('#shippingkirimdropship').hide();
        // $('#ordertotalkirimdropship').hide();
        // $("#shippingbill").show();
        // $("#ordertotalbill").show();
        $("#kirimdropship").val('kososng');
        var pointer = 'kosong';
        $.ajax({
                url:'{{url('/set/session/dropship/')}}',
                data:'name=' + pointer,
                type:'get',
                dataType: 'json',
                success:function (response) {
                  //  alert(response['isi']);
                }
            });
      }else{
        //   alert('ter centang');
          document.getElementById("delivery_name").required=true;
          document.getElementById("delivery_phone").required=true;
          document.getElementById("delivery_province").required=true;
          document.getElementById("delivery_streetAddress").required=true;
          document.getElementById("deliverycalc_shipping_city").required=true;
          document.getElementById("delivery_kecamatan").required=true;
          document.getElementById("delivery_kelurahan").required=true;
          document.getElementById("delivery_kodepos").required=true;
          document.getElementById("delivery_email").required=true;
          document.getElementById("deliveryprovinceRow").required=true;
          document.getElementById("deliverycityRow").required=true;
        //   $("#shippingbill").hide();
        //   $("#ordertotalbill").hide();
        //   $("#shippingkirimdropship").show();
        //   $("#ordertotalkirimdropship").show();
          $("#kirimdropship").val('on');
          var pointer = 'on';
        $.ajax({
                url:'{{url('/set/session/dropship/')}}',
                data:'name=' + pointer,
                type:'get',
                dataType: 'json',
                success:function (response) {
                  //  alert(response['isi']);
                }
            });
      }

    });
    $('#shipdifferetadsisi').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
       // alert("tidak centang / kososng");
        document.getElementById("delivery_name").required=false;
        document.getElementById("delivery_phone").required=false;
        document.getElementById("delivery_province").required=false;
        document.getElementById("delivery_streetAddress").required=false;
        document.getElementById("deliverycalc_shipping_city").required=false;
        document.getElementById("delivery_kecamatan").required=false;
        document.getElementById("delivery_kelurahan").required=false;
        document.getElementById("delivery_kodepos").required=false;
        document.getElementById("delivery_email").required=false;
        document.getElementById("deliveryprovinceRow").required=false;
        document.getElementById("deliverycityRow").required=false;
        document.getElementById("delivery_streetAddress").val('');
        document.getElementById("deliverycalc_shipping_city").val('');
      
        
        // $('#shippingkirimdropship').hide();
        // $('#ordertotalkirimdropship').hide();
        // $("#shippingbill").show();
        // $("#ordertotalbill").show();
        $("#kirimdropship").val('kososng');
        var pointer = 'kosong';
        $.ajax({
                url:'{{url('/set/session/dropship/')}}',
                data:'name=' + pointer,
                type:'get',
                dataType: 'json',
                success:function (response) {
                   // alert(response['isi']);
                }
            });
      }else{
          // alert('ter centang');
          document.getElementById("delivery_name").required=true;
          document.getElementById("delivery_phone").required=true;
          document.getElementById("delivery_province").required=true;
          document.getElementById("delivery_streetAddress").required=true;
          document.getElementById("deliverycalc_shipping_city").required=true;
          document.getElementById("delivery_kecamatan").required=true;
          document.getElementById("delivery_kelurahan").required=true;
          document.getElementById("delivery_kodepos").required=true;
          document.getElementById("delivery_email").required=true;
          document.getElementById("deliveryprovinceRow").required=true;
          document.getElementById("deliverycityRow").required=true;
        //   $('#shippingkirimdropship').removeAttr("style");
        //   $("#shippingbill").hide();
        //   $("#ordertotalbill").hide();
        //   $("#shippingkirimdropship").show();
        //   $("#ordertotalkirimdropship").show();
          $("#kirimdropship").val('on');
          var pointer = 'on';
        $.ajax({
                url:'{{url('/set/session/dropship/')}}',
                data:'name=' + pointer,
                type:'get',
                dataType: 'json',
                success:function (response) {
                    // alert(response['isi']);
                }
            });
      }

    });
            $('#label').on('change', function() {
                // alert('abc');
                $('#labelsubmit').click();
            });

            var provincerow = $('#billing_province').val();

            $('#provinceRow').val(provincerow)

            var cityrow = $('#billing_city').val();

            $('#cityRow').val(cityrow)


            var totalorders = $('#totals').text();

            $('#totalorders').val(totalorders);
            $('#totalorders1').val(totalorders);

            var subtotal = $('#subtotalcheckout').text();

            $('#subtotalorderinput').val(subtotal);
        });
</script>
<script>
    $(document).ready(function(){
        var text = $('#billing_city2 option:selected').html()

        $("#placecitynamehere2").val(text);
    });
    $("select.provinces").change(function(){
            var pointer = $(this).children("option:selected").val();
            // alert(pointer);
            $.ajax({
                url:'{{url('/ongkir/city/')}}',
                data:'province=' + pointer,
                type:'get',
                dataType: 'json',
                success:function (response) {
                    console.log(response['rajaongkir']['results']);

                    $("#calc_shipping_city").attr('disabled', false);

                    var $select = $('#calc_shipping_city');

                    $select.find('option').remove();
                    $.each(response['rajaongkir']['results'],function(key, value)
                    {
                        $select.append('<option value=' + value.city_id + '>' + value.city_name + '</option>');
                    });
                }
            });
        });

        $("select.citys").change(function(){
            var pointer = $(this).children("option:selected").val();
            var cityname = $(this).children("option:selected").text();
            // alert(cityname);
            $("#placecitynamehere").val(cityname);
            $("#billcityname").val(cityname);
            $.ajax({
                url:'{{url('/ongkir/calculate/')}}',
                data:'billCity=' + pointer,
                type:'get',
                dataType: 'json',
                success:function (response) {
                    alert(response['ongkir']);
                    $("#ongkirss").html("Flat rate: Rp " + response['ongkir']);
                    $("#ongkirinput").val(response['ongkirraw']);
                    $("#totalorder").html("Rp " + response['total']);
                    $("#totalorders").val(response['totalraw']);
                    $("#total_order").val(response['totalraw']);
                    $("#subtotalorderinput").val(response['subtotal']);
                    var subtotal = $('#subtotalcheckout').text();
                    var totalorderres = parseFloat(subtotal) + parseFloat(response['ongkirraw'])
                    $("#totalorderres1").html("Rp " + totalorderres);
                    $("#totalorders1").val(totalorderres);
                    $("#total_order_reseller").val(totalorderres);
                    var element = document.getElementById("buttonsalah");
                    element.classList.add("d-none");
                    var element1 = document.getElementById("buttonorder");
                    element1.classList.remove("d-none");
                }
            });
        });
</script>
<script>
    // $('#reseller').click(function () {
    //     if ($('#reseller').attr('checked', true)) {
    //         alert('checked')

    //     } else {
    //         alert('checkeded')
    //         // $('#cryinglightning').trigger("click");
    //     }
    // })

    // $(document).ready(function(){
    //     $('#reseller').click(function(){
    //         if($(this).prop("checked") == true){
    //             var loc = location.href;
    //             loc = loc.split("&")[0]; // to get the URL till /?s=sunglass
    //             $('input[type="checkbox"]').each(function(i){
    //                 if(this.checked){
    //                     @if (Request::get('label') == 'Manual')
    //                         loc += "&" + $(this).attr("name") + "=" + $(this).attr("value");
    //                     @else
    //                         // loc += "?" + $(this).attr("name") + "=" + $(this).attr("value");
    //                         loc;
    //                     @endif
    //                 }
    //             });
    //             location.href = loc;
    //         }
    //         else if($(this).prop("checked") == false){
    //             $('#cryinglightning')[0].click();
    //         }
    //     });
    // });



    $(document).ready(function(){
        var text = $('#delivery_city2 option:selected').html()

        $("#deliveryplacecitynamehere2").val(text);
    });
    $("select.deliveryprovinces").change(function(){
            var pointer = $(this).children("option:selected").val();

            $.ajax({
                url:'{{url('/ongkir/city/')}}',
                data:'province=' + pointer,
                type:'get',
                dataType: 'json',
                success:function (response) {
                    console.log(response['rajaongkir']['results']);

                    $("#deliverycalc_shipping_city").attr('disabled', false);

                    var $select = $('#deliverycalc_shipping_city');

                    $select.find('option').remove();
                    $.each(response['rajaongkir']['results'],function(key, value)
                    {
                        $select.append('<option value=' + value.city_id + '>' + value.city_name + '</option>');
                    });
                }
            });
        });

        $("select.deliverycitys").change(function(){
            var pointer = $(this).children("option:selected").val();
            var cityname = $(this).children("option:selected").text();
            $("#delivplacecitynamehere").val(cityname);
            $.ajax({
                url:'{{url('/ongkir/calculateDeliv/')}}',
                data:'billCity=' + pointer,
                type:'get',
                dataType: 'json',
                success:function (response) {
                    // alert(response['ongkir']);
                    $("#ongkirssDeliv").html("Flat rate: Rp " + response['ongkir']);
                    $("#ongkirinputDeliv").val(response['ongkirraw']);
                    $("#totalorderDeliv").html("Rp " + response['total']);
                    $("#totalorders").val(response['totalraw']);
                    $("#total_order_deliv").val(response['totalraw']);
                    var subtotal = $('#subtotalcheckout').text();
                    var totalorderres = parseFloat(subtotal) + parseFloat(response['ongkirraw'])
                    $("#totalorderres1Deliv").html("Rp " + totalorderres);
                    $("#totalorders1").val(totalorderres);
                    $("#total_order_reseller_deliv").val(totalorderres);
                    var element = document.getElementById("buttonsalah");
                    element.classList.add("d-none");
                    var element1 = document.getElementById("buttonorder");
                    element1.classList.remove("d-none");
                }
            });
        });
</script>
@endsection