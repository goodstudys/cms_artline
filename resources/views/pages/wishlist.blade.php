@extends('master')
{{-- @section('nav')
  @include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')
            <!-- Breadcrumb area Start -->
            <div class="breadcrumb-area pt--40 pb--30 pb-md--20">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 text-center">
                            <h1 class="page-title">Wishlist</h1>
                            <ul class="breadcrumb justify-content-center">
                                <li><a href="{{ Route('index') }}">Home</a></li>
                                <li class="current"><span>Wishlist</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Breadcrumb area End -->
    
            <!-- Main Content Wrapper Start -->
            <div id="content" class="main-content-wrapper">
                <div class="page-content-inner">
                    <div class="container">
                        <div class="row pt--50 pt-md--40 pt-sm--20 pb--65 pb-md--45 pb-sm--25">
                            <div class="col-12" id="main-content">
                                <div class="table-content table-responsive">
                                    <table class="table table-style-2 wishlist-table text-center">
                                        <thead>
                                            <tr>
                                                <th>&nbsp;</th>
                                                <th>&nbsp;</th>
                                                <th class="text-left">Product</th>
                                                <th>Stock Status</th>
                                                <th>Price</th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="product-remove text-left"><a href=""><i class="dl-icon-close"></i></a></td>
                                                <td class="product-thumbnail text-left">
                                                    <img src="{{asset('assets/img/products/prod-11-1-70x88.jpg')}}" alt="Product Thumnail">
                                                </td>
                                                <td class="product-name text-left wide-column">
                                                    <h3>
                                                        <a href="product-details.html">Serum Non Toxic Halal</a>
                                                    </h3>
                                                </td>
                                                <td class="product-stock">
                                                    In Stock
                                                </td>
                                                <td class="product-price">
                                                    <span class="product-price-wrapper">
                                                        <span class="money">Rp 149.000</span>
                                                    </span>
                                                </td>
                                                <td class="product-action-btn">
                                                    <a href="{{Route('cart')}}">Add to cart</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="product-remove text-left"><a href=""><i class="dl-icon-close"></i></a></td>
                                                <td class="product-thumbnail text-left">
                                                    <img src="{{asset('assets/img/products/prod-29-1-70x88.jpg')}}" alt="Product Thumnail">
                                                </td>
                                                <td class="product-name text-left wide-column">
                                                    <h3>
                                                        <a href="product-details.html">Anti Aging Moisturizer</a>
                                                    </h3>
                                                </td>
                                                <td class="product-stock">
                                                    In Stock
                                                </td>
                                                <td class="product-price">
                                                    <span class="product-price-wrapper">
                                                        <span class="money">Rp 149.000</span>
                                                    </span>
                                                </td>
                                                <td class="product-action-btn">
                                                    <a href="{{Route('cart')}}">Add to cart</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="product-remove text-left"><a href=""><i class="dl-icon-close"></i></a></td>
                                                <td class="product-thumbnail text-left">
                                                    <img src="{{asset('assets/img/products/prod-30-1-70x88.jpg')}}" alt="Product Thumnail">
                                                </td>
                                                <td class="product-name text-left wide-column">
                                                    <h3>
                                                        <a href="product-details.html">Orange Juice Spray</a>
                                                    </h3>
                                                </td>
                                                <td class="product-stock">
                                                    In Stock
                                                </td>
                                                <td class="product-price">
                                                    <span class="product-price-wrapper">
                                                        <span class="money">Rp 149.000</span>
                                                    </span>
                                                </td>
                                                <td class="product-action-btn">
                                                    <a href="{{Route('cart')}}">Add to cart</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="product-remove text-left"><a href=""><i class="dl-icon-close"></i></a></td>
                                                <td class="product-thumbnail text-left">
                                                    <img src="{{asset('assets/img/products/prod-31-1-70x88.jpg')}}" alt="Product Thumnail">
                                                </td>
                                                <td class="product-name text-left wide-column">
                                                    <h3>
                                                        <a href="product-details.html">Green Tea Perfume</a>
                                                    </h3>
                                                </td>
                                                <td class="product-stock">
                                                    In Stock
                                                </td>
                                                <td class="product-price">
                                                    <span class="product-price-wrapper">
                                                        <span class="money">Rp 149.000</span>
                                                    </span>
                                                </td>
                                                <td class="product-action-btn">
                                                    <a href="{{Route('cart')}}">Add to cart</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Content Wrapper Start -->
@endsection