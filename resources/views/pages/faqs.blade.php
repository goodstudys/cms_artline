@extends('master')
{{-- @section('nav')
  @include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')
<!-- Breadcrumb area Start -->
<div class="breadcrumb-area breadcrumb-bg pt--40 pb--50">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="page-title">FAQ</h1>
                <ul class="breadcrumb justify-content-center">
                    <li><a href="{{ Route('index') }}">Home</a></li>
                    <li class="current"><span>FAQs Page</span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb area End -->

<!-- Main Content Wrapper Start -->
<div id="content" class="main-content-wrapper">
    <div class="page-content-inner blog-page-sidebar">
        <div class="container">
            <div class="row pt--70 pt-md--50 pt-sm--30 pb--50 pb-md--30 pb-sm--20">
                <div class="col-12">
                    <div class="row mb--40 mb-md--30 mb-sm--20">
                        <div class="col-12">
                            <h2 class="heading-secondary mt--2 mt-md--4">General Question</h2>
                        </div>
                    </div>
                    @include('components.faqs.general',['general'=>$general])
                    <div class="row mb--40 mb-md--30 mb-sm--20">
                        <div class="col-12">
                            <h2 class="heading-secondary mt--2 mt-md--4">Payment</h2>
                        </div>
                    </div>
                    @include('components.faqs.payment',['payment'=>$payment])
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main Content Wrapper Start -->
@endsection