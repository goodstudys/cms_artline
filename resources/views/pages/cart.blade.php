@extends('master')
{{-- @section('nav')
  @include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')

        <!-- Breadcrumb area Start -->
        <div class="breadcrumb-area pt--40 pb--30 pb-md--20">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 text-center">
                            <h1 class="page-title">Shopping Cart</h1>
                            <ul class="breadcrumb justify-content-center">
                                <li><a href="index.html">Home</a></li>
                                <li class="current"><span>Shopping Cart</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Breadcrumb area End -->

            <!-- Main Content Wrapper Start -->
            <div id="content" class="main-content-wrapper">
                <div class="page-content-inner">
                    <div class="container">
                        @if (Session::has('status'))
                            <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                        @endif
                        @if (Cart::getContent()->count() != 0)
                            @if ($provinces != 'Connection problem')
                                <div class="row pt--50 pt-md--40 pt-sm--20 pb--80 pb-md--60 pb-sm--40">
                                    <div class="col-lg-8 mb-md--30">
                                        <div class="row no-gutters">
                                            <div class="col-12">
                                                <div class="table-content table-responsive">
                                                    <table class="table text-center">
                                                        <thead>
                                                        <tr>
                                                            <th>&nbsp;</th>
                                                            <th>&nbsp;</th>
                                                            <th width="5%">&nbsp;</th>
                                                            <th class="text-left">Product</th>
                                                            <th>price</th>
                                                            <th>quantity</th>
                                                            <th>total</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <div style="display: none">
                                                            {{ $total = 0 }}
                                                            {{ $totalss = 0 }}
                                                            {{ $zero = 0 }}
                                                        </div>
                                                        @foreach($datas as $data)
                                                            <tr>
                                                                <td class="product-remove text-left"><a href="{{ url('/cart/destroy/'. $data->id) }}"><i class="dl-icon-close"></i></a></td>
                                                                <td class="product-thumbnail text-left">
                                                                    <img src="{{ asset('/upload/users/products/'. $data->attributes->image) }}" alt="Product Thumnail">
                                                                </td>
                                                                <td></td>
                                                                <td class="product-name text-left wide-column">
                                                                    <h3>
                                                                        <a href="product-details.html">{{ $data->name. ' ('.$data->attributes->colorname.')'}} </a>
                                                                    </h3>
                                                                </td>
                                                                <td class="product-price">
                                                                <span class="product-price-wrapper">
                                                                    @if (Session::has('kode'))
                                                                        @if (Session::get('category') == $data->attributes->category)
                                                                            @if (Session::get('discAmount') == '0' && Session::get('discPercent') == '0')
                                                                                <span class="money">Rp {{ number_format($data->price) }}</span>
                                                                                <span style="display: none;">{{ $newprice = $data->price }}</span>
                                                                            @elseif(Session::get('discAmount') == '0')
                                                                                <span class="product-price-old">
                                                                                    <span class="money">Rp {{ number_format($data->price) }}</span>
                                                                                </span>
                                                                                <span class="money">Rp {{ number_format($data->price - ($data->price*Session::get('discPercent')/100)) }}</span>
                                                                                <span style="display: none;">{{ $newprice = $data->price - ($data->price*Session::get('discPercent')/100) }}</span>
                                                                            @elseif(Session::get('discPercent') == '0')
                                                                                {{-- <span class="product-price-old">
                                                                                    <span class="money">Rp {{ number_format($data->price) }}</span>
                                                                                </span> --}}
                                                                                 <span class="money">Rp {{ number_format($data->price) }}</span>
                                                                                <span style="display: none;">{{ $newprice = $data->price-Session::get('discAmount') }}</span>
                                                                            @endif
                                                                        @else
                                                                            <span class="money">Rp {{ number_format($data->price) }}</span>
                                                                        @endif
                                                                    @else
                                                                        <span class="money">Rp {{ number_format($data->price) }}</span>
                                                                        <span style="display: none;">{{ $newprice = $data->price }}</span>
                                                                    @endif
                                                                </span>
                                                                </td>
                                                                <td class="product-quantity">
                                                                    <input type="hidden" id="row{{ $data->id }}" value="{{ $data->id }}">
                                                                    <div class="quantity">
                                                                        <input type="number" class="quantity-input" name="qty" id="qty-1{{$data->id}}" value="{{ $data->quantity }}" min="1">
                                                                        <div class="dec qtybutton" id="min{{$data->id}}">-</div>
                                                                        <div class="inc qtybutton" id="plus{{$data->id}}">+</div>
                                                                    </div>
                                                                </td>
                                                                <td class="product-total-price">
                                                                <span class="product-price-wrapper">
                                                                    @if (Session::has('kode'))
                                                                        @if (Session::get('category') == $data->attributes->category)
                                                                        <span class="product-price-old">
                                                                            <span class="money">Rp {{ number_format($newprice * $data->quantity) }}</span>
                                                                        </span>
                                                                            <span class="money">Rp {{ number_format($newprice * $data->quantity-Session::get('discAmount') ) }}</span>
                                                                            <span class="money" style="display: none">{{ $prodprice = $newprice * $data->quantity-Session::get('discAmount')  }}</span>
                                                                        @else
                                                                            <span class="money" style="display: none">{{ $prodprice = $data->price * $data->quantity }}</span>
                                                                            <span class="money">Rp {{ number_format($data->price * $data->quantity) }}</span>
                                                                        @endif
                                                                    @else
                                                                        <span class="money">Rp {{ number_format($data->price * $data->quantity) }}</span>
                                                                        <span class="money" style="display: none">{{ $prodprice = $data->price * $data->quantity }}</span>
                                                                    @endif
                                                                </span>
                                                                    <span style="display: none">{{ $perweight = $data->attributes->weight * $data->quantity }}</span>
                                                                </td>
                                                                <td>
                                                                    <span style="display: none">{{ $allweight = $total += $perweight  }}</span>
                                                                    <span style="display: none">{{ Session::put('weightRow',  $allweight)  }}</span>
                                                                    <span style="display: none;">{{ Session::put('qtys',  $totalss += $data->quantity)  }}</span>
                                                                </td>
                                                                <td>
                                                                   @if (Session::has('kode'))
                                                                        <span style="display: none">{{ $gettotal = $zero += $prodprice  }}</span>
                                                                        <span style="display: none">{{ Session::put('getTotal',  $gettotal)  }}</span>
                                                                   @else
                                                                        <span style="display: none">{{ $gettotal = $zero += $prodprice  }}</span>
                                                                        <span style="display: none">{{ Session::put('getTotal',  $gettotal)  }}</span>
                                                                   @endif
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row no-gutters border-top pt--20 mt--20">
                                            <div class="col-sm-6">
                                                <form class="cart-form" action="{{ route('coupon.use') }}">
                                                    <input type="hidden" name="minOrder" value="{{ Session::get('getTotal') }}">
                                                    <div class="coupon">
                                                        <input type="text" id="coupon" name="coupon" class="cart-form__input" placeholder="Coupon Code">
                                                        <input type="submit" class="cart-form__btn" value="Apply Coupon">
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-sm-6 text-sm-right">
                                                <a href="{{ route('cart.clear') }}" class="cart-form__btn">Clear Cart</a>
                                                <button class="cart-form__btn" id="update-cart" onclick="window.location.reload();">Update Cart</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="cart-collaterals">
                                            <div class="cart-totals">
                                                <h5 class="font-size-14 font-bold mb--15">Cart totals</h5>
                                                <span id="reminder" class="text-danger">Cart change, please update cart</span>
                                                <div class="cart-calculator">
{{--                                                    <div class="cart-calculator__item">--}}
{{--                                                        <div class="cart-calculator__item--head">--}}
{{--                                                            <span>Subtotal</span>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="cart-calculator__item--value">--}}
{{--                                                            <span>Rp {{ number_format(Cart::getSubTotal()) }}</span>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
                                                    <div class="cart-calculator__item">
                                                        <div class="cart-calculator__item--head">
                                                            <span>Coupon</span>
                                                        </div>
                                                        <div class="cart-calculator__item--value row">
                                                            @if (Session::has('kode'))
                                                                <div class="col-11"><span>{{ Session::get('kode') }}</span></div>
                                                                <div class="float-right mt--1"><a class="float-right" href="{{ route('coupon.cancel') }}"><i class="dl-icon-close"></i></a></div>
                                                            @else
                                                                <div class="col-11"><span>Tidak menggunakan coupon</span></div>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="cart-calculator__item order-total">
                                                        <div class="cart-calculator__item--head">
                                                            <span>Total</span>
                                                        </div>
                                                        <div class="cart-calculator__item--value">
                                                    <span class="product-price-wrapper">
                                                        @if (Session::has('kode'))
                                                            <span class="money">Rp {{ number_format(Session::get('getTotal')) }}</span>
                                                        @else
                                                            <span class="money">Rp {{ number_format(Cart::getTotal()) }}</span>
                                                        @endif
                                                    </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <a id="check" href="{{Route('checkout')}}" class="btn btn-fullwidth btn-style-1">
                                                Proceed To Checkout
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <h1 class="text-center">No internet connection</h1>
                            @endif
                        @else
                            <h1 class="text-center">Empty cart</h1>
                        @endif
                    </div>
                </div>
            </div>
            <!-- Main Content Wrapper Start -->
@endsection
@section('extra_script')
    <script>
        @if(Cart::getContent()->count() != 0)
            @if ($provinces != 'Connection problem')
                $(document).ready(function () {
                    console.log('ready')
                    $("#reminder").hide();

                    $("select.provinces").change(function(){
                        var id = $(this).children("option:selected").val();

                        $("#provinceRow").val(id)
                    });

                    $("select.citys").change(function(){
                        var id = $(this).children("option:selected").val();

                        $("#cityRow").val(id)
                    });

                    {{--$("#calc_shipping_province").change(function(){--}}
                    {{--    var provid = $(this).val();--}}

                    {{--    $.ajax({--}}
                    {{--        url:'{{ url('/ongkir/city') }}',--}}
                    {{--        type: 'post',--}}
                    {{--        data: {province:provid},--}}
                    {{--        dataType: 'json',--}}
                    {{--        success:function(response){--}}

                    {{--            var len = response.length;--}}

                    {{--            $("#calc_shipping_city").empty();--}}
                    {{--            for( var i = 0; i<len; i++){--}}
                    {{--                var id = response[i]['city_id'];--}}
                    {{--                var name = response[i]['city_name'];--}}

                    {{--                $("#calc_shipping_city").append("<option value='"+id+"'>"+name+"</option>");--}}

                    {{--            }--}}
                    {{--        }--}}
                    {{--    });--}}
                    {{--});--}}

                    @foreach($datas as $data)
                        $("#plus{{$data->id}}").on('click', function () {
                            $("#cart-change").val("1");
                            $("#reminder").show();
                            $("#check").addClass('disabled')
                            var newQty = $("#qty-1{{$data->id}}").val();
                            var rowId = $("#row{{ $data->id }}").val();
                            // alert(rowId)
                            $.ajax({
                                url:'{{url('/cart/update')}}',
                                data:'rowId=' + rowId + '&newQty=' + newQty + '&type=inc',
                                type:'get',
                                success:function (response) {
                                    console.log(response);
                                }
                            });
                        });
                        $("#min{{$data->id}}").on('click', function () {
                            $("#cart-change").val("1");
                            $("#reminder").show();
                            var newQty = $("#qty-1{{$data->id}}").val();
                            var rowId = $("#row{{ $data->id }}").val();
                            $.ajax({
                                url:'{{ url('cart/update') }}',
                                data:'rowId=' + rowId + '&newQty=' + newQty + '&type=dec',
                                type:'get',
                                success:function (response) {
                                    console.log(response);
                                },
                                error: function(req, err){ console.log('my message' + err); }
                            })

                        })
                    @endforeach
                })
            @endif
        @endif
    </script>
@stop
