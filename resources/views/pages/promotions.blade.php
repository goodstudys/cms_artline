@extends('master')
{{-- @section('nav')
@include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')
<!-- Breadcrumb area Start -->
<div class="breadcrumb-area pt--40 pb--30 pb-md--20">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="page-title">Promotions</h1>
                <ul class="breadcrumb justify-content-center">
                    <li><a href="{{ Route('index') }}">Home</a></li>
                    <li class="current"><span>Promotions </span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb area End -->
<!-- Main Content Wrapper Start -->
<div id="content" class="main-content-wrapper">
    <div class="shop-page-wrapper">
        <div class="container-fluid">
            <div class="row shop-fullwidth pt--30 pt-sm--20 pb--60 pb-md--50 pb-sm--40">
                <div class="card-columns">
                    @foreach ($promotions as $item)
                    <div class="card" style="border:none;">
                        <div class="banner-box banner-type-8 banner-type-8-left-bottom banner-hover-3">
                            <div class="banner-inner">
                                <div class="banner-image">
                                    <img src="{{ asset('/upload/users/promotions/'. $item->image) }}" alt="Banner">
                                </div>
                                <a href="{{ Route('shop') }}">
                                    <div class="banner-info">
                                        <div class="banner-info--inner">
                                            <p class="font-size-28 color--white lts-8 font-normal">{{$item->title}}</p>
                                        </div>
                                    </div>
                                </a>
                                <a class="banner-link banner-overlay" href="{{ Route('shop') }}">Shop Now</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main Content Wrapper Start -->
@endsection
