@extends('master')
{{-- @section('nav')
@include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')

<!-- Breadcrumb area Start -->
<div class="breadcrumb-area breadcrumb-bg pt--40 pb--50">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="page-title">Contact Us</h1>
                <ul class="breadcrumb justify-content-center">
                    <li><a href="index.html">Home</a></li>
                    <li class="current"><span>Contact Us</span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb area End -->

<!-- Main Content Wrapper Start -->
<div id="content" class="main-content-wrapper">
    @if (session('status') == 'success')
    <div style="  width: 80%;
    margin: 20px auto;" class="alert alert-success" role="alert">
        <strong>Well done!</strong> {{ session('message') }}.
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif
    <div class="page-content-inner">
        <div class="container">

            <div class="row pt--80 pt-md--60 pt-sm--40 pb--75 pb-md--55 pb-sm--35">
                <div class="col-md-5 order-md-1 order-2">

                    <ul class="contact-info-2 mb--50">
                        <li class="contact-info__item">
                            <i class="fa fa-map-marker"></i>
                            <span> Jl. Kenjeran 123 <br> Surabaya</span>
                        </li>
                        <li class="contact-info__item">
                            <i class="fa fa-envelope-o"></i>
                            <span>info@artline.co.id</span>
                        </li>
                        <li class="contact-info__item">
                            <i class="fa fa-mobile"></i>
                            <span>+62 816 356 567</span>
                        </li>
                        <li class="contact-info__item">
                            <i class="fa fa-clock-o"></i>
                            <span>8:00 – 21:00, Mon – Sat</span>
                            <span>9:00 – 20:00, Sun</span>
                        </li>
                    </ul>

                    <!-- Social Icons Start Here -->
                    <ul class="social social-round dark-bg">
                        <li class="social__item">
                            <a href="twitter.com" class="social__link">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="social__item">
                            <a href="plus.google.com" class="social__link">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>
                        <li class="social__item">
                            <a href="facebook.com" class="social__link">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="social__item">
                            <a href="youtube.com" class="social__link">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </li>
                        <li class="social__item">
                            <a href="instagram.com" class="social__link">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- Social Icons End Here -->
                </div>
                <div class="col-md-7 order-md-2 order-1 mb-sm--30">


                    <!-- Contact form Start Here -->
                    @include('components.contact.form')
                    <!-- Contact form end Here -->


                </div>

            </div>
        </div>

        <!-- maps Start Here -->
        @include('components.contact.map')
        <!-- maps Start Here -->

    </div>
</div>
<!-- Main Content Wrapper Start -->

@endsection