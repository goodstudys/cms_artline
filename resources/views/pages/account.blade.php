@extends('master')
{{-- @section('nav')
  @include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')
<!-- Main Content Wrapper Start -->
<div id="content" class="main-content-wrapper">
    <div class="page-content-inner">
        <div class="container">
            <div class="row pt--50 pt-md--40 pt-sm--20 pb--80 pb-md--60 pb-sm--40">
                <div class="col-12">
                    <div class="user-dashboard-tab flex-column flex-md-row">
                        @include('components.account.tablist')
                        <div class="user-dashboard-tab__content tab-content">
                            @include('components.account.contentuser')
                            @include('components.account.dropship')
                            @include('components.account.order.index')
                            @include('components.account.contentdownloads')
                            @include('components.account.address.index')
                            @include('components.account.pengajuanReseller.index')
                            @include('components.account.testiomonial.index')
                            @include('components.account.contentdetails')
                            @include('components.account.newsletter.index')
                            @include('components.account.preorder.index')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main Content Wrapper Start -->
@endsection
@section('extra_script')
<script>
    $(function() {
        document.getElementById("inputother").style.visibility = 'hidden';
});
    function readURL(input,ab) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(ab)
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
<script>
    function checkAlert(evt) {
  if (evt.target.value === "5") {
    document.getElementById("inputother").required=true;
    document.getElementById("inputother").style.visibility = 'visible';
  }else{
    if(document.getElementById("inputother").required == true){
        document.getElementById("inputother").required=false;
        document.getElementById("inputother").style.visibility = 'hidden';
    }
  }
}
    function changeradioother(){
          var other= document.getElementById("other");
          other.value=document.getElementById("inputother").value;
          }
    // function setRequired(){
    
    //     document.getElementById("inputother").required=true;
    // }
    
    // function removeRequired(){
    //     alert("I am an alert box!");
    // if(document.getElementById("inputother").required == true){
    //     document.getElementById("inputother").required=false;
    // }
   // }
</script>
<script>
    $("select.rate-option").change(function(){
            var id = $(this).children("option:selected").val();

            $("#rate").val(id)
        });
</script>
@stop