@extends('master')
{{-- @section('nav')
  @include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')
    
        <!-- Breadcrumb area Start -->
        <div class="breadcrumb-area pt--40 pb--30 pb-md--20">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 text-center">
                        <h1 class="page-title">New Products</h1>
                        <ul class="breadcrumb justify-content-center">
                            <li><a href="{{ Route('index') }}">Home</a></li>
                            <li class="current"><span>New Products</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- Breadcrumb area End -->

        <!-- Main Content Wrapper Start -->
        <div id="content" class="main-content-wrapper">
            <div class="shop-page-wrapper">
                <div class="container-fluid">
                    <div class="row shop-fullwidth mt--2 mt-sm--4 pt--25 pt-md--15 pt-sm--5 pb--90 pb-md--70 pb-sm--50">
                        <div class="col-12">
                            <div class="shop-toolbar">
                                <div class="shop-toolbar__inner">
                                    <div class="row align-items-center">
                                        <div class="col-md-6 text-md-left text-center mb-sm--5">
                                            <div class="shop-toolbar__left">
                                                <p class="product-pages">Showing 20 of 20 results</p>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="shop-toolbar__right">
                                                {{-- <a href="#" class="product-filter-btn shop-toolbar__btn">
                                                    <span>Filters</span>
                                                    <i></i>
                                                </a> --}}
                                                {{-- <div class="product-ordering">
                                                    <a href="" class="product-ordering__btn shop-toolbar__btn">
                                                        <span>Short By</span>
                                                        <i></i>
                                                    </a>
                                                    <ul class="product-ordering__list">
                                                        <li class="active"><a href="#">Sort by popularity</a></li>
                                                        <li><a href="#">Sort by average rating</a></li>
                                                        <li><a href="#">Sort by newness</a></li>
                                                        <li><a href="#">Sort by price: low to high</a></li>
                                                        <li><a href="#">Sort by price: high to low</a></li>
                                                    </ul>
                                                </div> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="advanced-product-filters">
                                    <div class="product-filter">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="product-widget product-widget--price">
                                                    <h3 class="widget-title">Price</h3>
                                                    <ul class="product-widget__list">
                                                        <li>
                                                            <a href="#">
                                                                <span class="ammount">Rp 20.000</span>
                                                                <span> - </span>
                                                                <span class="ammount">Rp 40.000</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <span class="ammount">Rp 40.000</span>
                                                                <span> - </span>
                                                                <span class="ammount">Rp 50.000</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <span class="ammount">Rp 50.000</span>
                                                                <span> - </span>
                                                                <span class="ammount">Rp 60.000</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <span class="ammount">Rp 60.000</span>
                                                                <span> - </span>
                                                                <span class="ammount">Rp 80.000</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <span class="ammount">Rp 80.000</span>
                                                                <span> - </span>
                                                                <span class="ammount">Rp 100.000</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <span class="ammount">Rp 100.000</span>
                                                                <span> + </span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="product-widget product-widget--brand">
                                                    <h3 class="widget-title">Brands</h3>
                                                    <ul class="product-widget__list">
                                                        <li>
                                                            <a href="#">
                                                                <span>Revlon</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <span>Wardah</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <span>Maybelline</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <span>OPI</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="product-widget product-widget--color">
                                                    <h3 class="widget-title">Color</h3>
                                                    <ul class="product-widget__list product-color-swatch">
                                                        <li>
                                                            <a href="#" class="product-color-swatch-btn black">
                                                                <span class="product-color-swatch-label">Black</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="product-color-swatch-btn blue">
                                                                <span class="product-color-swatch-label">Blue</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="product-color-swatch-btn grey">
                                                                <span class="product-color-swatch-label">Grey</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="product-color-swatch-btn pink">
                                                                <span class="product-color-swatch-label">Pink</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="product-color-swatch-btn red">
                                                                <span class="product-color-swatch-label">Red</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="product-color-swatch-btn violet">
                                                                <span class="product-color-swatch-label">Violet</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" class="product-color-swatch-btn yellow">
                                                                <span class="product-color-swatch-label">Yellow</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="product-widget product-widget--size">
                                                    <h3 class="widget-title">Size</h3>
                                                    <ul class="product-widget__list">
                                                        <li>
                                                            <a href="#">
                                                                <span>L</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <span>M</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <span>S</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <span>XL</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#">
                                                                <span>XXL</span>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <a href="#" class="btn-close"><i class="dl-icon-close"></i></a>
                                    </div>
                                </div>
                            </div>

                            <!--view products -->
                            @include('components.newlook.viewproducts')
                            <!--view products -->

                            {{-- <nav class="pagination-wrap mt--35 mt-md--25">
                                    @if ($products->lastPage() > 1)
                                    <ul class="pagination">
                                        <li><a href="{{ $products->url(1) }}"
                                                class="prev page-number {{ ($products->currentPage() == 1) ? ' disabled' : '' }}"><i
                                                    class="fa fa-long-arrow-left"></i></a></li>
            
            
            
                                        @for ($i = 1; $i <= $products->lastPage(); $i++)
            
            
                                            @if (($products->currentPage() == $i))
                                            <li> <span class="current page-number">{{ $i }}</span></li>
                                            @else
                                            <li><a href="{{ $products->url($i) }}" class="page-number">{{ $i }}</a></li>
                                            @endif
            
            
            
                                            @endfor
            
            
            
            
                                            @if ($products->currentPage() == $products->lastPage())
                                            <li><a href="#" class=" next page-number"><i class="fa fa-long-arrow-right"></i></a>
                                            </li>
                                            @else
                                            <li><a href="{{ $products->url($products->currentPage()+1) }}"
                                                    class=" next page-number"><i class="fa fa-long-arrow-right"></i></a></li>
                                            @endif
            
            
                                    </ul>
                                    @endif
                            </nav> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Main Content Wrapper Start -->
        @include('components.newlook.productModal')
@endsection