@extends('master')
{{-- @section('nav')
@include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')
<!-- Main Content Wrapper Start -->
<div id="content" class="main-content-wrapper">
    <div class="page-content-inner">
        <div class="container">
            <div class="row justify-content-center ptb--80 ptb-md--60 ptb-sm--40">
                <div class="col-lg-10" id="main-content">
                    <div class="single-post">
                        <!-- Single Blog Start Here -->
                        <article class="single-post-details">
                            <div class="entry-header">
                                <div class="entry-meta category-link mb--10">
                                    <a href="{{ Route('blogs') }}">Life Style</a>
                                </div>
                                <h2 class="entry-title">{{$blog != null ? $blog->title:''}}</h2>
                                <div class="entry-meta mb--30">
                                    <a href="{{ Route('blogs') }}"
                                        class="posted-on">{{Carbon\Carbon::parse($blog->dateCreated)->format('M d,  Y ')}}</a>
                                    <span class="meta-separator">-</span>
                                    <a href="{{ Route('blogs') }}" class="posted-by">{{$users->displayName}}</a>
                                </div>
                                <div class="entry-excerpt max-w-70 max-w-md-90 mx-auto mb--30">
                                    <p>{{$blog->subtitle}}</p>
                                </div>
                            </div>
                            <div class="entry-thumbnail max-w-70 max-w-md-90 mx-auto mb--30">
                                <img src="{{ asset('/upload/users/blogs/'. $blog->image) }}" alt="Post Thumbnail">
                            </div>
                            <div class="entry-content">
                                {!!$blog->article!!}
                            </div>
                            @if ($blog->video != null)
                            <div class="entry-thumbnail max-w-70 max-w-md-90 mx-auto mb--30">
                                <video width="600" controls>
                                    <source src="{{asset('/upload/users/blogs/video/'.$blog->video)}}" type="video/mp4">
                                    Your browser does not support HTML5 video.
                                  </video>
                            </div>
                            @endif

                            <div class="entry-footer-meta">
                                <div class="tag-list">
                                    <span>
                                        <i class="fa fa-tags"></i>
                                    </span>
                                    <span>
                                        <a href="{{ Route('blogs') }}">Hijab</a>,
                                        <a href="{{ Route('blogs') }}">Pants</a>,
                                        <a href="{{ Route('blogs') }}">Daily</a>
                                    </span>
                                </div>
                                <div class="author">
                                    <span>
                                        Author: <a href="{{ Route('blogs') }}">{{$users->displayName}}</a>
                                    </span>
                                </div>
                            </div>
                        </article>
                        <!-- Single Blog End Here -->

                        <!-- Social Sharing Icons Start Here -->
                        <div class="post-share-wrapper">
                            <div class="post-share sticky-social">
                                <ul class="social social-big social-round social-sharing gallery-color vertical">
                                    <li class="social__item">
                                        <a href="facebook.com" class="social__link facebook">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li class="social__item">
                                        <a href="twitter.com" class="social__link twitter">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li class="social__item">
                                        <a href="plus.google.com" class="social__link pinterest">
                                            <i class="fa fa-pinterest-p"></i>
                                        </a>
                                    </li>
                                    <li class="social__item">
                                        <a href="plus.google.com" class="social__link google-plus">
                                            <i class="fa fa-google-plus"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Social Sharing Icons End Here -->

                        <!-- Post Navigation Start Here -->
                        <nav class="post-navigation">
                            <div class="nav-links">
                                @if ($next && $previous)
                                <div class="nav-prev nav-links__inner">
                                    <a href="{{ Route('blogs.show',$previous)}}" class="nav-links__link">Prev
                                        Post</a>
                                    <span href="{{ Route('blogs.show',$previous)}}" class="nav-links__text">Prev
                                        Post</span>
                                    <div class="nav-links__thumb">
                                        <img src="{{ asset('/upload/users/blogs/'. $prevdata->image) }}" alt="Prev Post">
                                    </div>
                                    <div class="nav-links__content">
                                        <h4 class="nav-links__title">{{ $prevdata->title }}</h4>
                                        <span class="nav-links__meta">{{Carbon\Carbon::parse($prevdata->dateCreated)->format('M d,  Y ')}} - by {{$prevusers->displayName}}</span>
                                    </div>
                                </div>
                                <div class="nav-next nav-links__inner">
                                    <a href="{{ Route('blogs.show',$next)}}" class="nav-links__link">Next Post</a>
                                    <span href="{{ Route('blogs.show',$next)}}" class="nav-links__text">Next Post</span>
                                    <div class="nav-links__thumb">
                                        <img src="{{ asset('/upload/users/blogs/'. $nextdata->image) }}" alt="Next Post">
                                    </div>
                                    <div class="nav-links__content">
                                        <h4 class="nav-links__title">{{ $nextdata->title }}</h4>
                                        <span class="nav-links__meta">{{Carbon\Carbon::parse($nextdata->dateCreated)->format('M d,  Y ')}} - by {{$nextusers->displayName}}</span>
                                    </div>
                                </div>
                                @elseif($next)
                                <div class="nav-prev nav-links__inner">
                                    <a href="" class="nav-links__link"></a>
                                    <span href="" class="nav-links__text"></span>
                                    <div class="nav-links__thumb">
                                        <!--<img src="" alt="Prev Post">-->
                                    </div>
                                    <div class="nav-links__content">
                                        <h4 class="nav-links__title"></h4>
                                        <span class="nav-links__meta"></span>
                                    </div>
                                </div>
                                <div class="nav-next nav-links__inner">
                                    <a href="{{ URL::to( 'blogs/show/' . $next ) }}" class="nav-links__link">Next
                                        Post</a>
                                    <span href="{{ URL::to( 'blogs/show/' . $next ) }}" class="nav-links__text">Next
                                        Post</span>
                                    <div class="nav-links__thumb">
                                        <img src="{{ asset('/upload/users/blogs/'. $nextdata->image) }}" alt="Next Post">
                                    </div>
                                    <div class="nav-links__content">
                                        <h4 class="nav-links__title">{{ $nextdata->title }}</h4>
                                        <span class="nav-links__meta">{{Carbon\Carbon::parse($nextdata->dateCreated)->format('M d,  Y ')}} - by {{$nextusers->displayName}}</span>
                                    </div>
                                </div>
                                @elseif($previous)
                                <div class="nav-prev nav-links__inner">
                                    <a href="{{ URL::to( 'blogs/show/' . $previous ) }}" class="nav-links__link">Prev
                                        Post</a>
                                    <span href="{{ URL::to( 'blogs/show/' . $previous ) }}" class="nav-links__text">Prev
                                        Post</span>
                                    <div class="nav-links__thumb">
                                        <img src="{{ asset('/upload/users/blogs/'. $prevdata->image) }}" alt="Prev Post">
                                    </div>
                                    <div class="nav-links__content">
                                        <h4 class="nav-links__title">{{ $prevdata->title }}</h4>
                                        <span class="nav-links__meta">{{Carbon\Carbon::parse($prevdata->dateCreated)->format('M d,  Y ')}} - by {{$prevusers->displayName}}</span>
                                    </div>
                                </div>
                                <div class="nav-next nav-links__inner">
                                    <a href="" class="nav-links__link"></a>
                                    <span href="" class="nav-links__text"></span>
                                    <div class="nav-links__thumb">
                                        <!--<img src="" alt="Next Post">-->
                                    </div>
                                    <div class="nav-links__content">
                                        <h4 class="nav-links__title"></h4>
                                        <span class="nav-links__meta"></span>
                                    </div>
                                </div>
                                @endif

                            </div>
                        </nav>
                        <!-- Post Navigation Start Here -->

                        <!-- Comments Start Here -->
                        {{-- <div class="comments-area">
                            <div class="comment">
                                @include('components.blog.commentlist')
                            </div>
                            @include('components.blog.formcomment')
                        </div> --}}
                        <!-- Comments Start Here -->
                    </div>
                </div>
            </div>
        </div>
        @include('components.blog.relatedpost',$blogs)
    </div>
</div>
<!-- Main Content Wrapper Start -->
@endsection
