@extends('master')
{{-- @section('nav')
@include('components.navigations.navigation')
@endsection --}}
@section('bodyclass',"default-color")
@section('main')

<!-- Breadcrumb area Start -->
<div class="breadcrumb-area breadcrumb-bg pt--40 pb--50">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="page-title">The Blog</h1>
                <ul class="breadcrumb justify-content-center">
                    <li><a href="{{ Route('index') }}">Home</a></li>
                    <li class="current"><span>Blog</span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- Breadcrumb area End -->

<!-- Main Content Wrapper Start -->
<div id="content" class="main-content-wrapper">
    <div class="page-content-inner">
        <div class="container">
            <div class="row pt--80 pt-md--60 pt-sm--50">
                @foreach ($blogs as $item)
                <div class="col-sm-6 col-lg-4 mb--40 mb-md--30 mb-sm--25">
                    <article class="post">
                        <div class="post-media">
                            <figure class="image text-center">
                                <a href="{{ route('blogs.show', $item->blogrowPointer) }}">
                                    <img src="{{ asset('/upload/users/blogs/'. $item->blogImage) }}" alt="Post">
                                    <span class="link-overlay"></span>
                                </a>
                            </figure>
                        </div>
                        <div class="post-info">
                            <div class="post-meta category-link">
                                @if ($item->blogCategory != "")
                                @foreach(explode(',', $item->blogCategory) as $cat)
                                <a href="{{ Route('blogs') }}">{{$cat}},</a>
                                @endforeach
                                @endif
                            </div>
                            <h3 class="post-title"><a
                                    href="{{ route('blogs.show', $item->blogrowPointer) }}">{{$item->blogTitle}}</a>
                            </h3>
                            <div class="post-meta d-flex align-items-center justify-content-center mb--20">
                                <a href="{{ Route('blogs') }}" class="posted-on">
                                    <span
                                        class="entry-date">{{Carbon\Carbon::parse($item->blogDate)->format('M d,  Y ')}}</span>
                                </a>
                                <span class="meta-separator">-</span>
                                <a href="{{ Route('blogs') }}" class="posted-by">by {{$item->userDisplayname}}</a>
                            </div>
                            <a href="{{ route('blogs.show', $item->blogrowPointer) }}" class="read-more">Read More <i
                                    class="fa fa-caret-right"></i></a>
                        </div>
                    </article>
                </div>
                @endforeach
            </div>
            <div class="row pb--80 pb-md--60 pb-sm--50">
                <div class="col-12">
                    <nav class="pagination-wrap">
                        {{$blogs->links('pages.shop.pagin')}}

                        {{-- @if ($blogs->lastPage() > 1)
                        <ul class="pagination">
                            <li><a href="{{ $blogs->url(1) }}"
                                    class="prev page-number {{ ($blogs->currentPage() == 1) ? ' disabled' : '' }}"><i
                                        class="fa fa-long-arrow-left"></i></a></li>



                            @for ($i = 1; $i <= $blogs->lastPage(); $i++)


                                @if (($blogs->currentPage() == $i))
                                <li> <span class="current page-number">{{ $i }}</span></li>
                                @else
                                <li><a href="{{ $blogs->url($i) }}" class="page-number">{{ $i }}</a></li>
                                @endif



                                @endfor




                                @if ($blogs->currentPage() == $blogs->lastPage())
                                <li><a href="#" class=" next page-number"><i class="fa fa-long-arrow-right"></i></a>
                                </li>
                                @else
                                <li><a href="{{ $blogs->url($blogs->currentPage()+1) }}" class=" next page-number"><i
                                            class="fa fa-long-arrow-right"></i></a></li>
                                @endif


                        </ul>
                        @endif --}}
                        {{-- <li><a href="#" class="prev page-number"><i class="fa fa-long-arrow-left"></i></a></li>
                            <li><span class="current page-number">1</span></li>
                            <li><a href="#" class="page-number">2</a></li>
                            <li><a href="#" class="page-number">3</a></li>
                            <li><a href="#" class="next page-number"><i class="fa fa-long-arrow-right"></i></a></li> --}}

                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Main Content Wrapper Start -->

@endsection
