{{-- @extends('errors::minimal')

@section('title', __('Service Unavailable'))
@section('code', '503')
@section('message', __($exception->getMessage() ?: 'Service Unavailable')) --}}
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Artline Indonesia</title>
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{asset('maintenance/images/icons/favicon.ico')}}"/>
	<link rel="apple-touch-icon" sizes="180x180" href="{{asset('maintenance/images/icons/apple-touch-icon.png')}}">
	<link rel="icon" type="image/png" sizes="32x32" href="{{asset('maintenance/images/icons/favicon-32x32.png')}}">
	<link rel="icon" type="image/png" sizes="16x16" href="{{asset('maintenance/images/icons/favicon-16x16.png')}}">
	<link rel="manifest" href="{{asset('maintenance/images/icons/site.webmanifest')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('maintenance/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('maintenance/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('maintenance/fonts/iconic/css/material-design-iconic-font.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('maintenance/vendor/animate/animate.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('maintenance/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('maintenance/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('maintenance/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body>
	
			

	<div class="flex-w flex-str size1 overlay1">
		<div class="flex-w flex-col-sb wsize1 bg0 p-l-65 p-t-37 p-b-50 p-r-80 respon1">
			<div class="wrappic1">
				<a href="#">
					<img src="{{asset('maintenance/images/icons/logo.png')}}" alt="IMG" style="width: 10rem;">
				</a>
			</div>		
	
			<div class="w-full flex-c-m p-t-80 p-b-90">
				<div class="wsize2">
					<h3 class="l1-txt1 p-b-34 respon3">
						Coming Soon
					</h3>

					<p class="m2-txt1 p-b-46">
						We are upgrading our website
					</p>
					<div>
						<p>Contact us for inquiries</p>
						<p>
							<a href="https://api.whatsapp.com/send?phone=+6282111118239" target="_blank">
								<img src="{{asset('maintenance')}}/images/whatsapp_button.png" alt="Whatsapp Link" style="width: 12rem;">
							</a>
						</p>
					</div>
				</div>
			</div>
			
			<div class="flex-w">
				<a href="https://www.facebook.com/ArtlineID/" class="size3 flex-c-m how-social trans-04 m-r-15 m-b-10">
					<i class="fa fa-facebook"></i>
				</a>

				<a href="https://www.instagram.com/artlineid/" class="size3 flex-c-m how-social trans-04 m-r-15 m-b-10">
					<i class="fa fa-instagram"></i>
				</a>

				<a href="https://www.youtube.com/c/ArtlineIndonesia" class="size3 flex-c-m how-social trans-04 m-r-15 m-b-10">
					<i class="fa fa-youtube-play"></i>
				</a>
			</div>
			<div class="flex-w">
				<p>T-shirt Marker, Highlighter, Permanent Marker, White Board Marker, Chalk Marker</p>
				<p>Artline Indonesia © 2020. All Rights Reserved. Subsidiary of <a href="http://www.shachihata.co.jp">Shachihata Japan</a>.</p>
			</div>
		</div>
			

		<div class="wsize1 simpleslide100-parent respon2">
			<!--  -->
			<div class="simpleslide100">
				<div class="simpleslide100-item bg-img1" style="background-image: url('{{asset('maintenance')}}/images/bg01.jpg');"></div>
				<div class="simpleslide100-item bg-img1" style="background-image: url('{{asset('maintenance')}}/images/bg02.jpg');"></div>
				<div class="simpleslide100-item bg-img1" style="background-image: url('{{asset('maintenance')}}/images/bg03.jpg');"></div>
			</div>
		</div>
	</div>



	

<!--===============================================================================================-->	
	<script src="{{asset('maintenance/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('maintenance/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{asset('maintenance/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('maintenance/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('maintenance/vendor/tilt/tilt.jquery.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('maintenance/js/main.js')}}"></script>

</body>
</html>