
<!DOCTYPE html>
<html lang="en">

    @include('components.partials.head')

<body>
               <!-- Main Content Wrapper Start -->
               <div id="content" class="main-content-wrapper">
                <div class="page-content-inner">
                    <div class="container">
                        <div class="row">
                            <div class="error-area ptb--150 ptb-md--100 ptb-sm--80">
                                <div class="row align-items-center">
                                    <div class="col-md-5 text-center text-md-left">
                                        <figure class="error-img">
                                            <img src="{{asset('assets/img/others/home_404.png')}}" alt="404">
                                        </figure>
                                    </div>
                                    <div class="col-md-7 text-center text-md-left">
                                        <div class="error-text">
                                            <h2>@yield('code')</h2>
                                            <h4>@yield('title')</h4>
                                            <p class="max-w-70 max-md-90 mb--40 mb-md--30">@yield('message')</p>
                                            <a href="/" class="btn btn-medium btn-style-1">Back To Home</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('components.partials.foot') 
</body>
</html>










<!--

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title></title>

      
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

  
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .code {
                border-right: 2px solid;
                font-size: 26px;
                padding: 0 15px 0 15px;
                text-align: center;
            }

            .message {
                font-size: 18px;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="code">
                
            </div>

            <div class="message" style="padding: 10px;">
               
            </div>
        </div>
    </body>
</html>
-->