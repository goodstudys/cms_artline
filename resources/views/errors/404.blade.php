@extends('errors::minimal')

@section('title', __('Oops! That page can\'t be found.'))
@section('code', '404')
@section('message', __('Sorry, but the page you are looking for is not found. Please, make sure you have typed the current URL.'))
