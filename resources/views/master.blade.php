<!doctype html>
<html class="no-js" lang="id">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="meta description">
    <!-- Favicons -->
    <link rel="shortcut icon" href="{{asset('assets/img/favicon1.ico')}}" type="image/x-icon">
    <link rel="apple-touch-icon" href="{{asset('assets/img/icon.png')}}">

    <!-- Title -->
    <title>Artline</title>
    @include('components.partials.head')
    @yield('extra_css')
    <style>
        .float {
            position: fixed;
            width: 60px;
            height: 60px;
            bottom: 40px;
            left: 40px;
            background-color: #25d366;
            color: #FFF;
            border-radius: 50px;
            text-align: center;
            font-size: 30px;
            box-shadow: 2px 2px 3px #999;
            z-index: 100;
        }

        .my-float {
            margin-top: 16px;
        }

        .cd-cart__trigger,
        .cd-cart__content {
            position: fixed;
            bottom: 20px;
            right: 5%;
            transition: transform .2s;
        }

        .cd-cart--empty .cd-cart__trigger,
        .cd-cart--empty .cd-cart__content {
            // hide cart
            transform: translateY(150px);
        }

        .cd-cart__layout {
            position: absolute;
            bottom: 0;
            right: 0;
            z-index: 2;
            overflow: hidden;
            height: 72px;
            width: 72px;
            border-radius: var(--radius);
            transition: height .4s .1s, width .4s .1s, box-shadow .3s;
            transition-timing-function: cubic-bezier(.67, .17, .32, .95);
            background: var(--cd-color-3);
            box-shadow: 0 4px 30px rgba(#000, .17);
        }

        .cd-cart--open .cd-cart__layout {
            height: 100%;
            width: 100%;
            transition-delay: 0s;
        }

        .cd-cart__product--deleted {
            // this class is added to an item when it is removed form the cart
            position: absolute;
            left: 0;
            width: 100%;
            opacity: 0;
            animation: cd-item-slide-out .3s forwards;
        }

        @keyframes cd-item-slide-out {
            0% {
                transform: translateX(0);
                opacity: 1;
            }

            100% {
                transform: translateX(80px);
                opacity: 0;
            }
        }
    </style>
    <style>
        .select-custom {
            -webkit-tap-highlight-color: transparent;
            background-color: #fff;
            border-radius: 5px;
            border: 1px solid #e8e8e8;
            box-sizing: border-box;
            clear: both;
            cursor: pointer;
            display: block;
            float: left;
            font-family: inherit;
            font-size: 14px;
            font-weight: 400;
            height: 42px;
            line-height: 40px;
            outline: 0;
            padding-left: 18px;
            padding-right: 30px;
            position: relative;
            text-align: left !important;
            -webkit-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            white-space: nowrap;
            width: auto;
            width: 100%;
            border-color: #cdcdcd;
            -webkit-border-radius: 0;
            -moz-border-radius: 0;
            -ms-border-radius: 0;
            -o-border-radius: 0;
            border-radius: 0;
            color: #181818;
        }

        .select-custom:disabled {
            background-color: #e4e1e1;
            cursor: not-allowed;
        }
    </style>


</head>


<body class=@yield('bodyclass')>

    <!-- Preloader Start -->
    <div class="charoza-preloader active">
        <div class="charoza-preloader-inner h-100 d-flex align-items-center justify-content-center">
            <div class="charoza-child charoza-bounce1"></div>
            <div class="charoza-child charoza-bounce2"></div>
            <div class="charoza-child charoza-bounce3"></div>
        </div>
    </div>
    <!-- Preloader End -->

    <!-- Main Wrapper Start -->
    <div class="wrapper">
        @if (Request::is('about') || Request::is('about/*'))
        @include('components.navigations.navabout')
        @elseif(Request::is('/') || Request::is('/*'))
        @include('components.navigations.navhome')
        @else
        @include('components.navigations.navigation')
        @endif
        {{-- @yield('nav') --}}

        @yield('main')

        @include('components.footer')

        <!-- Search from Start -->
        <div class="searchform__popup" id="searchForm">
            <a href="#" class="btn-close"><i class="dl-icon-close"></i></a>
            <div class="searchform__body">
                <p>Start typing and press Enter to search</p>
                <form action="{{Route('filter.search')}}" method="get" class="searchform">
                    <input type="text" name="q" id="popup-search" class="searchform__input"
                        placeholder="Search Entire Store...">
                    <button type="submit" class="searchform__submit"><i class="dl-icon-search10"></i></button>
                </form>
            </div>
        </div>
        <!-- Search from End -->

        <!-- Side Navigation Start -->
        <aside class="side-navigation" id="sideNav">
            <div class="side-navigation-wrapper">
                <div class="side-navigation-inner">
                    <div class="widget mb--30">
                        <ul class="sidenav-menu">
                            <li><a href="{{ Route('about') }}">About Us</a></li>
                            <li><a href="{{ Route('promotions') }}">Promotions</a></li>
                            <li><a href="{{ Route('contact') }}">Contac Us</a></li>
                            <li><a href="#">Affiliate Program</a></li>
                            <li><a href="#">Work For Artline</a></li>
                        </ul>
                    </div>
                    <div class="widget mb--30">
                        <div class="banner-box banner-type-1 banner-hover-1">
                            <div class="banner-inner">
                                <div class="banner-image">
                                    <img src="{{asset('assets/img/banner/bg01.jpg')}}" alt="Banner">
                                </div>
                                <div class="banner-info">
                                    <a class="banner-title-1 text-uppercase" href="{{ Route('shop') }}">New</a>
                                </div>
                                <a class="banner-link banner-overlay" href="{{ Route('shop') }}">New</a>
                            </div>
                        </div>
                    </div>
                    <div class="widget mb--30">
                        <div class="text-widget">
                            <p>
                                <a href="#">+62.31.745.88899, +62.81.223.889988</a>
                                <a href="mailto:info@charoza.com">support@artline.co.id</a>
                                Pergudangan Manukan, Surabaya, East Java - Indonesia
                            </p>
                        </div>
                    </div>
                    <div class="widget mb--5">
                        <div class="text-widget google-map-link">
                            <p>
                                <a href="https://www.google.com/maps" target="_blank">Google Maps</a>
                            </p>
                        </div>
                    </div>
                    <div class="widget">
                        <div class="text-widget">
                            <!-- Social Icons Start Here -->
                            <ul class="social social-small">
                                <li class="social__item">
                                    <a href="twitter.com" class="social__link">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="social__item">
                                    <a href="plus.google.com" class="social__link">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                                <li class="social__item">
                                    <a href="facebook.com" class="social__link">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="social__item">
                                    <a href="youtube.com" class="social__link">
                                        <i class="fa fa-youtube"></i>
                                    </a>
                                </li>
                                <li class="social__item">
                                    <a href="instagram.com" class="social__link">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </li>
                            </ul>
                            <!-- Social Icons End Here -->
                        </div>
                    </div>
                    <div class="widget mtb--15">
                        <div class="text-widget">
                            <p>
                                <img src="{{asset('assets/img/others/payments.png')}}" alt="payment">
                            </p>
                        </div>
                    </div>
                    <div class="widget">
                        <div class="text-widget">
                            <p class="copyright-text">&copy; 2020 Artline Shop All rights reserved</p>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!-- Side Navigation End -->

        <!-- Mini Cart Start -->
        <aside class="mini-cart" id="miniCart">
            <div class="mini-cart-wrapper">
                <a href="" class="btn-close"><i class="dl-icon-close"></i></a>
                <div class="mini-cart-inner">
                    @if (Cart::getContent()->count() != 0)
                    <h5 class="mini-cart__heading mb--40 mb-lg--30">Shopping Cart</h5>
                    <div class="mini-cart__content">
                        <ul class="mini-cart__list">
                            @foreach($datas as $data)
                            <li class="mini-cart__product">
                                <a href="{{ url('/cart/destroy/'. $data->id) }}" class="remove-from-cart remove">
                                    <i class="dl-icon-close"></i>
                                </a>
                                <div class="mini-cart__product__image">
                                    <img src="{{ asset('/upload/users/products/'. $data->attributes->image) }}"
                                        alt="products">
                                </div>
                                <div class="mini-cart__product__content">
                                    <a class="mini-cart__product__title">{{ $data->name }} ({{ $data->quantity }}x)</a>
                                    <span class="mini-cart__product__quantity">Rp
                                        {{ number_format($data->price) }}</span>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        <div class="mini-cart__total">
                            <span>Subtotal:</span>
                            <span class="ammount">Rp {{ number_format(Cart::getSubTotal()) }}</span>
                        </div>
                        <div class="mini-cart__buttons">
                            <a href="{{Route('cart')}}" class="btn btn-fullwidth btn-style-1">View Cart</a>
                            <a href="{{Route('checkout')}}" class="btn btn-fullwidth btn-style-1">Checkout</a>
                        </div>
                    </div>
                    @else
                    <h3>Empty cart</h3>
                    @endif
                </div>
            </div>
        </aside>
        <!-- Mini Cart End -->

        <!-- Global Overlay Start -->
        <div class="charoza-global-overlay"></div>
        <!-- Global Overlay End -->

        @include('components.productModal')

        @if (Cart::getContent()->count() == 0)

        @else
        @if (Request::is('cart/checkout') || Request::is('cart/checkout/*'))

        @else
        <div class="cd-cart js-cd-cart">
            <a href="#0" class="cd-cart__trigger text-replace">
                <!-- Cart -->
                <ul class="cd-cart__count">
                    <!-- cart items count -->
                    @if (Cart::getContent()->count() == 0)
                    <li>0</li>
                    <li>0</li>
                    @else
                    <li>{{ Cart::getContent()->count() }}</li>
                    <li>0</li>
                    @endif

                </ul> <!-- .cd-cart__count -->
            </a>

            <div id="keranjang" class="cd-cart__content">
                <div class="cd-cart__layout">
                    <header class="cd-cart__header">
                        <h2>Cart</h2>
                        <span class="cd-cart__undo">Item removed. <a href="#0">Undo</a></span>
                    </header>

                    <div class="cd-cart__body">
                        <ul>
                            @if (Cart::getContent()->count() == 0)

                            @else
                            @foreach ($datas as $data)
                            <li class="cd-cart__product">
                                <div class="cd-cart__image">
                                    <a href="#0">
                                        <img src="{{ asset('/upload/users/products/'. $data->attributes->image) }}"
                                            alt="placeholder">
                                    </a>
                                </div>

                                <div class="cd-cart__details">
                                    <h3 class="truncate" style="padding-left: 0px;text-align: left;"><a
                                            href="#0">{{ $data->name }}</a></h3>

                                    <span class="cd-cart__price" style="padding-left: 0px;text-align: left;">Rp.
                                        {{ number_format($data->price) }}</span>

                                    <div class="cd-cart__actions">
                                        <a href="{{ url('/cart/destroy/'. $data->id) }}"
                                            style="padding-right: 12px">Delete</a>

                                        <div class="cd-cart__quantity">
                                            <label for="cd-product-productId">Qty</label>

                                            <span class="cd-cart__select">
                                                {{ $data->quantity }}

                                                <!--<svg class="icon" viewBox="0 0 12 12">
													<polyline fill="none" stroke="currentColor"
														points="2,4 6,8 10,4 " /></svg>  -->
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                            @endif

                            <!-- products added to the cart will be inserted here using JavaScript -->
                        </ul>
                    </div>

                    <footer class="cd-cart__footer">
                        <a href="{{Route('checkout')}}" class="cd-cart__checkout">
                            <em>Checkout - Rp.<span>{{ number_format(Cart::getSubTotal()) }}</span>
                                <svg class="icon icon--sm" viewBox="0 0 24 24">
                                    <g fill="none" stroke="currentColor">
                                        <line stroke-width="2" stroke-linecap="round" stroke-linejoin="round" x1="3"
                                            y1="12" x2="21" y2="12" />
                                        <polyline stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                                            points="15,6 21,12 15,18 " />
                                    </g>
                                </svg>
                            </em>
                        </a>
                    </footer>
                </div>
            </div> <!-- .cd-cart__content -->
        </div> <!-- cd-cart -->
        @endif

        @endif

    </div>
    <!-- Main Wrapper End -->

    @include('components.partials.foot')
    <script>
        $(document).ready(function(){
        if($(".dropdown  a").attr("href")==window.location.href){
            $(".dropdown").attr("class","dropdown active");
        }
       else{
          $(".dropdown").attr("class","dropdown");
         }
    });
		function myFunction(bool) { // toggle cart visibility
			var element = document.getElementById("keranjang");
		
			if( bool ) {
				element.classList.remove("cd-cart--open");
			} else {
				element.classList.add('cd-cart--open')
			}
		};
    </script>
    @yield('extra_script')

</body>

</html>