@extends('admin.app')
@section('main')

<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-12">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Edit Reseller </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Reseller</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    @if (Session::has('status'))
                    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                    @endif

                    @if (count($errors) > 0)

                    <div class="alert alert-danger">

                        <strong>Whoops!</strong> There were some problems with your input.

                        <ul>

                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>

                            @endforeach

                        </ul>

                    </div>

                    @endif
                    <div class="card">
                        <h5 class="card-header">Edit User</h5>
                        <div class="card-body">
                            <form action="{{route('reseller.update',$data->rowPointer)}}" method="POST"
                                enctype="multipart/form-data" class="form">
                                @csrf
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Nama User</label>
                                    <select name="user_id" id="level" class="form-control" required>
                                        <option value="" selected="true" disabled="disabled">Pilih User</option>

                                        @foreach($user as $item)
                                        <option value="{{ $item->id }}"
                                            {{ ($data->user_id == $item->id ) ? ' selected' : '' }}>
                                            {{ $item->firstName }}
                                        </option>
                                        @endforeach
                                        {{-- @foreach ($data as $item)
                                        <option value="{{$item->id}}">{{$item->firstName}}</option>
                                        @endforeach --}}

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Keperluan Belanja </label>
                                    <select name="shopfor" id="level" class="form-control" onchange="checkAlert(event)">
                                        {{-- <option value="" selected="true" disabled="disabled">Pilih Keperluan</option>
                                        <option value="Toko Offline"
                                        {{ (strpos($data->shopfor, "Toko Offline")) ? ' selected' : '' }}>Toko Offline
                                        </option>
                                        <option value="Toko Online"
                                            {{ (strpos($data->shopfor, "Toko Online")) ? ' selected' : '' }}>Toko Online
                                        </option>
                                        <option value="Supplier/Perusahaan pengadaan barang"
                                            {{ (strpos($data->shopfor, "Supplier/Perusahaan pengadaan barang")) ? ' selected' : '' }}>
                                            Supplier/Perusahaan
                                            pengadaan
                                            barang</option>
                                        <option value="Lembaga Pendidikan"
                                            {{ (strpos($data->shopfor, "Lembaga Pendidikan")) ? ' selected' : '' }}>
                                            Lembaga
                                            Pendidikan</option> --}}
                                        <option value="1" {{ ($data->shopfor == '1' ) ? ' selected' : '' }}>
                                            Toko Offline
                                        </option>
                                        <option value="2" {{ ($data->shopfor == '2' ) ? ' selected' : '' }}>
                                            Toko Online
                                        </option>
                                        <option value="3"
                                            {{ ($data->shopfor == '3' ) ? ' selected' : '' }}>
                                            Supplier/Perusahaan
                                            pengadaan
                                            barang</option>
                                        <option value="4"
                                            {{ ($data->shopfor == '4' ) ? ' selected' : '' }}>Lembaga
                                            Pendidikan</option>
                                        <option id="other" value="5"
                                            {{ ($data->shopfor == '1' ||$data->shopfor == '2' ||$data->shopfor == '3' || $data->shopfor == '4') ? '' : 'selected' }}>
                                            Other</option>
                                    </select>
                                </div>
                                <input id="inputother" placeholder="Other" class="form-control" @if ($data->shopfor !=
                                '1' ||$data->shopfor != '2'
                                ||$data->shopfor != '3' || $data->shopfor != '4')
                                type="text" value="{{$data->shopfor}}" required
                                @else
                                type="hidden"
                                @endif
                                name="othertext" onchange="changeradioother()">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="inputText3" class="col-form-label">KTP</label>
                                            <input id="subtitle" name="ktp" value="{{$data->ktp}}" type="text"
                                                class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="inputText3" class="col-form-label">Foto KTP</label>
                                            <input type='file' name="ktp_image" class="form-control"
                                                onchange="readURL(this,'#ktp_image');" />
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Preview Image</label><br>
                                            @if ($data->ktp_image ==null)
                                            <img id="ktp_image" style="width: 20%"
                                                src="{{asset('assets/img/default.png')}}" alt="your image" />
                                            @else
                                            <img id="ktp_image" style="width: 20%"
                                                src="{{asset('/upload/reseller/'.$data->ktp_image)}}"
                                                alt="your image" />
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="inputText3" class="col-form-label">NPWP (optional)</label>
                                            <input id="subtitle" name="npwp" value="{{$data->npwp}}" type="text"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="inputText3" class="col-form-label">Foto NPWP</label>
                                            <input type='file' name="npwp_image" class="form-control"
                                                onchange="readURL(this,'#npwp_image');" />
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Preview Image</label><br>
                                            @if ($data->npwp_image ==null)
                                            <img id="npwp_image" style="width: 20%"
                                                src="{{asset('assets/img/default.png')}}" alt="your image" />
                                            @else
                                            <img id="npwp_image" style="width: 20%"
                                                src="{{asset('/public/upload/reseller/'.$data->npwp_image)}}"
                                                alt="your image" />
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Alamat Usaha</label>
                                    <input id="link" name="work_address" value="{{$data->work_address}}" type="text"
                                        class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Kota Usaha</label>
                                    <input id="email" name="work_city" value="{{$data->work_city}}" type="text"
                                        class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Nomor telepon perusahaan</label>
                                    <input id="link" name="work_phone" value="{{$data->work_phone}}" type="text"
                                        class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Jabatan</label>
                                    <input id="isi" name="position" value="{{$data->position}}" type="text"
                                        class="form-control" required>
                                </div>

                                <div class="form__group">
                                    <input type="submit" value="Save" class="btn btn-style-1 btn-submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('extrascript')
<script>
    function readURL(input,ab) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $(ab)
                        .attr('src', e.target.result)
                        .width(80)
                        .height(80);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>
<script>
    function checkAlert(evt) {
  if (evt.target.value === "5") {
    document.getElementById("inputother").required=true;
    document.getElementById("inputother").style.visibility = 'visible';
  }else{
    if(document.getElementById("inputother").required == true){
        document.getElementById("inputother").required=false;
        document.getElementById("inputother").style.visibility = 'hidden';
    }
  }
}
    function changeradioother(){
          var other= document.getElementById("other");
          other.value=document.getElementById("inputother").value;
          }
    // function setRequired(){
    
    //     document.getElementById("inputother").required=true;
    // }
    
    // function removeRequired(){
    //     alert("I am an alert box!");
    // if(document.getElementById("inputother").required == true){
    //     document.getElementById("inputother").required=false;
    // }
   // }
</script>
@endsection