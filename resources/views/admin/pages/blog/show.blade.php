@extends('admin.app')
@section('main')

<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-10">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Detail Artikel </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Artikel</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Detail Artikel</h5>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">Judul</label>
                                <input id="title" name="title" type="text" class="form-control"
                                    value="{{ $data->title }}" readonly="">
                            </div>
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">Sub Judul</label>
                                <input id="subtitle" name="subtitle" type="text" class="form-control"
                                    value="{{ $data->subtitle }}" readonly="">
                            </div>
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">Kategori</label>
                                <input id="category" name="category" type="text" class="form-control"
                                    value="{{ $data->category }}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">Artikel</label>
                                <textarea id="article" name="article" type="text" class="form-control"
                                    readonly>{{ $data->article }}</textarea>
                            </div>
                            <div class="row container">
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="inputText3" class="col-form-label">Gambar</label><br>
                                        <img src="{{ asset('upload/users/blogs/'. $data->image) }}" alt=""
                                            style="width:200px;height:200px;object-fit:cover;">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    @if ($data->video != null)
                                    <div class="form-group">
                                        <label for="inputText3" class="col-form-label">Video</label><br>
                                        <video width="200px" height="200px" controls>
                                            <source src="{{ asset('upload/users/blogs/video/'. $data->video) }}"
                                                type="video/mp4">
                                            Your browser does not support HTML5 video.
                                        </video>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection