@extends('admin.app')
@section('main')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Artikel</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                            <li class="breadcrumb-item active"><a href="#" class="breadcrumb-link">Artikel</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    @if (Session::has('status'))
    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
    @endif
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Daftar Artikel<a href="{{ route('blog.create') }}"
                        class="float-right btn btn-sm btn-primary">Tambah Artikel</a></h5>
                <div class="card-body">
                    <div class="table-responsive ">
                        <table id="example" class="table table-striped table-bordered awal" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Kategori</th>
                                    <th>Judul</th>
                                    <th>Sub Judul</th>
                                    <th>Gambar</th>
                                    <th>Aktif</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($blogs as $item)
                                <tr>
                                    <th scope="row">{{$item->id}}</th>
                                    <td>{{$item->category}}</td>
                                    <td>{{$item->title}}</td>
                                    <td>{{$item->subtitle}}</td>
                                    <td><img src="{{ asset('/upload/users/blogs/'.$item->image) }}" width="100" alt=""
                                            srcset="">
                                    </td>
                                    <td> @if ($item->isActive != 1)
                                        <a href="{{ route('blog.status', $item->rowPointer) }}" class="edit"
                                            title="Show" data-toggle="Show"><i class="material-icons">&#xe14c;</i></a>
                                        @else
                                        <a href="{{ route('blog.status', $item->rowPointer) }}" class="delete"
                                            title="Hide" data-toggle="Hide"><i class="material-icons">&#xe876;</i></a>
                                        @endif</td>
                                    <td> <a href="{{ route('blog.show', $item->rowPointer) }}" class="view" title="View"
                                            data-toggle="tooltip"><i class="material-icons">&#xE417;</i></a>
                                        <a href="{{ route('blog.edit', $item->rowPointer) }}" class="edit" title="Edit"
                                            data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                                        <a href="#" data-target="#exampleModal{{$item->rowPointer}}" data-toggle="modal"
                                            class="delete" title="Delete" data-toggle="tooltip"><i
                                                class="material-icons">&#xE872;</i></a></td>
                                </tr>
                                @endforeach

                            </tbody>
                            <tfoot>
                                <th>No</th>
                                <th>Kategori</th>
                                <th>Judul</th>
                                <th>Sub Judul</th>
                                <th>Gambar</th>
                                <th>Aktif</th>
                                <th>Aksi</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@foreach ($blogs as $item)
<div class="modal fade" id="exampleModal{{$item->rowPointer}}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus Artikel ({{$item->title}})</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus Artikel {{$item->title}}</p>
            </div>
            <div class="modal-footer">
                <a href="{{Route('blog.delete',$item->rowPointer)}}" class="btn btn-primary">Ya</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection