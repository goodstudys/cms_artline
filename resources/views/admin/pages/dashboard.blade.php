@extends('admin.app')
@section('main')

<div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
        <!-- ============================================================== -->
        <!-- pageheader  -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Dashboard <a
                    href="{{Route('dashboard.print')}}"
                        class="float-right btn btn-sm btn-primary">Print Dashboard</a></h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader  -->
        <!-- ============================================================== -->
        <div class="ecommerce-widget">
            <div class="row">
                <!-- ============================================================== -->
                <!-- sales  -->
                <!-- ============================================================== -->
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                    <div class="card border-3 border-top border-top-primary">
                        <div class="card-body">
                            <h5 class="text-muted">Total orderan baru</h5>
                            <div class="metric-value d-inline-block">
                                <h1 class="mb-1">{{ $news->count() }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                    <div class="card border-3 border-top border-top-primary">
                        <div class="card-body">
                            <h5 class="text-muted">Total order cancel</h5>
                            <div class="metric-value d-inline-block">
                                <h1 class="mb-1">{{ $cancels->count() }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                    <div class="card border-3 border-top border-top-primary">
                        <div class="card-body">
                            <h5 class="text-muted">Total yang harus dikirim</h5>
                            <div class="metric-value d-inline-block">
                                <h1 class="mb-1">{{ $sends->count() }}</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                    <!-- ============================================================== -->
                    <!-- sales traffice source  -->
                    <!-- ============================================================== -->
                    <div class="card">
                        <h5 class="card-header"> List orderan baru</h5>
                        <div class="card-body p-0">
                            <ul class="traffic-sales list-group list-group-flush">
                                @foreach($news as $new)
                                <li class="traffic-sales-content list-group-item "><span
                                        class="traffic-sales-name">{{ $new->billName }}</span><span
                                        class="traffic-sales-amount">{{ $new->quantity }} Barang <span
                                            class=" ml-4 bg-success-light"></span><span class="ml-1 text-success">Rp.
                                            {{ number_format($new->total) }}</span></span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                    <!-- ============================================================== -->
                    <!-- sales traffice source  -->
                    <!-- ============================================================== -->
                    <div class="card">
                        <h5 class="card-header"> List order cancel</h5>
                        <div class="card-body p-0">
                            <ul class="traffic-sales list-group list-group-flush">
                                @foreach($cancels as $cancel)
                                <li class="traffic-sales-content list-group-item "><span
                                        class="traffic-sales-name">{{ $cancel->billName }}</span><span
                                        class="traffic-sales-amount">{{ $cancel->quantity }} Barang <span
                                            class=" ml-4 bg-success-light"></span><span class="ml-1 text-danger">Rp.
                                            {{ number_format($cancel->total) }}</span></span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end sales traffice source  -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- sales traffic country source  -->
                <!-- ============================================================== -->
                <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                    <!-- ============================================================== -->
                    <!-- sales traffice source  -->
                    <!-- ============================================================== -->
                    <div class="card">
                        <h5 class="card-header"> List yang harus dikirim</h5>
                        <div class="card-body p-0">
                            <ul class="traffic-sales list-group list-group-flush">
                                @foreach($sends as $send)
                                <li class="traffic-sales-content list-group-item "><span
                                        class="traffic-sales-name">{{ $send->billName }}</span><span
                                        class="traffic-sales-amount">{{ $send->quantity }} Barang <span
                                            class=" ml-4 bg-success-light"></span><span class="ml-1 text-warning">Rp.
                                            {{ number_format($send->total) }}</span></span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end sales traffice country source  -->
                <!-- ============================================================== -->
            </div>

            {{--            <div class="row">--}}
            {{--                <!-- ============================================================== -->--}}

            {{--                <!-- ============================================================== -->--}}

            {{--                              <!-- recent orders  -->--}}
            {{--                <!-- ============================================================== -->--}}
            {{--                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">--}}
            {{--                    <div class="card">--}}
            {{--                        <h5 class="card-header">Recent Orders</h5>--}}
            {{--                        <div class="card-body p-0">--}}
            {{--                            <div class="table-responsive">--}}
            {{--                                <table class="table">--}}
            {{--                                    <thead class="bg-light">--}}
            {{--                                        <tr class="border-0">--}}
            {{--                                            <th class="border-0">#</th>--}}
            {{--                                            <th class="border-0">Image</th>--}}
            {{--                                            <th class="border-0">Product Name</th>--}}
            {{--                                            <th class="border-0">Product Id</th>--}}
            {{--                                            <th class="border-0">Quantity</th>--}}
            {{--                                            <th class="border-0">Price</th>--}}
            {{--                                            <th class="border-0">Order Time</th>--}}
            {{--                                            <th class="border-0">Customer</th>--}}
            {{--                                            <th class="border-0">Status</th>--}}
            {{--                                        </tr>--}}
            {{--                                    </thead>--}}
            {{--                                    <tbody>--}}
            {{--                                        <tr>--}}
            {{--                                            <td>1</td>--}}
            {{--                                            <td>--}}
            {{--                                                <div class="m-r-10"><img src="{{asset('assets/img/products/beauty_6-500x625.jpg')}}"
            alt="user" class="rounded" width="45">
        </div>--}}
        {{--                                            </td>--}}
        {{--                                            <td>Product #1 </td>--}}
        {{--                                            <td>id000001 </td>--}}
        {{--                                            <td>20</td>--}}
        {{--                                            <td>$80.00</td>--}}
        {{--                                            <td>27-08-2018 01:22:12</td>--}}
        {{--                                            <td>Patricia J. King </td>--}}
        {{--                                            <td><span class="badge-dot badge-brand mr-1"></span>InTransit </td>--}}
        {{--                                        </tr>--}}
        {{--                                        <tr>--}}
        {{--                                            <td>2</td>--}}
        {{--                                            <td>--}}
        {{--                                                <div class="m-r-10"><img src="{{asset('assets/images/product-pic-2.jpg')}}"
        alt="user" class="rounded" width="45">
    </div>--}}
    {{--                                            </td>--}}
    {{--                                            <td>Product #2 </td>--}}
    {{--                                            <td>id000002 </td>--}}
    {{--                                            <td>12</td>--}}
    {{--                                            <td>$180.00</td>--}}
    {{--                                            <td>25-08-2018 21:12:56</td>--}}
    {{--                                            <td>Rachel J. Wicker </td>--}}
    {{--                                            <td><span class="badge-dot badge-success mr-1"></span>Delivered </td>--}}
    {{--                                        </tr>--}}
    {{--                                        <tr>--}}
    {{--                                            <td>3</td>--}}
    {{--                                            <td>--}}
    {{--                                                <div class="m-r-10"><img src="{{asset('assets/images/product-pic-3.jpg')}}"
    alt="user" class="rounded" width="45">
</div>--}}
{{--                                            </td>--}}
{{--                                            <td>Product #3 </td>--}}
{{--                                            <td>id000003 </td>--}}
{{--                                            <td>23</td>--}}
{{--                                            <td>$820.00</td>--}}
{{--                                            <td>24-08-2018 14:12:77</td>--}}
{{--                                            <td>Michael K. Ledford </td>--}}
{{--                                            <td><span class="badge-dot badge-success mr-1"></span>Delivered </td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            <td>4</td>--}}
{{--                                            <td>--}}
{{--                                                <div class="m-r-10"><img src="{{asset('assets/images/product-pic-4.jpg')}}"
alt="user" class="rounded" width="45"></div>--}}
{{--                                            </td>--}}
{{--                                            <td>Product #4 </td>--}}
{{--                                            <td>id000004 </td>--}}
{{--                                            <td>34</td>--}}
{{--                                            <td>$340.00</td>--}}
{{--                                            <td>23-08-2018 09:12:35</td>--}}
{{--                                            <td>Michael K. Ledford </td>--}}
{{--                                            <td><span class="badge-dot badge-success mr-1"></span>Delivered </td>--}}
{{--                                        </tr>--}}
{{--                                        <tr>--}}
{{--                                            <td colspan="9"><a href="#" class="btn btn-outline-light float-right">View Details</a></td>--}}
{{--                                        </tr>--}}
{{--                                    </tbody>--}}
{{--                                </table>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- ============================================================== -->--}}
{{--                <!-- end recent orders  -->--}}

{{--            </div>--}}

</div>
</div>
</div>

@endsection