@extends('admin.app')
@section('main')

    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-10">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Tambah Faq </h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Faq</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Tambah</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        @if (Session::has('status'))
                            <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                        @endif

                        @if (count($errors) > 0)

                            <div class="alert alert-danger">

                                <strong>Whoops!</strong> There were some problems with your input.

                                <ul>

                                    @foreach ($errors->all() as $error)

                                        <li>{{ $error }}</li>

                                    @endforeach

                                </ul>

                            </div>

                        @endif
                        <div class="card">
                            <h5 class="card-header">Tambah Faq</h5>
                            <div class="card-body">
                                <form action="{{ route('faq.store')  }}" method="POST" enctype="multipart/form-data" class="form">
                                    @csrf
                                    <div class="form-group">
                                        <label for="inputText3" class="col-form-label">Type</label>
                                        <select name="type" id="type" class="form-control" required>
                                            <option value="1">General Question</option>
                                            <option value="2">payment</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputText3" class="col-form-label">Title</label>
                                        <input id="title" name="title" type="text" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputText3" class="col-form-label">Description</label>
                                        <textarea name="description" id="description" class="form-control" cols="30" rows="10" required></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">
    
                                        </div>
                                        <div class="col-sm-6 pl-0">
                                            <div class="text-right">
                                                <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                                <a class="btn btn-space btn-secondary" style="color: white"
                                                onclick="window.location.href='{{ route('admin.faq') }}'">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
