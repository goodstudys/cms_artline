@extends('admin.app')
@section('main')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Faq</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                            <li class="breadcrumb-item active"><a href="#" class="breadcrumb-link">Faq</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->

    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            @if (Session::has('status'))
            <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
            @endif
            <div class="card">
                <h5 class="card-header">Daftar Faq<a href="{{ route('faq.create') }}"
                        class="float-right btn btn-sm btn-primary">Tambah Faq</a></h5>
                <div class="card-body">
                    <div class="table-responsive ">
                        <table id="example" class="table table-striped table-bordered awal" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Type</th>
                                    <th>title</th>
                                    <th>description</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $index=> $data)
                                <tr>
                                    <th scope="row">{{ $index+1 }}</th>
                                    <td>@if ($data->type == '1')
                                        General Question
                                        @else
                                        Payment
                                        @endif</td>
                                    <td>{{ $data->title }}</td>
                                    <td>{{ $data->description }}</td>
                                    <td>
                                        <!--<a href="{{ route('faq.show', $data->id) }}" class="view" title="View"-->
                                        <!--    data-toggle="tooltip"><i class="material-icons">&#xE417;</i></a>-->
                                        @if ($data->isActive != 1)
                                        <a href="{{ route('faq.edit', $data->id) }}" class="edit" title="Edit"
                                            data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                                        @else
                                        {{-- <a href="{{ route('testimonial.status', $data->rowPointer) }}" class="delete"
                                            title="Hide" data-toggle="Hide"><i class="material-icons">&#xe14c;</i></a> --}}
                                        @endif
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <th>No</th>
                                <th>Type</th>
                                <th>title</th>
                                <th>description</th>
                                <th>Actions</th>
                            </tfoot>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection