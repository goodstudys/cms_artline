<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Oswald:600&display=swap" rel="stylesheet">
</head>
<style>
    section {
        display: -webkit-flex;
        display: flex;
    }

    * {
        box-sizing: border-box;
    }

    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    article {
        -webkit-flex: 3;
        -ms-flex: 3;
        flex: 3;
        background-color: #fff;
        padding: 10px;
    }

    nav {
        -webkit-flex: 2;
        -ms-flex: 2;
        flex: 2;
        background: #fff;
        padding: 20px;
    }

    footer {

        padding: 10px;

        color: black;
    }

    .kanan {
        text-align: right;
    }

    .box {
        border: 1px solid black;
    }

    * {
        color-adjust: exact;
        -webkit-print-color-adjust: exact;
        print-color-adjust: exact;
    }
</style>
<style type="text/css" media="print">
    @page {
        size: landscape;
    }

    * {
        color-adjust: exact;
        -webkit-print-color-adjust: exact;
        print-color-adjust: exact;
    }
</style>

<body>

    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <table style="border: 1px solid black" width="100%" style="padding: 10px 10px 10px 10px;">
                <tr>
                    <td>
                        @if($order->deliveryName != null)
                        <img src="" width="140px" alt="" srcset="" style="padding-left: 10px;">
                    @else
                        <img src="{{asset('assets/img/logo/logo.png')}}" width="140px" alt="" srcset="" style="padding-left: 10px;">
                    @endif
                    </td>
                    <td colspan="2" style="padding-top: 10px;">
                        @if ($order->bookingCode != null)
                        {!!DNS1D::getBarcodeHTML($order->bookingCode, "C128",1.4,50)!!}
                        <span style="margin-left: 41px;">{{ $order->bookingCode }}</span>
                        @endif
                    </td>
                    <td>
                        @if ($order->kurirName == 'jne' || $order->kurirName == 'JNE' )
                        <img src="https://www.jne.co.id/frontend/images/material/logo.jpg" width="110px" alt=""
                            srcset="">
                        @elseif($order->kurirName == 'jnt' || $order->kurirName == 'JNT' || $order->kurirName == 'j&t')
                        <img src="{{asset('assets/img/ekspedisi/jnt.png')}}" width="110px" alt="" srcset="">
                        @elseif($order->kurirName == 'pos' || $order->kurirName == 'POS' || $order->kurirName == 'pos
                        indonesia')
                        <img src="https://www.posindonesia.co.id/photos/7/Logo%20Pos%20Indonesia%20hires.jpg"
                            width="110px" alt="" srcset="">
                        @elseif($order->kurirName == 'tiki' || $order->kurirName == 'TIKI')
                        <img src="https://tiki.id/images/logo/nav.png" width="110px" alt="" srcset="">
                        @else
                        <img src="https://www.jne.co.id/frontend/images/material/logo.jpg" width="110px" alt=""
                            srcset="">
                        @endif

                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 10px;">Kota Asal:</td>
                    <td><b>Kota Surabaya</b></td>
                    <td>Kota Tujuan:</td>
                    <td><b>{{ $order->billCity }}</b></td>
                </tr>
                <tr>
                    <td style="padding-left: 10px;">Deskripsi:</td>
                    <td><b>Artline Logistic</b></td>
                    <td>Jasa Kirim:</td>
                    <td><b>{{ $order->kurirName }}</b></td>
                </tr>
                <tr>
                    <td style="padding-left: 10px;">No. Pesenan:</td>
                    <td><b>{{$order->orderCode}}</b></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="4" style="padding-left: 10px; padding-right: 10px;">
                        <hr style="border: 1px solid black; ">
                    </td>
                </tr>
                <td colspan="2" style="padding-left: 10px;">
                    <div style="background-color: black; width: 250px;">
                        <span
                            style="font-size: 20px; color: white; font-family: 'Oswald', sans-serif; padding-left: 5px;">PENGIRIM</span>
                    </div>
                </td>
                <td colspan="2">
                    <div style="background-color: black; width: 210px;">
                        <span
                            style="font-size: 20px; color: white; font-family: 'Oswald', sans-serif; padding-left: 5px;">PENERIMA</span>
                    </div>
                </td>
                <tr>
                    <td style="padding-left: 10px;">Nama:</td>
                    <td><b>Artline Store</b></td>
                    <td>Nama:</td>
                    <td><b>{{ $order->billName }}</b></td>
                </tr>
                <tr>
                    <td style="padding-left: 10px;">Alamat:</td>
                    <td><b>Kota Surabaya</b></td>
                    <td>Alamat:</td>
                    <td><b>{{ $order->billAddress }}</b></td>
                </tr>
                <tr>
                    <td style="padding-left: 10px;">Kota:</td>
                    <td><b>Surabaya</b></td>
                    <td>Kota:</td>
                    <td><b>{{ $order->billCity }}</b></td>
                </tr>
                <tr>
                    <td style="padding-left: 10px;">Telp:</td>
                    <td><b>0895633180045</b></td>
                    <td>Telp:</td>
                    <td><b>{{ $order->billPhone }}</b></td>
                </tr>
                <tr>
                    <td colspan="4" style="padding-left: 10px; padding-right: 10px;">
                        <hr style="border: 1px solid black; ">
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <span class="float-right" style="padding-right: 10px;"><b>Estimasi Ongkos Kirim: Rp
                                {{ number_format($order->kurirRate) }}</b></span>
                    </td>
                </tr>
                {{--            <tr>--}}
                {{--                <td colspan="4" style="padding-left: 10px; padding-right: 10px; padding-bottom: 20px;">--}}
                {{--                    <div class="box" style="padding-left: 10px; padding-right: 10px;">--}}
                {{--                        <span style="font-family: 'Oswald', sans-serif; font-size: 11px; padding-top: 10px; font-weight: 300;">DATA INI MASIH BELUM TERVERIFIKASI KE SYSTEM, BILA TERDAPAT KESALAHAN PADA PENULISAN ALAMAT NAMA PENGIRIM DAN PENERIMA BISA DIRUBAH DI CASH COUNTER TERDEKAT DENGAN MENYERTAKA NO TIKE INI PADA PETUGAS, TERIMAKASIH</span>--}}
                {{--                    </div>--}}
                {{--                </td>--}}
                {{--            </tr>--}}
            </table>
            <hr style="border-top: 2px dashed black;">
            <div class="row">
                <div class="col-5">
                    <table border="0" width='100%'>
                        <tr>
                            <td><b>Daftar Produk</b></td>
                        </tr>
                    </table>
                </div>
                <div class="col-5">
                    <table border="0" width='100%'>
                        <tr>
                            <td width="80%">No. Pesanan:</td>
                            <td>{{$order->orderCode}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <table border="0" width='100%'>
                <tr style="border-bottom: 1px solid black">
                    <td><b>#</b></td>
                    <td><b>Nama Produk</b></td>
                    <td><b>SKU</b></td>
                    <td><b>Qty</b></td>
                </tr>
                @foreach ($detail as $index => $item)
                <tr style="border-bottom: 1px solid black">
                    <td>{{$index+1}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->sku}}</td>
                    <td>{{$item->quantity}}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>
{{--<script>--}}
{{--    window.print();--}}
{{--</script>--}}

</html>