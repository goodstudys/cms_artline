@extends('admin.app')
@section('main')

<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-12">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Detail Order </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Order</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->

            <div class="row">
                <div class="col-12 p-3">
                    <button type="button" class="btn btn-primary" data-toggle="modal"
                        data-target=".bd-example-modal-xl">Open The Product Ordered</button>

                </div>
                <!-- ============================================================== -->
                <!-- basic form -->
                <!-- ============================================================== -->
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Billing Order</h5>
                        <div class="card-body">
                            <form action="#" id="basicform" data-parsley-validate="">
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Name</label>
                                    <input id="firstName" name="firstName" type="text" class="form-control"
                                        value="{{ $order->billName }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Company</label>
                                    <input id="company" name="company" type="text" class="form-control"
                                        value="{{ $order->billCompany }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Province</label>
                                    <select name="" id="" class="form-control" disabled>
                                        @if ($provinces != 'Connection problem')
                                            <option value="">Pilih provinsi…</option>
                                            @foreach($provinces['rajaongkir']['results'] as $province)
                                                <option value="{{ $province['province_id'] }}" name="province" id="province" {{ ( $order->billProvince == $province['province_id'] ) ? ' selected' : '' }}>{{ $province['province'] }} </option>
                                            @endforeach
                                        @else
                                            <option value="">Internet connection problem</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">City</label>
                                    <input id="sku" name="kecamatan" type="text" class="form-control"
                                           value="{{ $order->billCity }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <textarea class="form-control" name="address" id="address" rows="3"
                                        readonly="">{{ $order->billAddress }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Kecamatan</label>
                                    <input id="sku" name="kecamatan" type="text" class="form-control"
                                        value="{{ $order->billKecamatan }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Kelurahan</label>
                                    <input id="sku" name="kelurahan" type="text" class="form-control"
                                        value="{{ $order->billKelurahan }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Phone</label>
                                    <input id="sku" name="phone" type="text" class="form-control"
                                        value="{{ $order->billPhone }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">KodePos</label>
                                    <input id="sku" name="kodepos" type="text" class="form-control"
                                        value="{{ $order->billKodePos }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Email</label>
                                    <input id="sku" name="email" type="text" class="form-control"
                                        value="{{ $order->billEmail }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="notes">Notes</label>
                                    <textarea class="form-control" name="notes" id="notes" rows="3"
                                        readonly="">{{ $order->billNotes }}</textarea>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end basic form -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- horizontal form -->
                <!-- ============================================================== -->
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Detail Dropshipper</h5>
                        <div class="card-body">
                            <form action="#" id="basicform" data-parsley-validate="">
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Kode Booking</label>
                                    <input id="firstName" name="firstName" type="text" class="form-control"
                                        value="{{ $order->bookingCode }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Nama</label>
                                    <input id="company" name="company" type="text" class="form-control"
                                        value="{{ $order->deliveryName }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Phone</label>
                                    <input id="province" name="province" type="text" class="form-control"
                                        value="{{ $order->deliveryPhone }}" readonly="">
                                </div>
                                {{-- <div class="form-group">
                                    <label for="inputText3" class="col-form-label">City</label>
                                    <input id="city" name="city" type="text" class="form-control"
                                        value="{{ $order->deliveryCity }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <textarea class="form-control" name="address" id="address" rows="3"
                                        readonly="">{{ $order->deliveryAddress }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Kecamatan</label>
                                    <input id="sku" name="kecamatan" type="text" class="form-control"
                                        value="{{ $order->deliveryKecamatan }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Kelurahan</label>
                                    <input id="sku" name="kelurahan" type="text" class="form-control"
                                        value="{{ $order->deliveryKelurahan }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Phone</label>
                                    <input id="sku" name="phone" type="text" class="form-control"
                                        value="{{ $order->deliveryPhone }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">KodePos</label>
                                    <input id="sku" name="kodepos" type="text" class="form-control"
                                        value="{{ $order->deliveryKodePos }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Email</label>
                                    <input id="sku" name="email" type="text" class="form-control"
                                        value="{{ $order->deliveryEmail }}" readonly="">
                                </div> --}}
                                <div class="form-group">
                                    <label for="notes">Notes</label>
                                    <textarea class="form-control" name="notes" id="notes" rows="3"
                                        readonly="">{{ $order->deliveryNotes }}</textarea>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end horizontal form -->
                <!-- ============================================================== -->
            </div>
        </div>
    </div>
    <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12 p-3">
                    <div class="card">
                        <h5 class="card-header">Product Orders</h5>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="bg-light">
                                        <tr class="border-0">
                                            <th class="border-0">#</th>
                                            <th class="border-0">Image</th>
                                            <th class="border-0">Product Name</th>
                                            <th class="border-0">Color</th>
                                            <th class="border-0">Quantity</th>
                                            <th class="border-0">Weight</th>
                                            <th class="border-0">Price</th>
{{--                                            <th class="border-0">Disc Amount</th>--}}
{{--                                            <th class="border-0">Disc Percent</th>--}}
{{--                                            <th class="border-0">Final Price</th>--}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <div style="display:none;">{{$total = 0 }}</div>
                                        @foreach ($detail as $index => $item)
                                        <tr>
                                            <td>{{$index+1}}</td>
                                            <td>
                                                <div class="m-r-10"><img src="{{ asset('/upload/users/products/'.$item->image) }}"
                                                        alt="user" class="rounded" width="45"></div>
                                            </td>
                                            <td>{{$item->name}} </td>
                                            <td>
                                                <div class="product-color-swatch variation-wrapper">
                                                    <div class="swatch-wrapper">
                                                        <a class="product-color-swatch-btn variation-btn"
                                                            data-toggle="tooltip" data-placement="top"
                                                            title="{{$item->colorName}}">
                                                            <span class="product-color-swatch-label">{{$item->colorName}}</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{$item->quantity}}</td>
                                            <td>{{$item->weight}}</td>
                                            <td style="display:none;">{{ $perweight = $item->weight * $item->quantity }}
                                            </td>
                                            <td style="display:none;">{{ $allweight = $total += $perweight  }}</td>
                                            <td>Rp.{{number_format($item->price, 2)}}</td>
{{--                                            <td>Rp.{{number_format($item->discAmount, 2)}} </td>--}}
{{--                                            <td>{{$item->discPercent}} % </td>--}}
{{--                                            <td> @if ($item->discAmount === '0' && $item->discPercent === '0')--}}
{{--                                                Rp.{{number_format($item->price * $item->quantity, 2)}}--}}
{{--                                                @elseif($item->discAmount === '0')--}}
{{--                                                Rp.{{number_format(($item->price - ($item->price*$item->discPercent/100))*$item->quantity, 2)}}--}}
{{--                                                @elseif($item->discPercent === '0')--}}
{{--                                                Rp.{{number_format(($item->price-$item->discAmount)*$item->quantity, 2)}}--}}
{{--                                                @endif</td>--}}
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Payment Information</h5>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="bg-light">
                                        <tr class="border-0">
                                            <th class="border-0">Shipping</th>
                                            <th class="border-0">Total Weight</th>
                                            <th class="border-0">Shipping Subtotal</th>
                                            <th class="border-0">Merchandise Subtotal</th>
                                            <th class="border-0">Promo code</th>
                                            <th class="border-0">Order Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{$order->kurirName}} </td>
                                            <td>{{ $allweight }}</td>
                                            <td>Rp.{{number_format($order->total-$item->subtotal, 2)}}
                                            </td>
                                            <td>Rp.{{number_format($item->subtotal, 2)}}</td>
                                            @if ($item->promocode == null)
                                                <td>-</td>
                                            @else
                                                <td>{{ $item->promocode }}</td>
                                            @endif
                                            <td>Rp.{{number_format($item->total, 2)}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Payment Confirmation</h5>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class="bg-light">
                                        <tr class="border-0">

                                            <th class="border-0">Name as in Bank Account</th>
                                            <th class="border-0">Transfer From Bank</th>
                                            <th class="border-0">Transfer From Bank Account No.</th>
                                            <th class="border-0">Transfer To</th>
                                            <th class="border-0">Amount Transferred</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            @foreach ($confirm as $item)
                                            <td>{{$item->nama}} </td>
                                            <td>{{ $item->bank }}</td>
                                            <td>{{$item->akun}}
                                            </td>
                                            <td>{{$item->tujuan}}</td>
                                            <td>Rp.{{number_format($item->jumlah, 2)}}</td>
                                            @endforeach

                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Large modal -->


@endsection
