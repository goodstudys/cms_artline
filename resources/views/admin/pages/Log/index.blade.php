@extends('admin.app')
@section('main')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Log Activity</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                            <li class="breadcrumb-item active"><a href="#" class="breadcrumb-link">Log Activity</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    @if (Session::has('status'))
    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
    @endif
    <div class="row">

        <!-- ============================================================== -->
        <!-- data table multiselects  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Log Activity</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered awal" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>user</th>
                                    <th>table</th>
                                    <th>konteks</th>
                                    <th>type</th>
                                    <th>information</th>
                                    <th>created at</th>
                                    {{-- <th>Aksi</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $index => $item)
                                <tr>
                                    <td>{{$index+1}}</td>
                                    <td>{{$item->displayName}} </td>
                                    <td>{{$item->table}}</td>
                                    <td>{{$item->konteks}} </td>
                                    <td>{{$item->type}}</td>
                                    <td>{{$item->information}}</td>
                                    {{-- <td>{{date('d-m-Y', strtotime($item->created_at))}}</td> --}}
                                <td>{{date('d-m-Y H:i:s', strtotime($item->created_at))}}</td>
                                    {{-- <td>
                                        {{-- <a href="{{ route('coupon.edit', $item->rowPointer) }}" class="edit"
                                            title="Edit" data-toggle="tooltip"><i
                                                class="material-icons">&#xE254;</i></a>
                                        {{-- {{ route('coupon.destroy', $item->rowPointer) }} --}}
                                        {{-- <a href="#" data-target="#exampleModal{{$item->rowPointer}}" data-toggle="modal"
                                            class="delete" title="Delete" data-toggle="tooltip"><i
                                                class="material-icons">&#xE872;</i></a> --}} 
                                    {{-- </td> --}}
                                </tr>
                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>user</th>
                                    <th>table</th>
                                    <th>konteks</th>
                                    <th>type</th>
                                    <th>information</th>
                                    <th>created at</th>
                                    {{-- <th>Aksi</th> --}}
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end data table multiselects  -->
        <!-- ============================================================== -->
    </div>
</div>
{{--
@foreach ($data as $item)
<div class="modal fade" id="exampleModal{{$item->rowPointer}}" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hapus Coupon ({{$item->kode}})</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus kupon {{$item->kode}}</p>
            </div>
            <div class="modal-footer">
                <a href="{{Route('coupon.delete',$item->rowPointer)}}" class="btn btn-primary">Ya</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
            </div>
        </div>
    </div>
</div>
@endforeach --}}
@endsection