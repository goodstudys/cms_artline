<!DOCTYPE html>
<html>

<head>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>

<body>

    <h2>List orderan baru</h2>

    <table>
        <tr>
            <th>Nama Pembeli</th>
            <th>Jumlah Produk</th>
            <th>Total</th>
        </tr>
        @foreach ($news as $item)
        <tr>
            <td>{{$item->billName}}</td>
            <td>{{$item->quantity}}</td>
            <td>{{$item->total}}</td>
        </tr>

        @endforeach

    </table>
    <h2>List orderan Cancel</h2>

    <table>
        <tr>
            <th>Nama Pembeli</th>
            <th>Jumlah Produk</th>
            <th>Total</th>
        </tr>
        @foreach ($cancels as $item)
        <tr>
            <td>{{$item->billName}}</td>
            <td>{{$item->quantity}}</td>
            <td>{{$item->total}}</td>
        </tr>
        @endforeach


    </table>
    <h2>List orderan harus kirim</h2>

    <table>
        <tr>
            <th>Nama Pembeli</th>
            <th>Provinsi</th>
            <th>Kota</th>
            <th>Alamat</th>
            <th>Jumlah Produk</th>
            <th>Total</th>
        </tr>
        @foreach ($sends as $item)
        <tr>
            <td>{{$item->billName}}</td>
            <td> <select style="border:0px; outline:0px;" name="" id="" class="form-control" disabled>
                    @if ($provinces != 'Connection problem')
                    <option style="border:0px; outline:0px;" value="">Pilih provinsi…</option>
                    @foreach($provinces['rajaongkir']['results'] as $province)
                    <option style="border:0px; outline:0px;" value="{{ $province['province_id'] }}" name="province" id="province"
                        {{ ( $item->billProvince == $province['province_id'] ) ? ' selected' : '' }}>
                        {{   $province['province'] }} </option>
                    @endforeach
                    @else
                    <option value="">Internet connection problem</option>
                    @endif

            </td>
            <td> <select style="border:0px; outline:0px;" name="" id="" class="form-control" disabled>
                    @if ($citys != 'Connection problem')
                    <option style="border:0px; outline:0px;" value="">Select a City…</option>
                    @foreach($citys['rajaongkir']['results'] as $city)
                    <option style="border:0px; outline:0px;" value="{{ $city['city_id'] }}"
                        {{ ( $item->billCity == $city['city_id'] ) ? ' selected' : '' }}>{{ $city['city_name'] }}
                    </option>
                    @endforeach
                    @else
                    <option value="">Internet connection problem</option>
                    @endif
                </select></td>
            <td>{{$item->billAddress}}</td>
            <td>{{$item->quantity}}</td>
            <td>{{$item->total}}</td>
        </tr>
        @endforeach


    </table>

</body>
<script>
    window.print();
</script>

</html>