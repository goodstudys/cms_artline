@extends('admin.app')
@section('main')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Hak Akses</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                            <li class="breadcrumb-item active"><a href="#" class="breadcrumb-link">Hak Akses</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    @if (Session::has('status'))
    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
    @endif
    <div class="row">

        <!-- ============================================================== -->
        <!-- data table multiselects  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <div class="card-header">
                    <h5 class="mb-0">Setting hak akses</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered awal" style="width:100%">
                            <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama depan</th>
                                <th>Nama belakang</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $index => $item)
                                <tr>
                                    <td>{{$index+1}}</td>
                                    <td>{{$item->firstName}} </td>
                                    <td>{{$item->lastName}}</td>
                                    @if ($item->level == 1)
                                        <td>Manager</td>
                                    @elseif ($item->level == 2)
                                        <td>Admin</td>
                                    @elseif ($item->level == 3)
                                        <td>User</td>
                                    @elseif ($item->level == 4)
                                        <td>Reseller</td>
                                    @endif
                                    <td>
                                        @if (Auth::user()->id != $item->id)
                                            <a href="#" style="color:#71748d"  data-toggle="modal" class="view" data-backdrop="static"
                                               data-target="#exampleModalCenter{{$item->id}}" title="View"
                                               data-toggle="tooltip">
                                                <button class="btn btn-primary">Setting</button></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nama depan</th>
                                <th>Nama belakang</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end data table multiselects  -->
        <!-- ============================================================== -->
    </div>

</div>
<div class="modal fade bd-example-modal-xl-create" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12 mt-3">

            </div>
        </div>
    </div>
</div>
@foreach ($data as $item)
    <div class="modal fade" id="exampleModalCenter{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Set hak akses</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('hakakses.update', $item->rowPointer) }}" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Level</label>
                            <select name="level" id="level" class="form-control">
                                <option value="" selected="true" disabled="disabled">Pilih level</option>
                                <option value="1">Manager</option>
                                <option value="2">Admin</option>
                                <option value="3">User</option>
                                <option value="4">Reseller</option>
                            </select>
                        </div>
                    </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endforeach
@endsection
