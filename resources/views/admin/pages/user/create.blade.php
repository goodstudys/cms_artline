@extends('admin.app')
@section('main')

<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-12">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Tambah User </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">User</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Create</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    @if (Session::has('status'))
                    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                    @endif

                    @if (count($errors) > 0)

                    <div class="alert alert-danger">

                        <strong>Whoops!</strong> There were some problems with your input.

                        <ul>

                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>

                            @endforeach

                        </ul>

                    </div>

                    @endif
                    <div class="card">
                        <h5 class="card-header">Tambah User</h5>
                        <div class="card-body">
                            <form action="{{ route('user.store')  }}" method="POST" enctype="multipart/form-data"
                                class="form">
                                @csrf
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="inputText3" class="col-form-label">First Name</label>
                                            <input id="title" name="firstName" type="text" class="form-control"
                                                required>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="inputText3" class="col-form-label">Last Name</label>
                                            <input id="subtitle" name="lastName" type="text" class="form-control"
                                                required>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="inputText3" class="col-form-label">Display Name</label>
                                            <input id="button" name="displayName" type="text" class="form-control"
                                                required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Company</label>
                                    <input id="link" name="company" type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Email</label>
                                    <input id="email" name="email" type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">No. Handphone</label>
                                    <input id="link" name="phone" type="text" class="form-control" required>
                                </div>
                                <div style="display: none" class="form-group">
                                    <label for="inputText3" class="col-form-label">isi</label>
                                    <input id="isi" name="isi" type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Level</label>
                                    <select name="level" id="level" class="form-control">
                                        <option value="" selected="true" disabled="disabled">Pilih level</option>
                                        @if ( Auth::user()->level == 1)
                                        <option value="1">Manager</option>
                                        <option value="2">Admin</option>
                                        <option value="3">User</option>
                                        <option value="4">Reseller</option>
                                        @else
                                        <option value="3">User</option>
                                        <option value="4">Reseller</option>
                                        @endif

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Password</label>
                                    <input id="password" name="password" type="password" class="form-control"
                                        onkeyup="check();" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Password</label>
                                    <input id="confirm_password" name="confirm_password" type="password" class="form-control"
                                        onkeyup="check();" required>
                                </div>
                                <span id='message'></span>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Foto Profil</label>
                                    <input id="image" name="image" type="file" class="form-control" required>
                                </div>
                                <div class="form__group">
                                    <input type="submit" value="Save" class="btn btn-style-1 btn-submit">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('extrascript')
<script>
    var check = function() {
        if (document.getElementById('password').value ==
          document.getElementById('confirm_password').value) {
          document.getElementById('message').style.color = 'green';
          document.getElementById('message').innerHTML = 'matching';
          document.getElementById('isi').value = 'matching';
        } else {
          document.getElementById('message').style.color = 'red';
          document.getElementById('message').innerHTML = 'not matching';
          document.getElementById('isi').value = '';
        }
      }
</script>
@endsection