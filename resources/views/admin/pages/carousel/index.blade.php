@extends('admin.app')
@section('main')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Carousel</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                            <li class="breadcrumb-item active"><a href="#" class="breadcrumb-link">Carousel</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    @if (Session::has('status'))
    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
    @endif
    <div class="row">

        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Daftar Carousel
                    {{-- @for ($i = 0; $i < count($carousels); $i++)
                     @if ($i== 0 || count($carousels) == 0)
                     <a href="{{ route('carousel.create') }}" class="float-right btn btn-sm btn-primary">Tambah
                    Carousel</a>
                    @else <a href="{{ route('carousel.create') }}" class="float-right btn btn-sm btn-primary">Tambah
                        Carousel</a>
                    @endif

                    @endfor --}}

                    @if (count($carousels) == 0 || count($carousels) == 1)
                    <a href="{{ route('carousel.create') }}" class="float-right btn btn-sm btn-primary">Tambah
                        Carousel</a>
                    {{-- @elseif (count($carousels) == 1)
                    <a href="{{ route('carousel.create') }}" class="float-right btn btn-sm btn-primary">Tambah
                        Carousel</a> --}}
                    @else

                    @endif


                </h5>
                <div class="card-body">
                    <div class="table-responsive ">
                        <table id="example" class="table table-striped table-bordered second" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Title</th>
                                    <th>Subtitle</th>
                                    <th>Button</th>
                                    <th>Link</th>
                                    <th>Image</th>
                                    <th>Aktif</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($carousels as $item)
                                <tr>
                                    <th scope="row">{{$item->id}}</th>
                                    <td>{{$item->title}}</td>
                                    <td>{{$item->subtitle}}</td>
                                    <td>{{$item->button}}</td>
                                    <td>{{$item->link}}</td>
                                    <td><img src="{{ asset('/upload/users/carousels/'.$item->image) }}" width="100">
                                    </td>
                                    <td> @if ($item->isActive != 1)
                                        <a href="{{ route('carousel.status', $item->rowPointer) }}" class="edit"
                                            title="Show" data-toggle="Show"><i class="material-icons">&#xe14c;</i></a>
                                        @else
                                        <a href="{{ route('carousel.status', $item->rowPointer) }}" class="delete"
                                            title="Hide" data-toggle="Hide"><i class="material-icons">&#xe876;</i></a>
                                        @endif</td>
                                    <td>
                                        <a href="{{ route('carousel.edit', $item->rowPointer) }}" class="edit"
                                            title="Edit" data-toggle="tooltip"><i
                                                class="material-icons">&#xE254;</i></a>
                                        {{-- <a href="{{ route('carousel.status', $item->rowPointer) }}" class="delete"
                                        title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                                        --}}
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Judul</th>
                                    <th>Sub Judul</th>
                                    <th>Button</th>
                                    <th>Link</th>
                                    <th>Gambar</th>
                                    <th>Aktif</th>
                                    <th>Actions</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection