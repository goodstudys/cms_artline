@extends('admin.app')
@section('extracss')
<link rel="stylesheet" href="{{asset('assets/vendor/datepicker/tempusdominus-bootstrap-4.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-select/css/bootstrap-select.css')}}">
@endsection
@section('main')

<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-10">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Edit Promotion </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Promotion</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    @if (Session::has('status'))
                    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                    @endif

                    @if (count($errors) > 0)

                    <div class="alert alert-danger">

                        <strong>Whoops!</strong> There were some problems with your input.

                        <ul>

                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>

                            @endforeach

                        </ul>

                    </div>

                    @endif
                    <div class="card">
                        <h5 class="card-header">Edit Promotion</h5>
                        <div class="card-body">
                            <form action="{{ route('promotion.update', $data->rowPointer)  }}" method="POST"
                                enctype="multipart/form-data" class="form">
                                @csrf
                                <div class="form-group">
                                    <label for="title">Judul</label>
                                    <input id="title" name="title" type="text" required="" data-parsley-trigger="change"
                                        class="form-control" value="{{ $data->title }}">

                                </div>
                                <div class="form-group">
                                    <label for="link">Link</label>
                                    <input id="link" name="link" type="text" class="form-control" required=""
                                        data-parsley-trigger="change" value="{{ $data->link }}">

                                </div>

                                <div class="form-group">
                                    <label for="image">Promotion image</label>
                                    <input id="image" name="image" type="file" value="{{$data->image}}"
                                        class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail">Aktif</label><br>
                                    <select class="selectpicker" id="isActive" name="isActive"
                                        value="{{$data->isActive}}" data-width="100%">

                                        <option value=1 {{ $data->isActive == 1? 'selected' : '' }}>Ya </option>
                                        <option value=0 {{ $data->isActive == 0? 'selected' : '' }}>Tidak</option>
                                    </select>
                                </div>


                                <div class="row">
                                    <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="text-right">
                                            <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                            <a href="{{Route('admin.promotion')}}"
                                                class="btn btn-space btn-secondary">Cancel</a>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('extrascript')
<script src="{{asset('assets/vendor/bootstrap-select/js/bootstrap-select.js')}}"></script>
@endsection