@extends('admin.app')
@section('main')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">About</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                            <li class="breadcrumb-item active"><a href="#" class="breadcrumb-link">About</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    @if (Session::has('status'))
    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
    @endif
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="card">
                <h5 class="card-header">Daftar About</h5>
                <div class="card-body">
                    <div class="table-responsive ">
                        <table id="example" class="table table-striped table-bordered awal" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Header Image</th>
                                    <th>Description</th>
                                    <th>Banner Image</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $item)
                                <tr>
                                    <th scope="row">{{$item->id}}</th>
                                    <td><img src="{{ asset('/upload/users/abouts/'.$item->header_image) }}" width="100" alt=""
                                            srcset=""></td>
                                    <td>{{$item->description}}</td>
                                    <td><img src="{{ asset('/upload/users/abouts/'.$item->banner_image) }}" width="100" alt=""
                                            srcset="">
                                    </td>
                                    <td> 
                                        <a href="{{ route('about.edit', $item->id) }}" class="edit" title="Edit"
                                            data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                                        </td>
                                </tr>
                                @endforeach

                            </tbody>
                            <tfoot>
                                <th>No</th>
                                <th>Header Image</th>
                                <th>Description</th>
                                <th>Banner Image</th>
                                <th>Aksi</th>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection