@extends('admin.app')
@section('main')

<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-10">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Edit About </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">About</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    @if (Session::has('status'))
                    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                    @endif

                    @if (count($errors) > 0)

                    <div class="alert alert-danger">

                        <strong>Whoops!</strong> There were some problems with your input.

                        <ul>

                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>

                            @endforeach

                        </ul>

                    </div>

                    @endif
                    <div class="card">
                        <h5 class="card-header">Edit About</h5>
                        <div class="card-body">
                            <form action="{{ route('about.update', $data->id)  }}" method="POST"
                                enctype="multipart/form-data" class="form">
                                @csrf
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Header Image</label><br>
                                    <img src="{{ asset('/upload/users/abouts/'.$data->header_image) }}" width="50%" alt=""
                                            srcset=""><br>
                                    <input id="header_image" name="header_image" type="file" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Description</label>
                                    <textarea name="description" id="description" cols="30" rows="10"
                                        class="form-control" required>{{ $data->description }} </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Banner Image</label>
                                    <br>
                                    <img src="{{ asset('/upload/users/abouts/'.$data->banner_image) }}" width="50%" alt=""
                                            srcset=""><br>
                                    <input id="banner_image" name="banner_image" type="file" class="form-control">
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="text-right">
                                            <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                            <a href="{{Route('admin.about')}}"
                                                class="btn btn-space btn-secondary">Cancel</a>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection