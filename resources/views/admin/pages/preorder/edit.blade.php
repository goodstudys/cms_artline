@extends('admin.app')
@section('main')

    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-10">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Edit Pre-Order Reseller </h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Pre-Order </a>
                                        </li>
                                        <li class="breadcrumb-item active" aria-current="page">Edit Pre-Order Reseller</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        @if (Session::has('status'))
                            <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                        @endif

                        @if (count($errors) > 0)

                            <div class="alert alert-danger">

                                <strong>Whoops!</strong> There were some problems with your input.

                                <ul>

                                    @foreach ($errors->all() as $error)

                                        <li>{{ $error }}</li>

                                    @endforeach

                                </ul>

                            </div>

                        @endif
                        <div class="card">
                            <h5 class="card-header">Edit Pre-Order Reseller
                                <a href="{{ route('preorder.add', $data->rowPointer) }}" class="float-right btn btn-sm btn-primary">Tambah
                                    Produk</a>
                            </h5>
                            <div class="card-body">
                                <div class="table-responsive nowrap">
                                    <table class="table nowrap">
                                        <thead class="bg-light">
                                        <tr class="border-0">

                                            <th class="border-0">Nama</th>
                                            <th class="border-0">Variasi</th>
                                            <th class="border-0">SKU</th>
                                            <th class="border-0">Harga</th>
                                            <th class="border-0">Jumlah</th>
                                            <th class="border-0">Total</th>
                                            <th class="border-0">Aksi</th>

                                        </tr>
                                        </thead>
                                        <tbody class="variasiappd">
                                        <span class="d-none">{{ $zero = 0 }}</span>
                                        @foreach($order as $item)
                                            <tr>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->colorName }}</td>
                                                <td>{{ $item->sku }}</td>
                                                <td>Rp {{number_format($item->price, 2)}}</td>
                                                <td>{{ $item->quantity }}</td>
                                                <td>Rp {{number_format($item->price*$item->quantity, 2)}}</td>
                                                <td class="d-none">{{ $prodprice = $item->price*$item->quantity }}</td>
                                                <td class="d-none">{{ $gettotal = $zero += $prodprice }}</td>
                                                <td> <a href="#" class="edit" title="Edit" data-toggle="modal"
                                                        data-target=".bd-example-modal-xl{{$item->id}}variasi"><i
                                                            class="material-icons">&#xE254;</i></a>
                                                    <a href="{{ route('preorder.destroy',$item->id) }}" class="delete" title="Delete"
                                                       data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <th colspan="5">Total</th>
                                            <th>
                                                <input type="text" class="d-none" name="order" value="{{ $data->rowPointer }}">
                                                <input style="width: 250px" id="total" type="text" name="color" required
                                                       data-parsley-trigger="change" autocomplete="off"
                                                       class="form-control" value="Rp. {{ number_format($gettotal) }} + Rp. {{ number_format($data->kurirRate) }} Ongkir" readonly="">
                                            </th>
                                        </tr>
                                        </tfoot>
                                    </table>



                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach ($order as $item)
        <div class="modal fade bd-example-modal-xl{{$item->id}}variasi" tabindex="-1" role="dialog"
             aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                        <form action="{{ route('preorder.update', $item->id) }}" method="POST">
                            @csrf
                            <div class="card">
                                <div class="row">
                                    <div class="col-9">
                                        <h5 class="card-header">Edit product order
                                            ({{$item->name}})</h5>
                                    </div>
                                </div>
                                <input type="hidden" name="default" value="{{ $item->rowPointer }}">
                                <div class="card-body p-0">
                                    <div class="table-responsive nowrap">
                                        <table class="table nowrap">
                                            <thead class="bg-light">
                                            <tr class="border-0">

                                                <th class="border-0">Nama</th>
                                                <th class="border-0">Variasi</th>
                                                <th class="border-0">SKU</th>
                                                <th class="border-0">Harga</th>
                                                <th class="border-0">Jumlah</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>
                                                    <input id="name" type="text" name="name" required
                                                           data-parsley-trigger="change" autocomplete="off"
                                                           class="form-control" value="{{$item->name}}" readonly="">
                                                </td>
                                                <td>
                                                    <input id="name" type="text" name="color" required
                                                           data-parsley-trigger="change" autocomplete="off"
                                                           class="form-control" value="{{$item->colorName}}" readonly="">
                                                </td>
                                                <td>
                                                    <input id="name" type="text" name="sku" required
                                                           data-parsley-trigger="change" autocomplete="off"
                                                           class="form-control" value="{{$item->sku}}" readonly="">
                                                </td>
                                                <td>
                                                    <input id="name" type="text" name="price" required
                                                           data-parsley-trigger="change" autocomplete="off"
                                                           class="form-control" value="Rp. {{ number_format($item->price) }}" readonly="">
                                                    <input type="hidden" id="pricess" value="{{ $item->price }}">
                                                </td>
                                                <td>
                                                    <input id="quantity" type="text" name="quantity" required
                                                           data-parsley-trigger="change" autocomplete="off"
                                                           class="form-control" value="{{$item->quantity}}">
                                                    <input type="text" name="orderPointer" class="d-none" value="{{ $data->rowPointer }}">
                                                    <input type="text" name="courRate" class="d-none" value="{{ $data->kurirRate }}">
                                                    <input type="hidden" id="totalss" name="allTotal"  value="{{ $gettotal+$data->kurirRate }}">
                                                    <input type="hidden" id="totalss1" name="allTotals" >
                                                    <input type="hidden" id="qtybefore" value="{{$item->quantity}}">
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>



                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                                </div>
                                <div class="col-sm-6 pl-0">
                                    <p class="text-right">
                                        <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                        <button data-dismiss="modal" aria-label="Close" class="btn btn-space btn-secondary"
                                                id="mimodal">Cancel</button>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection
@section('extrascript')
    <script>
        $("#quantity").change(function(){
            var qty = $("#quantity").val()
            var price = $("#pricess").val()
            var total = $("#totalss").val()
            var qtybefore = $("#qtybefore").val()
            var domath1 = parseInt(price) * parseFloat(qty)
            if (parseInt(qty) < parseInt(qtybefore)) {
                var domath2 = parseFloat(total) - parseFloat(domath1)
            } else if(parseInt(qty) > parseInt(qtybefore))
                var domath2 = parseFloat(total) + parseFloat(domath1)
            else {}
            $("#totalss1").val(domath2)
        });
    </script>
@endsection

