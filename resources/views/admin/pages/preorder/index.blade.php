@extends('admin.app')
@section('main')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">List Pre-Order Reseller</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                            <li class="breadcrumb-item active"><a href="#" class="breadcrumb-link">List Pre-Order Reseller</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->
    @if (Session::has('status'))
    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
    @endif
    <div class="row">
        <!-- ============================================================== -->
        <!-- data table  -->
        <!-- ============================================================== -->
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <form action="{{ route('order.printMultirple') }}" method="get">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">Daftar Pesanan
                            <button type="submit" class="float-right btn btn-sm btn-primary mr-4">Print</button>
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered second" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Pesan</th>
                                        <th>Kode Order</th>
                                        <th>Nama</th>
                                        <th>Tujuan Transfer</th>
                                        <th>Status</th>
                                        <th>Konfirmasi</th>
                                        <th>Kode Booking</th>
                                        <th>Print</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($orders as $index => $item)
                                    <tr>
                                        <th scope="row">{{$index+1}}</th>
                                        <td>{{$item->dateCreated}}</td>
                                        <td>{{$item->orderCode }}</td>
                                        <td>{{$item->billName}}</td>
                                        @if ($item->confirmationsorderId != null)
                                        <td>{{$item->confirmationstujuan}}</td>
                                        @else
                                        <td>Belum melakukan konfirmasi</td>
                                        @endif
                                        <td>
                                            @if ($item->status == 'NEW')
                                                <a class="btn btn-primary btn-sm" href="{{ route('order.status',$item->rowPointer) }}" style="color: white">Packing</a>
                                                <a class="btn btn-danger btn-sm" href="{{ route('order.cancel',$item->rowPointer) }}" style="color: white">Cancel</a>
                                            @elseif ($item->status == 'PACKING')
                                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                        data-target=".bd-example-modal-xl{{$item->id}}sent">Sent</button>
                                                <a class="btn btn-danger btn-sm" href="{{ route('order.cancel',$item->rowPointer) }}" style="color: white">Cancel</a>
                                            @elseif ($item->status == 'SENT')
                                                <a class="btn btn-primary btn-sm" href="{{ route('order.status',$item->rowPointer) }}" style="color: white">Delivered</a>
                                            @elseif ($item->status == 'DELIVERED')
                                            <span class="text-success">Success</span>
                                            @elseif ($item->status == 'CANCEL')
                                            <span class="text-danger">CANCEL</span>
                                            @endif

                                        </td>
                                        <td> <label class="custom-control custom-checkbox">
                                                @if ($item->confirmationsorderId == null)
                                                <input type="checkbox" class="custom-control-input " disabled>
                                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target=".bd-example-modal-xl{{$item->id}}input">Input</button>
                                                @else
                                                <input type="checkbox" class="custom-control-input " checked disabled>
                                                <a href="#" data-toggle="modal" class="view"
                                                    data-target=".bd-example-modal-xl{{$item->id}}" title="View"
                                                    data-toggle="tooltip"><i class="material-icons">&#xE417;</i></a>
                                                @endif
                                                <span class="custom-control-label"></span>
                                            </label></td>
                                        <td>
                                            <label class="custom-control custom-checkbox">
                                                @if ($item->bookingCode == null)
                                                <input type="checkbox" class="custom-control-input " disabled>
                                                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                                    data-target=".bd-example-modal-xl{{$item->id}}booking">Input</button>
                                                @else
                                                <input type="checkbox" class="custom-control-input " checked disabled>
                                                <a href="#" data-toggle="modal" class="view"
                                                    data-target=".bd-example-modal-xl{{$item->id}}bookingview"
                                                    title="View" data-toggle="tooltip"><i
                                                        class="material-icons">&#xE417;</i></a>
                                                @endif
                                                <span class="custom-control-label"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input " name="prints[]"
                                                    value="{{ $item->id }}">
                                                <span class="custom-control-label"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <a href="{{ route('preorder.edit', $item->rowPointer) }}" class="edit"
                                                title="Edit" data-toggle="tooltip"><i
                                                    class="material-icons">&#xE254;</i></a>
                                            <a href="{{ route('preorder.show', $item->rowPointer) }}" class="view"
                                                title="View" data-toggle="tooltip"><i
                                                    class="material-icons">&#xE417;</i></a>
                                            <a href="{{ route('order.print', $item->rowPointer) }}" class="view"
                                                title="print label" data-toggle="tooltip"><i
                                                    class="material-icons">&#xe2c4;</i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Tanggal Pesan</th>
                                        <th>Kode Order</th>
                                        <th>Nama</th>
                                        <th>Total</th>
                                        <th>Tujuan Transfer</th>
                                        <th>Status</th>
                                        <th>Konfirmasi</th>
                                        <th>Kode Booking</th>
                                        <th>Print</th>
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- ============================================================== -->
        <!-- end data table  -->
        <!-- ============================================================== -->
    </div>


</div>
@foreach ($orders as $item)
<div class="modal fade bd-example-modal-xl{{$item->id}}sent" tabindex="-1" role="dialog"
    aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="container-fluid  modal-content">
            <div class="row">
                <!-- ============================================================== -->
                <!-- validation form -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Informasi Pengiriman</h5>
                        <div class="card-body">
                            <form action="{{ route('order.status',$item->rowPointer) }}" id="basicform" method="get"
                                data-parsley-validate="">
                                {{-- <input id="inputUserName" type="text" name="name" data-parsley-trigger="change"
                                        required="" placeholder="Enter user name" value="{{$item->id}}"
                                autocomplete="off"
                                class="form-control"> --}}
                                <div class="form-group">
                                    <label for="nama">No. Resi</label>
                                    <input id="resi" type="text" name="resi" data-parsley-trigger="change" required=""
                                        autocomplete="off" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="bank">Kurir</label>
                                    <input id="courier" type="text" name="courier" data-parsley-trigger="change"
                                        required="" placeholder="Contoh : JNE" class="form-control">
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="text-right">
                                            <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                            <button data-dismiss="modal" aria-label="Close"
                                                class="btn btn-space btn-secondary">Cancel</button>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end validation form -->
                <!-- ============================================================== -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-xl{{$item->id}}sent" tabindex="-1" role="dialog"
    aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="container-fluid  modal-content">
            <div class="row">
                <!-- ============================================================== -->
                <!-- validation form -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Informasi Pengiriman</h5>
                        <div class="card-body">
                            <form action="{{ route('order.status',$item->rowPointer) }}" id="basicform" method="get"
                                data-parsley-validate="">
                                {{-- <input id="inputUserName" type="text" name="name" data-parsley-trigger="change"
                                        required="" placeholder="Enter user name" value="{{$item->id}}"
                                autocomplete="off"
                                class="form-control"> --}}
                                <div class="form-group">
                                    <label for="nama">No. Resi</label>
                                    <input id="resi" type="text" name="resi" data-parsley-trigger="change" required=""
                                        autocomplete="off" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="bank">Kurir</label>
                                    {{-- <select class="form-control category_id" id="courier" name="courier" data-width="100%">

                                            <option value="">Pilih kategori…</option>
                                            @foreach($categorys as $category)
                                                <option value="{{ $category['KodeGroup'] }}">{{ $category['NamaGroup'] }} </option>
                                            @endforeach
                                    </select> --}}
                                    <input id="courier" type="text" name="courier" data-parsley-trigger="change"
                                        required="" placeholder="Contoh : JNE, JNT, TIKI, POS" class="form-control">
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="text-right">
                                            <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                            <button data-dismiss="modal" aria-label="Close"
                                                class="btn btn-space btn-secondary">Cancel</button>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end validation form -->
                <!-- ============================================================== -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-xl{{$item->id}}" tabindex="-1" role="dialog"
    aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                <div class="card">
                    <h5 class="card-header">Kofirmasi Pembayaran</h5>
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="bg-light">
                                    <tr class="border-0">

                                        <th class="border-0">Nama Pengirim di Rekening Bank
                                        </th>
                                        <th class="border-0">Transfer dari Bank</th>
                                        <th class="border-0">No. Rekening</th>
                                        <th class="border-0">Tujuan Transfer</th>
                                        <th class="border-0">Jumlah Transfer (Rp)</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>

                                        <td>{{$item->confirmationsnama}} </td>
                                        <td>{{ $item->confirmationsbank }}</td>
                                        <td>{{$item->confirmationsakun}}
                                        </td>
                                        <td>{{$item->confirmationstujuan}}</td>
                                        <td>Rp.{{number_format($item->confirmationsjumlah, 2)}}
                                        </td>


                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-xl{{$item->id}}input" tabindex="-1" role="dialog"
    aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="container-fluid  modal-content">
            <div class="row">
                <!-- ============================================================== -->
                <!-- validation form -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Konfirmasi Pembayaran</h5>
                        <div class="card-body">
                            <div class="card-body">
                                <form action="{{ route('order.changeConfirm') }}" id="basic-form" method="POST"
                                    enctype="multipart/form-data" class="">
                                    @csrf

                                    <input type="hidden" name="order" id="order" class="form-control"
                                        value="{{ $item->id }}">

                                    <div class="form__group">
                                        <label class="form__label" for="dropship_name">Nama Pengirim di Rekening
                                            Bank</label>
                                        <input type="text" name="nama" id="nama" class="form-control">
                                    </div>


                                    <div class="form__group">
                                        <label class="form__label" for="dropship_name">Transfer dari Bank<span
                                                class="required">*</span></label>
                                        <input type="text" name="bank" id="bank" class="form-control">
                                    </div>


                                    <div class="form__group">
                                        <label class="form__label" for="dropship_name">Masukkan No. Rekening
                                            Anda<span class="required">*</span></label>
                                        <input type="text" name="akun" id="akun" class="form-control">
                                    </div>


                                    <div class="form__group">
                                        <label class="form__label" for="dropship_name">Tujuan Transfer<span
                                                class="required">*</span></label>
                                        <input type="text" name="tujuan" id="tujuan" class="form-control">
                                    </div>


                                    <div class="form__group">
                                        <label class="form__label" for="dropship_name">Jumlah Transfer (Rp)<span
                                                class="required">*</span></label>
                                        <input type="text" name="jumlah" id="jumlah" class="form-control">
                                    </div>


                                    <div class="row mt-4">
                                        <div class="col-1">
                                            <div class="form__group">
                                                <input type="submit" value="Submit" class="btn btn-space btn-primary">
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <button data-dismiss="modal" aria-label="Close"
                                                class="btn btn-space btn-secondary">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- ============================================================== -->
                            <!-- end validation form -->
                            <!-- ============================================================== -->
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end validation form -->
                    <!-- ============================================================== -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-xl{{$item->id}}booking" tabindex="-1" role="dialog"
    aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="container-fluid  modal-content">
            <div class="row">
                <!-- ============================================================== -->
                <!-- validation form -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Input Kode Booking</h5>
                        <div class="card-body">
                            <div class="card-body">
                                <form action="{{ route('order.bookingcode') }}" id="basic-form" method="POST"
                                    enctype="multipart/form-data" class="">
                                    @csrf

                                    <input type="hidden" name="order" id="order" class="form-control"
                                        value="{{ $item->id }}">

                                    <div class="form__group">
                                        <label class="form__label" for="dropship_name">Kode Booking</label>
                                        <input type="text" name="booking" id="booking" class="form-control">
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-1">
                                            <div class="form__group">
                                                <input type="submit" value="Submit" class="btn btn-space btn-primary">
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <button data-dismiss="modal" aria-label="Close"
                                                class="btn btn-space btn-secondary">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- ============================================================== -->
                            <!-- end validation form -->
                            <!-- ============================================================== -->
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end validation form -->
                    <!-- ============================================================== -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-xl{{$item->id}}bookingview" tabindex="-1" role="dialog"
    aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="container-fluid  modal-content">
            <div class="row">
                <!-- ============================================================== -->
                <!-- validation form -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">Input Kode Booking</h5>
                        <div class="card-body">
                            <div class="card-body">
                                <form action="{{ route('order.bookingcode') }}" id="basic-form" method="POST"
                                    enctype="multipart/form-data" class="">
                                    @csrf

                                    <input type="hidden" name="order" id="order" class="form-control"
                                        value="{{ $item->id }}">

                                    <div class="form__group">
                                        <label class="form__label" for="dropship_name">Kode Booking</label>
                                        <input type="text" name="booking" id="booking" class="form-control"
                                            value="{{ $item->bookingCode }}">
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-2">
                                            <div class="form__group">
                                                <input type="submit" value="Save Change"
                                                    class="btn btn-space btn-primary">
                                            </div>
                                        </div>
                                        <div class="col-1">
                                            <button data-dismiss="modal" aria-label="Close"
                                                class="btn btn-space btn-secondary">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- ============================================================== -->
                            <!-- end validation form -->
                            <!-- ============================================================== -->
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end validation form -->
                    <!-- ============================================================== -->
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection
