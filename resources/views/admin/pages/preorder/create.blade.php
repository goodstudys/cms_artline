@extends('admin.app')
@section('main')
    <div class="container-fluid  dashboard-content">
        <!-- ============================================================== -->
        <!-- pageheader -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <h2 class="pageheader-title">Tambah Produk Pre-Order</h2>
                    <div class="page-breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                <li class="breadcrumb-item active"><a href="#" class="breadcrumb-link">Pre-Order</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end pageheader -->
        <!-- ============================================================== -->
        @if (Session::has('status'))
            <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
        @endif
        <div class="row">

            <!-- ============================================================== -->
            <!-- data table multiselects  -->
            <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">Daftar Produk
                            <a href="{{ route('preorder.edit', request()->route('id')) }}" class="float-right btn btn-sm btn-primary">Kembali</a>
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered awal" style="width:100%">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Harga</th>
                                    <th>Gambar</th>
                                    <th>Variasi</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($products as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td><a href="#" style="color:#71748d" data-toggle="modal" class="view"
                                               data-backdrop="static"
                                               data-target=".bd-example-modal-xl{{$item->id}}{{$item->price}}" title="View"
                                               data-toggle="tooltip">{{$item->name}}</a> </td>
                                        <td> <a href="#" style="color:#71748d" data-toggle="modal" class="view"
                                                data-backdrop="static"
                                                data-target=".bd-example-modal-xl{{$item->id}}{{$item->price}}" title="View"
                                                data-toggle="tooltip">{{$item->price}}</a></td>
                                        <td><img src="{{ asset('/upload/users/products/'. $item->image1) }}" width="50"
                                                 alt="" srcset=""></td>
                                        <td><a href="{{ route('product.showVariasi', $item->rowPointer) }}"
                                               class=" view btn btn-info m-2" style="color:white" title="View"
                                               data-toggle="tooltip">View</a> </td>
                                        <td>
                                            <button data-target="#exampleModal{{$item->rowPointer}}" id="add{{$item->rowPointer}}" data-toggle="modal"
                                                    class="btn btn-primary" title="Tambah Produk" data-toggle="tooltip">Add</button>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Harga</th>
                                    <th>Gambar</th>
                                    <th>Variasi</th>
                                    <th>Aksi</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end data table multiselects  -->
            <!-- ============================================================== -->
        </div>

    </div>

    <div class="modal fade" id="importExcel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="post" action="{{ route('product.import.excel') }}" enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Import Excel</h5>
                    </div>
                    <div class="modal-body">

                        @csrf

                        <label>Pilih file excel</label>
                        <div class="form-group">
                            <input type="file" name="file" required="required">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Import</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    @foreach ($products as $item)
        <div class="modal fade bd-example-modal-xl{{$item->id}}{{$item->price}}" tabindex="-1" role="dialog"
             aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                        <form action="{{ route('product.quickUpdate', $item->rowPointer) }}" method="get">
                            <div class="card">
                                <h5 class="card-header">Edit Produk ({{$item->name}})</h5>
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead class="bg-light">
                                            <tr class="border-0">

                                                <th class="border-0">Nama
                                                </th>

                                                <th class="border-0">Brand</th>
                                                <th class="border-0">Harga</th>
                                                <th class="border-0">Diskon Nominal (Rp)</th>
                                                <th class="border-0">Diskon Persen</th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            <tr>
                                                <td> <input id="name" type="text" name="name" value="{{$item->name}}"
                                                            data-parsley-trigger="change" required="" placeholder="Nama"
                                                            autocomplete="off" class="form-control">
                                                </td>
                                                <td> <input id="brand" type="text" name="brand" value="{{$item->brand}}"
                                                            data-parsley-trigger="change" required="" placeholder="Brand"
                                                            autocomplete="off" class="form-control">
                                                </td>
                                                <td> <input id="price" type="text" name="price" value="{{$item->price}}"
                                                            data-parsley-trigger="change" required="" placeholder="Harga"
                                                            autocomplete="off" class="form-control">
                                                </td>
                                                <td> <input id="discAmount" type="text" name="discAmount"
                                                            value="{{$item->discAmount}}" data-parsley-trigger="change"
                                                            required="" placeholder="Jumlah diskon" autocomplete="off"
                                                            class="form-control">
                                                </td>
                                                <td> <input id="discPercent" type="text" name="discPercent"
                                                            value="{{$item->discPercent}}" data-parsley-trigger="change"
                                                            required="" placeholder="Diskon Persen" autocomplete="off"
                                                            class="form-control">
                                                </td>
                                            </tr>


                                            </tbody>
                                        </table>



                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                                </div>
                                <div class="col-sm-6 pl-0">
                                    <p class="text-right">
                                        <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                        <button data-dismiss="modal" aria-label="Close"
                                                class="btn btn-space btn-secondary">Cancel</button>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-xl{{$item->id}}variasi" tabindex="-1" role="dialog"
             aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                        <form action="{{ route('group.store') }}" method="get">
                            <div class="card">
                                <div class="row">
                                    <div class="col-9">
                                        <h5 class="card-header">Tambah variasi ({{$item->name}})</h5>
                                    </div>
                                </div>
                                <input type="hidden" name="default" value="{{ $item->rowPointer }}">
                                <div class="card-body p-0">
                                    <div class="table-responsive nowrap">
                                        <table class="table nowrap">
                                            <thead class="bg-light">
                                            <tr class="border-0">

                                                <th class="border-0">Nama</th>
                                                {{--                                            <th class="border-0">Hex</th>--}}
                                                {{--                                            <th class="border-0">Warna</th>--}}
                                                <th class="border-0">Harga</th>
                                                <th class="border-0">Diskon Nominal (Rp)</th>
                                                <th class="border-0">Diskon Persen</th>
                                                <th class="border-0">Net Price</th>
                                                <th class="border-0">Aksi</th>

                                            </tr>
                                            </thead>
                                            <tbody class="variasiappd{{ $item->rowPointer }}">

                                            <tr>
                                                {{--                                            <td> <input id="name[]" type="text" name="name[]" required--}}
                                                {{--                                                    data-parsley-trigger="change" autocomplete="off"--}}
                                                {{--                                                    class="form-control d-none">--}}
                                                {{--                                            </td>--}}
                                                {{--                                                                                        <td> <input id="1" type="text" name="hex[]" required--}}
                                                {{--                                                                                                data-parsley-trigger="change" autocomplete="off"--}}
                                                {{--                                                                                                class="form-control pickcolor" onclick="colorpicker(this)">--}}
                                                {{--                                                                                        </td>--}}
                                                {{--                                            --}}{{--                                            <td> <input id="color[]" type="text" name="color[]" required--}}
                                                {{--                                            --}}{{--                                                    data-parsley-trigger="change" autocomplete="off"--}}
                                                {{--                                            --}}{{--                                                    class="form-control">--}}
                                                {{--                                            --}}{{--                                            </td>--}}
                                                {{--                                            --}}{{-- <td> <input id="price" type="text" name="price" required--}}
                                                {{--                                                    data-parsley-trigger="change" autocomplete="off"--}}
                                                {{--                                                    class="form-control">--}}
                                                {{--                                            </td> --}}
                                                {{--                                            --}}{{-- <td> <input id="discAmount[]" type="text" name="discAmount[]" required--}}
                                                {{--                                                    data-parsley-trigger="change" autocomplete="off"--}}
                                                {{--                                                    class="form-control" value="0">--}}
                                                {{--                                            </td>--}}
                                                {{--                                            <td> <input id="discPercent[]" type="text"--}}
                                                {{--                                                         name="discPercent[]" required--}}
                                                {{--                                                    data-parsley-trigger="change" autocomplete="off"--}}
                                                {{--                                                    class="form-control" value="0">--}}
                                                {{--                                            </td> --}}
                                                {{--                                            <td><input id="pricesss1" type="number" name="price[]" class="form-control d-none" value="0"></td>--}}
                                                {{--                                            <td><input id="discamount1" class="form-control d-none" name="discAmount[]" onclick="colorpicker(this)" type="text"></td>--}}
                                                {{--                                            <td><input id="discpercent2" class="form-control d-none" name="discPercent[]" onclick="disabledamount(this)" type="text" value="0"></td>--}}
                                                {{--                                            <td><input id="netprice2" class="form-control d-none"  type="text" value="0" readonly></td>--}}
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td><span class="fa fa-plus add-variasi-single"></span></td>

                                            </tr>


                                            </tbody>
                                        </table>



                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                                </div>
                                <div class="col-sm-6 pl-0">
                                    <p class="text-right">
                                        <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                        <button
                                            {{--                                    onclick="window.location.reload();" --}}
                                            data-dismiss="modal"
                                            aria-label="Close" class="btn btn-space btn-secondary" id="mimodal">Cancel</button>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="exampleModal{{$item->rowPointer}}" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Produk ({{$item->name}})</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="{{ route('preorder.store')  }}" method="POST">
                        @csrf
                        <div class="container">
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">Pilih Variasi</label>
                                <input type="text" class="d-none" id="myproduct{{$item->rowPointer}}" value="{{$item->rowPointer}}" readonly>
                                <select name="variasi" id="variasi" class="form form-control variasi">
                                    <option value="0" selected>{{$item->colorName}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputText3" class="col-form-label">Jumlah</label>
                                <input id="qty" name="qty" type="text" class="form-control">
                                <input type="text" name="orderPointer" class="d-none" value="{{ request()->route('id') }}" readonly>
                                <input type="text" name="prodPointer" class="d-none" value="{{ $item->rowPointer }}" readonly>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="submit" value="Add" class="btn btn-primary btn-submit">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@endsection
@section('extrascript')
    <script type="text/javascript" src="{{ asset('assets/colorpicker/js/colorpicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/colorpicker/js/eye.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/colorpicker/js/utils.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/colorpicker/js/layout.js') }}"></script>
    <script>
        $('.hexcolor').ColorPicker({
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            },
            onChange: function (hsb, hex, rgb) {
                $('.hexcolor').val('#'+hex);
            }
        })

        $("#mimodal").on('hidden.bs.modal', function () {
            $(this).data('bs.modal', null);
        });

        function disabledamount(e) {
            console.log(e)
            if (e.id == 'discpercent2') {
                var a = $('#pricesss1')
                var b = $('#discamount1')
                var c = $('#discpercent2')
                var d = $('#netprice2')
            } else {
                var pointer = e.placeholder
                var plus = parseInt(pointer)+1
                var min = parseInt(pointer)-1
                var a = $('#pricesss'+ min)
                var b = $('#discamount'+ min)
                var c = $('#discpercent'+ pointer)
                var d = $('#netprice'+ pointer)
                console.log(pointer)
            }
            $(b).attr('disabled', true);
            $(c).attr('disabled', false);
            $(b).change(function(){
                //   var a = $(this.val()) / $("#price").val()

                $(c).val($(this).val()/$(a).val()*100);
                $(d).val($(a).val()-$(b).val())
            });
            $(c).change(function(){

                $(b).val($(this).val()/100*$(a).val());
                $(d).val($(a).val()-$(a).val()*$(c).val()/100);
            });

            $(b).append(
                '<input name="discAmount[]" class="form-control" type="text" value="0">'
            );
        }

        function colorpicker(e) {
            console.log(e)
            // if (e.id == 1) {
            //     var a = $('.pickcolor')
            // } else {
            //     var pointer = e.id
            //     var a = document.getElementById(pointer)
            // }
            // $(a).ColorPicker({
            //     onBeforeShow: function () {
            //         $(this).ColorPickerSetColor(this.value);
            //     },
            //     onChange: function (hsb, hex, rgb) {
            //         $(a).val('#'+hex);
            //     }
            // })
            if (e.id == 'discamount1') {
                var a = $('#pricesss1')
                var b = $('#discamount1')
                var c = $('#discpercent2')
                var d = $('#netprice2')
            } else {
                var pointer = e.placeholder
                var plus = parseInt(pointer)+1
                var min = parseInt(pointer)-1
                var a = $('#pricesss'+ min)
                var b = $('#discamount'+ min)
                var c = $('#discpercent'+ pointer)
                var d = $('#netprice'+ pointer)
                console.log(pointer)
            }
            $(c).attr('disabled', true);
            $(b).attr('disabled', false);
            $(b).change(function(){
                //   var a = $(this.val()) / $("#price").val()

                $(c).val($(this).val()/$(a).val()*100);
                $(d).val($(a).val()-$(b).val())
            });
            $(c).change(function(){

                $(b).val($(this).val()/100*$(a).val());
                $(d).val($(a).val()-$(a).val()*$(c).val()/100);
            });

            $(c).append(
                '<input name="discPercent[]" class="form-control" type="text" value="0">'
            );
        };


        var i = 2;
        var x = 2;

        @foreach($products as $item)
        $(".add-variasi-single").click(function(){
            $(".variasiappd{{ $item->rowPointer }}").append(
                '<tr class="appended">'+
                '<td> <input id="name[]" type="text" name="name[]" required data-parsley-trigger="change"  autocomplete="off" class="form-control"> </td>' +
                // '<td> <input id="a'+(i++)+'" type="text" name="hex[]" onclick="colorpicker(this)" required autocomplete="off" class="form-control"> </td>' +
                // '<td> <input id="color[]" type="text" name="color[]" required data-parsley-trigger="change"  autocomplete="off" class="form-control"> </td>' +
                // '<td> <input id="price[]" type="text" name="price[]" required data-parsley-trigger="change"  autocomplete="off" class="form-control"> </td>' +
                // '<td> <input id="discAmount[]" type="text" name="discAmount[]" required data-parsley-trigger="change"  autocomplete="off" class="form-control" value="0"> </td>' +
                // '<td> <input id="discPercent[]" type="text" name="discPercent[]" required data-parsley-trigger="change"  autocomplete="off" class="form-control" value="0"> </td>' +
                '<td><input id="pricesss'+(i)+'" name="price[]" type="number" class="form-control"  placeholder="'+(i)+'" value="0"></td>' +
                '<td><input id="discamount'+(i++)+'" name="discAmount[]" class="form-control" onclick="colorpicker(this)" placeholder="'+(i)+'" type="text"></td>'+
                '<td><input id="discpercent'+(i)+'" name="discPercent[]" class="form-control"  placeholder="'+(i)+'" onclick="disabledamount(this)" type="text" value="0"></td>'+
                '<td><input id="netprice'+(i)+'" class="form-control" placeholder="'+(i)+'" type="text" value="0" readonly></td>'+
                '<td><span class="fa fa-minus minus"></span></td>'+
                '</tr>'
            );

            function disabledamount(e) {
                console.log(e)
                if (e.id == 'discpercent2') {
                    var a = $('#pricesss1')
                    var b = $('#discamount1')
                    var c = $('#discpercent2')
                    var d = $('#netprice2')
                } else {
                    var pointer = e.placeholder
                    var plus = parseInt(pointer)+1
                    var min = parseInt(pointer)-1
                    var a = $('#pricesss'+ min)
                    var b = $('#discamount'+ min)
                    var c = $('#discpercent'+ pointer)
                    var d = $('#netprice'+ pointer)
                    console.log(pointer)
                }
                $(b).attr('disabled', true);
                $(c).attr('disabled', false);
                $(b).change(function(){
                    //   var a = $(this.val()) / $("#price").val()

                    $(c).val($(this).val()/$(a).val()*100);
                    $(d).val($(a).val()-$(b).val())
                });
                $(c).change(function(){

                    $(b).val($(this).val()/100*$(a).val());
                    $(d).val($(a).val()-$(a).val()*$(c).val()/100);
                });

                $(b).append(
                    '<input name="discAmount[]" class="form-control" type="text" value="0">'
                );
            }

            function colorpicker(e) {
                console.log(e)
                // if (e.id == 1) {
                //     var a = $('.pickcolor')
                // } else {
                //     var pointer = e.id
                //     var a = document.getElementById(pointer)
                // }
                // $(a).ColorPicker({
                //     onBeforeShow: function () {
                //         $(this).ColorPickerSetColor(this.value);
                //     },
                //     onChange: function (hsb, hex, rgb) {
                //         $(a).val('#'+hex);
                //     }
                // })
                if (e.id == 'discamount1') {
                    var a = $('#pricesss1')
                    var b = $('#discamount1')
                    var c = $('#discpercent2')
                    var d = $('#netprice2')
                } else {
                    var pointer = e.placeholder
                    var plus = parseInt(pointer)+1
                    var min = parseInt(pointer)-1
                    var a = $('#pricesss'+ min)
                    var b = $('#discamount'+ min)
                    var c = $('#discpercent'+ pointer)
                    var d = $('#netprice'+ pointer)
                    console.log(pointer)
                }
                $(c).attr('disabled', true);
                $(b).attr('disabled', false);
                $(b).change(function(){
                    //   var a = $(this.val()) / $("#price").val()

                    $(c).val($(this).val()/$(a).val()*100);
                    $(d).val($(a).val()-$(b).val())
                });
                $(c).change(function(){

                    $(b).val($(this).val()/100*$(a).val());
                    $(d).val($(a).val()-$(a).val()*$(c).val()/100);
                });

                $(c).append(
                    '<input name="discPercent[]" class="form-control" type="text" value="0">'
                );
            };
        });

        $('.variasiappd{{ $item->rowPointer }}').on('click', '.minus', function () {
            $(this).closest('tr').remove();
            return false;
        });

        $("#add{{$item->rowPointer}}").click(function(){
            var pointer = $("#myproduct{{$item->rowPointer}}").val()

            console.log('{{url('admin/pre-order/groupJson/')}}' + '/' + pointer)

            $.ajax({
                url:'{{url('admin/pre-order/groupJson/')}}' + '/' + pointer,
                type:'get',
                dataType: 'json',
                success:function (response) {
                    console.log(response.data);

                    $(".variasi").attr('disabled', false);

                    var $select = $('.variasi');

                    if (response.data === null) {

                    } else {
                        $.each(response.data.detail,function(key, value)
                        {
                            $select.append('<option value=' + value.rowPointer + '>' + value.name + '</option>');
                        });
                    }
                }
            });

        });

        $('aaa').on('click', function () {
            console.log('oyee')
            var pointer = $(this).children("option:selected").val();


        });
        @endforeach

    </script>
@endsection
