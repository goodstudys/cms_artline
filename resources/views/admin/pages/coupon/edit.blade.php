@extends('admin.app')
@section('extracss')
{{-- <link rel="stylesheet" href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}">
<link href="{{asset('assets/vendor/fonts/circular-std/style.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/libs/css/style.css')}}">
<link rel="stylesheet" href="{{asset('assets/vendor/fonts/fontawesome/css/fontawesome-all.css')}}"> --}}
<link rel="stylesheet" href="{{asset('assets/vendor/datepicker/tempusdominus-bootstrap-4.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-select/css/bootstrap-select.css')}}">
@endsection
@section('main')

<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-10">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Tambah Kupon </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Kupon</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Tambah</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    @if (Session::has('status'))
                    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                    @endif

                    @if (count($errors) > 0)

                    <div class="alert alert-danger">

                        <strong>Whoops!</strong> There were some problems with your input.

                        <ul>

                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>

                            @endforeach

                        </ul>

                    </div>

                    @endif
                    <div class="card">
                        <h5 class="card-header">Edit Kupon</h5>
                        <div class="card-body">
                            <form action="{{ route('coupon.update', $data->rowPointer)  }}" method="POST"
                                enctype="multipart/form-data" class="form">
                                @csrf
                                <div class="form-group">
                                    <label for="inputUserName">Kode</label>
                                    <input id="kode" type="text" name="kode" data-parsley-trigger="change"
                                        value="{{$data->kode}}" required="" placeholder="Kode" autocomplete="off"
                                        class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail">Kategori</label><br>
                                    <select class="selectpicker" id="category_id" name="category_id"
                                        value="{{$data->category_id}}" data-width="100%">
                                        @if ($categorys != 'Connection problem')
                                        @foreach($categorys as $category)
                                            <option value="{{ $category['KodeGroup'] }}"
                                                {{ $data->category_id == $category['KodeGroup']? 'selected' : '' }}>
                                                {{ $category['NamaGroup'] }} </option>
                                        @endforeach
                                        @else
                                            <option value="" disabled selected>Internet connection problem</option>
                                        @endif
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword">Diskon Nominal (Rp)</label>
                                    <input id="discAmount" type="text" name="discAmount" value="{{$data->discAmount}}"
                                        data-parsley-trigger="change" required="" placeholder="Diskon Nominal"
                                        autocomplete="off" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword">Diskon Persen</label>
                                    <input id="discPercent" type="text" name="discPercent"
                                        value="{{$data->discPercent}}" data-parsley-trigger="change" required=""
                                        placeholder="Diskon Persen" autocomplete="off" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword">Minimal Pembelian</label>
                                    <input id="min" type="text" name="min" data-parsley-trigger="change" value="{{ $data->minOrder }}"
                                           required="" placeholder="Minimal Pembelian" autocomplete="off" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword">Date Start</label>
                                    <div class="input-group date" id="datetimepicker11" data-target-input="nearest">
                                        <input type="text" name="date_start" value="{{date('d-m-Y', strtotime($data->date_start))}}"
                                            class="form-control datetimepicker-input" data-target="#datetimepicker11" />
                                        <div class="input-group-append" data-target="#datetimepicker11"
                                            data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar-alt"></i></div>
                                        </div>
                                    </div>
                                    {{-- <input type="text" placeholder="Date Start"
                                        class="form-control datetimepicker-input" id="date_start" name="date_start"
                                        data-toggle="datetimepicker" data-target="#date_start" /> --}}
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword">Date End</label>
                                    <div class="input-group date" id="datetimepicker12" data-target-input="nearest">
                                        <input type="text" name="date_end" value="{{date('d-m-Y', strtotime($data->date_end))}}" class="form-control datetimepicker-input"
                                            data-target="#datetimepicker12" />
                                        <div class="input-group-append" data-target="#datetimepicker12"
                                            data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar-alt"></i></div>
                                        </div>
                                    </div>
                                    {{-- <input type="text" placeholder="Date End" class="form-control datetimepicker-input"
                                        id="date_end" name="date_end" data-toggle="datetimepicker"
                                        data-target="#date_end" /> --}}
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail">Aktif</label><br>
                                    <select class="selectpicker" id="isActive" name="isActive"
                                        value="{{$data->isActive}}" data-width="100%">

                                        <option value=1 {{ $data->isActive == 1? 'selected' : '' }}>Ya </option>
                                        <option value=0 {{ $data->isActive == 0? 'selected' : '' }}>Tidak</option>
                                    </select>
                                </div>


                                <div class="row">
                                    <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="text-right">
                                            <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                            <a href="{{Route('admin.coupon')}}"
                                                class="btn btn-space btn-secondary">Cancel</a>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('extrascript')
<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
<script src="{{asset('assets/vendor/slimscroll/jquery.slimscroll.js')}}"></script>
<script src="{{asset('assets/libs/js/main-js.js')}}"></script>
<script src="{{asset('assets/vendor/datepicker/moment.js')}}"></script>
<script src="{{asset('assets/vendor/datepicker/tempusdominus-bootstrap-4.js')}}"></script>
<script src="{{asset('assets/vendor/datepicker/datepicker.js')}}"></script>
<script src="{{asset('assets/vendor/bootstrap-select/js/bootstrap-select.js')}}"></script>

@endsection
