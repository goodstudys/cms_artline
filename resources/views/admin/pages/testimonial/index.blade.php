@extends('admin.app')
@section('main')
<div class="container-fluid  dashboard-content">
    <!-- ============================================================== -->
    <!-- pageheader -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="page-header">
                <h2 class="pageheader-title">Testimoni</h2>
                <div class="page-breadcrumb">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                            <li class="breadcrumb-item active"><a href="#" class="breadcrumb-link">Testimoni</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- end pageheader -->
    <!-- ============================================================== -->

    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            @if (Session::has('status'))
            <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
            @endif
            <div class="card">
                <h5 class="card-header">Daftar Testimoni</h5>
                <div class="card-body">
                    <div class="table-responsive ">
                        <table id="example" class="table table-striped table-bordered awal" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Rate</th>
                                    <th>Message</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($testimonials as $data)
                                <tr>
                                    <th scope="row">{{ $data->id }}</th>
                                    <td>{{ $data->rate }}</td>
                                    <td>{{ $data->message }}</td>
                                    <td> <a href="{{ route('testimonial.show', $data->rowPointer) }}" class="view"
                                            title="View" data-toggle="tooltip"><i
                                                class="material-icons">&#xE417;</i></a>
                                        @if ($data->isActive != 1)
                                        <a href="{{ route('testimonial.status', $data->rowPointer) }}" class="edit"
                                            title="Show" data-toggle="Show"><i class="material-icons">&#xe14c;</i></a>
                                        @else
                                        <a href="{{ route('testimonial.status', $data->rowPointer) }}" class="delete"
                                            title="Hide" data-toggle="Hide"><i class="material-icons">&#xe876;</i></a>
                                        @endif
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <th>No</th>
                                <th>Rate</th>
                                <th>Message</th>
                                <th>Actions</th>
                            </tfoot>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection