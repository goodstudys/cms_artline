@extends('admin.app')
@section('main')

    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-10">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Detail Testimonial </h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Testimonial</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Detail</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">Detail Testimonial</h5>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Rate</label>
                                    <input id="rate" name="rate" type="text" class="form-control" value="{{ $data->rate }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Message</label>
                                    <textarea id="message" name="message" type="text" class="form-control" readonly>{{ $data->message }}</textarea>
                                </div>
                                <div class="form__group">
                                    <div class="row container">
                                        @if ($data->isActive != 1)
                                            <div>
                                                <a href="{{ route('testimonial.status', $data->rowPointer) }}" class="btn btn-primary btn-submit">Show</a>
                                            </div>
                                        @else
                                            <div>
                                                <a href="{{ route('testimonial.status', $data->rowPointer) }}" class="btn btn-primary btn-submit">Hide</a>
                                            </div>
                                        @endif
                                        <div class="ml-2">
                                            <a href="{{ route('admin.testimonial') }}" class="btn btn-light btn-submit">Back</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
