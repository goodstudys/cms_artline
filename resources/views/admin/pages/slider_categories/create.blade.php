@extends('admin.app')
@section('main')

    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-10">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Create Slider Category </h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Slider Category </a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        @if (Session::has('status'))
                            <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                        @endif

                        @if (count($errors) > 0)

                            <div class="alert alert-danger">

                                <strong>Whoops!</strong> There were some problems with your input.

                                <ul>

                                    @foreach ($errors->all() as $error)

                                        <li>{{ $error }}</li>

                                    @endforeach

                                </ul>

                            </div>

                        @endif
                        <div class="card">
                            <h5 class="card-header">Create Slider Category </h5>
                            <div class="card-body">
                                <form action="{{ route('sliderCat.store')  }}" method="POST" enctype="multipart/form-data" class="form">
                                    @csrf
                                    <div class="form-group">
                                        <label for="inputEmail">Kategori</label><br>
                                        <select class="form-control categ" id="category" name="category" data-width="100%">
                                            @if ($categories == 'Connection problem')
                                            <option value="">No internet Connection</option>
                                            @else
                                            <option value="">Pilih kategori…</option>
                                            @foreach($categories as $key)
                                            <option value="{{ $key->id }}">{{ $key->name }} </option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputText3" class="col-form-label">Slider image</label>
                                        <input id="image" name="image" type="file" class="form-control" required>
                                    </div>
                                    <div class="form__group">
                                        <input type="submit" value="Save" class="btn btn-style-1 btn-submit">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
