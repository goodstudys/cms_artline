@extends('admin.app')
@section('main')

<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Detail Variasi Produk </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('admin') }}" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="{{ route('admin.product') }}" class="breadcrumb-link">Variasi Produk </a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Detail</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    @if (Session::has('status'))
                    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                    @endif

                    @if (count($errors) > 0)

                    <div class="alert alert-danger">

                        <strong>Whoops!</strong> There were some problems with your input.

                        <ul>

                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>

                            @endforeach

                        </ul>

                    </div>

                    @endif
                    <div class="card">
                        <h5 class="card-header">Detail Variasi Produk </h5>
                        <div class="card-body">
                            <div class="table-responsive nowrap">
                                <table class="table nowrap">
                                    <thead class="bg-light">
                                        <tr class="border-0">

                                            <th class="border-0">Nama</th>
                                            {{-- <th class="border-0">SKU</th>
                                            <th class="border-0">Qty</th> --}}
                                            {{--                                            <th class="border-0">Warna</th>--}}
                                            <th class="border-0">Harga</th>
                                            <th class="border-0">Diskon Nominal (Rp)</th>
                                            <th class="border-0">Diskon Persen</th>
                                            <th class="border-0">Net Price</th>
                                            {{-- <th class="border-0">Aktif</th> --}}
                                            <th class="border-0">Aksi

                                            </th>

                                        </tr>
                                    </thead>
                                    <tbody class="variasiappd">
                                        @if (count($groups) == 0||$groups[0]->groupsProductId == null ||
                                        $groups[0]->detailsName == null)
                                        <tr>
                                            <td>kosong</td>
                                        </tr>
                                        @else
                                        @foreach ($groups as $item)
                                        <tr>
                                            <td> <input id="name[]" type="text" name="name[]" required
                                                    data-parsley-trigger="change" autocomplete="off"
                                                    class="form-control" value="{{$item->detailsName}}" disabled>
                                            </td>
                                            {{-- <td> <input id="skuvariasi[]" type="text" name="skuvariasi[]" required
                                                    data-parsley-trigger="change" autocomplete="off"
                                                    class="form-control" value="{{$item->sku}}" disabled>
                                            </td>
                                            <td> <input id="skuvariasi[]" type="text" name="qty[]" required
                                                        data-parsley-trigger="change" autocomplete="off"
                                                        class="form-control" value="{{$item->quantity}}" disabled>
                                            </td> --}}
                                            {{--                                            <td> <input id="hex[]" type="text" name="hex[]" required--}}
                                            {{--                                                    data-parsley-trigger="change" autocomplete="off"--}}
                                            {{--                                                    class="form-control" value="{{$item->detailsColorHEX}}"
                                            disabled>--}}
                                            {{--                                            </td>--}}
                                            {{--                                            <td> <input id="color[]" type="text" name="color[]" required--}}
                                            {{--                                                    data-parsley-trigger="change" autocomplete="off"--}}
                                            {{--                                                    class="form-control" value="{{$item->detailsColorName}}"
                                            disabled>--}}
                                            {{--                                            </td>--}}
                                            <td> <input id="price[]" type="text" name="price[]" required
                                                    data-parsley-trigger="change" autocomplete="off"
                                                    class="form-control" value="{{$item->detailsPrice}}" disabled>
                                            </td>
                                            <td> <input id="discAmount[]" type="text" name="discAmount[]" required
                                                    data-parsley-trigger="change" autocomplete="off"
                                                    class="form-control" value="{{$item->detailsDiscAmount}}" disabled>
                                            </td>
                                            <td> <input id="discPercent[]" type="text" name="discPercent[]" required
                                                    data-parsley-trigger="change" autocomplete="off"
                                                    class="form-control" value="{{$item->detailsDiscPercent}}" disabled>
                                            </td>
                                            <td> @if ($item->detailsDiscAmount != 0.00)
                                                <div style="display: none">
                                                    {{$benar = $item->detailsPrice - $item->detailsDiscAmount}}</div>

                                                <input id="discPercent[]" type="text" name="discPercent[]" required
                                                    data-parsley-trigger="change" autocomplete="off"
                                                    class="form-control" value="{{$benar}}" disabled>
                                                @elseif($item->detailsDiscPercent != 0)
                                                <div style="display: none">
                                                    {{$benar = $item->detailsPrice * $item->detailsDiscPercent / 100}}
                                                </div>
                                                <div style="display: none">
                                                    {{$value = $item->detailsPrice - $benar}}</div>
                                                <input id="discPercent[]" type="text" name="discPercent[]" required
                                                    data-parsley-trigger="change" autocomplete="off"
                                                    class="form-control" value="{{$value}}" disabled>
                                                @else
                                                <input id="price[]" type="text" name="price[]" required
                                                    data-parsley-trigger="change" autocomplete="off"
                                                    class="form-control" value="{{$item->detailsPrice}}" disabled>
                                                @endif
                                            </td>
                                            {{-- <td> @if ($item->isActive != 1)
                                                <a href="{{ route('group.status', $item->detailsRowPointer) }}"
                                                    class="edit" title="Show" data-toggle="Show"><i
                                                        class="material-icons">&#xe876;</i></a>
                                                @else
                                                <a href="{{ route('group.status', $item->detailsRowPointer) }}"
                                                    class="delete" title="Hide" data-toggle="Hide"><i
                                                        class="material-icons">&#xe14c;</i></a>
                                                @endif</td> --}}
                                            <td> <a href="#" class="edit" title="Edit" data-toggle="modal"
                                                    data-target=".bd-example-modal-xl{{$item->detailsRowPointer}}variasi"><i
                                                        class="material-icons">&#xE254;</i></a>
                                                <a href="{{ route('group.destroy',$item->detailsRowPointer) }}"
                                                    class="delete" title="Delete" data-toggle="tooltip"><i
                                                        class="material-icons">&#xE872;</i></a>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@foreach ($groups as $item)
<div class="modal fade bd-example-modal-xl{{$item->detailsRowPointer}}variasi" tabindex="-1" role="dialog"
    aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="col-xl-12 col-lg-12 col-md-6 col-sm-12 col-12">
                <form action="{{ route('group.update',$item->detailsRowPointer,$item->rowPointer) }}" method="POST">
                    @csrf
                    <div class="card">
                        <div class="row">
                            <div class="col-9">
                                <h5 class="card-header">Edit variasi
                                    ({{$item->detailsName}},{{$item->detailsRowPointer}})
                                </h5>
                            </div>
                        </div>
                        <input type="hidden" name="default" value="{{ $item->rowPointer }}">
                        <div class="card-body p-0">
                            <div class="table-responsive nowrap">
                                <table class="table nowrap">
                                    <thead class="bg-light">
                                        <tr class="border-0">

                                            <th class="border-0">Nama</th>
                                            {{-- <th class="border-0">Sku</th>
                                            <th class="border-0">Quantity</th> --}}
                                            {{--                                            <th class="border-0">Warna</th>--}}
                                            <th class="border-0">Harga</th>
                                            <th class="border-0">Diskon Nominal (Rp)</th>
                                            <th class="border-0">Diskon Persen</th>
                                            {{-- <th class="border-0">Aksi</th> --}}

                                        </tr>
                                    </thead>
                                    <tbody class="variasiappd{{ $item->rowPointer }}">

                                        <tr>
                                            <td> <input id="name" type="text" name="name" required
                                                    data-parsley-trigger="change" autocomplete="off"
                                                    class="form-control" value="{{$item->detailsName}}">
                                            </td>
                                            {{--                                            <td> <input id="colorhex" type="text" name="colorhex" required--}}
                                            {{--                                                    data-parsley-trigger="change" autocomplete="off"--}}
                                            {{--                                                    class="form-control pickcolor" value="{{$item->detailsColorHEX}}"--}}
                                            {{--                                                    onclick="colorpicker(this)">--}}
                                            {{--                                            </td>--}}
                                            {{--                                            <td> <input id="colorname" type="text" name="colorname" required--}}
                                            {{--                                                    data-parsley-trigger="change" value="{{$item->detailsColorName}}"--}}
                                            {{--                                                    autocomplete="off" class="form-control">--}}
                                            {{--                                            </td>--}}
                                            {{-- <td> <input id="skuvariasi{{ $item->rowPointer }}" type="text"
                                                    name="skuvariasi" required data-parsley-trigger="change"
                                                    value="{{$item->sku}}" autocomplete="off" class="form-control">
                                            </td>
                                            <td> <input id="quantity{{ $item->rowPointer }}" type="number"
                                                    name="quantity" required data-parsley-trigger="change"
                                                    value="{{$item->quantity}}" autocomplete="off" class="form-control">
                                            </td> --}}
                                            <td> <input id="price{{ $item->rowPointer }}" type="text" name="price"
                                                    required data-parsley-trigger="change"
                                                    value="{{$item->detailsPrice}}" autocomplete="off"
                                                    class="form-control">
                                            </td>
                                            <td> <input id="discAmount{{ $item->rowPointer }}" type="text"
                                                    name="discAmount" required data-parsley-trigger="change"
                                                    value="{{$item->detailsDiscAmount}}" autocomplete="off"
                                                    class="form-control" value="0">

                                            </td>
                                            <td> <input id="discPercent{{ $item->rowPointer }}" type="text"
                                                    name="discPercent" required data-parsley-trigger="change"
                                                    value="{{$item->detailsDiscPercent}}" autocomplete="off"
                                                    class="form-control" value="0">

                                            </td>
                                            {{--                                            <td> <input id="netprice{{ $item->rowPointer }}"
                                            type="text" name="netprice" required--}}
                                            {{--                                                        data-parsley-trigger="change" value=""--}}
                                            {{--                                                        autocomplete="off" class="form-control" value="0">--}}
                                            {{--                                            </td>--}}
                                            <input type="hidden" name="pointer" id="pointer"
                                                value="{{$item->rowPointer}}">
                                            {{-- <td><span class="fa fa-plus add-variasi-single"></span></td> --}}

                                        </tr>


                                    </tbody>
                                </table>



                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                        </div>
                        <div class="col-sm-6 pl-0">
                            <p class="text-right">
                                <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                <button data-dismiss="modal" aria-label="Close" class="btn btn-space btn-secondary"
                                    id="mimodal">Cancel</button>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endforeach
@endsection
