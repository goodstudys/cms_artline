@extends('admin.app')
@section('main')

<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-10">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Tambah Produk </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Produk</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Tambah</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    @if (Session::has('status'))
                    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                    @endif

                    @if (count($errors) > 0)

                    <div class="alert alert-danger">

                        <strong>Whoops!</strong> There were some problems with your input.

                        <ul>

                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>

                            @endforeach

                        </ul>

                    </div>

                    @endif
                    <div class="card">
                        <h5 class="card-header">Tambah Produk</h5>
                        <div class="card-body">
                            <form action="{{ route('product.store')  }}" method="POST" enctype="multipart/form-data"
                                class="form">
                                @csrf
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Nama</label>
                                    <input id="name" name="name" type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Flag</label>
                                    <select class="form-control flag" id="flag" name="flag" data-width="100%" required>
                                        <option value="DEFAULT" selected>DEFAULT</option>
                                        <option value="NEW">NEW</option>
                                        <option value="SALE">SALE</option>
                                        <option value="HOTDEAL">HOTDEAL</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="description">Deskripsi</label>
                                    <textarea class="form-control" name="description" id="description" rows="3"
                                        required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Warna</label>
                                    <input id="colorhex" name="colorhex" type="text" class="form-control"
                                        autocomplete="off" search="false" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Nama Warna</label>
                                    <input id="colorname" name="colorname" type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Sku</label>
                                    <input id="sku" name="sku" type="text" class="form-control" required>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail">Kategori</label><br>
                                    <select class="form-control categ" id="category" name="category" data-width="100%">
                                        @if ($categories == 'Connection problem')
                                        <option value="">No internet Connection</option>
                                        @else
                                        <option value="">Pilih kategori…</option>
                                        @foreach($categories as $key)
                                        <option value="{{ $key->id }}">{{ $key->name }} </option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                                {{-- <div class="form-group">
                                    <label for="inputEmail">Sub Kategori</label><br>
                                    <select class="form-control sub" id="sub" name="sub" data-width="100%">
                                        @if ($subcategories == 'Connection problem')
                                        <option value="">No internet Connection</option>
                                        @else
                                        <option value="">Pilih kategori…</option>
                                        @foreach($subcategories as $key)
                                        <option value="{{ $key->id }}">{{ $key->name }} </option>
                                @endforeach
                                @endif
                                </select>
                        </div> --}}
                        <div class="form-group">
                            <label for="">Sub Kategori</label>
                            <select class="form-control" name="subcategory" id="subcategory">
                                <option value="0" disable="true" selected="true">Pilih Sub kategori…
                                </option>
                            </select>
                        </div>
                        {{-- <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Sub Kategori</label>
                                    <input id="subcategory" name="subcategory" type="text" class="form-control">
                                </div> --}}
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Tags</label>
                            <input id="tags" name="tags" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Brand</label>
                            <input id="brand" name="brand" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Berat</label>
                            <input id="weight" name="weight" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Dimensi</label>
                            <input id="dimensions" name="dimensions" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Harga</label>
                            <input id="price" name="price" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Harga Grosir</label>
                            <input id="wholesale_price" name="wholesale_price" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="">Tipe Grosir</label>
                            <select class="form-control" name="wholesaleType" id="wholesaleType">
                                <option value="LUSINAN" disable="true" selected="true">LUSINAN
                                </option>
                                <option value="SET" disable="true" selected="true">SET
                                </option>
                                <option value="GROSS" disable="true" selected="true">GROSS
                                </option>
                                <option value="PACK" disable="true" selected="true">PACK
                                </option>
                                <option value="TOPLES" disable="true" selected="true">TOPLES
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Diskon Nominal (Rp)</label>
                            <input id="discAmount" name="discAmount" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Diskon Persen</label>
                            <input id="discPercent" name="discPercent" type="text" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Gambar 1</label>
                            <input id="image1" name="image1" type="file" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Gambar 2</label>
                            <input id="image2" name="image2" type="file" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Gambar 3</label>
                            <input id="image3" name="image3" type="file" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="inputText3" class="col-form-label">Gambar 4</label>
                            <input id="image4" name="image4" type="file" class="form-control">
                        </div>
                        <div class="form__group">
                            <input type="submit" value="Save" class="btn btn-style-1 btn-submit">
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection
@section('extrascript')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('assets/colorpicker/js/colorpicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/colorpicker/js/eye.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/colorpicker/js/utils.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/colorpicker/js/layout.js') }}"></script>
<script>
    $("select.categ").change(function(){
            var pointer = $(this).children("option:selected").val();

            $.ajax({
                url:'{{url('/getSub/')}}',
                data:'categories_id=' + pointer,
                type:'get',
                dataType: 'json',
                success:function (response) {
                    console.log(response);

                    $("#subcategory").attr('disabled', false);

                    var $select = $('#subcategory');

                    $select.find('option').remove();
                    $.each(response,function(key, value)
                    {
                        $select.append('<option value=' + value.id + '>' + value.name + '</option>');
                    });
                }
            });
        });
</script>
<script !src="">
    $('#colorhex').ColorPicker({
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            },
            onChange: function (hsb, hex, rgb) {
                $('#colorhex').val('#'+hex);
            }
        })
</script>
{{-- <script type="text/javascript">
    $('#category').on('change', function(e){
    console.log(e);
    var categories_id = e.target.value;
    $.get('/getSub?categories_id=' + categories_id,function(data) {
      console.log('iniiiiii' +data);
      $('#city').empty();
      $('#city').append('<option value="0" disable="true" selected="true">=== Select Regencies ===</option>');

      $.each(data, function(index, regenciesObj){
        $('#city').append('<option value="'+ regenciesObj.id +'">'+ regenciesObj.name +'</option>');
      })
    });
  });
</script> --}}
@endsection