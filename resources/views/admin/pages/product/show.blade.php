@extends('admin.app')
@section('main')

    <div class="container-fluid dashboard-content">
        <div class="row">
            <div class="col-xl-10">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header" id="top">
                            <h2 class="pageheader-title">Detail Produk </h2>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Produk</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Detail</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        @if (Session::has('status'))
                            <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                        @endif

                        @if (count($errors) > 0)

                            <div class="alert alert-danger">

                                <strong>Whoops!</strong> There were some problems with your input.

                                <ul>

                                    @foreach ($errors->all() as $error)

                                        <li>{{ $error }}</li>

                                    @endforeach

                                </ul>

                            </div>

                        @endif
                        <div class="card">
                            <h5 class="card-header">Detail Produk</h5>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Nama</label>
                                    <input id="name" name="name" type="text" class="form-control" value="{{ $data->name }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="description">Deskripsi</label>
                                    <textarea class="form-control" name="description" id="description" rows="3" readonly="">{{ $data->description }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Warna</label>
                                    <input id="colorhex" name="colorhex" type="text" class="form-control" value="{{ $data->colorHex }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Nama Warna</label>
                                    <input id="colorname" name="colorname" type="text" class="form-control" value="{{ $data->colorName }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Sku</label>
                                    <input id="sku" name="sku" type="text" class="form-control" value="{{ $data->sku }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Kategori</label>
                                    <input id="category" name="category" type="text" class="form-control" value="{{ $data->category }}" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Sub Kategori</label>
                                    <input id="subcategory" name="subcategory" type="text" class="form-control" value="{{ $data->subcategory }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Tags</label>
                                    <input id="tags" name="tags" type="text" class="form-control" value="{{ $data->tags }}" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Brand</label>
                                    <input id="brand" name="brand" type="text" class="form-control" value="{{ $data->brand }}" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Berat</label>
                                    <input id="weight" name="weight" type="text" class="form-control" value="{{ $data->weight }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Dimensi</label>
                                    <input id="dimensions" name="dimensions" type="text" class="form-control" value="{{ $data->dimensions }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Harga</label>
                                    <input id="price" name="price" type="text" class="form-control" value="{{ $data->price }}" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Harga Grosir</label>
                                    <input id="wholesale_price" name="wholesale_price" type="text" class="form-control" value="{{ $data->wholesale_price }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Tipe Grosir</label>
                                    <input id="wholesale_type" name="wholesale_type" type="text" class="form-control" value="{{ $data->wholesale_type }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Diskon Nominal (Rp)</label>
                                    <input id="discAmount" name="discAmount" type="text" class="form-control" value="{{ $data->discAmount }}" readonly="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Diskon Persen</label>
                                    <input id="discPercent" name="discPercent" type="text" class="form-control" value="{{ $data->discPercent }}" readonly="">
                                </div>
                                <div class="row container">
                                    <div class="form-group">
                                        <label for="inputText3" class="col-form-label">Gambar 1</label><br>
                                        <img src="{{ asset('upload/users/products/'. $data->image1) }}" alt="" style="width:200px;height:200px;object-fit:cover;">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputText3" class="col-form-label">Gambar 2</label><br>
                                        <img src="{{ asset('upload/users/products/'. $data->image2) }}" alt="" style="width:200px;height:200px;object-fit:cover;">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputText3" class="col-form-label">Gambar 3</label><br>
                                        <img src="{{ asset('upload/users/products/'. $data->image3) }}" alt="" style="width:200px;height:200px;object-fit:cover;">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputText3" class="col-form-label">Gambar 4</label><br>
                                        <img src="{{ asset('upload/users/products/'. $data->image4) }}" alt="" style="width:200px;height:200px;object-fit:cover;">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
