@extends('admin.app')
@section('extracss')
<link rel="stylesheet" href="{{asset('assets/vendor/datepicker/tempusdominus-bootstrap-4.css')}}" />
<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-select/css/bootstrap-select.css')}}">
@endsection
@section('main')

<div class="container-fluid dashboard-content">
    <div class="row">
        <div class="col-xl-10">
            <!-- ============================================================== -->
            <!-- pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="page-header" id="top">
                        <h2 class="pageheader-title">Edit Produk </h2>
                        <div class="page-breadcrumb">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Produk</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- end pageheader  -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    @if (Session::has('status'))
                    <div class="mt-4 alert alert-{{ session('status') }}" role="alert">{{ session('message') }}</div>
                    @endif

                    @if (count($errors) > 0)

                    <div class="alert alert-danger">

                        <strong>Whoops!</strong> There were some problems with your input.

                        <ul>

                            @foreach ($errors->all() as $error)

                            <li>{{ $error }}</li>

                            @endforeach

                        </ul>

                    </div>

                    @endif
                    <div class="card">
                        <h5 class="card-header">Edit Produk</h5>
                        <div class="card-body">
                            <form action="{{ route('product.update', $data->rowPointer) }}" method="POST"
                                enctype="multipart/form-data" class="form">
                                @csrf
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Nama</label>
                                    <input id="name" name="name" type="text" class="form-control"
                                        value="{{ $data->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Flag</label>
                                    <select class="form-control flag" id="flag" name="flag" data-width="100%">
                                        <option  value="DEFAULT" {{ ($data->flag == 'DEFAULT' ) ? ' selected' : '' }}>DEFAULT</option>
                                        <option  value="NEW" {{ ($data->flag == 'NEW' ) ? ' selected' : '' }}>NEW</option>
                                        <option value="SALE" {{ ($data->flag == 'SALE' ) ? ' selected' : '' }}>SALE</option>
                                        <option value="HOTDEAL" {{ ($data->flag == 'HOTDEAL' ) ? ' selected' : '' }}>HOTDEAL</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="description">Deskripsi</label>
                                    <textarea class="form-control" name="description" id="description"
                                        rows="3">{{ $data->description }}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Warna</label>
                                    <input id="colorhex" name="colorhex" type="text" class="form-control"
                                        value="{{ $data->colorHex }}" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Nama Warna</label>
                                    <input id="colorname" name="colorname" type="text" class="form-control"
                                        value="{{ $data->colorName }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Sku</label>
                                    <input id="sku" name="sku" type="text" class="form-control"
                                        value="{{ $data->sku }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Kategori</label>
                                    <select class="form-control category" id="category" name="category" data-width="100%">
                                            <option value="">Pilih kategori…</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}"{{ ($data->category == $category->id ) ? ' selected' : '' }}>{{ $category->name }} </option>
                                                @endforeach
                                        
                                    </select>
                                </div>
                                @if ($data->subcategory == null)
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Sub Kategori</label>
                                    <select class="form-control subcategory" id="subcategory" name="subcategory" data-width="100%">
                                        <option value="0" disable="true" selected="true">Pilih Sub kategori…
                                        </option>
                                        
                                    </select>
                                </div>
                                @else
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Sub Kategori</label>
                                    <select class="form-control subcategory" id="subcategory" name="subcategory" data-width="100%">
                                            <option value="">Pilih kategori…</option>
                                                @foreach($sub_categories as $category)
                                                    <option value="{{ $category->id }}"{{ ($data->subcategory == $category->id ) ? ' selected' : '' }}>{{ $category->name }} </option>
                                                @endforeach
                                        
                                    </select>
                                </div>
                                @endif
                                {{-- <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Sub Kategori</label>
                                    <input id="subcategory" name="subcategory" type="text" class="form-control"
                                        value="{{ $data->subcategory }}">
                                </div> --}}
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Tags</label>
                                    <input id="tags" name="tags" type="text" class="form-control"
                                        value="{{ $data->tags }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Brand</label>
                                    <input id="brand" name="brand" type="text" class="form-control"
                                        value="{{ $data->brand }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Berat</label>
                                    <input id="weight" name="weight" type="text" class="form-control"
                                        value="{{ $data->weight }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Dimensi</label>
                                    <input id="dimensions" name="dimensions" type="text" class="form-control"
                                        value="{{ $data->dimensions }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Harga</label>
                                    <input id="price" name="price" type="text" class="form-control"
                                        value="{{ $data->price }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Harga Grosir</label>
                                    <input id="wholesale_price" name="wholesale_price" type="text" class="form-control" value="{{ $data->wholesale_price }}">
                                </div>
                                <div class="form-group">
                                    <label for="">Tipe Grosir</label>
                                    <select class="form-control" name="wholesaleType" id="wholesaleType">
                                        <option value="LUSINAN" {{ ($data->wholesale_type == 'LUSINAN' ) ? ' selected' : '' }}>
                                            LUSINAN</option>
                                        <option value="SET" {{ ($data->wholesale_type == 'SET' ) ? ' selected' : '' }}>SET
                                        </option>
                                        <option value="GROSS" {{ ($data->wholesale_type == 'GROSS' ) ? ' selected' : '' }}>
                                            GROSS</option>
                                        <option value="PACK" {{ ($data->wholesale_type == 'PACK' ) ? ' selected' : '' }}>PACK
                                        </option>
                                        <option value="TOPLES" {{ ($data->wholesale_type == 'TOPLES' ) ? ' selected' : '' }}>
                                            TOPLES
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Diskon Nominal (Rp)</label>
                                    <input id="discAmount" name="discAmount" type="text" class="form-control"
                                        value="{{ $data->discAmount }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Diskon Persen</label>
                                    <input id="discPercent" name="discPercent" type="text" class="form-control"
                                        value="{{ $data->discPercent }}">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Gambar 1</label>
                                    <input id="image1" name="image1" type="file" class="form-control" value="">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Gambar 2</label>
                                    <input id="image2" name="image2" type="file" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Gambar 3</label>
                                    <input id="image3" name="image3" type="file" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="inputText3" class="col-form-label">Gambar 4</label>
                                    <input id="image4" name="image4" type="file" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail">Aktif</label><br>
                                    <select class="selectpicker" id="isActive" name="isActive"
                                        value="{{$data->isActive}}" data-width="100%">

                                        <option value=1 {{ $data->isActive == 1? 'selected' : '' }}>Ya </option>
                                        <option value=0 {{ $data->isActive == 0? 'selected' : '' }}>Tidak</option>
                                    </select>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0">

                                    </div>
                                    <div class="col-sm-6 pl-0">
                                        <p class="text-right">
                                            <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                            <a href="{{Route('admin.product')}}"
                                                class="btn btn-space btn-secondary">Cancel</a>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('extrascript')

<script src="{{asset('assets/vendor/bootstrap-select/js/bootstrap-select.js')}}"></script>

<script type="text/javascript" src="{{ asset('assets/colorpicker/js/colorpicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/colorpicker/js/eye.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/colorpicker/js/utils.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/colorpicker/js/layout.js') }}"></script>
<script>
    $("select.category").change(function(){
            var pointer = $(this).children("option:selected").val();

            $.ajax({
                url:'{{url('/getSub/')}}',
                data:'categories_id=' + pointer,
                type:'get',
                dataType: 'json',
                success:function (response) {
                    console.log(response);

                    $("#subcategory").attr('disabled', false);

                    var $select = $('#subcategory');

                    $select.find('option').remove();
                    $.each(response,function(key, value)
                    {
                        $select.append('<option value=' + value.id + '>' + value.name + '</option>');
                    });
                }
            });
        });
</script>
<script>
    $('#colorhex').ColorPicker({
            onBeforeShow: function () {
                $(this).ColorPickerSetColor(this.value);
            },
            onChange: function (hsb, hex, rgb) {
                $('#colorhex').val('#'+hex);
            }
        })

</script>
@endsection
