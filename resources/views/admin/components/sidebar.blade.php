<div class="nav-left-sidebar sidebar-dark">
    <div class="menu-list">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav flex-column">
                    <li class="nav-divider">
                        Menu
                    </li>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('admin') || Request::is('admin') ) ? 'active' : '' }}"
                            href="{{ route('admin') }}"><i class="fa fa-fw fa-window-maximize"></i>Dashboard <span
                                class="badge badge-success">6</span></a>
                    </li>
                    <div class="dropdown">
                        <a class="nav-link   {{ ( Request::is('/akun-customer') || Request::is('/akun-sales') ) ? 'active' : '' }}"
                            href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                            aria-expanded="false"><i class="fa fa-fw fa-folder-open"></i>Menu Pesanan
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="nav-link   {{ ( Request::is('/order') || Request::is('/order/*') ) ? 'active' : '' }}"
                                href="{{ route('admin.order') }}"><i class="fa fa-fw fa-folder-open"></i>Basic Order</a>
                            <a class="nav-link   {{ ( Request::is('/pre-order') || Request::is('/pre-order/*') ) ? 'active' : '' }}"
                                href="{{ route('admin.preorder') }}"><i
                                    class="fa fa-fw fa-folder-open"></i>Pre-Order</a>
                        </div>
                    </div>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/product') || Request::is('/product/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.product') }}"><i class="fa fa-fw fa-archive"></i>Produk</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/job_request') || Request::is('/job_request/*') ) ? 'active' : '' }}"
                            href="{{ route('jobrequest.index') }}"><i class="fa fa-fw fa-envelope-open"></i>Request
                            Job</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/slider') || Request::is('/slider/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.slider') }}"><i class="fa fa-fw fa-play-circle"></i>Slider</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/slider-categories') || Request::is('/slider-categories/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.sliderCat') }}"><i class="fa fa-fw fa-image"></i>Slider Category</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/promotion') || Request::is('/promotion/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.promotion') }}"><i class="fa fa-fw fa-bullhorn"></i>Promosi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/testimonial') || Request::is('/testimonial/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.testimonial') }}"><i class="fa fa-fw fa-users"></i>Testimoni</a>
                    </li>


                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/coupon') || Request::is('/coupon/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.coupon') }}"><i class="fa fa-fw fa-percent"></i>Kupon</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/carousel') || Request::is('/carousel/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.carousel') }}"><i class="fa fa-fw fa-window-maximize"></i>Carousel</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/blog') || Request::is('/blog/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.blog') }}"><i class="fa fa-fw fa-book"></i>Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/reseller') || Request::is('/reseller/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.reseller') }}"><i class="fa fa-fw fa-book"></i>reseller</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/about') || Request::is('/about/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.about') }}"><i class="fa fa-fw fa-info"></i>About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/faq') || Request::is('/faq/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.faq') }}"><i class="fa fa-fw fa-question"></i>Faq</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/user') || Request::is('/user/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.user') }}"><i class="fa fa-fw fa-user"></i>User</a>
                    </li>
                    @if (Auth::user()->level == 1)
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/hak-akses') || Request::is('/hak-akses/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.hakakses') }}"><i class="fa fa-fw fa-cog"></i>Hak akses</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/web') || Request::is('/web/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.web') }}"><i class="fa fa-fw fa-window-maximize"></i>Web Setting</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link   {{ ( Request::is('/log-activity') || Request::is('/log-activity/*') ) ? 'active' : '' }}"
                            href="{{ route('admin.log') }}"><i class="fa fa-fw fa-history"></i>Log</a>
                    </li>
                    @endif
                    <li class="nav-item">
                        <div class="mx-auto" style="height: 100px;">

                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>