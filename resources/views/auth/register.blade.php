@extends('layouts.app')

@section('content')
<section class="signup">
    <!-- <img src="images/signup-bg.jpg" alt=""> -->
    <div class="container">
        <div class="signup-content">
            <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data" id="signup-form"
                class="signup-form">
                @csrf
                <p class="form-title" style="line-height: 1.66;margin: 0; padding: 0;font-weight: 900;color: #222;font-size: 24px;text-transform: uppercase;text-align: center;margin-bottom: 40px;">{{ __('Register') }}</p>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <input id="firstName" type="text" placeholder="First Name"
                                class="form-input @error('firstName') is-invalid @enderror" name="firstName"
                                value="{{ old('firstName') }}" required autocomplete="firstName" autofocus>

                            @error('firstName')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <input id="lastName" placeholder="Last Name" type="text"
                                class="form-input @error('lastName') is-invalid @enderror" name="lastName"
                                value="{{ old('lastName') }}" required autocomplete="lastName" autofocus>

                            @error('lastName')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <input id="displayName" type="text" placeholder="Display Name"
                        class="form-input @error('displayName') is-invalid @enderror" name="displayName"
                        value="{{ old('displayName') }}" required autocomplete="displayName" autofocus>

                    @error('displayName')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input id="company" type="text" placeholder="Company"
                        class="form-input @error('company') is-invalid @enderror" name="company"
                        value="{{ old('company') }}" required autocomplete="company" autofocus>

                    @error('company')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input id="phone" type="text" placeholder="Phone"
                        class="form-input @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}"
                        required autocomplete="phone" autofocus>

                    @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="image">Click me to upload image</label>
                    <input id="image" type="file" placeholder="Profil Image"
                        class="form-input @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}"
                        required autofocus>

                    @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input id="email" type="email" placeholder="Email"
                        class="form-input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"
                        required autocomplete="email">

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input id="password" type="password" placeholder="Password"
                        class="form-input @error('password') is-invalid @enderror" name="password" required
                        autocomplete="new-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-input"
                        name="password_confirmation" required autocomplete="new-password">

                </div>
                <div class="form-group">
                    <input type="submit" name="submit" class="form-submit" value="Sign up" />
                </div>
            </form>
            <p class="loginhere">
                Have already an account ? <a href="{{ route('login') }}" class="loginhere-link">{{ __('Login') }} here</a>
            </p>
        </div>
    </div>
</section>
@endsection
