@extends('layouts.app')

@section('content')
<section class="signup">
    <!-- <img src="images/signup-bg.jpg" alt=""> -->
    <div class="container">
        <div class="signup-content">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
            <form method="POST" action="{{ route('password.confirm') }}" enctype="multipart/form-data" id="signup-form"
                class="signup-form">
                @csrf
                <h2 class="form-title">{{ __('Confirm Password') }}</h2>


                <div class="form-group">
                    <input id="password" type="password" placeholder="Password" class="form-input @error('password') is-invalid @enderror"
                        name="password" required autocomplete="current-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                {{-- <div class="form-group">
                    <input id="password" type="password" placeholder="Password"
                        class="form-input @error('password') is-invalid @enderror" name="password" required
                        autocomplete="new-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-input"
                        name="password_confirmation" required autocomplete="new-password">

                </div> --}}




                {{-- <div class="form-group">
                    <input id="phone" type="text" placeholder="Phone"
                        class="form-input @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}"
                required autocomplete="phone" autofocus>

                @error('phone')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
        </div>

        <div class="form-group">
            <label for="image">Click me to upload image</label>
            <input id="image" type="file" placeholder="Profil Image"
                class="form-input @error('image') is-invalid @enderror" name="image" value="{{ old('image') }}" required
                autofocus>

            @error('image')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <input id="email" type="email" placeholder="Email" class="form-input @error('email') is-invalid @enderror"
                name="email" value="{{ old('email') }}" required autocomplete="email">

            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <input id="password" type="password" placeholder="Password"
                class="form-input @error('password') is-invalid @enderror" name="password" required
                autocomplete="new-password">

            @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div>
        <div class="form-group">
            <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-input"
                name="password_confirmation" required autocomplete="new-password">

        </div> --}}
        {{-- <div class="form-group">
                    <input type="text" class="form-input" name="name" id="name" placeholder="Your Name" />
                </div>
                <div class="form-group">
                    <input type="email" class="form-input" name="email" id="email" placeholder="Your Email" />
                </div>
                <div class="form-group">
                    <input type="text" class="form-input" name="password" id="password" placeholder="Password" />
                    <span toggle="#password" class="zmdi zmdi-eye field-icon toggle-password"></span>
                </div>
                <div class="form-group">
                    <input type="password" class="form-input" name="re_password" id="re_password"
                        placeholder="Repeat your password" />
                </div>
                <div class="form-group">
                    <input type="checkbox" name="agree-term" id="agree-term" class="agree-term" />
                    <label for="agree-term" class="label-agree-term"><span><span></span></span>I agree all statements in
                        <a href="#" class="term-service">Terms of service</a></label>
                </div> --}}
        <div class="form-group">
            <input type="submit" name="submit" class="form-submit" value="Confirm Password" />
            @if (Route::has('password.request'))
            <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        @endif
        </div>

        </form>
        <p class="loginhere">
            Don't have an account ? <a href="{{ route('register') }}" class="loginhere-link">{{ __('Register') }}
                here</a>
        </p>

    </div>
    </div>
</section>
@endsection