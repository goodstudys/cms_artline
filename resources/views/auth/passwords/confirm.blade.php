@extends('layouts.app')

@section('content')
<section class="signup" style="padding-bottom: 400px;">
    <!-- <img src="images/signup-bg.jpg" alt=""> -->
    <div class="container">
        <div class="signup-content">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
            <form method="POST" action="{{ route('password.confirm') }}" enctype="multipart/form-data" id="signup-form"
                class="signup-form">
                @csrf
                <h2 class="form-title">{{ __('Confirm Password') }}</h2>


                <div class="form-group">
                    <input id="password" type="password" placeholder="Password"
                        class="form-input @error('password') is-invalid @enderror" name="password" required
                        autocomplete="current-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <input type="submit" name="submit" class="form-submit" value="Confirm Password" />
                    @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                    @endif
                </div>

            </form>

        </div>
    </div>
</section>
@endsection