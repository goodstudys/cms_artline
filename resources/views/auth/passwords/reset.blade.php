@extends('layouts.app')

@section('content')

<section class="signup" style="padding-bottom: 400px;">
    <!-- <img src="images/signup-bg.jpg" alt=""> -->
    <div class="container">
        <div class="signup-content">
            @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
            @endif
            <form method="POST" action="{{ route('password.update') }}" enctype="multipart/form-data" id="signup-form"
                class="signup-form">
                @csrf
                <h2 class="form-title">{{ __('Reset Password') }}</h2>

                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group">
                    <input id="email" type="email" placeholder="Email Address"
                        class="form-input @error('email') is-invalid @enderror" name="email"
                        value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input id="password" type="password" placeholder="Password"
                        class="form-input @error('password') is-invalid @enderror" name="password" required
                        autocomplete="new-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-input"
                        name="password_confirmation" required autocomplete="new-password">

                </div>



                <div class="form-group">
                    <input type="submit" name="submit" class="form-submit" value="Reset Password" />
                </div>

            </form>

        </div>
    </div>
</section>
@endsection