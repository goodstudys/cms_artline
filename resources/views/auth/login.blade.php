@extends('layouts.app')

@section('content')
<section class="signup" style="padding-bottom: 400px;">
    <!-- <img src="images/signup-bg.jpg" alt=""> -->
    <div class="container">
        <div class="signup-content">
            <form method="POST" action="{{ route('login') }}" enctype="multipart/form-data" id="signup-form"
                class="signup-form">
                @csrf
                <p class="form-title" style="line-height: 1.66;margin: 0; padding: 0;font-weight: 900;color: #222;font-size: 24px;text-transform: uppercase;text-align: center;margin-bottom: 40px;">{{ __('Login') }}</p>

                <div class="form-group">
                    <input type="text" name="check" value="user" readonly="" class="d-none">
                    <input id="email" type="text" placeholder="Email or Phone number"
                        class="form-input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"
                        required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input id="password" type="password" placeholder="Password"
                        class="form-input @error('password') is-invalid @enderror" name="password" required
                        autocomplete="current-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="submit" name="submit" class="form-submit" value="Login" />
                    @if (Route::has('password.request'))
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                    @endif
                </div>

            </form>
            <p class="loginhere">
                Don't have an account ? <a href="{{ route('register') }}" class="loginhere-link">{{ __('Register') }}
                    here</a>
                    <br>
                    <a href="{{ route('index') }}" class="loginhere-link">
                            Back To Shop</a>
            </p>




        </div>
    </div>
</section>
@endsection
